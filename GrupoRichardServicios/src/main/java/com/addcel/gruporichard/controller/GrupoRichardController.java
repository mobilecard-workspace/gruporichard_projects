package com.addcel.gruporichard.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.gruporichard.services.GrupoRichardPagosService;
import com.addcel.gruporichard.services.GrupoRichardService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class GrupoRichardController {
	
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardController.class);
	
	@Autowired
	private GrupoRichardService grService;
	
	@Autowired
	private GrupoRichardPagosService dfpService;
	
	@RequestMapping(value = "/servicios/home")
	public ModelAndView home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);	
		return new ModelAndView("home");	
	}
	
	@RequestMapping(value = "/consultaKIT", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String agregarTienda(@RequestParam("json")String json) {	
		logger.info("Dentro del servicio: GrupoRichard/consultaKIT");
		return grService.buscarTiendaKIT(json);
	}
	
	@RequestMapping(value = "/agregarUsuario", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String agregarUsuario(@RequestParam("json")String json) {	
		logger.info("Dentro del servicio: GrupoRichard/agregarUsuario");
		return grService.agregarUsuario(json);
	}
	
	@RequestMapping(value = "/login", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String login(@RequestParam("json")String json) {		
		logger.info("Dentro del servicio: GrupoRichard/login.");
		return grService.login(json);
	}
	
	@RequestMapping(value = "/actualizarUsuario", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String actualizarUsuario(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: GrupoRichard/actualizarUsuario");
		return grService.actualizarUsuario(json);
	}
	
	@RequestMapping(value = "/actualizarPassword", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String actualizarPassword(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: GrupoRichard/actualizarPassword");
		return grService.actualizarPassword(json);
	}
	
	@RequestMapping(value = "/recuperarPassword", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String recuperarPassword(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: GrupoRichard/recuperarPassword");
		return grService.recuperarPassword(json);
	}
	
	@RequestMapping(value = "/notificaLocalizacion", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String notificaLocalizacion(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: GrupoRichard/notificaLocalizacion");
		return grService.notificaLocalizacion(json);
	}

}