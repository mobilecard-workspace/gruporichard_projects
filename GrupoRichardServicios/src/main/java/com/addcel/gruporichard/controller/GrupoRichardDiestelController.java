package com.addcel.gruporichard.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.gruporichard.services.GrupoRichardDiestelService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class GrupoRichardDiestelController {
	
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardDiestelController.class);
	
	@Autowired
	private GrupoRichardDiestelService diestelService;
	
	
	@RequestMapping(value = "/Diestel/obtenCategorias", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String obtenCategorias(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/Diestel/obtenCategorias");
		return diestelService.obtenCategorias(data);
	}
	
	@RequestMapping(value = "/Diestel/montosRecarga", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String obtenerDestinos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/Diestel/montosRecarga");
		return diestelService.montosRecarga(data);
	}
	
	@RequestMapping(value = "/Diestel/obtenCatalogoServicios", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String obtenCatalogoServicios(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/Diestel/obtenCatalogoServicios");
		return diestelService.obtenCatalogoServicios(data);
	}
	
	@RequestMapping(value = "/Diestel/obtenInfoPorServicio", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultarItinerario(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/Diestel/obtenInfoPorServicio");
		return diestelService.obtenInfoPorServicio(data);
	}
	
}