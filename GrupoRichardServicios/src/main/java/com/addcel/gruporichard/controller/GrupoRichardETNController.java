package com.addcel.gruporichard.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.gruporichard.services.GrupoRichardETNService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class GrupoRichardETNController {
	
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardETNController.class);
	
	@Autowired
	private GrupoRichardETNService etnService;
	
	
	@RequestMapping(value = "/ETN/ObtenerOrigenes", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String obtenerOrigenes(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/ObtenerOrigenes");
		return etnService.obtenerOrigenes();
	}
	
	@RequestMapping(value = "/ETN/ObtenerDestinos", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String obtenerDestinos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/ObtenerDestinos");
		return etnService.obtenerDestinos(data);
	}
	
	@RequestMapping(value = "/ETN/ObtenerCorridas", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String obtenerCorridas(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/ObtenerCorridas");
		return etnService.obtenerCorridas(data);
	}
	
	@RequestMapping(value = "/ETN/ConsultarItinerario", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultarItinerario(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/consultarItinerario");
		return etnService.consultarItinerario(data);
	}
	
	@RequestMapping(value = "/ETN/ConsultarOcupacion", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultarOcupacion(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/consultarOcupacion");
		return etnService.consultarOcupacion(data);
	}
	
	@RequestMapping(value = "/ETN/ConsultarDiagrama", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultarDiagrama(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/consultarDiagrama");
		return etnService.consultarDiagrama(data);
	}
	
	@RequestMapping(value = "/ETN/ConsultaBoletoEtn", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultaBoletoEtn(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/ETN/consultaBoletoEtn");
		return etnService.consultaBoletoEtn(data);
	}
	
}