package com.addcel.gruporichard.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.gruporichard.services.GrupoRichardPagosService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class GrupoRichardPagosController {
	
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardPagosController.class);
	
	@Autowired
	private GrupoRichardPagosService dfpService;
	
	
	@RequestMapping(value = "/getToken", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String getToken(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/getToken");
		return dfpService.generaToken(data);
	}
	
	@RequestMapping(value = "/consultaMonedero", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultaMonedero(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/consultaMonedero");
		return dfpService.consultaMonedero(data);
	}
	
	@RequestMapping(value = "/consultaSaldoMegacable", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultaSaldoMegacable(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/consultaSaldoMegacable");
		return dfpService.consultaSaldoMegacable(data);
	}
	
	@RequestMapping(value = "/consultaMontoInterjet", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String consultaMontoInterjet(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: GrupoRichard/consultaMontoInterjet");
		return dfpService.consultaMontoInterjet(data);
	}
	
	@RequestMapping(value = "/pagoProductos", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoProductos(@RequestParam("json") String json) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoProductos");
		return dfpService.procesaPagoProductos(json);	
	}
	
	@RequestMapping(value = "/pagoTAE", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoTAE(@RequestParam("json") String json) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoTAE");
		return dfpService.procesaPagos(json, "TAE");	
	}
	
	@RequestMapping(value = "/pagoIAVE", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoIAVE(@RequestParam("json") String json) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoIAVE");
		return dfpService.procesaPagos(json, "IAVE");	
	}
	
	@RequestMapping(value = "/pagoPASE", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoPASE(@RequestParam("json") String json) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoPASE");
		return dfpService.procesaPagos(json, "PASE");	
	}

	@RequestMapping(value = "/pagoMegacable", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoMegacable(@RequestParam("json") String json) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoMegacable");
		return dfpService.procesaPagos(json, "MEGA");	
	}
	
	@RequestMapping(value = "/pagoInterjet", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public ModelAndView  pagoInterjet(@RequestParam("json") String json, HttpServletRequest request, HttpServletResponse response) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoInterjet");
		return dfpService.procesaPagosInterjet(json, request, response);	
	}
	
	@RequestMapping(value = "/pagoETN", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public ModelAndView  pagoETN(@RequestParam("json") String json, HttpServletRequest request, HttpServletResponse response) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoETN");
		return dfpService.procesaPagosETN(json, request, response);	
	}
	
	@RequestMapping(value = "/pagoRecargaDiestel", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoRecargaDiestel(@RequestParam("json") String json, HttpServletRequest request, HttpServletResponse response) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoRecargaDiestel");
		return dfpService.procesaPagosRecargasDiestel(json, request, response);	
	}
	
	@RequestMapping(value = "/pagoServicioDiestel", /*method=RequestMethod.POST,*/ produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  pagoServicioDiestel(@RequestParam("json") String json, HttpServletRequest request, HttpServletResponse response) {	
		logger.info("Dentro del servicio: GrupoRichard/pagoServicioDiestel");
		return dfpService.procesaPagosServicioDiestel(json, request, response);
	}
	
	@RequestMapping(value = "/busquedaPagos", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String busquedaPagos(@RequestParam("json") String json) {
		logger.info("Dentro del servicio: GrupoRichard/busquedaPagos");
		return dfpService.busquedaPagos(json);
	}
}