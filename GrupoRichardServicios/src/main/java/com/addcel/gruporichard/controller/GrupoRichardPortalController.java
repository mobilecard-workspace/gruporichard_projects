package com.addcel.gruporichard.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.gruporichard.model.vo.ListaArchivos;
import com.addcel.gruporichard.model.vo.LoginVO;
import com.addcel.gruporichard.model.vo.UsuarioVO;
import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.model.vo.common.KitVO;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;
import com.addcel.gruporichard.model.vo.portal.BusquedaTiendaRequest;
import com.addcel.gruporichard.services.GrupoRichardPortalService;
import com.addcel.utils.AddcelCrypto;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes({"usuario","compra"})
public class GrupoRichardPortalController {
	
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardPortalController.class);
	
	@Autowired
	private GrupoRichardPortalService webService;
	
	
	@RequestMapping(value = "/portal/login")
	public ModelAndView login(@ModelAttribute("login") LoginVO login, ModelMap modelo) {	
		logger.debug("Dentro del servicio: /login");
		ModelAndView mav = null;
		try{
			
			logger.debug("Iniciar pantalla, login" );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
			
		}catch(Exception e){
			mav = new ModelAndView("portal/error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		}
		
		return mav;	
	}
	
	@RequestMapping(value = "/portal/loginFinal", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView loginFinal( @ModelAttribute("login") LoginVO login, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/loginFinal");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
			
		try{
			
			logger.debug("Iniciar session, login: {}",login.getLogin() );
			
			usuario = webService.login(login);
			
			logger.debug("usuario.IdError: {}", usuario.getIdError());
			logger.debug("usuario.MensajeError: {}", usuario.getMensajeError());
			
			if(usuario.getIdError() == 0){
				mav = new ModelAndView("portal/welcome");
				logger.debug("usuario.nombres: {}", usuario.getNombre());
				logger.debug("usuario.status: {}", usuario.getStatus());
				
				//Se sube a session el objeto Usuario
				modelo.put("usuario", usuario);
				mav.addObject("usuario", usuario);
				
			}else{
				login.setPassword("");
				mav = new ModelAndView("portal/login", "usuario", login);
				mav.addObject("mensaje", usuario.getMensajeError());
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /loginFinal: {}", e );
			mav = new ModelAndView("error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}
		return mav;	
	}
	
	@RequestMapping("/portal/logout")
	public ModelAndView logout(SessionStatus status, ModelMap modelo) {
		ModelAndView mav = null;
		try{
			status.setComplete();
			modelo.remove("usuario");
			logger.debug("Cerrando session y redireccionando a la pagina de login." );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			logger.error("Ocurrio un error general al cerrar session: {}", e );
			mav = new ModelAndView("error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}

		return mav;
	}
	
	@RequestMapping(value = "/portal/welcome", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView welcome(ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/welcome");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/welcome." );
				mav = new ModelAndView("portal/welcome");
				mav.addObject("usuario", usuario);
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/welcome: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/altaTienda", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView altaTienda(  @ModelAttribute("tienda") TiendaPortalVO tienda, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/altaTienda");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/altaTienda." );
				mav = new ModelAndView("portal/altaTienda");
				mav.addObject("comboKits", webService.buscarKITSCombo());
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/altaTiendaFinal", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView altaTiendaFinal(  @ModelAttribute("tienda") TiendaPortalVO tienda, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/altaTiendaFinal");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				resp = webService.agregarTienda(tienda, usuario);
				
				logger.debug("Inicia pantalla , portal/altaTiendaFinal." );
				if(resp.getIdError() == 0){
					mav = new ModelAndView("portal/altaTienda", "tienda", new TiendaPortalVO());
				}else{
					mav = new ModelAndView("portal/altaTienda", "tienda", tienda);
				}
				
				mav.addObject("comboKits", webService.buscarKITSCombo());
				mav.addObject("idError", resp.getIdError());
				mav.addObject("mensaje", resp.getMensajeError());
			}
			
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaTienda: {}", e );
		}
		return mav;	
	}
	
	
	@RequestMapping(value = "/portal/busquedaTienda", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView busquedaTienda( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/busquedaTienda");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/busquedaTienda." );
				mav = new ModelAndView("portal/busquedaTienda");
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/filtroTiendas", method=RequestMethod.POST)
	public @ResponseBody String filtroTienda(@ModelAttribute("BusquedaTiendaRequest") BusquedaTiendaRequest busquedaTienda, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/filtroTienda");
		String json = null;
		try{
			json = webService.buscarTiendasPortal(busquedaTienda, (UsuarioVO) modelo.get("usuario"));
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return json;	
	}
	
	@RequestMapping(value = "/portal/buscarKITSCombo", method={RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody HashMap<String, Object> buscarKITSCombo(ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/buscarKITSCombo");
		UsuarioVO usuario = null;
		HashMap<String, Object> resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			resp = webService.buscarKITSCombo(usuario);
		}catch(Exception e){
			logger.error("Ocurrio un error general en /portal/buscarKITSCombo: {}", e);
		}
		return resp;	
	}
	
	@RequestMapping(value = "/portal/actualizaTienda", method={RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody AbstractVO actualizaTienda(@RequestBody TiendaPortalVO tienda, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/actualizaTienda");
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			resp = webService.actualizaTienda(tienda, usuario);
		}catch(Exception e){
			logger.error("Ocurrio un error general en /portal/actualizaTienda: {}", e);
		}
		return resp;	
	}
	
	@RequestMapping(value = "/portal/altaKit", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView altaKit(  @ModelAttribute("tienda") KitVO kit, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/altaKit");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/altaKit." );
				mav = new ModelAndView("portal/altaKit", "kit", new KitVO());
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/altaKitFinal", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView altaKitFinal(  @ModelAttribute("kit") KitVO kit, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/altaKitFinal");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				resp = webService.agregarKit(kit, usuario);
				
				logger.debug("Inicia pantalla , portal/altaKitFinal." );
				if(resp.getIdError() == 0){
					mav = new ModelAndView("portal/altaKit", "kit", new KitVO());
				}else{
					kit.setCarnet(AddcelCrypto.decryptTarjeta(kit.getCarnet()));
					kit.setVigencia(AddcelCrypto.decryptTarjeta(kit.getVigencia()));
					mav = new ModelAndView("portal/altaKit", "kit", kit);
				}
				
				mav.addObject("idError", resp.getIdError());
				mav.addObject("mensaje", resp.getMensajeError());
			}
			
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/actualizaKit", method={RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody AbstractVO actualizaKit(@RequestBody TiendaPortalVO tienda, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/actualizaKit");
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			resp = webService.actualizaKit(tienda, usuario, false);
		}catch(Exception e){
			logger.error("Ocurrio un error general en /portal/actualizaKit: {}", e);
		}
		return resp;	
	}
	
	@RequestMapping(value = "/portal/actualizaKitStatus", method={RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody AbstractVO actualizaKitStatus(@RequestBody TiendaPortalVO tienda, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/actualizaKitStatus");
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			resp = webService.actualizaKit(tienda, usuario, true);
		}catch(Exception e){
			logger.error("Ocurrio un error general en /portal/actualizaKitStatus: {}", e);
		}
		return resp;	
	}
	
	@RequestMapping(value = "/portal/busquedaKits", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView busquedaKit( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/busquedaKits");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/busquedaKits." );
				mav = new ModelAndView("portal/busquedaKits");
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaKits: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/filtroKits", method=RequestMethod.POST)
	public @ResponseBody String filtroKits(@ModelAttribute("BusquedaTiendaRequest") BusquedaTiendaRequest busquedaKit, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/filtroKits");
		String json = null;
		try{
			
			json = webService.buscarKitsPortal(busquedaKit, (UsuarioVO) modelo.get("usuario"));
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/filtroKits: {}", e );
		}
		return json;	
	}
	
	@RequestMapping(value = "/portal/busquedaUsuarios", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView busquedaUsuarios( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/busquedaUsuarios");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/busquedaUsuarios." );
				mav = new ModelAndView("portal/busquedaUsuarios");
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaUsuarios: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/filtroUsuarios", method=RequestMethod.POST)
	public @ResponseBody String filtroUsuarios(@ModelAttribute("BusquedaTiendaRequest") BusquedaTiendaRequest busquedaUsuarios, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/filtroKits");
		String json = null;
		try{
			
			json = webService.buscarUsuariosPortal(busquedaUsuarios, (UsuarioVO) modelo.get("usuario"));
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/filtroKits: {}", e );
		}
		return json;	
	}
	
	@RequestMapping(value = "/portal/actualizaUsuario", method={RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody AbstractVO actualizaUsuario(@RequestBody UsuarioVO user, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/actualizaUsuario");
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			resp = webService.actualizaUsuario(user, usuario, false);
		}catch(Exception e){
			logger.error("Ocurrio un error general en /portal/actualizaUsuario: {}", e);
		}
		return resp;	
	}
	
	@RequestMapping(value = "/portal/actualizaUsuarioStatus", method={RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody AbstractVO actualizaUsuarioStatus(@RequestBody UsuarioVO user, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/actualizaUsuarioStatus");
		UsuarioVO usuario = null;
		AbstractVO resp = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			resp = webService.actualizaUsuario(user, usuario, true);
		}catch(Exception e){
			logger.error("Ocurrio un error general en /portal/actualizaUsuarioStatus: {}", e);
		}
		return resp;	
	}
	
	@RequestMapping(value = "/portal/altaArchivos", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView altaArchivos(@ModelAttribute("listaArchivos") ListaArchivos listaArchivos, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/altaArchivos");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/altaArchivos." );
				mav = new ModelAndView("portal/altaArchivos", "listaArchivos" ,new ListaArchivos());
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaArchivos: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/altaArchivosProcesar", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView altaArchivosProcesar(@ModelAttribute("listaArchivos") ListaArchivos listaArchivos, ModelMap modelo, HttpServletRequest request) {
		logger.debug("Dentro del servicio: /portal/altaArchivosProcesar");
		UsuarioVO usuario = null;
		ModelAndView mav = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				mav =  webService.altaArchivosProcesar(listaArchivos, usuario);
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaArchivos: {}", e );
		}
		return mav;	
	}

	
	@RequestMapping(value = "/portal/altaArchivosRecuperar", method = RequestMethod.GET)
	public void altaArchivosRecuperar(@RequestParam("nombreArchivo") String nombreArchivo, ModelMap modelo, HttpServletResponse response) {
		logger.debug("Dentro del servicio: /portal/altaArchivosRecuperar");
		UsuarioVO usuario = null;
		ModelAndView mav = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				mav =  webService.altaArchivosRecuperar(nombreArchivo, response);
				
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaArchivosRecuperar: {}", e );
		}
	}

}