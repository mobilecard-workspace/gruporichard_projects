package com.addcel.gruporichard.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.gruporichard.model.vo.BusquedaPagosRequest;
import com.addcel.gruporichard.model.vo.BusquedaPagosResponse;
import com.addcel.gruporichard.model.vo.DatosPago;
import com.addcel.gruporichard.model.vo.etn.BusquedaPagosETNResponse;
import com.addcel.gruporichard.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.gruporichard.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	
	public String getFechaActual();

	public int diferenciaFechaSegundos(String fechaToken);	
	
	public int agregarBitacora(TBitacoraVO tbitacora);
	
	public int agregarBitacoraProsa(TBitacoraProsaVO tbitacoraProsa);
	
	public int actualizarBitacora(TBitacoraVO tbitacora);
	
	public int actualizarBitacoraProsa(TBitacoraProsaVO tbitacoraProsa);
	
	public int agregarGrupoRichardDetalle(DatosPago datosPago);
	
	public int updateGrupoRichardDetalle(@Param(value="idPago") int idPago,
									@Param(value="idBitacora") int idBitacora,
									@Param(value="folio") String folio,
									@Param(value="numAutorizacion") String numAutorizacion,
									@Param(value="status") int status,
									@Param(value="cargo") double cargo,
									@Param(value="comision") double comision,
									@Param(value="telefono") String telefono
									);
	
	public List<BusquedaPagosResponse> selectGpoRichardDetalle(BusquedaPagosRequest busqueda);
	
	public List<BusquedaPagosETNResponse> selectGpoRichardDetalleETN(BusquedaPagosRequest busqueda);
	
	public List<String> getMonendero(@Param(value="imei")  String imei);
	
	public int actualizaMonendero(@Param(value="imei")  String imei,
								@Param(value="monto")  double monto);
	
	public String getAfiliacion(@Param(value="idTienda") int idTienda);
}
