package com.addcel.gruporichard.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.gruporichard.model.vo.UsuarioVO;
import com.addcel.gruporichard.model.vo.common.AbstractLabelValue;
import com.addcel.gruporichard.model.vo.common.KitVO;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;
import com.addcel.gruporichard.model.vo.portal.BusquedaTiendaRequest;

public interface PortalConfigMapper {
	
	public int agregarTienda(TiendaPortalVO tienda);
	
	public int actualizarTienda(TiendaPortalVO tienda);
	
	public List<TiendaPortalVO> buscarTiendas(@Param(value = "idTienda") int idTienda,
										@Param(value = "status") int status,
										@Param(value = "nombre") String nombre,
										@Param(value = "imei") String imei);
	
	public List<TiendaPortalVO> buscarTiendasPortal(BusquedaTiendaRequest busquedaTienda);
	
	public int agregarTarjeta(TiendaPortalVO TiendaPortalVO);
	
	public int actualizarTarjeta(@Param(value = "idTarjeta") int idTarjeta,
								@Param(value = "status") int status,
								@Param(value = "vigencia") String vigencia);
	
	public int agregarKit(KitVO kit);
	
	public List<AbstractLabelValue> buscarKITSCombo( );
	
	public List<TiendaPortalVO> buscarKITS(@Param(value = "idKit") int idKit,
									@Param(value = "idTienda") int idTienda,
									@Param(value = "status") int status,
									@Param(value = "imei") String imei,
									@Param(value = "carnet") String carnet,
									@Param(value = "dispositivo") String dispositivo,
									@Param(value = "sim") String sim,
									@Param(value = "telefono") String telefono);
	
	public List<TiendaPortalVO> buscarKitsPortal(BusquedaTiendaRequest busquedaTienda);
	
	public int actualizarKit(TiendaPortalVO TiendaPortalVO);
	
	public int actualizarKitAltaCARNET(@Param(value = "idKit") int idKit);
	
	public List<UsuarioVO> buscarUsuariosPortal(BusquedaTiendaRequest busquedaTienda);
	
	public int actualizarUsuario(UsuarioVO usuario);
	
}
