package com.addcel.gruporichard.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.gruporichard.model.vo.UsuarioVO;
import com.addcel.gruporichard.model.vo.common.TiendaVO;

public interface UsuarioConfigMapper {
	
	
	public List<TiendaVO> buscarTiendaKIT(@Param(value = "imei") String imei);
	
	public int agregarUsuario(UsuarioVO usuario);
	
	public int actualizarUsuario(UsuarioVO usuario);
	
	public int actualizarPassword(UsuarioVO usuario);
	
	public List<UsuarioVO> validarUsuario(
										@Param(value = "idTienda") int idTienda,
										@Param(value = "idUsuario") int idUsuario,
										@Param(value = "login") String login,
										@Param(value = "isKit") String isKit);

	public int valorNotificacion(  );
	
	public int agregarBitacoraLocalizacion(HashMap<String, Object> localizacion);
}
