package com.addcel.gruporichard.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.gruporichard.model.vo.common.TProveedorVO;


public interface UtilsMapper {
	
	public String getTParametro(@Param(value = "clave") String clave);
	
	public TProveedorVO getTProveedor(@Param(value = "idProveedor") int idProveedor);

}
