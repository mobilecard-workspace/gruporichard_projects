package com.addcel.gruporichard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusquedaPagosResponse {

	private int idBitacora;
	private String mail;
	private String nombres;
	private String descProducto;
	private String telefono;
	private String tarjetaTag;
	private String referenciaMegacable;
	private double total;
	private String numAutorizacion;
	private String folio;
	private String tipoTarjeta;
	private String tipoPago;
	private String fecha;
	
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getDescProducto() {
		return descProducto;
	}
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTarjetaTag() {
		return tarjetaTag;
	}
	public void setTarjetaTag(String tarjetaTag) {
		this.tarjetaTag = tarjetaTag;
	}
	public String getReferenciaMegacable() {
		return referenciaMegacable;
	}
	public void setReferenciaMegacable(String referenciaMegacable) {
		this.referenciaMegacable = referenciaMegacable;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	
	
}