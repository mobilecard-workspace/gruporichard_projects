package com.addcel.gruporichard.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gruporichard.model.vo.diestel.CamposMovilesVO;
import com.addcel.gruporichard.model.vo.etn.DatosViajeETN;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosPago {

	private int idPago;
	private int idUsuario;
	private int idTienda;
	private int idBitacora;
	private int idProveedor;
	private String password;
	private String nombres;
	private String mail;
	private String descProducto;
	private int producto;
	private String telefono;
	private String tarjetaTag;
	private String pin;
	private String referenciaMegacable;
	private String referencia;
	private String idServicio;
	private String pnr;
	private List<DatosViajeETN> viajes;
	private List<CamposMovilesVO> campos;
	private double total;
	private String token;

	private String afiliacion;
	private String imei;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	private String cx;
	private String cy;

	private int idTipoTarjeta;
	private String tarjeta;
	private String vigencia;
	private String cvv2;
	private int tipoPago;
	

	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getDescProducto() {
		return descProducto;
	}
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	public int getIdTipoTarjeta() {
		return idTipoTarjeta;
	}
	public void setIdTipoTarjeta(int idTipoTarjeta) {
		this.idTipoTarjeta = idTipoTarjeta;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public int getIdTienda() {
		return idTienda;
	}
	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIdPago() {
		return idPago;
	}
	public void setIdPago(int idPago) {
		this.idPago = idPago;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTarjetaTag() {
		return tarjetaTag;
	}
	public void setTarjetaTag(String tarjetaTag) {
		this.tarjetaTag = tarjetaTag;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public int getProducto() {
		return producto;
	}
	public void setProducto(int producto) {
		this.producto = producto;
	}
	public String getReferenciaMegacable() {
		return referenciaMegacable;
	}
	public void setReferenciaMegacable(String referenciaMegacable) {
		this.referenciaMegacable = referenciaMegacable;
	}
	public int getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(int tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public List<DatosViajeETN> getViajes() {
		return viajes;
	}
	public void setViajes(List<DatosViajeETN> viajes) {
		this.viajes = viajes;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}
	public List<CamposMovilesVO> getCampos() {
		return campos;
	}
	public void setCampos(List<CamposMovilesVO> campos) {
		this.campos = campos;
	}
}
