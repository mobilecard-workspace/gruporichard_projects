package com.addcel.gruporichard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gruporichard.model.vo.bridge.CompraResponseVO;
import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.model.vo.megacable.RespuestaServicio;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosPagoResponse extends AbstractVO {

	private int idBitacora;
	private String numAutorizacion;
	private String folio;
	private double cargo;
	private double comision;
	
	private int status;
	private int codigoRechazo;
	private String descRechazo;
	private String fecha;
	
	public DatosPagoResponse(){
		
	}
	
	public DatosPagoResponse(CompraResponseVO response){
		super(response.getResultado() == 1? 0: response.getResultado() + 1, response.getMensaje());
		
		this.idBitacora = response.getReferencia();
		this.numAutorizacion = response.getNumAutorizacion();
		this.folio = response.getFolio();
		this.cargo = response.getCargo();
		this.comision = response.getComision();
		
		this.status = response.getResultado();// == 1? 1: response.getResultado();
		this.codigoRechazo = Integer.parseInt(response.getClaveRechazo() != null && !"".equals(response.getClaveRechazo())? response.getClaveRechazo() : "0");
		this.descRechazo = response.getDescripcionRechazo();
//		this.fecha = response.get;
	}
	
	public DatosPagoResponse(RespuestaServicio response){
		super(response.getIdError(), response.getMsgError());
		
		if(response.getDatos() != null){
			this.idBitacora = Integer.parseInt(response.getDatos().getIdMobilecard() != null ? response.getDatos().getIdMobilecard(): "0");
			this.numAutorizacion = response.getDatos().getAutorizacion();
//			this.folio = response.getFolio();
			this.cargo = Double.parseDouble(  response.getDatos().getMonto() != null?  response.getDatos().getMonto(): "0.0");
			this.comision =  Double.parseDouble(  response.getDatos().getComision()!= null?  response.getDatos().getComision(): "0.0");
		}
		
		this.status = response.getIdError() == 0? 1: response.getIdError();
		if(response.getMsgError() != null){
			String[] data = response.getMsgError().split("-");
			if(data.length == 2){
				this.codigoRechazo = Integer.parseInt(data[0] != null? data[0]: "");
				this.descRechazo = data[1];
			}
		}
	}

	public DatosPagoResponse(int idError, String mensajeError){
		super(idError, mensajeError);
	}
	
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public double getCargo() {
		return cargo;
	}
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getDescRechazo() {
		return descRechazo;
	}
	public void setDescRechazo(String descRechazo) {
		this.descRechazo = descRechazo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCodigoRechazo() {
		return codigoRechazo;
	}
	public void setCodigoRechazo(int codigoRechazo) {
		this.codigoRechazo = codigoRechazo;
	}
	
	
}
