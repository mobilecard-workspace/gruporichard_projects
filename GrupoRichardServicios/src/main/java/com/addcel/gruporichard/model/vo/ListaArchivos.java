package com.addcel.gruporichard.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.web.multipart.MultipartFile;

import com.addcel.gruporichard.model.vo.common.AbstractVO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaArchivos extends AbstractVO {

	private List<MultipartFile> archivos;

	public int size() {
		return archivos != null ? archivos.size() : 0;
	}

	public List<MultipartFile> getArchivos() {
		return archivos;
	}

	public void setArchivos(List<MultipartFile> archivos) {
		this.archivos = archivos;
	}

}
