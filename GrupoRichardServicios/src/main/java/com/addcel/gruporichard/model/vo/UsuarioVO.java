package com.addcel.gruporichard.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.model.vo.common.TipoTarjetaVO;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UsuarioVO extends AbstractVO{
	private int idUsuario;
	private int idProveedor;
    private int idTienda;
    private String login;
    private String password;

    private String nombre;
    private String apellido;
    private String materno;
    private String mail;
    private String telefono;
    private int tiempoNoti;
    
    private String newPassword;
    private int status;
    private int statusTienda;
    
    private String imei;
    private String software;
    private String modelo;
    
    private int isPortal;
    
    private List<TipoTarjetaVO> tipoTarjeta;
    
    public UsuarioVO() {	
	}
    
    public UsuarioVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public List<TipoTarjetaVO> getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(List<TipoTarjetaVO> tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public int getStatusTienda() {
		return statusTienda;
	}

	public void setStatusTienda(int statusTienda) {
		this.statusTienda = statusTienda;
	}

	public int getIsPortal() {
		return isPortal;
	}

	public void setIsPortal(int isPortal) {
		this.isPortal = isPortal;
	}

	public int getTiempoNoti() {
		return tiempoNoti;
	}

	public void setTiempoNoti(int tiempoNoti) {
		this.tiempoNoti = tiempoNoti;
	}
    
		
}
