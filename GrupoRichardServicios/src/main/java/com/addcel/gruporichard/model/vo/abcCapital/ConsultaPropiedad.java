package com.addcel.gruporichard.model.vo.abcCapital;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType (XmlAccessType.NONE)
public class ConsultaPropiedad{
	
	@XmlAttribute(name="name")
	private String name;
	
	@XmlElement(name="field")
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
