package com.addcel.gruporichard.model.vo.abcCapital;

import com.addcel.gruporichard.model.vo.abcCapital.T24Result.Body.Table.Row.Field;
import com.addcel.gruporichard.model.vo.common.AbstractVO;

public class ConsultaSaldoResponse extends AbstractVO{
	
	private String carnet;
	private double saldoCta;
	private double saldoDispCta;
	
	private static final String SAL_CTA ="SAL.CTA";
	private static final String SAL_DISP_CTA ="SAL.DISP.CTA";
	
	public ConsultaSaldoResponse(){
		
	}
	
	public ConsultaSaldoResponse(int idError, String mensajeError){
		super(idError, mensajeError);
	}
	
	public ConsultaSaldoResponse(T24Result result){
		super((result.getBody().getError() != null)? 10: 0, 
				(result.getBody().getError() != null)? result.getBody().getError().getMessage(): null);
		
		if(result.getBody().getTable() != null && result.getBody().getTable().getRow() != null){
			for(Field campo: result.getBody().getTable().getRow().getField()){
				if(SAL_CTA.equals(campo.getName())){
					saldoCta = Double.parseDouble(campo.getValue());
				}
				
				if(SAL_DISP_CTA.equals(campo.getName())){
					saldoDispCta = Double.parseDouble(campo.getValue());
				}
			}
		}
	}
	
	public String getCarnet() {
		return carnet;
	}
	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public double getSaldoCta() {
		return saldoCta;
	}

	public void setSaldoCta(double saldoCta) {
		this.saldoCta = saldoCta;
	}

	public double getSaldoDispCta() {
		return saldoDispCta;
	}

	public void setSaldoDispCta(double saldoDispCta) {
		this.saldoDispCta = saldoDispCta;
	}
	
	
}
