package com.addcel.gruporichard.model.vo.bridge;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompraResponseVO {

	private String folio;
    private int resultado;
    private String mensaje;
    
    private String numAutorizacion; 
    private int referencia;
    private double cargo;
    private double comision;
    
    private String claveRechazo;
    private String descripcionRechazo;
    
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public int getResultado() {
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	public int getReferencia() {
		return referencia;
	}
	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}
	public double getCargo() {
		return cargo;
	}
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getClaveRechazo() {
		return claveRechazo;
	}
	public void setClaveRechazo(String claveRechazo) {
		this.claveRechazo = claveRechazo;
	}
	public String getDescripcionRechazo() {
		return descripcionRechazo;
	}
	public void setDescripcionRechazo(String descripcionRechazo) {
		this.descripcionRechazo = descripcionRechazo;
	}    
    
}
