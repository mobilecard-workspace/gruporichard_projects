package com.addcel.gruporichard.model.vo.bridge;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gruporichard.model.vo.DatosPago;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompraVO {

	private int idAplicacion;
    private long idUsuario;
    private String nombre;
    private String apellido;
    private String materno;
    private String email;
	
	private int tipoTarjeta;
	private String tarjetaBanco;
	private String tarjeta;					//variable para el envio de la tarjeta TAG para IVA y PASE
	private String vigencia;
    private String cvv2;
    private String producto;
    private String telefono;
    private String pin;
    private String imei;
    
    private String cx;
    private String cy;
    private String tipo;
    private String software;
    private String modelo;
    private String key;
    
    public CompraVO(){
    	
    }
    
    public CompraVO(DatosPago datosPago){
    	this.idAplicacion = datosPago.getIdProveedor();
    	this.idUsuario = datosPago.getIdUsuario();
        this.nombre = datosPago.getNombres();
//        this.apellido = datosPago.get;
//        this.materno = datosPago.get;
        this.email = datosPago.getMail();
    	
        this.tipoTarjeta = datosPago.getIdTipoTarjeta();
    	this.tarjetaBanco = datosPago.getTarjeta();
    	this.vigencia = datosPago.getVigencia();
        this.cvv2 = datosPago.getCvv2();
        this.producto = String.valueOf(datosPago.getProducto());
        this.telefono = datosPago.getTelefono();
        this.tarjeta = datosPago.getTarjetaTag();
        this.pin = datosPago.getPin();
        this.imei = datosPago.getImei();
        
        this.cx = datosPago.getCx();
        this.cy = datosPago.getCy();
        this.tipo = datosPago.getTipo();
        this.software = datosPago.getSoftware();
        this.modelo = datosPago.getModelo();
        this.key = datosPago.getImei();
    }

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getTarjetaBanco() {
		return tarjetaBanco;
	}

	public void setTarjetaBanco(String tarjetaBanco) {
		this.tarjetaBanco = tarjetaBanco;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCx() {
		return cx;
	}

	public void setCx(String cx) {
		this.cx = cx;
	}

	public String getCy() {
		return cy;
	}

	public void setCy(String cy) {
		this.cy = cy;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
}
