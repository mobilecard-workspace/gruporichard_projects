package com.addcel.gruporichard.model.vo.common;

public class AbstractVO {	
	private int idError;
	private String mensajeError;
	
	public AbstractVO() {}
	
	public AbstractVO(String mensajeError) {	
		this.mensajeError = mensajeError;
	}
	
	public AbstractVO(int idError, String mensajeError) {	
		this.idError = idError;
		this.mensajeError = mensajeError;
	}
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}	
}
