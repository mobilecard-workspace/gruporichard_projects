package com.addcel.gruporichard.model.vo.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class KitVO extends AbstractVO{
	private int idKit;
	private int version;
	private int idTienda;
	private String mascara;
    private String carnet;
    private String vigencia;
    private String imei;
	private String dispositivo;
	private String sim;
	private String telefono;
	private String folio;
	private String nombreTitular;
	
	private String imeiConf;
	private String dispositivoConf;
	private String simConf;
	private String telefonoConf;
	
    private int idUserAlta;
    private int idUserBaja;
    private String fechaAlta;
    private String fechaBaja;
	
	private int status;
	private int isActiva;
	
	public KitVO(){
		
	}
	
	public KitVO(TiendaPortalVO tienda) {	
		super();
		
		this.idKit = tienda.getIdKit();
		this.idTienda = tienda.getIdTienda();
		this.mascara = tienda.getMascara();
	    this.carnet = tienda.getCarnet();
	    this.vigencia = tienda.getVigencia();
	    this.imei = tienda.getImei();
		this.dispositivo = tienda.getDispositivo();
		this.sim = tienda.getSim();
		this.telefono = tienda.getTelefono();
		this.folio = tienda.getFolio();
		this.nombreTitular = tienda.getNombreTitular();
		this.isActiva = tienda.getIsActiva();
		this.status = tienda.getStatus();
		
	    this.idUserAlta = tienda.getIdUserAlta();
	    this.idUserBaja = tienda.getIdUserBaja();
	    this.fechaAlta = tienda.getFechaAlta();
	    this.fechaBaja = tienda.getFechaBaja();
		
		this.status = tienda.getStatus();
	}
	
	public KitVO(String mensajeError) {	
		super(mensajeError);
	}
	
	public KitVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}

	public int getIdKit() {
		return idKit;
	}

	public void setIdKit(int idKit) {
		this.idKit = idKit;
	}

	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}

	public int getIdUserAlta() {
		return idUserAlta;
	}

	public void setIdUserAlta(int idUserAlta) {
		this.idUserAlta = idUserAlta;
	}

	public int getIdUserBaja() {
		return idUserBaja;
	}

	public void setIdUserBaja(int idUserBaja) {
		this.idUserBaja = idUserBaja;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getImeiConf() {
		return imeiConf;
	}

	public void setImeiConf(String imeiConf) {
		this.imeiConf = imeiConf;
	}

	public String getDispositivoConf() {
		return dispositivoConf;
	}

	public void setDispositivoConf(String dispositivoConf) {
		this.dispositivoConf = dispositivoConf;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getSimConf() {
		return simConf;
	}

	public void setSimConf(String simConf) {
		this.simConf = simConf;
	}

	public String getTelefonoConf() {
		return telefonoConf;
	}

	public void setTelefonoConf(String telefonoConf) {
		this.telefonoConf = telefonoConf;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getIsActiva() {
		return isActiva;
	}

	public void setIsActiva(int isActiva) {
		this.isActiva = isActiva;
	}

}
