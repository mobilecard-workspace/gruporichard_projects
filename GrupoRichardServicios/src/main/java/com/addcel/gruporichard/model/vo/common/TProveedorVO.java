package com.addcel.gruporichard.model.vo.common;

public class TProveedorVO {
	private int idProveedor;
	private String nombre;
	private double comision;
	private String afiliacion;
	private double tipoCambio;
	private String maxTransaccion;
	
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getMaxTransaccion() {
		return maxTransaccion;
	}
	public void setMaxTransaccion(String maxTransaccion) {
		this.maxTransaccion = maxTransaccion;
	}
	
	
	
}
