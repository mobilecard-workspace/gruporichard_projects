package com.addcel.gruporichard.model.vo.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TiendaPortalVO extends AbstractVO{
	private int idTienda;
	private String nombre;
	
	private int idKit;
	private int version;
	private String afiliacion;
	private String mascara;
    private String carnet;
    private String vigencia;
    private String imei;
	private String dispositivo;
	private String sim;
	private String telefono;
	private String folio;
	private String nombreTitular;
	private int isActiva;
	
    private String estado;
    private String ciudad;
    private String calle;
    private String numInt;
    private String numExt;
    private String colonia;
    private String cp;
    
    private String cx;
    private String cy;
    
    private int idUserAlta;
    private int idUserBaja;
    private String fechaAlta;
    private String fechaBaja;
	
	private int status;
	
	public TiendaPortalVO(){
		
	}
	
	public TiendaPortalVO(String mensajeError) {	
		super(mensajeError);
	}
	
	public TiendaPortalVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public int getIdKit() {
		return idKit;
	}

	public void setIdKit(int idKit) {
		this.idKit = idKit;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumInt() {
		return numInt;
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	public String getNumExt() {
		return numExt;
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCx() {
		return cx;
	}

	public void setCx(String cx) {
		this.cx = cx;
	}

	public String getCy() {
		return cy;
	}

	public void setCy(String cy) {
		this.cy = cy;
	}

	public int getIdUserAlta() {
		return idUserAlta;
	}

	public void setIdUserAlta(int idUserAlta) {
		this.idUserAlta = idUserAlta;
	}

	public int getIdUserBaja() {
		return idUserBaja;
	}

	public void setIdUserBaja(int idUserBaja) {
		this.idUserBaja = idUserBaja;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public int getIsActiva() {
		return isActiva;
	}

	public void setIsActiva(int isActiva) {
		this.isActiva = isActiva;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	
}
