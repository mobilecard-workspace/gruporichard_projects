package com.addcel.gruporichard.model.vo.common;

public class TipoTarjetaVO {	
	private int idTipoTarjeta;
	private String descripcion;
	
	public TipoTarjetaVO() {}
	
	public TipoTarjetaVO(int idTipoTarjeta, String descripcion) {	
		this.idTipoTarjeta = idTipoTarjeta;
		this.descripcion = descripcion;
	}

	public int getIdTipoTarjeta() {
		return idTipoTarjeta;
	}

	public void setIdTipoTarjeta(int idTipoTarjeta) {
		this.idTipoTarjeta = idTipoTarjeta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
