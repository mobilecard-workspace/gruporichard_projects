package com.addcel.gruporichard.model.vo.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TokenVO extends AbstractVO{
	private int idProveedor;
	private String usuario;
	private String password;
	
	public TokenVO(){
		
	}
	
	public TokenVO(String usuario, String password){
		this.usuario = usuario;
		this.password = password;
	}

	public TokenVO(int idProveedor, String usuario, String password){
		this.idProveedor = idProveedor;
		this.usuario = usuario;
		this.password = password;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
