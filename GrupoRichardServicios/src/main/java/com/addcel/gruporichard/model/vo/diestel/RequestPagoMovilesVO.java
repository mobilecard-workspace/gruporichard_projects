package com.addcel.gruporichard.model.vo.diestel;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gruporichard.model.vo.DatosPago;
import com.addcel.utils.AddcelCrypto;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPagoMovilesVO {
	
	private String software;
	private String token;
	private String tipo;
	private String imei;
	private String cy;
	private String cx;
	private String login;
	private String modelo;
	private String cvv2;
	private String password;
	private String referencia;
	private String key;
	private String monto;
	private String idServicio;
	private int idPlataforma;
	private List<CamposMovilesVO> campos;
	
	private String tarjeta;
	private String vigencia;
	private int idAplicacion;
    private long idUsuario;
    private String nombre;
    private String apellido;
    private String materno;
    private String email;
    private int tipoTarjeta;
	
    public RequestPagoMovilesVO(){
    	
    }
    
    public RequestPagoMovilesVO(DatosPago pago){
    	this.software = pago.getSoftware();
    	this.token = pago.getToken();
    	this.tipo = pago.getTipo();
    	this.imei = pago.getImei();
    	this.cy = pago.getCx();
    	this.cx = pago.getCy();
//    	this.login = pago.get;
    	this.modelo = pago.getModelo();
    	this.cvv2 = pago.getCvv2();
    	this.password = pago.getPassword();
    	this.referencia = pago.getReferencia();
    	this.key = pago.getWkey();
    	this.monto = pago.getTotal() + "";
    	this.idServicio = pago.getIdServicio();
    	this.idPlataforma = 6;
    	this.campos = pago.getCampos();

    	this.tarjeta = AddcelCrypto.encryptTarjeta(pago.getTarjeta());
    	this.vigencia = pago.getVigencia();
    	this.idAplicacion = 30;
    	this.idUsuario = pago.getIdUsuario();
    	this.nombre = pago.getNombres();
//    	this.apellido = pago.get;
//    	this.materno = pago.get;
    	this.email = pago.getMail();
    	this.tipoTarjeta = pago.getIdTipoTarjeta();
    }
	
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}
	public int getIdPlataforma() {
		return idPlataforma;
	}
	public void setIdPlataforma(int idPlataforma) {
		this.idPlataforma = idPlataforma;
	}
	public List<CamposMovilesVO> getCampos() {
		return campos;
	}
	public void setCampos(List<CamposMovilesVO> campos) {
		this.campos = campos;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
}
