package com.addcel.gruporichard.model.vo.etn;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BoletoETN {

	private int idBoleto;
	private int idReservacion;
//	private String corrida;
//	private String tipoCorrida;
	private String asiento;
	private String nombre;
	private String operacion;
	private String importeAsiento;
	private String tipoAsiento;
	public int getIdBoleto() {
		return idBoleto;
	}
	public void setIdBoleto(int idBoleto) {
		this.idBoleto = idBoleto;
	}
	public int getIdReservacion() {
		return idReservacion;
	}
	public void setIdReservacion(int idReservacion) {
		this.idReservacion = idReservacion;
	}
	public String getAsiento() {
		return asiento;
	}
	public void setAsiento(String asiento) {
		this.asiento = asiento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		if(nombre != null){
			this.nombre = nombre.trim();
		}
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getImporteAsiento() {
		return importeAsiento;
	}
	public void setImporteAsiento(String importeAsiento) {
		this.importeAsiento = importeAsiento;
	}
	public String getTipoAsiento() {
		return tipoAsiento;
	}
	public void setTipoAsiento(String tipoAsiento) {
		this.tipoAsiento = tipoAsiento;
	}
	
}
