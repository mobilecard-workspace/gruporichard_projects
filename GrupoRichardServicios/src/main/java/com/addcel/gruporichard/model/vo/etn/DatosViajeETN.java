package com.addcel.gruporichard.model.vo.etn;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosViajeETN {

	private String idOrigen;
	private String idDestino; 
	private String origen;
	private String destino; 
	private String fecha;
	private String hora;
	private int numAdulto;
	private int numNino;
	private int numInsen;
	private int numEstudiante;
	private int numMaestro;
	private String corrida;
	private String tipoCorrida;
	private String asientos;
	private String nombres;
	private String tipoProducto; 
	private String tipoViaje;
	private double tarifaAdulto;
	private double tarifaNino;
	private double tarifaInsen;
	private double tarifaEstudiante;
	private double tarifaMaestro;
	
	public String getIdOrigen() {
		return idOrigen;
	}
	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}
	public String getIdDestino() {
		return idDestino;
	}
	public void setIdDestino(String idDestino) {
		this.idDestino = idDestino;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public int getNumAdulto() {
		return numAdulto;
	}
	public void setNumAdulto(int numAdulto) {
		this.numAdulto = numAdulto;
	}
	public int getNumNino() {
		return numNino;
	}
	public void setNumNino(int numNino) {
		this.numNino = numNino;
	}
	public int getNumInsen() {
		return numInsen;
	}
	public void setNumInsen(int numInsen) {
		this.numInsen = numInsen;
	}
	public int getNumEstudiante() {
		return numEstudiante;
	}
	public void setNumEstudiante(int numEstudiante) {
		this.numEstudiante = numEstudiante;
	}
	public int getNumMaestro() {
		return numMaestro;
	}
	public void setNumMaestro(int numMaestro) {
		this.numMaestro = numMaestro;
	}
	public String getCorrida() {
		return corrida;
	}
	public void setCorrida(String corrida) {
		this.corrida = corrida;
	}
	public String getTipoCorrida() {
		return tipoCorrida;
	}
	public void setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
	}
	public String getAsientos() {
		return asientos;
	}
	public void setAsientos(String asientos) {
		this.asientos = asientos;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public String getTipoViaje() {
		return tipoViaje;
	}
	public void setTipoViaje(String tipoViaje) {
		this.tipoViaje = tipoViaje;
	}
	public double getTarifaAdulto() {
		return tarifaAdulto;
	}
	public void setTarifaAdulto(double tarifaAdulto) {
		this.tarifaAdulto = tarifaAdulto;
	}
	public double getTarifaNino() {
		return tarifaNino;
	}
	public void setTarifaNino(double tarifaNino) {
		this.tarifaNino = tarifaNino;
	}
	public double getTarifaInsen() {
		return tarifaInsen;
	}
	public void setTarifaInsen(double tarifaInsen) {
		this.tarifaInsen = tarifaInsen;
	}
	public double getTarifaEstudiante() {
		return tarifaEstudiante;
	}
	public void setTarifaEstudiante(double tarifaEstudiante) {
		this.tarifaEstudiante = tarifaEstudiante;
	}
	public double getTarifaMaestro() {
		return tarifaMaestro;
	}
	public void setTarifaMaestro(double tarifaMaestro) {
		this.tarifaMaestro = tarifaMaestro;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

}
