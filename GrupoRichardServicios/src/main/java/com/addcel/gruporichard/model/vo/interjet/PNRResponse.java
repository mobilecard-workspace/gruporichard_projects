package com.addcel.gruporichard.model.vo.interjet;

import com.addcel.gruporichard.model.vo.common.AbstractVO;

public class PNRResponse extends AbstractVO{
    private PNRMobile datos;
    
    public PNRResponse(){
    	
    }
    
    public PNRResponse(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}

	public PNRMobile getDatos() {
		return datos;
	}

	public void setDatos(PNRMobile datos) {
		this.datos = datos;
	}
    
}