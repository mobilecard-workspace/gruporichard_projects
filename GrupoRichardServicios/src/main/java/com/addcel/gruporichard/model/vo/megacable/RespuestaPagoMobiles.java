package com.addcel.gruporichard.model.vo.megacable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RespuestaPagoMobiles {
	private String tarjeta;
	private String monto;
	private String comision;
	private String idMobilecard;
	private String autorizacion;
	
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getIdMobilecard() {
		return idMobilecard;
	}
	public void setIdMobilecard(String idMobilecard) {
		this.idMobilecard = idMobilecard;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getComision() {
		return comision;
	}
	public void setComision(String comision) {
		this.comision = comision;
	}
}
