package com.addcel.gruporichard.model.vo.megacable;

public class RespuestaServicio {
	private int idError;
	private String msgError;
	private RespuestaPagoMobiles datos;
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public RespuestaPagoMobiles getDatos() {
		return datos;
	}
	public void setDatos(RespuestaPagoMobiles datos) {
		this.datos = datos;
	}
	
}
