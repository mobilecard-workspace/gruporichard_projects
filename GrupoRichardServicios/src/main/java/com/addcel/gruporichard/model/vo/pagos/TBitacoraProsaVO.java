package com.addcel.gruporichard.model.vo.pagos;

public class TBitacoraProsaVO {	
	private int idBitacoraProsa;
	private int idBitacora;
	private int idUsuario;
	private String tarjeta;
	private String transaccion;
	private String autorizacion;
	private String fecha;
	private String bit_hora;
	private String concepto;
	private String cargo;
	private double comision;
	private String cx;
	private String cy;
	
	public TBitacoraProsaVO(){
		
	}
	
	public TBitacoraProsaVO (TBitacoraVO tb, String cx, String cy){
		this.idBitacora = tb.getIdBitacora();
		this.idUsuario= tb.getIdUsuario();
		this.tarjeta= tb.getTarjetaCompra();
//		this.transaccion= tb.get;
//		this.autorizacion= tb.get;
//		this.fecha= tb.get;
//		this.bit_hora= tb.get;
		this.concepto= tb.getBitConcepto();
		this.cargo= tb.getBitCargo();
//		this.comision= tb.get;
		this.cx= cx;
		this.cy= cy;
	}
	
	public int getIdBitacoraProsa() {
		return idBitacoraProsa;
	}
	public void setIdBitacoraProsa(int idBitacoraProsa) {
		this.idBitacoraProsa = idBitacoraProsa;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(String bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	
		
}
