package com.addcel.gruporichard.model.vo.pagos;

import com.addcel.utils.AddcelCrypto;

public class TBitacoraVO {	
	private int idBitacora;
	private int idUsuario;
	private int idProveedor;
	private int idProducto;
	private String bitFecha;
	private String bitHora;
	private String bitConcepto;
	private String  bitCargo;
	private String bitTicket;
	private String bitNoAutorizacion;
	private int bitCodigoError;
	private String bitCardId;
	private int bitStatus;
	private String imei;
	private String destino;
	private String tarjetaCompra;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	
	public TBitacoraVO(){}	
	
	public TBitacoraVO(int idUsuario,int idProveedor,String bitCargo, String bitConcepto, String imei,
			String destino, String tarjetaCompra, String tipo, String software,
			String modelo, String wkey) {
		super();
		this.idUsuario = idUsuario;
		this.idProveedor = idProveedor;
		this.bitCargo = bitCargo;
		this.bitConcepto = bitConcepto;
		this.imei = imei;
		this.destino = destino;
		try{
			this.tarjetaCompra = AddcelCrypto.encryptTarjeta(tarjetaCompra);
		}catch(Exception e){
			
		}
		this.tipo = tipo;
		this.software = software;
		this.modelo = modelo;
		this.wkey = wkey;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getBitFecha() {
		return bitFecha;
	}
	public void setBitFecha(String bitFecha) {
		this.bitFecha = bitFecha;
	}
	public String getBitHora() {
		return bitHora;
	}
	public void setBitHora(String bitHora) {
		this.bitHora = bitHora;
	}
	public String getBitConcepto() {
		return bitConcepto;
	}
	public void setBitConcepto(String bitConcepto) {
		this.bitConcepto = bitConcepto;
	}
	public String getBitCargo() {
		return bitCargo;
	}
	public void setBitCargo(String bitCargo) {
		this.bitCargo = bitCargo;
	}
	public String getBitTicket() {
		return bitTicket;
	}
	public void setBitTicket(String bitTicket) {
		this.bitTicket = bitTicket;
	}
	public String getBitNoAutorizacion() {
		return bitNoAutorizacion;
	}
	public void setBitNoAutorizacion(String bitNoAutorizacion) {
		this.bitNoAutorizacion = bitNoAutorizacion;
	}
	public int getBitCodigoError() {
		return bitCodigoError;
	}
	public void setBitCodigoError(int bitCodigoError) {
		this.bitCodigoError = bitCodigoError;
	}
	public String getBitCardId() {
		return bitCardId;
	}
	public void setBitCardId(String bitCardId) {
		this.bitCardId = bitCardId;
	}
	public int getBitStatus() {
		return bitStatus;
	}
	public void setBitStatus(int bitStatus) {
		this.bitStatus = bitStatus;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getTarjetaCompra() {
		return tarjetaCompra;
	}
	public void setTarjetaCompra(String tarjetaCompra) {
		this.tarjetaCompra = tarjetaCompra;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}	
	}
