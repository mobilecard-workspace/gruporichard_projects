package com.addcel.gruporichard.model.vo.portal;



public class BusquedaTiendaRequest {	
	private int curPage;
	private int recordsPerPage;
	private int sortBy;
	private String dir;
	private int filterBy;
	private String filterValue;
	
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public int getRecordsPerPage() {
		return recordsPerPage;
	}
	public void setRecordsPerPage(int recordsPerPage) {
		this.recordsPerPage = recordsPerPage;
	}
	public int getSortBy() {
		return sortBy;
	}
	public void setSortBy(int sortBy) {
		this.sortBy = sortBy;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
	public int getFilterBy() {
		return filterBy;
	}
	public void setFilterBy(int filterBy) {
		this.filterBy = filterBy;
	}
	public String getFilterValue() {
		return filterValue;
	}
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}
	
}
