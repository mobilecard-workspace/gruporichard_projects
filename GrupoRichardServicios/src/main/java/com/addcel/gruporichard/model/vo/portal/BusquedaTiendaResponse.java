package com.addcel.gruporichard.model.vo.portal;

import java.util.List;

import com.addcel.gruporichard.model.vo.common.AbstractBusqueda;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;


public class BusquedaTiendaResponse extends AbstractBusqueda {	
	private List<TiendaPortalVO> data;

	public List<TiendaPortalVO> getData() {
		return data;
	}

	public void setData(List<TiendaPortalVO> data) {
		this.data = data;
	}
	
	
	
}
