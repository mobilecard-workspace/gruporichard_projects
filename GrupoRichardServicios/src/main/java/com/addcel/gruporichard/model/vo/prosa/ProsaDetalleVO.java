package com.addcel.gruporichard.model.vo.prosa;

import java.io.Serializable;

public class ProsaDetalleVO implements Serializable{
	
	private static final long serialVersionUID = -3216445473867726951L;
	
	private int idDetalle;
	private long idBitacora;
	private int idProveedor;
	private String idUsuario;
	private String fecha;
	private int status;
	private String descProducto;
	private Double total;
	private String moneda;
	private Double tipoCambio;
	private String tipoTarjeta;
	private String nombres;
	private String email;
	
	public ProsaDetalleVO() {}
	
	public ProsaDetalleVO(long idBitacora, String idUsuario,
			int idProveedor, String descProducto, Double total,
			String moneda, Double tipoCambio,
			String tipoTarjeta, String nombres, String email
			) {
		super();		
		this.idBitacora = idBitacora;
		this.idUsuario = idUsuario;	
		this.idProveedor = idProveedor;
		this.descProducto = descProducto;
		this.total = total;
		this.moneda = moneda;
		this.tipoCambio = tipoCambio;
		this.tipoTarjeta = tipoTarjeta;
		this.nombres = nombres;
		this.email = email;
	}

	public int getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(int idDetalle) {
		this.idDetalle = idDetalle;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescProducto() {
		return descProducto;
	}

	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
