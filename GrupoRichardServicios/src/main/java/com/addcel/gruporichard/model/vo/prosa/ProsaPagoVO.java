package com.addcel.gruporichard.model.vo.prosa;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ProsaPagoVO {
	private int error;
	private String msg;
	private boolean isAuthorized;
	private boolean isRejected;
	private boolean isProsaError;
	private String transaccion;
	private String transactionId;
	private String bancoAdquirente;

	private String nombreComercio;
	private String direccionComercio;
	private String afiliacion;
	private String importe;
	private String moneda;
	private String fechaHora;
	
	private String claveOperacion;
	private String claveRechazo;
	private String numeroCaja;
	private String producto;
	private String bancoEmisor;
	private String marca;
	private String autorizacion;
	private String poblacion;
	
	private String descripcionRechazo;
	
	public int getError() {
		return error;
	}
	public void setError(int error) {
		this.error = error;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean isAuthorized() {
		return isAuthorized;
	}
	public void setIsAuthorized(boolean isAuthorized) {
		this.isAuthorized = isAuthorized;
	}
	public boolean isRejected() {
		return isRejected;
	}
	public void setIsRejected(boolean isRejected) {
		this.isRejected = isRejected;
	}
	public boolean isProsaError() {
		return isProsaError;
	}
	public void setIsProsaError(boolean isProsaError) {
		this.isProsaError = isProsaError;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getBancoAdquirente() {
		return bancoAdquirente;
	}
	public void setBancoAdquirente(String bancoAdquirente) {
		this.bancoAdquirente = bancoAdquirente;
	}
	public String getNombreComercio() {
		return nombreComercio;
	}
	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}
	public String getDireccionComercio() {
		return direccionComercio;
	}
	public void setDireccionComercio(String direccionComercio) {
		this.direccionComercio = direccionComercio;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	public String getClaveOperacion() {
		return claveOperacion;
	}
	public void setClaveOperacion(String claveOperacion) {
		this.claveOperacion = claveOperacion;
	}
	public String getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(String numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getBancoEmisor() {
		return bancoEmisor;
	}
	public void setBancoEmisor(String bancoEmisor) {
		this.bancoEmisor = bancoEmisor;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
	public String getDescripcionRechazo() {
		return descripcionRechazo;
	}
	public void setDescripcionRechazo(String descripcionRechazo) {
		this.descripcionRechazo = descripcionRechazo;
	}
	public String getClaveRechazo() {
		return claveRechazo;
	}
	public void setClaveRechazo(String claveRechazo) {
		this.claveRechazo = claveRechazo;
	}
}
