package com.addcel.gruporichard.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.utils.ConstantesGpoRichard;
import com.addcel.gruporichard.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class GrupoRichardDiestelService {
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardDiestelService.class);

	@Autowired
	private UtilsService utilService;	
	
	public String obtenCategorias (String json){		
		AbstractVO resp = null;
		String respServicio = null;
		try {
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_CATE_DIESTEL, "json=" + json);
		}catch(Exception e){
			resp = new AbstractVO(50, "Error al llamado al Diestel: " + e.getMessage());
			logger.error("Ocurrio un error general Diestel obtenCategorias: {}", e);
		}finally{
			if(resp != null)
				respServicio = AddcelCrypto.encryptHard(utilService.objectToJson(resp));
		}
		return respServicio;
	}
	
	public String montosRecarga (String json){		
		AbstractVO resp = null;
		String respServicio = null;
		try {
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_MONTO_DIESTEL, "json=" + json);
		}catch(Exception e){
			resp = new AbstractVO(50, "Error al llamado al Diestel: " + e.getMessage());
			logger.error("Ocurrio un error general Diestel montosRecarga: {}", e);
		}finally{
			if(resp != null)
				respServicio = AddcelCrypto.encryptHard(utilService.objectToJson(resp));
		}
		return respServicio;
	}	

	public String obtenCatalogoServicios (String json){		
		AbstractVO resp = null;
		String respServicio = null;
		try {
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_CAT_SER_DIESTEL, "json=" + json);
		}catch(Exception e){
			resp = new AbstractVO(50, "Error al llamado al Diestel: " + e.getMessage());
			logger.error("Ocurrio un error general Diestel obtenCatalogoServicios: {}", e);
		}finally{
			if(resp != null)
				respServicio = AddcelCrypto.encryptHard(utilService.objectToJson(resp));
		}
		return respServicio;
	}
	
	public String obtenInfoPorServicio (String json){		
		AbstractVO resp = null;
		String respServicio = null;
		try {
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_INFO_SER_DIESTEL, "json=" + json);
		}catch(Exception e){
			resp = new AbstractVO(50, "Error al llamado al Diestel: " + e.getMessage());
			logger.error("Ocurrio un error general Diestel obtenInfoPorServicio: {}", e);
		}finally{
			if(resp != null)
				respServicio = AddcelCrypto.encryptHard(utilService.objectToJson(resp));
		}
		return respServicio;
	}
	
}
