package com.addcel.gruporichard.services;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.gruporichard.utils.ConstantesGpoRichard;
import com.addcel.gruporichard.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class GrupoRichardETNService {
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardETNService.class);

	@Autowired
	private UtilsService utilService;	
	
	@SuppressWarnings("unchecked")
	public String obtenerOrigenes(){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_ORIG_ETN, null);
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN obtenerOrigenes.", null, null);
			logger.error("Ocurrio un error general ETN obtenerOrigenes: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(utilService.objectToJson(resp));
		}
		return respServicio;
	}	

	@SuppressWarnings("unchecked")
	public String obtenerDestinos (String json){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			resp = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_DEST_ETN, "origen=" + resp.get("origen"));
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN obtenerDestinos.", null, null);
			logger.error("Ocurrio un error general ETN obtenerDestinos: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(respServicio);
		}
		return respServicio;
	}
	
	@SuppressWarnings("unchecked")
	public String obtenerCorridas (String json){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			resp = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_CORR_ETN, 
					"origen=" + resp.get("origen") +
					"&destino=" + resp.get("destino") +
					"&fechaIni=" + resp.get("fechaIni") +
					"&numAdulto=" + resp.get("numAdulto") +
					"&numNino=" + resp.get("numNino") +
					"&numInsen=" + resp.get("numInsen") +
					"&numEst=" + resp.get("numEst") +
					"&numMto=" + resp.get("numMto") +
					"&numPaisano=" + resp.get("numPaisano")
					);
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN obtenerCorridas.", null, null);
			logger.error("Ocurrio un error general ETN obtenerCorridas: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(respServicio);
		}
		return respServicio;
	}
	
	@SuppressWarnings("unchecked")
	public String consultarItinerario (String json){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			resp = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_ITINE_ETN, 
					"corrida=" + resp.get("corrida") +
					"&origen=" + resp.get("origen") +
					"&fecha=" + resp.get("fecha")
					);
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN consultarItinerario.", null, null);
			logger.error("Ocurrio un error general ETN consultarItinerario: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(respServicio);
		}
		return respServicio;
	}
	
	@SuppressWarnings("unchecked")
	public String consultarOcupacion (String json){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			resp = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_OCUPA_ETN, 
					"corrida=" + resp.get("corrida") +
					"&origen=" + resp.get("origen") +
					"&destino=" + resp.get("destino")+
					"&fecha=" + resp.get("fecha")
					);
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN consultarOcupacion.", null, null);
			logger.error("Ocurrio un error general ETN consultarOcupacion: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(respServicio);
		}
		return respServicio;
	}
	
	@SuppressWarnings("unchecked")
	public String consultarDiagrama (String json){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			resp = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_DIAG_ETN, 
					"corrida=" + resp.get("corrida") +
					"&fecha=" + resp.get("fecha")
					);
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN consultarDiagrama.", null, null);
			logger.error("Ocurrio un error general ETN consultarDiagrama: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(respServicio);
		}
		return respServicio;
	}
	
	@SuppressWarnings("unchecked")
	public String consultaBoletoEtn (String json){		
		HashMap<String, Object> resp = null;
		String respServicio = null;
		try {
			resp = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_BOLT_ETN, 
					"modulo=" + resp.get("modulo") +
					"&idBusqueda=" + resp.get("idBusqueda")
					);
			resp = (HashMap<String, Object>) utilService.jsonToObject(respServicio, HashMap.class);
			resp = utilService.getRespuestaBusquedas(resp, 0, "Exito.");
			
		}catch(Exception e){
			resp = utilService.getRespuestaBusquedas(50, "Ocurrio un error general ETN consultaBoletoEtn.", null, null);
			logger.error("Ocurrio un error general ETN consultaBoletoEtn: {}", e);
		}finally{
			respServicio = AddcelCrypto.encryptHard(respServicio);
		}
		return respServicio;
	}
	

}
