package com.addcel.gruporichard.services;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.gruporichard.model.mapper.BitacorasMapper;
import com.addcel.gruporichard.model.mapper.UsuarioConfigMapper;
import com.addcel.gruporichard.model.mapper.UtilsMapper;
import com.addcel.gruporichard.model.vo.BusquedaPagosRequest;
import com.addcel.gruporichard.model.vo.BusquedaPagosResponse;
import com.addcel.gruporichard.model.vo.DatosPago;
import com.addcel.gruporichard.model.vo.DatosPagoResponse;
import com.addcel.gruporichard.model.vo.UsuarioVO;
import com.addcel.gruporichard.model.vo.abcCapital.ConsultaSaldoResponse;
import com.addcel.gruporichard.model.vo.bridge.CompraResponseVO;
import com.addcel.gruporichard.model.vo.bridge.CompraVO;
import com.addcel.gruporichard.model.vo.common.TiendaVO;
import com.addcel.gruporichard.model.vo.common.TokenVO;
import com.addcel.gruporichard.model.vo.diestel.CamposMovilesVO;
import com.addcel.gruporichard.model.vo.diestel.RequestPagoMovilesVO;
import com.addcel.gruporichard.model.vo.etn.BusquedaPagosETNResponse;
import com.addcel.gruporichard.model.vo.etn.DatosViajeETN;
import com.addcel.gruporichard.model.vo.interjet.PNRMobile;
import com.addcel.gruporichard.model.vo.interjet.PNRResponse;
import com.addcel.gruporichard.model.vo.megacable.RespuestaServicio;
import com.addcel.gruporichard.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.gruporichard.model.vo.pagos.TBitacoraVO;
import com.addcel.gruporichard.model.vo.prosa.ProsaPagoVO;
import com.addcel.gruporichard.utils.AddCelGenericMail;
import com.addcel.gruporichard.utils.ConstantesGpoRichard;
import com.addcel.gruporichard.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class GrupoRichardPagosService {
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardPagosService.class);

	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private UtilsMapper utilsMapper;
	
	@Autowired
	private BitacorasMapper bitacorasMapper;
	
	@Autowired
	private UsuarioConfigMapper usuarioMapper;

	
	private static final String patron = "ddHHmmssSSS";
	private static final String patronCom = "yyyy-MM-dd HH:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	private static final String KEY_TOKEN = "4TQft8qmN8qB5FENosr4WXtPHoBq9stC";
	private static final String USER_TOKEN = "userGRupoRichard";
	private static final String PASS_TOKEN = "uL8RmL2TNWOT6Lxi";
	
//	private static HashMap<String,String> AFILIACION = null;
	
	public String generaToken(String json){	
		TokenVO tokenVO = null;
		try{
			tokenVO = (TokenVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), TokenVO.class);
			if(!USER_TOKEN.equals(tokenVO.getUsuario()) || !PASS_TOKEN.equals(tokenVO.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este webservice.\"}";
			}else{
				json="{\"token\":\""+AddcelCrypto.encrypt(KEY_TOKEN, bitacorasMapper.getFechaActual())+"\",\"idError\":0,\"mensajeError\":\"\"}";
			}
		}catch(Exception e){
			json="{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
		}
		json = AddcelCrypto.encryptHard(json);
				
		return json;
	}
	
	@SuppressWarnings("unchecked")
	private void generaTokenExterno(DatosPago datosPago, String tipo){
		TokenVO tokenVO = null;
		String url = null;
		String respServicio = null;
		HashMap<String, String> respToken = new HashMap<String, String>();
		try{
			respToken.put("token", "");
			
			if("MEGA".equals(tipo)){
				tokenVO = new TokenVO(ConstantesGpoRichard.USER_TOKEN_MEGA, ConstantesGpoRichard.PASS_TOKEN_MEGA);
				url = ConstantesGpoRichard.URL_TOKEN_MEGA;
			}
			
			respServicio = utilService.peticionHttpsUrlParams(url, ConstantesGpoRichard.PARAMETER_NAME_JSON + AddcelCrypto.encryptHard(utilService.objectToJson(tokenVO)));
			
			respToken = (HashMap<String, String>) utilService.jsonToObject(AddcelCrypto.decryptHard(respServicio), HashMap.class);
			
			datosPago.setToken(respToken.get("token"));
		}catch(Exception e){
			logger.error("Ocurrio un error general durante la obtencion de token externo: {}", e.getMessage());
		}
	}

	
	public String consultaSaldoMegacable(String json){		
		DatosPagoResponse resp = null;
		String respServicio = null;
		try {
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_SALDO_MEGA, ConstantesGpoRichard.PARAMETER_NAME_JSON + json);
			json = AddcelCrypto.decryptHard(respServicio);
			
			if(json != null){
				json = json.replaceAll("msgError", "mensajeError");
			}
			
		}catch(Exception e){
			resp = new DatosPagoResponse(50, "Ocurrio un error general durante la consulta de Saldo a Megacable.");
			logger.error("Ocurrio un error general durante la consulta de Saldo a Megacable: {}", e);
		}finally{
			if ( resp != null){
				json = utilService.objectToJson(resp);
			}
			
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}	

	@SuppressWarnings("unchecked")
	public String consultaMontoInterjet(String json){		
		PNRResponse resp = null;
		String respServicio = null;
		PNRMobile pnrResp = null;
		HashMap<String, String> param = null;
		try {
			param = (HashMap<String, String>)utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);
			respServicio = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_MONTO_INJET, "pnr=" + param.get("pnr"));
			pnrResp = (PNRMobile) utilService.jsonToObject(respServicio, PNRMobile.class);
			
			resp = new PNRResponse();
			resp.setDatos(pnrResp);
			
		}catch(Exception e){
			resp = new PNRResponse(50, "Ocurrio un error general durante la consulta de Monto Interjet.");
			logger.error("Ocurrio un error general durante la consulta de Monto Interjet: {}", e);
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}	

	public String procesaPagoProductos(String json){		
		DatosPagoResponse resp = null;
		DatosPago datosPago = null;
		List<UsuarioVO> usuarioOriginal = null;
		int idBitcora = 0;
		try {
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), DatosPago.class);
			usuarioOriginal = usuarioMapper.validarUsuario(datosPago.getIdTienda(),  datosPago.getIdUsuario(), null, null);
			datosPago.setTipo("VISA");
			datosPago.setPassword(AddcelCrypto.encryptPassword(datosPago.getPassword()));
			resp = validarDataPagos(datosPago, usuarioOriginal, "PROD");
			
			if(resp == null ){
				datosPago.setAfiliacion(bitacorasMapper.getAfiliacion(usuarioOriginal.get(0).getIdTienda()));
				if(datosPago.getAfiliacion() == null || "".equals(datosPago.getAfiliacion())){
					resp = new DatosPagoResponse(41, "La tienda no tiene una afiliacion configurada.");
				}
			}
			if(resp == null ){
				logger.debug("Inicia proceso de compra.");
				datosPago.setTipoPago(1);
				idBitcora = insertaBitacoras(datosPago, true);
				if(idBitcora > 0){
					if(datosPago.getIdTipoTarjeta() != 0){
						resp = enviarPagoVisa(datosPago);
					}else{
						resp = new DatosPagoResponse();

						resp.setIdBitacora(datosPago.getIdPago());
						resp.setNumAutorizacion("");
						resp.setCodigoRechazo(0);
						resp.setDescRechazo("");
						resp.setFecha(formatoCom.format(new Date()));
						resp.setDescRechazo("");
						resp.setStatus(1);
						resp.setMensajeError("Se realizo el pago exitosamente..");
						resp.setCargo(datosPago.getTotal());
						
						AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailPagoProductos(datosPago, resp)));
						updateBitacoras(resp, datosPago.getIdPago(), null, false, null);
					}
					
				}else{
					resp = new DatosPagoResponse(40, "Ocurrio un error al guardar en bitacoras.");
				}
				
			}
		}catch(Exception e){
			resp = new DatosPagoResponse(45, "Ocurrio un error general durante el pago.");
			logger.error("Ocurrio un error general durante el pago: {}", e);
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		}
		return json;
	}	
	
	public String procesaPagos(String json, String tipo){		
		DatosPagoResponse resp = null;
		DatosPago datosPago = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendaOriginal = null; 
		String urlPago = null;
		int idBitcora = 0;
		
		try {
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), DatosPago.class);
			usuarioOriginal = usuarioMapper.validarUsuario(datosPago.getIdTienda(),  datosPago.getIdUsuario(), null, null);
			
			datosPago.setPassword(AddcelCrypto.encryptPassword(datosPago.getPassword()));
			resp = validarDataPagos(datosPago, usuarioOriginal, tipo);
			
			if(resp == null ){
				logger.debug("Inicia proceso de compra " + tipo);
				
				if(datosPago.getIdTipoTarjeta() == 4){
					tiendaOriginal = usuarioMapper.buscarTiendaKIT(usuarioOriginal.get(0).getImei());
					
					if(tiendaOriginal == null || (tiendaOriginal != null && tiendaOriginal.size() == 0)){
						resp = new DatosPagoResponse(11, "No existe la tienda.");
					}else{
						
						//********************************************************
						//******** Validar monedero CARNET limite minimo
						//********************************************************
						
						datosPago.setTarjeta(AddcelCrypto.decryptTarjeta(tiendaOriginal.get(0).getCarnet()));
						datosPago.setVigencia(AddcelCrypto.decryptTarjeta(tiendaOriginal.get(0).getVigencia()));
						
						/*ConsultaSaldoResponse respMonedero = consultaMonederoABCCap(null, datosPago);
						if(respMonedero.getSaldoDispCta() <= datosPago.getTotal()){
							resp = new DatosPagoResponse(41, "Saldo disponible en el CARNET insuficiente.");
						}*/
						
					}
				}
				if(resp == null){
					if("TAE".equals(tipo)){
						urlPago = ConstantesGpoRichard.URL_PAGO_TAE;
						datosPago.setTipoPago(2);
					}else if("IAVE".equals(tipo)){
						urlPago = ConstantesGpoRichard.URL_PAGO_IAVE;
						datosPago.setTipoPago(3);
					}else if("PASE".equals(tipo)){
						urlPago = ConstantesGpoRichard.URL_PAGO_PASE;
						datosPago.setTipoPago(4);
					}else if("MEGA".equals(tipo)){
						urlPago = ConstantesGpoRichard.URL_PAGO_MEGA;
						datosPago.setTipoPago(5);
						generaTokenExterno(datosPago, tipo);
					}
					
					idBitcora = insertaBitacoras(datosPago, false);
					
					if(idBitcora > 0){
						resp = enviaPagoExterno(datosPago, urlPago, tipo);
						
						updateBitacoras(resp, datosPago.getIdPago(), null, false, null);
						
						if(datosPago.getIdTipoTarjeta() == 4 && resp.getIdError() == 0){
							bitacorasMapper.actualizaMonendero(datosPago.getImei(), datosPago.getTotal());
						}
						
					}else{
						resp = new DatosPagoResponse(40, "Ocurrio un error al guardar en bitacoras.");
					}
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse(45, "Ocurrio un error general durante el pago.");
			logger.error("Ocurrio un error general durante el pago: {}", e);
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		}
		return json;
	}
	
	public ModelAndView procesaPagosInterjet(String json, HttpServletRequest request, HttpServletResponse response ){		
		DatosPagoResponse resp = null;
		DatosPago datosPago = null;
		ModelAndView mav = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendaOriginal = null; 
		int idBitcora = 0;
		String tipo = "INTE";
		
		try {
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), DatosPago.class);
			usuarioOriginal = usuarioMapper.validarUsuario(datosPago.getIdTienda(),  datosPago.getIdUsuario(), null, null);
			
			datosPago.setPassword(AddcelCrypto.encryptPassword(datosPago.getPassword()));
			resp = validarDataPagos(datosPago, usuarioOriginal, tipo);
			
			if(resp == null ){
				logger.debug("Inicia proceso de compra " + tipo);
				datosPago.setTipoPago(6);
				datosPago.setIdTipoTarjeta(5);
				
				tiendaOriginal = usuarioMapper.buscarTiendaKIT(usuarioOriginal.get(0).getImei());
				if(tiendaOriginal == null || (tiendaOriginal != null && tiendaOriginal.size() == 0)){
					resp = new DatosPagoResponse(11, "No existe la tienda.");
				}else{
					datosPago.setTarjeta(tiendaOriginal.get(0).getCarnet());
				}
				
				idBitcora = insertaBitacoras(datosPago, false);
				utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_TRAN_INJET, 
						"user=" + datosPago.getIdUsuario() +
						"&pnr=" + datosPago.getPnr() +
						"&monto=" + datosPago.getTotal()
						);
				
				if(idBitcora > 0){
					response.setContentType("text/html");
					//response.getWriter().print("Escribiendo en el Html");
//					request.getRequestDispatcher(ConstantesGrupoRichard.URL_PAGO_INJET + 
//							"idPago=" + datosPago.getIdPago() + "&tarjeta" + datosPago.getTarjeta() + "&email=" + datosPago.getMail() + 
//							"&referencia=" + datosPago.getPnr() + "&monto=" + datosPago.getTotal()).forward(request, response);
					response.sendRedirect(ConstantesGpoRichard.URL_PAGO_INJET + 
						"idPago=" + datosPago.getIdPago() + "&tarjeta=" + datosPago.getTarjeta() + "&email=" + datosPago.getMail() + 
						"&referencia=" + datosPago.getPnr() + "&monto=" + datosPago.getTotal());
				}else{
					resp = new DatosPagoResponse(40, "Ocurrio un error al guardar en bitacoras.");
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse(45, "Ocurrio un error general durante el pago.");
			logger.error("Ocurrio un error general durante el pago: {}", e);
		}finally{
			if(resp != null){
				mav = new ModelAndView("interjet/errorValidacionPago");
				mav.addObject("resp", resp);
			}
		}
		return mav;
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView procesaPagosETN(String json, HttpServletRequest request, HttpServletResponse response ){		
		DatosPagoResponse resp = null;
		DatosPago datosPago = null;
		ModelAndView mav = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendaOriginal = null; 
		int idBitcora = 0;
		String tipo = "ETN";
		int idViaje = 0;
		HashMap<String, Object> respSer = null;
		
		try {
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), DatosPago.class);
			usuarioOriginal = usuarioMapper.validarUsuario(datosPago.getIdTienda(),  datosPago.getIdUsuario(), null, null);
			
			datosPago.setPassword(AddcelCrypto.encryptPassword(datosPago.getPassword()));
			resp = validarDataPagos(datosPago, usuarioOriginal, tipo);
			tiendaOriginal = usuarioMapper.buscarTiendaKIT(usuarioOriginal.get(0).getImei());
			if(tiendaOriginal == null || (tiendaOriginal != null && tiendaOriginal.size() == 0)){
				resp = new DatosPagoResponse(11, "No existe la tienda.");
			}else{
				datosPago.setTarjeta(tiendaOriginal.get(0).getCarnet());
			}
			
			if(resp == null ){
				logger.debug("Inicia proceso de compra " + tipo);
				datosPago.setTipoPago(7);
				datosPago.setIdTipoTarjeta(5);
				
				idBitcora = insertaBitacoras(datosPago, false);
				respSer = (HashMap<String, Object>) utilService.jsonToObject(utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_SEC_ETN,null), HashMap.class);
				
				if(idBitcora > 0 && respSer.containsKey("secuencia") && (Integer)respSer.get("secuencia") > 0){
					idViaje = (Integer) respSer.get("secuencia");
					logger.debug("SECUENCIA ETN: {}", respSer.get("secuencia"));
					
					for(DatosViajeETN viejETN: datosPago.getViajes()){
						respSer = (HashMap<String, Object>) utilService.jsonToObject(utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_RESE_ETN,
								"&idPago=" + datosPago.getIdPago() +
								"&idOrigen=" + viejETN.getIdOrigen() +
								"&idDestino=" + viejETN.getIdDestino() +
								"&origen=" + viejETN.getOrigen() +
								"&destino=" + viejETN.getDestino() +
								"&fecha=" + viejETN.getFecha() +
								"&hora=" + viejETN.getHora() +
								"&numAdulto=" + viejETN.getNumAdulto() +
								"&numNino=" +  viejETN.getNumNino() +
								"&numInsen=" + viejETN.getNumInsen() +
								"&numEstudiante=" + viejETN.getNumEstudiante() +
								"&numMaestro=" + viejETN.getNumMaestro() +
								"&corrida=" + viejETN.getCorrida() +
								"&tipoCorrida=" + viejETN.getTipoCorrida() +
								"&asientos=" + viejETN.getAsientos() +
								"&nombres=" + viejETN.getNombres() +
								"&idUsuario=" + datosPago.getIdUsuario() +
								"&imei=" + datosPago.getImei() +
								"&tipoTelefono=0" +
								"&software=" + datosPago.getSoftware() +
								"&modelo=" + datosPago.getModelo() +
								"&tipoProducto=ETN" +
								"&tipoViaje=" + viejETN.getTipoViaje() +
								"&wkey=" + datosPago.getImei() +
								"&tarifaAdulto=" + viejETN.getTarifaAdulto() +
								"&tarifaNino=" + viejETN.getTarifaNino() +
								"&tarifaInsen=" + viejETN.getTarifaInsen() +
								"&tarifaEstudiante=" + viejETN.getTarifaEstudiante() +
								"&tarifaMaestro=" + viejETN.getTarifaMaestro() +
								"&idViaje=" + idViaje
								), HashMap.class);
						if(respSer.containsKey("numError") && (Integer)respSer.get("numError") != 0){
							continue;
						}
					}
					
					if(respSer.containsKey("numError") && (Integer)respSer.get("numError") == 0){
						bitacorasMapper.updateGrupoRichardDetalle(datosPago.getIdPago(), idViaje, null, null, 0, 0, 0, null);
						response.setContentType("text/html");
						//response.getWriter().print("Escribiendo en el Html");
//						request.getRequestDispatcher(ConstantesGrupoRichard.URL_PAGO_INJET + 
//								"idPago=" + datosPago.getIdPago() + "&tarjeta" + datosPago.getTarjeta() + "&email=" + datosPago.getMail() + 
//								"&referencia=" + datosPago.getPnr() + "&monto=" + datosPago.getTotal()).forward(request, response);
						response.sendRedirect(ConstantesGpoRichard.URL_PAGO_ETN + 
							"?idPago=" + datosPago.getIdPago() + "&tarjeta=" + datosPago.getTarjeta() + "&email=" + datosPago.getMail() + 
							"&referencia=" + idViaje + "&monto=" + datosPago.getTotal());
					}else{
						resp = new DatosPagoResponse((Integer)respSer.get("numError"), (String)respSer.get("descError"));
					}

				}else{
					resp = new DatosPagoResponse(40, "Ocurrio un error al guardar en bitacoras.");
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse(45, "Ocurrio un error general durante el pago.");
			logger.error("Ocurrio un error general durante el pago: {}", e);
		}finally{
			if(resp != null){
				mav = new ModelAndView("interjet/errorValidacionPago");
				mav.addObject("resp", resp);
			}
		}
		return mav;
	}
	
	public String procesaPagosRecargasDiestel(String json, HttpServletRequest request, HttpServletResponse response ){		
		DatosPagoResponse resp = null;
		DatosPago datosPago = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendaOriginal = null; 
		int idBitcora = 0;
		String tipo = "RECDIES";
		HashMap<String, Object> respSer = null;
		List<Map<String, Object>> lista = null;
		
		try {
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), DatosPago.class);
			usuarioOriginal = usuarioMapper.validarUsuario(datosPago.getIdTienda(),  datosPago.getIdUsuario(), null, null);
			
			datosPago.setPassword(AddcelCrypto.encryptPassword(datosPago.getPassword()));
			resp = validarDataPagos(datosPago, usuarioOriginal, tipo);
			
			if(resp == null ){
				logger.debug("Inicia proceso de compra " + tipo);
				datosPago.setTipoPago(2);
				
				if(datosPago.getIdTipoTarjeta() == 4){
					tiendaOriginal = usuarioMapper.buscarTiendaKIT(usuarioOriginal.get(0).getImei());
					
					if(tiendaOriginal == null || (tiendaOriginal != null && tiendaOriginal.size() == 0)){
						resp = new DatosPagoResponse(11, "No existe la tienda.");
					}else{
						datosPago.setTarjeta(AddcelCrypto.decryptTarjeta(tiendaOriginal.get(0).getCarnet()));
						datosPago.setVigencia(AddcelCrypto.decryptTarjeta(tiendaOriginal.get(0).getVigencia()));
					}
				}
				
				datosPago.setTelefono(datosPago.getReferencia());
				idBitcora = insertaBitacoras(datosPago, false);

				if(datosPago.getIdTipoTarjeta() == 4){
					datosPago.setIdTipoTarjeta(1);
				}
				
				if(idBitcora > 0){
					json = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_PAGO_REC_DIESTEL,
							ConstantesGpoRichard.PARAMETER_NAME_JSON + 
							AddcelCrypto.encryptSensitive("1234567890", utilService.objectToJson(new RequestPagoMovilesVO(datosPago))));
					
					json = AddcelCrypto.decryptHard(json);
					
					respSer = (HashMap<String, Object>) utilService.jsonToObject(json, HashMap.class);
					
					if(respSer.containsKey("idError") && ((Integer) respSer.get("idError")) == 0){
						resp = new DatosPagoResponse();
						resp.setStatus(1);
						
						lista = (List) respSer.get("datos");
						if(lista != null){
							for(Map data: lista){
								if(data.get("etiqueta").equals("REFERENCIA")){
	//								datosPago.setDescProducto("REFERENCIA: " + data.get("valor"));
									tipo = data.get("valor") + "";
								}else if(data.get("etiqueta").equals("AUTORIZACION")){
									resp.setFolio(data.get("valor") +"");
								}else if(data.get("etiqueta").equals("AUTORIZACION BANCARIA :")){
									resp.setNumAutorizacion((String)data.get("valor"));
								}else if(data.get("etiqueta").equals("COMISION :")){
									resp.setComision(Double.parseDouble((String)data.get("valor")));
								}else if(data.get("etiqueta").equals("MONTO")){
									resp.setCargo(Double.parseDouble((String)data.get("valor")));
	//							}else if(data.get("etiqueta").equals("TOTAL :")){
								}else if(data.get("etiqueta").equals("ID TRANSACCION :")){
									resp.setIdBitacora(Integer.parseInt((String)data.get("valor")));
								}
							}
						}else{
							resp.setStatus(0);
						}
						updateBitacoras(resp, datosPago.getIdPago(), null, false, tipo);
					}else{
						resp = new DatosPagoResponse();
						resp.setStatus(0);
						resp.setIdError((Integer) respSer.get("idError"));
						resp.setMensajeError((String) respSer.get("msgError"));
						updateBitacoras(resp, datosPago.getIdPago(), null, false, null);
					}
				}else{
					resp = new DatosPagoResponse(40, "Ocurrio un error al guardar en bitacoras.");
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse(45, "Ocurrio un error general durante el pago.");
			logger.error("Ocurrio un error general durante el pago: {}", e);
		}finally{
			if(respSer != null){
				json = utilService.objectToJson(respSer);
			}else{
				json = utilService.objectToJson(resp);
				json = json.replaceAll("mensajeError", "msgError");
			}
			
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		}
		return json;
	}
	
	public String procesaPagosServicioDiestel(String json, HttpServletRequest request, HttpServletResponse response ){		
		DatosPagoResponse resp = null;
		DatosPago datosPago = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendaOriginal = null; 
		int idBitcora = 0;
		String tipo = "SERDIES";
		HashMap<String, Object> respSer = null;
		List<Map<String, Object>> lista = null;
		
		try {
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), DatosPago.class);
			usuarioOriginal = usuarioMapper.validarUsuario(datosPago.getIdTienda(),  datosPago.getIdUsuario(), null, null);
			
			datosPago.setPassword(AddcelCrypto.encryptPassword(datosPago.getPassword()));
			resp = validarDataPagos(datosPago, usuarioOriginal, tipo);
			
			if(resp == null ){
				logger.debug("Inicia proceso de compra " + tipo);
				datosPago.setTipoPago(8);
				
				if(datosPago.getIdTipoTarjeta() == 4){
					tiendaOriginal = usuarioMapper.buscarTiendaKIT(usuarioOriginal.get(0).getImei());
					
					if(tiendaOriginal == null || (tiendaOriginal != null && tiendaOriginal.size() == 0)){
						resp = new DatosPagoResponse(11, "No existe la tienda.");
					}else{
						datosPago.setTarjeta(AddcelCrypto.decryptTarjeta(tiendaOriginal.get(0).getCarnet()));
						datosPago.setVigencia(AddcelCrypto.decryptTarjeta(tiendaOriginal.get(0).getVigencia()));
					}
				}
				
				for(CamposMovilesVO tmp: datosPago.getCampos()){
					if(tmp.getNombre().toUpperCase().indexOf("REFERENCIA")>=0){
						datosPago.setReferenciaMegacable(tmp.getValor());
					}else if(tmp.getNombre().toUpperCase().indexOf("MONTO")>=0){
						datosPago.setTotal(Double.parseDouble(tmp.getValor()!= null? tmp.getValor(): "0"));
					}
				}
				
//				datosPago.setDescProducto("Referencia: " + datosPago.getReferencia() + ", ID Servicio: " + datosPago.getIdServicio() );
				idBitcora = insertaBitacoras(datosPago, false);

				if(datosPago.getIdTipoTarjeta() == 4){
					datosPago.setIdTipoTarjeta(1);
				}
				
				if(idBitcora > 0){
					json = utilService.peticionHttpsUrlParams(ConstantesGpoRichard.URL_PAGO_SERV_DIESTEL,
							ConstantesGpoRichard.PARAMETER_NAME_JSON + 
							AddcelCrypto.encryptSensitive("1234567890", utilService.objectToJson(new RequestPagoMovilesVO(datosPago))));
					
					json = AddcelCrypto.decryptHard(json);
					
					respSer = (HashMap<String, Object>) utilService.jsonToObject(json, HashMap.class);
					
					if(respSer.containsKey("idError") && ((Integer) respSer.get("idError")) == 0){
						resp = new DatosPagoResponse();
						resp.setStatus(1);
						
						lista = (List) respSer.get("datos");
						for(Map data: lista){
							if(data.get("etiqueta").equals("REFERENCIA")){
//								datosPago.setDescProducto("REFERENCIA: " + data.get("valor"));
								tipo = data.get("valor") + "";
							}else if(data.get("etiqueta").equals("AUTORIZACION")){
								resp.setFolio(data.get("valor") +"");
							}else if(data.get("etiqueta").equals("AUTORIZACION BANCARIA :")){
								resp.setNumAutorizacion((String)data.get("valor"));
							}else if(data.get("etiqueta").equals("COMISION :")){
								resp.setComision(Double.parseDouble((String)data.get("valor")));
							}else if(data.get("etiqueta").equals("MONTO")){
								resp.setCargo(Double.parseDouble((String)data.get("valor")));
//							}else if(data.get("etiqueta").equals("TOTAL :")){
							}else if(data.get("etiqueta").equals("ID TRANSACCION :")){
								resp.setIdBitacora(Integer.parseInt((String)data.get("valor")));
							}
						}
						updateBitacoras(resp, datosPago.getIdPago(), null, false, tipo);
					}else{
						resp = new DatosPagoResponse();
						resp.setStatus(0);
						resp.setIdError((Integer) respSer.get("idError"));
						resp.setMensajeError((String) respSer.get("msgError"));
						updateBitacoras(resp, datosPago.getIdPago(), null, false, null);
					}
				}else{
					resp = new DatosPagoResponse(40, "Ocurrio un error al guardar en bitacoras.");
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse(45, "Ocurrio un error general durante el pago.");
			logger.error("Ocurrio un error general durante el pago: {}", e);
		}finally{
			if(respSer != null){
				json = utilService.objectToJson(respSer);
			}else{
				json = utilService.objectToJson(resp);
				json = json.replaceAll("mensajeError", "msgError");
			}
			
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		}
		return json;
	}
	
	
	private DatosPagoResponse enviaPagoExterno(DatosPago datosPago, String urlPago, String tipo){
		DatosPagoResponse resp = null;
		String respServicio = null;
		String key = null;
		String json = null;
		int tipoTaj = 0;
		
		tipoTaj = datosPago.getIdTipoTarjeta();
		
		if(datosPago.getIdTipoTarjeta() == 4){
			datosPago.setIdTipoTarjeta(1);
			datosPago.setTarjeta(AddcelCrypto.encryptTarjeta(datosPago.getTarjeta()));
			datosPago.setVigencia(AddcelCrypto.encryptTarjeta(datosPago.getVigencia()));
		}
		
		if("MEGA".equals(tipo)){
			
			json = utilService.objectToJson(datosPago);
			json = json.replaceAll("\"idTipoTarjeta\"", "\"id_tipo_tarjeta\"");
			json = json.replaceAll("\"total\"", "\"monto\"");
			json = json.replaceAll("\"nombres\"", "\"nombre\"");
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
			
			respServicio = utilService.peticionHttpsUrlParams(urlPago, ConstantesGpoRichard.PARAMETER_NAME_JSON + json);
			respServicio =  AddcelCrypto.decryptHard(respServicio);
			RespuestaServicio respuestaServicio = (RespuestaServicio) utilService.jsonToObject( respServicio, RespuestaServicio.class);
			
			resp = new DatosPagoResponse(respuestaServicio);
		}else{
			key = formato.format(new Date());
			json = AddcelCrypto.encryptSensitive(key, utilService.objectToJson(new CompraVO(datosPago)));
			
			respServicio = utilService.consumeServicio(urlPago, ConstantesGpoRichard.PARAMETER_NAME_JSON + json);
			CompraResponseVO compraResponse = (CompraResponseVO) utilService.jsonToObject( AddcelCrypto.decryptJson(key, respServicio), CompraResponseVO.class);
			
			resp = new DatosPagoResponse(compraResponse);
		}
		
		 datosPago.setIdTipoTarjeta(tipoTaj);
		
		return resp;
		
	}

	private DatosPagoResponse enviarPagoVisa(DatosPago datosPago){
		DatosPagoResponse respuesta = null;
		ProsaPagoVO prosaPagoVO = null;
		String concepto = null;
		try{
			
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(datosPago.getTarjeta(), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(datosPago.getNombres(), "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(datosPago.getCvv2(), "UTF-8") )
					.append( "&monto="    ).append( URLEncoder.encode(datosPago.getTotal() + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(datosPago.getAfiliacion(), "UTF-8") );
//						.append( "&moneda="   ).append( URLEncoder.encode("MXN", "UTF-8")) ;
					
			logger.info("Envío de datos Grupo Richard VISA: {}", data);
			
			String sb = new UtilsService().peticionHttpsUrlParams(ConstantesGpoRichard.URL_AUTH_PROSA, data.toString());
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				respuesta = new DatosPagoResponse();
				respuesta.setIdBitacora(datosPago.getIdBitacora());
				respuesta.setNumAutorizacion(prosaPagoVO.getAutorizacion());
				respuesta.setCodigoRechazo(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "0"));
				respuesta.setDescRechazo(prosaPagoVO.getDescripcionRechazo());
				respuesta.setFecha(formatoCom.format(new Date()));
//				respuesta.setTransaccion(String.valueOf(datosPago.getIdBitacora()));
				respuesta.setDescRechazo(prosaPagoVO.getDescripcionRechazo());
//				datosPago.setMsg(prosaPagoVO.getMsg());
				
				
				if(prosaPagoVO.isAuthorized()){
					concepto = "EXITO PAGO GRUPO RICHARD VISA AUTORIZADA";
					respuesta.setStatus(1);
					respuesta.setMensajeError("Se realizo el pago exitosamente..");
					
					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailPagoProductos(datosPago, respuesta)));
					
				}else if(prosaPagoVO.isRejected()){
					concepto = "ERROR PAGO GRUPO RICHARD VISA RECHAZADA";
					respuesta.setStatus(0);
					respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(prosaPagoVO.getDescripcionRechazo());
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"RECHAZADA")));
					
				}else if(prosaPagoVO.isProsaError()){
					concepto = "ERROR PAGO GRUPO RICHARD VISA ERROR";
					respuesta.setStatus(0);
					respuesta.setIdError(Integer.parseInt(
							prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(prosaPagoVO.getMsg());
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"ERROR")));
				}
				
				updateBitacoras(respuesta, datosPago.getIdPago(), concepto, true, null);
			}
		}catch(Exception e){
			respuesta = new DatosPagoResponse();
			logger.error("Ocurrio un error durante el llamado al banco: {}", e);
			respuesta.setStatus(100);
			respuesta.setMensajeError("Ocurrio un error durante el pago al banco");
			updateBitacoras(respuesta, datosPago.getIdPago(), "ERROR PAGO GRUPO RICHARD VISA GENERAL", true, null);
		}
		return respuesta;
	}
	
	public DatosPagoResponse validarDataPagos(DatosPago datosPago, List<UsuarioVO> usuarioOriginal, String tipo){		
		DatosPagoResponse resp = null;
		try {
			usuarioOriginal = usuarioMapper.validarUsuario(0,  datosPago.getIdUsuario(), null, null);

			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = new DatosPagoResponse(101, "No existe el usuario.");
			}else if(!usuarioOriginal.get(0).getPassword().equals(datosPago.getPassword())){
				resp = new DatosPagoResponse(102, "Password incorrecto.");
			}else if(usuarioOriginal.get(0).getStatus() != 1){
				resp = new DatosPagoResponse(111, "El usuario no está activo.");
			}else if(usuarioOriginal.get(0).getStatusTienda() != 1){
				resp = new DatosPagoResponse(112, "La tienda no está activa.");
			}else if(!usuarioOriginal.get(0).getImei().equals(datosPago.getImei() )){
				resp = new DatosPagoResponse(103, "El usuario \"" + usuarioOriginal.get(0).getLogin() +"\" no puede realizar una compra en este equipo.");
			}else if(datosPago.getToken() == null){
				resp = new DatosPagoResponse(50, "El parametro TOKEN no puede ser NULL.");
				logger.error("El parametro TOKEN no puede ser NULL");
			}else{
				datosPago.setToken(AddcelCrypto.decryptJson(KEY_TOKEN, datosPago.getToken()));
				logger.debug("token ==> {}", datosPago.getToken());
				try{
					if(datosPago.getToken() == null || (bitacorasMapper.diferenciaFechaSegundos(datosPago.getToken())) > 60){
//					if(false){
						resp = new DatosPagoResponse(51, "La Transacción no es válida.");
						logger.error("La Transacción ya no es válida");
					}else if(datosPago.getIdTipoTarjeta() != 4 && datosPago.getIdTipoTarjeta() != 0 && datosPago.getTarjeta() == null){
						resp = new DatosPagoResponse(52, "El número de tarjeta es obligatorio.");
					}else if(datosPago.getIdTipoTarjeta() != 4 && datosPago.getIdTipoTarjeta() != 0 && datosPago.getVigencia() == null){
						resp = new DatosPagoResponse(52, "La vigencia es obligatoria.");
					}else if(datosPago.getIdTipoTarjeta() != 4 && datosPago.getIdTipoTarjeta() != 0 && datosPago.getCvv2() == null){
						resp = new DatosPagoResponse(52, "El número CVV2 es obligatorio.");
						
					}else if("PROD".equals(tipo) && datosPago.getDescProducto() == null ){
						resp = new DatosPagoResponse(52, "La descripcion del producto es obligatoria.");
					}else if("TAE".equals(tipo) && datosPago.getTelefono() == null){
						resp = new DatosPagoResponse(50, "El telefono es obligatorio.");
					}else if(("IAVE".equals(tipo) || "PASE".equals(tipo)) && datosPago.getTarjetaTag() == null){
						resp = new DatosPagoResponse(50, "El número de TAG es obligatorio.");
					}else if("INTE".equals(tipo) && datosPago.getPnr() == null){
						resp = new DatosPagoResponse(53, "El número de Referencia es obligatorio.");
					}else{
						logger.debug("Validaciones correctas.");
					}
				}catch(Exception e){
					resp = new DatosPagoResponse(51, "La Transacción no es válida.");
					logger.error("La Transacción ya no es válida: {}", e.getMessage());
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse();
			
			resp.setMensajeError("Ocurrio un error: " + e.getMessage());
			resp.setStatus(2);
			resp.setMensajeError("Ocurrio un error");
			logger.error("Ocurrio un error al validar datos de pago: {}", e);
		}
		return resp;
	}	
	
	public String consultaMonedero(String json){	
		BusquedaPagosRequest busqueda = null;
		ConsultaSaldoResponse resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendas = null;
		try {
			busqueda = (BusquedaPagosRequest) utilService.jsonToObject(AddcelCrypto.decryptHard(json), BusquedaPagosRequest.class);
			usuarioOriginal = usuarioMapper.validarUsuario(0,  busqueda.getIdUsuario(), null, null);
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = new ConsultaSaldoResponse(101, "No existe el usuario.");
			}else if(usuarioOriginal.get(0).getStatus() != 1){
				resp = new ConsultaSaldoResponse(111, "El usuario no está activo.");
			}else if(usuarioOriginal.get(0).getStatusTienda() != 1){
				resp = new ConsultaSaldoResponse(112, "La tienda no está activa.");
			}else if(!usuarioOriginal.get(0).getImei().equals(busqueda.getImei() )){
				resp = new ConsultaSaldoResponse(103, "El usuario \"" + usuarioOriginal.get(0).getLogin() +"\" no puede realizar esta operacion en este equipo.");
			}else {
				tiendas = usuarioMapper.buscarTiendaKIT(busqueda.getImei());
				if(tiendas == null || (tiendas != null && tiendas.size() == 0)){
					resp = new ConsultaSaldoResponse(11, "No existe la tienda.");
				}else{
					
					//********************************************************
					//******** Validar monedero CARNET limite minimo
					//********************************************************
					
					busqueda.setTarjeta(AddcelCrypto.decryptTarjeta(tiendas.get(0).getCarnet()));
					
					resp = consultaMonederoABCCap(busqueda, null);
				}
			}
		}catch (Exception pe) {
			resp = new ConsultaSaldoResponse(2, "Error general al obtener detalle de pagos.");
			logger.error("Error general al obtener detalle de pagos: {}", pe);
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	
	
	private ConsultaSaldoResponse consultaMonederoABCCap(BusquedaPagosRequest busqueda, DatosPago datosPago){
		ConsultaSaldoResponse resp =  null;
		String respWS = null;
		
		List<String> respBusqueda = null;
		
		try{
			
			//***************************************
			// Consultar WS de ABC Capital
			//***************************************
			// Segenera respuesta dommy
			//***************************************
			
			//respWS = ConstantesGrupoRichard.CONSULTA_MONEDERO_EXITO;
			
			//resp = utilService.xmlToObject(respWS);
			resp = new ConsultaSaldoResponse();
			if(busqueda != null){
				respBusqueda = bitacorasMapper.getMonendero(busqueda.getImei());
				resp.setCarnet("XXXX XXXX XXXX " + busqueda.getTarjeta().substring(busqueda.getTarjeta().length() - 4));
			}else if( datosPago != null){
				respBusqueda = bitacorasMapper.getMonendero(datosPago.getImei());
				resp.setCarnet("XXXX XXXX XXXX " + datosPago.getTarjeta().substring(datosPago.getTarjeta().length() - 4));
			}
			
			if(respBusqueda != null && respBusqueda.size() > 1){
				resp.setSaldoCta(Double.parseDouble(respBusqueda.get(1)));
				resp.setSaldoDispCta(Double.parseDouble(respBusqueda.get(0)));
			}
			
		}catch(Exception e){
			resp = new ConsultaSaldoResponse(11, "Ocurrio un error al consultar el WS de ABC");
			logger.error("Error al consultar el WS de ABC Capital Monedero: {}", e);
		}
		
		return resp;
	}
	
	public String busquedaPagos(String json){	
		BusquedaPagosRequest busqueda = null;
		List<BusquedaPagosResponse> respBusqueda = null;
		HashMap<String, Object> resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<BusquedaPagosETNResponse> respETN = null;
		try {
			busqueda = (BusquedaPagosRequest) utilService.jsonToObject(AddcelCrypto.decryptHard(json), BusquedaPagosRequest.class);
			usuarioOriginal = usuarioMapper.validarUsuario(busqueda.getIdTienda(),  busqueda.getIdUsuario(), null, null);
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = utilService.getRespuestaBusquedas(101, "No existe el usuario.", null, null);
			}else if(usuarioOriginal.get(0).getStatus() != 1){
				resp = utilService.getRespuestaBusquedas(111, "El usuario no está activo.", null, null);
			}else if(usuarioOriginal.get(0).getStatusTienda() != 1){
				resp = utilService.getRespuestaBusquedas(112, "La tienda no está activa.", null, null);
			}else if(!usuarioOriginal.get(0).getImei().equals(busqueda.getImei() )){
				resp = utilService.getRespuestaBusquedas(103, "El usuario \"" + usuarioOriginal.get(0).getLogin() +"\" no puede realizar la busqueda en este equipo.", null, null);
			}else {
				if(busqueda.getTipoPago() != 5){
					respBusqueda = bitacorasMapper.selectGpoRichardDetalle(busqueda);
					if(respBusqueda != null && respBusqueda.size() > 0){
						resp = utilService.getRespuestaBusquedas(0, null, ConstantesGpoRichard.NOM_CAMPO_PAGOS, respBusqueda);
					}else{
						resp = utilService.getRespuestaBusquedas(71, "No hay resultados en la busqueda.", null, null);
					}
				}else if(busqueda.getTipoPago() == 5){
					respETN = bitacorasMapper.selectGpoRichardDetalleETN(busqueda);
					if(respETN != null && !respETN.isEmpty()){
						resp = utilService.getRespuestaBusquedas(0, null, ConstantesGpoRichard.NOM_CAMPO_PAGOS, respETN);
					}else{
						resp = utilService.getRespuestaBusquedas(71, "No hay resultados en la busqueda.", null, null);
					}
				}
				
			}
		}catch (Exception pe) {
			resp = utilService.getRespuestaBusquedas(2, "Error general al obtener detalle de pagos.", null, null);
			logger.error("Error general al obtener detalle de pagos: {}", pe);
		}finally{
			if(resp != null){
				json = utilService.objectToJson(resp);
			}else if(respETN != null){
				json = utilService.objectToJson(respETN);
			}
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	private int insertaBitacoras(DatosPago datosPago, boolean insertTBitacora){		
		TBitacoraVO tbitacora = null;
		String tarjeta = null;
		try{
			tbitacora = new TBitacoraVO(
					datosPago.getIdUsuario(),datosPago.getIdProveedor(), 
					String.valueOf(datosPago.getTotal()), "PAGO GRUPO RICHARD PRODUCTO", 
					datosPago.getImei(), "PAGO GRUPO RICHARD PRODUCTO",
					datosPago.getTarjeta(),datosPago.getModelo(), datosPago.getSoftware(), 
					datosPago.getModelo(),datosPago.getWkey());
			tarjeta = datosPago.getTarjeta();
			
			//No aplica cuando es pago en efectivo o pagos TAE, IAVE, PASE, el inserte en estos casos se hace en el AddcelBridge
			if(insertTBitacora && datosPago.getIdTipoTarjeta() != 0){
				
				tbitacora.setTarjetaCompra(AddcelCrypto.encryptTarjeta(tarjeta));
				logger.debug("Insertar en t_bitacora...");
				bitacorasMapper.agregarBitacora(tbitacora);
				
				logger.debug("idBitacora: {}" , tbitacora.getIdBitacora());
				
				TBitacoraProsaVO tbitacoraProsa = new TBitacoraProsaVO( tbitacora,"0.00","0.00");
				
				logger.debug("Insertar en t_bitacoraProsa...");
				bitacorasMapper.agregarBitacoraProsa(tbitacoraProsa);
				
				datosPago.setIdBitacora(tbitacora.getIdBitacora());
				
				datosPago.setTarjeta(tarjeta.substring(tarjeta.length() -4 , tarjeta.length()));
			}else{
				logger.debug("Pago diferente a Pago producto no Efectivo, solo se guarda en BitacoraDetalle...");
				datosPago.setTarjeta("ST");
			}
			
			logger.debug("Insertar en GPO_RICHARD_detalle...");
			bitacorasMapper.agregarGrupoRichardDetalle(datosPago);
			datosPago.setTarjeta(tarjeta);
		
		if(!insertTBitacora || datosPago.getIdTipoTarjeta() == 0){
			tbitacora.setIdBitacora(datosPago.getIdPago());
			datosPago.setIdBitacora(datosPago.getIdPago());
		}
		
		}catch(Exception e){
			tbitacora.setIdBitacora(0);
			logger.error("Error general al insertar en bitacoras: {}", e);
		}
		
		return tbitacora.getIdBitacora();
	}
	
	private void updateBitacoras(DatosPagoResponse datosPagoResp, int idPago, String concepto, boolean updateTBitacora, String telefono){
		TBitacoraVO tbitacora = new TBitacoraVO();
		if(updateTBitacora){
			tbitacora.setIdBitacora(datosPagoResp.getIdBitacora());
			tbitacora.setBitStatus(datosPagoResp.getStatus());
			tbitacora.setBitCodigoError(datosPagoResp.getCodigoRechazo());
			tbitacora.setBitConcepto(concepto);
			tbitacora.setBitNoAutorizacion(datosPagoResp.getNumAutorizacion());
			
			logger.debug("Actualizando t_bitacora...");
			bitacorasMapper.actualizarBitacora(tbitacora);
		}
		
		logger.debug("Actualizando GPO_RICHARD_detalle...");
		bitacorasMapper.updateGrupoRichardDetalle(idPago, datosPagoResp.getIdBitacora(), datosPagoResp.getFolio(), 
				datosPagoResp.getNumAutorizacion(), datosPagoResp.getStatus(), datosPagoResp.getCargo(), 
				datosPagoResp.getComision(), telefono);
	}
}
