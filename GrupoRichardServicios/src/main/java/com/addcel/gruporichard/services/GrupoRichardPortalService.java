package com.addcel.gruporichard.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.gruporichard.model.mapper.PortalConfigMapper;
import com.addcel.gruporichard.model.mapper.UsuarioConfigMapper;
import com.addcel.gruporichard.model.vo.ListaArchivos;
import com.addcel.gruporichard.model.vo.LoginVO;
import com.addcel.gruporichard.model.vo.UsuarioVO;
import com.addcel.gruporichard.model.vo.common.AbstractLabelValue;
import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.model.vo.common.KitVO;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;
import com.addcel.gruporichard.model.vo.portal.BusquedaTiendaRequest;
import com.addcel.gruporichard.model.vo.portal.BusquedaTiendaResponse;
import com.addcel.gruporichard.utils.ConstantesGpoRichard;
import com.addcel.gruporichard.utils.UtilsFileService;
import com.addcel.gruporichard.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class GrupoRichardPortalService {
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardPortalService.class);
	
	@Autowired
	private PortalConfigMapper portalMapper;
	
	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private UsuarioConfigMapper usuarioMapper;
	
	
	public UsuarioVO login(LoginVO login) {
		UsuarioVO usuario = null;
		List<UsuarioVO> usuarioOriginal = null;
		try {
			usuarioOriginal = usuarioMapper.validarUsuario(0, 0, login.getLogin(), "s");
			
			login.setPassword(AddcelCrypto.encryptPassword(login.getPassword()));
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				usuario = new UsuarioVO(107, "Usuario o Password incorrectos.");
			}else if(!usuarioOriginal.get(0).getPassword().equals(login.getPassword())){
				usuario = new UsuarioVO(107, "Usuario o Password incorrectos.");
			}else if(usuarioOriginal.get(0).getIsPortal() != 1){
				usuario = new UsuarioVO(107, "Usuario o Password incorrectos.");
			}else if(usuarioOriginal.get(0).getStatus() != 1){
				usuario = new UsuarioVO(105, ConstantesGpoRichard.ERROR_USUARIO_STATUS_INACTIVO);
			}else{
				usuario = usuarioOriginal.get(0);
				usuario.setPassword("");
				usuario.setImei("");
			}
		}catch(Exception e){
			usuario = new UsuarioVO(2, "Ocurrio un error en el login.");
			logger.error("Error en el login: {}", e.getMessage());
			
		}
		return usuario;		
	}	
	
	public List<AbstractLabelValue> buscarKITSCombo( ){	
		List<AbstractLabelValue> kits = null; 
		try {
			kits = portalMapper.buscarKITSCombo();
			if(kits == null || (kits != null && kits.size() == 0)){
				kits = new ArrayList<AbstractLabelValue>();
				kits.add(new AbstractLabelValue("0","No existen kits disponibles."));
			}else{
				logger.info("Total de KITs disponibles: {}", kits.size());
				
			}
		}catch (Exception pe) {
			kits = new ArrayList<AbstractLabelValue>();
			kits.add(new AbstractLabelValue("0","Ocurrio un error."));
			logger.error("Ocurrio un error general al actualizar la tienda  y KIT: {}", pe.getMessage());
		}
		return kits;
	}
	
	public AbstractVO agregarTienda(TiendaPortalVO tienda, UsuarioVO usuario) {
		AbstractVO resp = null;
		List<TiendaPortalVO> tiendas = null;
		try {
			resp = validarUsuario(usuario);
			if(resp.getIdError() == 0){
				//Validar si el kit esta disponible
				tiendas = portalMapper.buscarKITS(tienda.getIdKit(), 0, 0, null, null, null, null, null);
				
				if(tiendas == null || (tiendas != null && tiendas.size() > 1 && tiendas.get(0).getStatus() != 0)){
					resp = new AbstractVO(201, "El kit no esta disponible.");
				}else{
					tienda.setIdUserAlta(usuario.getIdUsuario());
					tienda.setIdTienda(0);
					
					portalMapper.agregarTienda(tienda);
					if(tienda.getIdTienda() == 0){
						resp = new AbstractVO(0, "Ocurrio un error al guardar la tienda.");
					}else{
						logger.debug("IdTienda: {}", tienda.getIdTienda());
						tienda.setStatus(1);
						tienda.setVersion(tiendas.get(0).getVersion());;
						portalMapper.actualizarKit(tienda);
						resp = new AbstractVO(0, "Tienda guardada exitosamente.");
					}
				}
			}
			
		} catch (DuplicateKeyException pe) {
			resp = new AbstractVO(200, "La tienda ya existe.");
			logger.error("La tienda ya existe: {}", pe.getMessage());
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al guardar la tienda.");
			logger.error("Ocurrio un error general al guardar la tienda: {}", pe);
		}
		return resp;
	}
	
	
	
	public String buscarTiendasPortal(BusquedaTiendaRequest busquedaTienda, UsuarioVO usuario){				
		BusquedaTiendaResponse resp = new BusquedaTiendaResponse();
		String json = null;
		try {
			
			if(validarSession(usuario) == null){
				busquedaTienda.setFilterValue(busquedaTienda.getFilterBy() > 1 &&  
						busquedaTienda.getFilterValue() != null && !"".equals(busquedaTienda.getFilterValue())? 
								busquedaTienda.getFilterValue() + "%" : busquedaTienda.getFilterValue());
				resp.setData(portalMapper.buscarTiendasPortal(busquedaTienda));
				
				if(resp.getData() != null && resp.getData().size() > 0){
					
					for(TiendaPortalVO tienda: resp.getData()){
	//					tienda.setCarnet(utilService.obtenerMascara(tienda.getCarnet(),true));
						//tienda.setVigencia(utilService.obtenerMascara(tienda.getVigencia(), false));
						tienda.setCarnet(tienda.getMascara());
						tienda.setVigencia("");
					}
					
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
				}else{
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
					resp.setData(new ArrayList<TiendaPortalVO>());
				}
			}else{
				resp.setTotalRecords(0);
				resp.setCurPage(1);
				resp.setData(new ArrayList<TiendaPortalVO>());
				resp.getData().add(new TiendaPortalVO());
				resp.getData().get(0).setNombre(ConstantesGpoRichard.ERROR_NO_SESSION);
			}
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public HashMap<String, Object> buscarKITSCombo(UsuarioVO usuario){				
		HashMap<String, Object> respExito = null;
		AbstractVO resp = null;
		try {
			resp = validarSession(usuario);
			
			if(resp == null){
				respExito = utilService.getRespuestaBusquedas(0, null, ConstantesGpoRichard.NOM_CAMPO_COMBO_KITS,  buscarKITSCombo());
			}else{
				respExito = utilService.getRespuestaBusquedas(resp.getIdError(), resp.getMensajeError(), null, null);
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al actualizar la tienda.");
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}
		return respExito;
	}
	
	public AbstractVO actualizaTienda(TiendaPortalVO tienda, UsuarioVO usuario){				
		AbstractVO resp = null;
		List<TiendaPortalVO> kitOriginal = null;
		List<TiendaPortalVO> kitNuevo = null;
		TiendaPortalVO kitUpdate = null;
		try {
			resp = validarSession(usuario);
			
			if(resp == null){
				resp = validarTienda(tienda);
				
				if(resp == null){
					tienda.setIdUserBaja(usuario.getIdUsuario());
					portalMapper.actualizarTienda(tienda);
					
					if(tienda.getIdKit() != 0){
						kitNuevo = portalMapper.buscarKITS(tienda.getIdKit(), 0, 0, null, null, null, null, null);
						
						if(kitNuevo == null || (kitNuevo != null && kitNuevo.size() > 1 && kitNuevo.get(0).getStatus() != 0)){
							resp = new AbstractVO(201, "El kit no esta disponible, los demas datos se han actualizado correctamente.");
						}else{
							kitOriginal = portalMapper.buscarTiendas(tienda.getIdTienda(), 0, null, null);
								
							kitUpdate = new TiendaPortalVO();
							kitUpdate.setIdKit(tienda.getIdKit());
							kitUpdate.setIdTienda(tienda.getIdTienda());
							kitUpdate.setStatus(1);
							kitUpdate.setVersion(kitNuevo.get(0).getVersion());
							
							portalMapper.actualizarKit(kitUpdate);				// Kit selecionado cambia status a ocupado y el id tienda
							
							if(kitOriginal.get(0).getIdKit() != 0){
								kitOriginal.get(0).setStatus(3);
								portalMapper.actualizarKit(kitOriginal.get(0));		// kit actual cambia estatus a borrado dejandolo solo como historial
								
								kitOriginal.get(0).setIdTienda(0);
								kitOriginal.get(0).setStatus(0);
								portalMapper.agregarKit(new KitVO(kitOriginal.get(0)));	// kit actual generar un uevo registro como disponible
							}
						}
						
					}
					
					if(resp == null && tienda.getStatus() == 0){
						resp = new AbstractVO(0, "Tienda actualizada correctamente.");
					}else if(resp == null && tienda.getStatus() == 1){
						resp = new AbstractVO(0, "Tienda activada correctamente.");
					}else if(tienda.getStatus() == 2){
						resp = new AbstractVO(0, "Tienda desactivada correctamente.");
					}
				}
			}
		} catch (DuplicateKeyException pe) {
			resp = new AbstractVO(200, "El nombre de tienda ya existe.");
			logger.error("La tienda ya existe: {}", pe.getMessage());
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al actualizar la tienda.");
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}
		return resp;
	}
	
	public AbstractVO agregarKit(KitVO kit, UsuarioVO usuario) {
		AbstractVO resp = null;
		try {
			resp = validarKit(kit);
			if(resp == null){
				kit.setIdUserAlta(usuario.getIdUsuario());
				kit.setStatus(0);;
				portalMapper.agregarKit(kit);
//				if(kit.getIdKit() == 0){
//					resp = new AbstractVO(50, "Ocurrio un error al guardar el kit.");
//				}else{
					resp = new AbstractVO(0, "Kit guardado correctamente.");
//				}
			}
		} catch (DuplicateKeyException pe) {
			resp = new AbstractVO(200, "El KIT ya existe.");
			logger.error("El KIT ya existe: {}", pe.getMessage());
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al guardar el kit.");
			logger.error("Ocurrio un error general al guardar el kit: {}", pe);
		}
		return resp;
	}
	
	public AbstractVO actualizaKit(TiendaPortalVO kit, UsuarioVO usuario, boolean type){				
		AbstractVO resp = null;
		TiendaPortalVO kitUpdate = null;
		try {
			resp = validarSession(usuario);
			
			if(resp == null){
				resp = validarKit(kit, type);
				
				if(resp == null){
					if(type){
						kit.setIdUserBaja(usuario.getIdUsuario());
						portalMapper.actualizarKit(kit);
						if(kit.getStatus() == 1 || kit.getStatus() == 0){
							resp = new AbstractVO(0, "Kit activo correctamente.");
						}else if(kit.getStatus() == 2){
							resp = new AbstractVO(0, "Kit desactivo correctamente.");
						}
					}else{
						List<TiendaPortalVO> kitOriginal = portalMapper.buscarKITS(kit.getIdKit(), 0, 0, null, null, null, null, null);
						
						if(kit.getCarnet().equals(kitOriginal.get(0).getCarnet()) ||
								kit.getVigencia().equals(kitOriginal.get(0).getVigencia()) ||
								kit.getImei().equals(kitOriginal.get(0).getImei())  ||
								kit.getDispositivo().equals(kitOriginal.get(0).getDispositivo()) ||
								kit.getSim().equals(kitOriginal.get(0).getSim()) ||
								kit.getTelefono().equals(kitOriginal.get(0).getTelefono()) ||
								kit.getFolio().equals(kitOriginal.get(0).getFolio()) ||
								kit.getNombreTitular().equals(kitOriginal.get(0).getNombreTitular()) 
								){
							
							kitUpdate = new TiendaPortalVO();
							kitUpdate.setIdKit(kit.getIdKit());
							kitUpdate.setVersion(kit.getVersion());
							kitUpdate.setStatus(3);
							kitUpdate.setIdUserBaja(usuario.getIdUsuario());
							kit.setStatus(1);
							kit.setIdTienda(kitOriginal.get(0).getIdTienda());
							kit.setIdUserAlta(usuario.getIdUsuario());
							
							if(kit.getCarnet() == null || "".equals(kit.getCarnet())){
								kit.setCarnet(kitOriginal.get(0).getCarnet());
								kit.setMascara(kitOriginal.get(0).getMascara());
							}
							
							portalMapper.actualizarKit(kitUpdate);
							portalMapper.agregarKit(new KitVO(kit));
							
						}
						resp = new AbstractVO(0, "Kit actualizado correctamente.");
					}
				}
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al actualizar la tienda.");
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}
		return resp;
	}
	
	public String buscarKitsPortal(BusquedaTiendaRequest busquedaTienda, UsuarioVO usuario){				
		BusquedaTiendaResponse resp = new BusquedaTiendaResponse();
		String json = null;
		try {
			if(validarSession(usuario) == null){
				busquedaTienda.setFilterValue(busquedaTienda.getFilterBy() > 1 &&  
						busquedaTienda.getFilterValue() != null && !"".equals(busquedaTienda.getFilterValue())? 
								busquedaTienda.getFilterValue() + "%" : busquedaTienda.getFilterValue());
				resp.setData(portalMapper.buscarKitsPortal(busquedaTienda));
				
				if(resp.getData() != null && resp.getData().size() > 0){
					
					for(TiendaPortalVO tienda: resp.getData()){
	//					tienda.setCarnet(utilService.obtenerMascara(tienda.getCarnet(),true));
						tienda.setVigencia(utilService.obtenerMascara(tienda.getVigencia(), false));
						tienda.setCarnet(tienda.getMascara());
	//					tienda.setVigencia("");
					}
					
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
				}else{
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
					resp.setData(new ArrayList<TiendaPortalVO>());
				}
			}else{
				resp.setTotalRecords(0);
				resp.setCurPage(1);
				resp.setData(new ArrayList<TiendaPortalVO>());
				resp.getData().add(new TiendaPortalVO());
				resp.getData().get(0).setImei(ConstantesGpoRichard.ERROR_NO_SESSION);
			}
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	@SuppressWarnings("unchecked")
	public String buscarUsuariosPortal(BusquedaTiendaRequest busquedaTienda, UsuarioVO usuario){				
		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<UsuarioVO> usuarios = null;
		String json = null;
		try {
			if(validarSession(usuario) == null){
				busquedaTienda.setFilterValue(busquedaTienda.getFilterBy() > 1 &&  
						busquedaTienda.getFilterValue() != null && !"".equals(busquedaTienda.getFilterValue())? 
								busquedaTienda.getFilterValue() + "%" : busquedaTienda.getFilterValue());
				usuarios = portalMapper.buscarUsuariosPortal(busquedaTienda);
				
				if(usuarios != null && usuarios.size() > 0){
					resp.put("data", usuarios);
					resp.put("totalRecords", usuarios.size() +1);
					resp.put("curPage", 1);
				}else{
					
					resp.put("data", new ArrayList<UsuarioVO>() );
					resp.put("totalRecords", 0);
					resp.put("curPage", 1);
				}
			}else{
				resp.put("totalRecords", 0);
				resp.put("curPage", 1);
				resp.put("data", new ArrayList<UsuarioVO>());
				((List<UsuarioVO>) resp.get("data")).add(new UsuarioVO());
				((List<UsuarioVO>) resp.get("data")).get(0).setLogin(ConstantesGpoRichard.ERROR_NO_SESSION);
			}
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar usuarios desde el portal: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public ModelAndView altaArchivosProcesar(ListaArchivos archivos, UsuarioVO usuario) {
		logger.debug("Dentro del servicio: /portal/altaArchivosProcesar");
		ModelAndView mav = null;
		AbstractVO resp = null;
		List<UtilsFileService> listArchivos = new ArrayList<UtilsFileService>();
		HashMap<String, String> tipoArchivos = new HashMap<String, String>();
		List<TiendaPortalVO> tiendas = null;
		StringBuffer cadena = null;
		KitVO kit = null;
		try{
			logger.debug("ControladorSubirFichero: Inicializando proceso copia ficheros");
			resp = validarArchivosProcesar(archivos, listArchivos, tipoArchivos);
			
			if(resp == null){
				String prefix = UtilsFileService.formatoFull.format(new Date());
				for (UtilsFileService archivo : listArchivos) {
					archivo.saveFile(prefix);
					archivo.closeBufferedReader();
				}
				
				tiendas = UtilsFileService.generaCombos(listArchivos);
				cadena = new StringBuffer();
				cadena.append("Id Tienda\t");
				cadena.append("Nombre\t");
				cadena.append("Id Kit\t");
				cadena.append("FOLIO\t");
				cadena.append("CARNET\t");
//				cadena.append("Vigencia\t");
				cadena.append("IMEI\t");
				cadena.append("Dispositivo\t");
				cadena.append("SIM\t");
				cadena.append("Telefono\t");
				cadena.append("Calle\t");
				cadena.append("Num Ext\t");
				cadena.append("Num Int\t");
				cadena.append("Colonia\t");
				cadena.append("CP\t");
				cadena.append("Ciudad\t");
				cadena.append("Estado\t");
				cadena.append("Latitud\t");
				cadena.append("Longitud\n");
				
				for(TiendaPortalVO combo: tiendas){
					combo.setIdUserAlta(usuario.getIdUsuario());
					combo.setStatus(1);
					
					if(tipoArchivos.get("Tiendas") != null ){
						portalMapper.agregarTienda(combo);
					}else{
						combo.setStatus(0);
					}
					
					if(tipoArchivos.containsKey("IMEI") ){
						kit = new KitVO(combo);
						portalMapper.agregarKit(kit);
						combo.setIdKit(kit.getIdKit());
					}
					
					cadena.append(combo.getIdTienda() ).append("	");
					cadena.append(UtilsFileService.isEmpty( combo.getNombre() )).append("	");
					
					cadena.append(combo.getIdKit()).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getFolio() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getMascara() )).append("	");
//					cadena.append(UtilsFileService.isEmpty(combo.getVigencia() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getImei() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getDispositivo() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getSim() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getTelefono() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getCalle() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getNumExt() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getNumInt() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getColonia() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getCp() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getCiudad() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getEstado() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getCx() )).append("	");
					cadena.append(UtilsFileService.isEmpty(combo.getCy() )).append("\n");
				}
				
				UtilsFileService.saveFile(ConstantesGpoRichard.URL_FOLDER_ARCHIVOS + prefix + " Resultado.txt", cadena.toString(), true);
				
				mav = new ModelAndView("portal/altaArchivosResultado");
				mav.addObject("mensaje", "El proceso fue completado satisfactoriamente.");
				mav.addObject("combos", tiendas);
				mav.addObject("archivo", prefix + " Resultado.txt");
			}else{
				mav = new ModelAndView("portal/altaArchivos", "listaArchivos" , new ListaArchivos());
				mav.addObject("mensaje", resp.getMensajeError());
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaArchivos: {}", e );
		}
		return mav;	
	}
	
	/**
     * Size of a byte buffer to read/write file
     */
    private static final int BUFFER_SIZE = 4096;
    
	public ModelAndView altaArchivosRecuperar(String nombreArchivo, HttpServletResponse response) {
		logger.debug("Dentro del servicio: /portal/altaArchivosRecuperar");
		ModelAndView mav = null;
		File downloadFile = null;
		FileInputStream inputStream = null;
		OutputStream outStream = null;
		try{
			logger.debug("Buscar Archivo: ");
		        // construct the complete absolute path of the file
		        downloadFile = new File(ConstantesGpoRichard.URL_FOLDER_ARCHIVOS + nombreArchivo );
		        inputStream = new FileInputStream(downloadFile);
		         
		        // set content attributes for the response
		        response.setContentType("text/plain");
		        response.setContentLength((int) downloadFile.length());
		 
		        // set headers for the response
		        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", downloadFile.getName()));
		 
		        // get output stream of the response
		        outStream = response.getOutputStream();
		 
		        byte[] buffer = new byte[BUFFER_SIZE];
		        int bytesRead = -1;
		 
		        // write bytes read from the input stream into the output stream
		        while ((bytesRead = inputStream.read(buffer)) != -1) {
		            outStream.write(buffer, 0, bytesRead);
		        }
		 
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/altaArchivosRecuperar: {}", e );
		}finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(outStream != null){
				try {
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return mav;	
	}
	
	public AbstractVO actualizaUsuario(UsuarioVO user, UsuarioVO usuario, boolean type){				
		AbstractVO resp = null;
		try {
			resp = validarSession(usuario);
			
			if(resp == null){
				if(type){
					portalMapper.actualizarUsuario(user);
					if(user.getStatus() == 1 || user.getStatus() == 0){
						resp = new AbstractVO(0, "Usuario activo correctamente.");
					}else if(user.getStatus() == 3){
						resp = new AbstractVO(0, "Usuario desactivo correctamente.");
					}
				}else{
					
					portalMapper.actualizarUsuario(user);
					resp = new AbstractVO(0, "Usuario actualizado correctamente.");
				}
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al actualizar la tienda.");
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}
		return resp;
	}
	
	private AbstractVO validarSession (UsuarioVO usuario){
		AbstractVO resp = null;
		if(usuario == null){
			resp = new AbstractVO(ConstantesGpoRichard.ID_NO_SESSION, ConstantesGpoRichard.ERROR_NO_SESSION);
		}
		return resp;
	}
	
	private AbstractVO validarUsuario(UsuarioVO usuario) {
		AbstractVO resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		try {
			
			if(usuario == null){
				resp = new AbstractVO(ConstantesGpoRichard.ID_NO_SESSION, "La session se termino.");
			}else{
				usuarioOriginal = usuarioMapper.validarUsuario(0, usuario.getIdUsuario(), null, "s");
				
				if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
					resp = new AbstractVO(101, "No existe el usuario.");
				}else if(usuarioOriginal.get(0).getStatus() != 1){
					resp = new AbstractVO(102, "Usuario inactivo, favor de contactar al Administrador.");
				}else{
					resp = new AbstractVO(0, "Usuario Valido.");
				}
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al validar Usuario.");
			logger.error("Ocurrio un error general al validarUsuario: {}", pe);
		}
		return resp;
	}
	
	public ModelAndView validarSessionController (UsuarioVO usuario){
		logger.info("Dentro del servicio: /portal/welcome");
		ModelAndView mav = null;
		try{
			if(usuario == null){
				logger.debug("Se termino la session, portal/login." );
				mav = new ModelAndView("portal/login", "usuario", new LoginVO());
				mav.addObject("mensaje", ConstantesGpoRichard.ERROR_NO_SESSION);
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general al validar session: {}", e );
		}
		return mav;	
	}
	
	
	private AbstractVO validarTienda(TiendaPortalVO tienda ){				
		AbstractVO resp = null;
		List<TiendaPortalVO> tiendaOriginal = null; 
		try {
			
			tiendaOriginal = portalMapper.buscarTiendas(tienda.getIdTienda(), 0, null, null);
			
			if(tiendaOriginal == null || (tiendaOriginal != null && tiendaOriginal.size() == 0)){
				resp = new AbstractVO(11, "No existe la tienda.");
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error general al actualizar la tienda.");
			logger.error("Ocurrio un error general al actualizar la tienda: {}", pe);
		}
		return resp;
	}
	
	private AbstractVO validarKit(KitVO kit ){				
		AbstractVO resp = null;
		List<TiendaPortalVO> kitOriginal = null; 
		try {
			
			if(kit.getCarnet() == null || "".equals(kit.getCarnet())){
				resp = new AbstractVO(50, "El campo CARNET es obligatorio.");
			}else if(kit.getVigencia() == null || "".equals(kit.getVigencia())){
				resp = new AbstractVO(50, "El campo VIGENCIA es obligatorio.");
			}else if(kit.getImei() == null || "".equals(kit.getImei())){
				resp = new AbstractVO(50, "El campo IMEI es obligatorio.");
			}else if(kit.getDispositivo() == null || "".equals(kit.getDispositivo())){
				resp = new AbstractVO(50, "El campo DISPOSITIVO es obligatorio.");
			}else{
				kit.setMascara(kit.getCarnet().substring(kit.getCarnet().length() - 4 ));
				kit.setCarnet(AddcelCrypto.encryptTarjeta(kit.getCarnet()));
				kit.setVigencia(AddcelCrypto.encryptTarjeta(kit.getVigencia()));
				
				kitOriginal = portalMapper.buscarKITS(0, 0, 0, null, kit.getCarnet(), null, null, null);
				
//				if(kitOriginal != null && kitOriginal.size() > 0){
//					resp = new AbstractVO(11, "El CARNET ya existe.");
//				}else{
					kitOriginal = portalMapper.buscarKITS(0, 0, 0, kit.getImei(), null, null, null, null);
					if(kitOriginal != null && kitOriginal.size() > 0){
						resp = new AbstractVO(11, "El IMEI ya existe.");
					}else{
						kitOriginal = portalMapper.buscarKITS(0, 0, 0, null, null, kit.getDispositivo(), null, null);
						if(kitOriginal != null && kitOriginal.size() > 0){
							resp = new AbstractVO(11, "El DISPOSITIVO ya existe.");
						}else{
							kitOriginal = portalMapper.buscarKITS(0, 0, 0, null, null, null, kit.getSim(), null);
							if(kitOriginal != null && kitOriginal.size() > 0){
								resp = new AbstractVO(11, "El SIM ya existe.");
							}else{
								kitOriginal = portalMapper.buscarKITS(0, 0, 0, null, null, null, null, kit.getTelefono());
								if(kitOriginal != null && kitOriginal.size() > 0){
									resp = new AbstractVO(11, "El TELEFONO ya existe.");
								}
							}
						}
					}
//				}
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error al validar el kit.");
			logger.error("Ocurrio un error general al validar el kit: {}", pe);
		}
		return resp;
	}
	
	private AbstractVO validarKit(TiendaPortalVO kit, boolean type ){				
		AbstractVO resp = null;
		List<TiendaPortalVO> kitOriginal = null;
		List<TiendaPortalVO> kitValida = null;
		try {
			kitOriginal = portalMapper.buscarKITS(kit.getIdKit(), 0, 0, null, null, null, null, null);
			
			if(kitOriginal == null || (kitOriginal != null && kitOriginal.size() == 0)){
				resp = new AbstractVO(201, "No existe el kit.");
			}else{
				if(type){
					if(kit.getStatus() == 1 && kitOriginal.get(0).getIdTienda() == 0){
						kit.setStatus(0);
					}
				}else{
//					if(kit.getCarnet() == null || "".equals(kit.getCarnet())){
//						resp = new AbstractVO(50, "El campo CARNET es obligatorio.");
//					}else 
					if(kit.getVigencia() == null || "".equals(kit.getVigencia())){
						resp = new AbstractVO(50, "El campo VIGENCIA es obligatorio.");
					}else if(kit.getImei() == null || "".equals(kit.getImei())){
						resp = new AbstractVO(50, "El campo IMEI es obligatorio.");
//					}else if(kit.getDispositivo() == null || "".equals(kit.getDispositivo())){
//						resp = new AbstractVO(50, "El campo DISPOSITIVO es obligatorio.");
					}else{
						if(kit.getCarnet() != null && !"".equals(kit.getCarnet())){
							kit.setMascara(kit.getCarnet().substring(kit.getCarnet().length() - 4 ));
							kit.setCarnet(AddcelCrypto.encryptTarjeta(kit.getCarnet()));
						}
						
						kit.setVigencia(AddcelCrypto.encryptTarjeta(kit.getVigencia()));
						
//						if(kit.getCarnet() != null && !"".equals(kit.getCarnet()) && !kit.getCarnet().equals(kitOriginal.get(0).getCarnet()) ){
//							kitValida = portalMapper.buscarKITS(0, 0, 0, null, kit.getCarnet(), null, null, null);
//							if(kitValida != null && kitValida.size() > 0){
//								resp = new AbstractVO(11, "El CARNET ya existe.");
//							}
//						}
						if(!kit.getImei().equals(kitOriginal.get(0).getImei())  ){
							kitValida = portalMapper.buscarKITS(0, 0, 0, kit.getImei(), null, null, null, null);
							if(kitValida != null && kitValida.size() > 0){
								resp = new AbstractVO(11, "El IMEI ya existe.");
							}
						}
						
						if(kit.getDispositivo()!= null && !"".equals(kit.getDispositivo()) && !kit.getDispositivo().equals(kitOriginal.get(0).getDispositivo()) ){
							kitValida = portalMapper.buscarKITS(0, 0, 0, null, null, kit.getDispositivo(), null, null);
							if(kitValida != null && kitValida.size() > 0){
								resp = new AbstractVO(11, "El DISPOSITIVO ya existe.");
							}
						}
						
						if(!kit.getSim().equals(kitOriginal.get(0).getSim())  ){
							kitValida = portalMapper.buscarKITS(0, 0, 0, null, null, null, kit.getSim(), null);
							if(kitValida != null && kitValida.size() > 0){
								resp = new AbstractVO(11, "El SIM ya existe.");
							}
						}
						
						if(!kit.getTelefono().equals(kitOriginal.get(0).getTelefono()) ){
							kitValida = portalMapper.buscarKITS(0, 0, 0, null, null, null, null, kit.getTelefono());
							if(kitValida != null && kitValida.size() > 0){
								resp = new AbstractVO(11, "El TELEFONO ya existe.");
							}
						}
					}
				}
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error al validar el kit.");
			logger.error("Ocurrio un error general al validar el kit: {}", pe);
		}
		return resp;
	}
	
	
	private AbstractVO validarArchivosProcesar(ListaArchivos listaArchivos, List<UtilsFileService> listArchivos, HashMap<String, String> tipoArchivos){			
		AbstractVO resp = null;
		UtilsFileService file = null;
//		int cont = 0;
		String error = "EL numero de registros no es valido: ";
		boolean isError = false;
		List<TiendaPortalVO> registrosBD = null; 
		String cadena = null;
		
		try {
			if (listaArchivos.getArchivos() == null) {
				resp = new AbstractVO(300, "Faltan archivos necesarios para el proceso.");
//			}if (listaArchivos.getArchivos() != null && listaArchivos.getArchivos().size() != 4) {
//				resp = new AbstractVO(300, "Faltan " + (4 - listaArchivos.getArchivos().size() ) + " archivos de 4 necesarios para el proceso.");
			}else{

				for (MultipartFile archivo : listaArchivos.getArchivos()) {
					file = new UtilsFileService(archivo);
					listArchivos.add(file);
					tipoArchivos.put(file.getType(), file.getType());
					
					if(file.getIdError() !=0){
						resp = utilService.concatenarRespuesta(resp, file, null);
					}
				}
				
				//Validar tipo de archivos enviados, se quita validacion por  || !tipoArchivos.containsKey("Dispositivo") 
				if(!tipoArchivos.containsKey("Tiendas")  && (!tipoArchivos.containsKey("IMEI")  || !tipoArchivos.containsKey("CARNET")  || !tipoArchivos.containsKey("SIM") )){
					resp = new AbstractVO(120, "Para procesar el alta de Kit's es necesario ingresar minimo 3 archivos (Tablets, CARNET,  SIM's).");
				}else if(tipoArchivos.containsKey("Tiendas")  && (tipoArchivos.containsKey("IMEI")  || tipoArchivos.containsKey("CARNET")  || tipoArchivos.containsKey("Dispositivo") || tipoArchivos.containsKey("SIM") )
						&& !(tipoArchivos.containsKey("IMEI")  && tipoArchivos.containsKey("CARNET")  && tipoArchivos.containsKey("Dispositivo") && tipoArchivos.containsKey("SIM"))){
					resp = new AbstractVO(120, "Para procesar el alta de Tiendas es necesario ingresar solo 1 archivo (Tiendas).");
				}else{
					UtilsFileService archivo = null;
					for(int cont = 0; cont < listArchivos.size(); cont++){
						archivo = listArchivos.get(cont);
						if(archivo.getType() != null){
							error += "<BR/>Archivo " + archivo.getFileName() + " contiene " + archivo.getNumberLine() + " registros." ;
							if(archivo.getType() != null && cont != 0 && !isError && archivo.getNumberLine() != listArchivos.get(cont - 1).getNumberLine()){
								isError = true;
							}
						}else {
							listArchivos.remove(cont--);
						}
					}
					if(isError){
						resp = utilService.concatenarRespuesta(resp, null, error);
					}
				}
				
				if(resp == null){
					for (UtilsFileService archivo : listArchivos) {
						archivo.setMensajeError( "");
						for (HashMap<String, String> data : archivo.getData()) {
							
							if("Tiendas".equals(archivo.getType())){
								registrosBD = portalMapper.buscarTiendas(0, 0, data.get("nombre"), null);
							
							}else if("IMEI".equals(archivo.getType())){
								registrosBD = portalMapper.buscarKITS(0, 0, 0, data.get("imei"), null, null, null, null);
							
							}else if("CARNET".equals(archivo.getType())){
								cadena = data.get("carnet");
								data.put("carnet",AddcelCrypto.encryptTarjeta(data.get("carnet")));
								data.put("vigencia",AddcelCrypto.encryptTarjeta(data.get("vigencia")));
								registrosBD = portalMapper.buscarKITS(0, 0, 0, null, data.get("carnet"), null, null, null);
								
							}else if("Dispositivo".equals(archivo.getType())){
								registrosBD = portalMapper.buscarKITS(0, 0, 0, null, null, data.get("sn"), null, null);
							
							}else if("SIM".equals(archivo.getType())){
								registrosBD = portalMapper.buscarKITS(0, 0, 0, null, null, null, data.get("sim"), null);
								if(registrosBD != null && registrosBD.size() == 0){
									registrosBD = portalMapper.buscarKITS(0, 0, 0, null, null, null, null, data.get("telefono"));
								}
							}
							
							if(registrosBD != null && registrosBD.size() > 0){
								archivo.setMensajeError( archivo.getMensajeError() + 
										(archivo.getIdError() == 0
												?"Los siguientes " + archivo.getType() +" ya existen:": "") + " <BR/>" +
										("Tiendas".equals(archivo.getType())? data.get("nombre"):
										 "IMEI".equals(archivo.getType())? data.get("imei"):
										 "CARNET".equals(archivo.getType())? cadena:
										 "Dispositivo".equals(archivo.getType())? data.get("carnet"):
										 "SIM".equals(archivo.getType())? data.get("sim") + " o TEL: " + data.get("telefono"): "") );
								archivo.setIdError(320);
							}
						}
						if(archivo.getIdError() !=0){
							resp = utilService.concatenarRespuesta(resp, archivo, null);
						}
					}
				}
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error durante las validaciónes.");
			logger.error("Ocurrio un error general al validar el kit: {}", pe);
		}
		return resp;
	}
	

}
