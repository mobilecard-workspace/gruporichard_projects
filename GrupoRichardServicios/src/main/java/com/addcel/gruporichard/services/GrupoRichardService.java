package com.addcel.gruporichard.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.addcel.gruporichard.model.mapper.PortalConfigMapper;
import com.addcel.gruporichard.model.mapper.UsuarioConfigMapper;
import com.addcel.gruporichard.model.vo.UsuarioVO;
import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;
import com.addcel.gruporichard.model.vo.common.TiendaVO;
import com.addcel.gruporichard.utils.AddCelGenericMail;
import com.addcel.gruporichard.utils.ConstantesGpoRichard;
import com.addcel.gruporichard.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class GrupoRichardService {
	private static final Logger logger = LoggerFactory.getLogger(GrupoRichardService.class);
	private static final String patron = "ddhhmmssSSS";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	@Autowired
	private UsuarioConfigMapper usuarioMapper;
	
	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private PortalConfigMapper portalMapper;
	
	
	public String buscarTiendaKIT(String json){				
		TiendaVO tienda = null;
		List<TiendaVO> tiendas = null; 
		try {
			tienda = (TiendaVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), TiendaVO.class);
			tiendas = usuarioMapper.buscarTiendaKIT(tienda.getImei());
			
			if(tiendas == null || (tiendas != null && tiendas.size() == 0)){
				tienda = new TiendaVO(20, "El equipo no esta activo en el sistema.");
			}else{
				tienda = tiendas.get(0);
				
				tienda.setCarnet(AddcelCrypto.decryptTarjeta(tienda.getCarnet()));
				tienda.setVigencia("");
				
				tienda.setCarnet("XXXX XXXX XXXX " + tienda.getCarnet().substring(tienda.getCarnet().length() -4));
			}
		}catch (Exception pe) {
			tienda = new TiendaVO(2, "Ocurrio un error general al buscar la tienda y KIT.");
			logger.error("Ocurrio un error general al actualizar la tienda  y KIT: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(tienda);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	public String agregarUsuario(String json) {
		AbstractVO resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		List<TiendaVO> tiendas = null; 
		HashMap<String, Object> respExito = null;
//		String password = null;
		try {
			UsuarioVO usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), UsuarioVO.class);	
			usuarioOriginal = usuarioMapper.validarUsuario(0, 0, usuario.getLogin(), null);
			tiendas = usuarioMapper.buscarTiendaKIT(usuario.getImei());
			
			if(usuarioOriginal != null && usuarioOriginal.size() > 0){
				resp = new AbstractVO(100, "Ya existe el usuario.");
			}else if(tiendas == null || (tiendas != null && tiendas.size() == 0)){
				resp = new AbstractVO(20, "El equipo no esta activo en el sistema.");
			}else if(tiendas.get(0).getStatus() != 1){
				resp = new AbstractVO(112, "La tienda no está activa.");
			}else{
//				password = utilService.generatePassword(12);
				usuario.setPassword(AddcelCrypto.encryptPassword(ConstantesGpoRichard.DEFAULT_PASSWORD));
				usuarioMapper.agregarUsuario(usuario);
				respExito = utilService.getRespuestaBusquedas(0, "Usuario guardado exitosamente.", ConstantesGpoRichard.NOM_CAMPO_ID_USUARIO, usuario.getIdUsuario());
				
				AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailRegistro(usuario.getMail(), ConstantesGpoRichard.DEFAULT_PASSWORD, usuario.getNombre())));
				
				try{
					List<TiendaPortalVO> tiendasAlta = portalMapper.buscarTiendas(tiendas.get(0).getIdTienda(), 0, null, null);
					
					if(tiendasAlta.get(0).getIsActiva() == 0 && tiendasAlta.get(0).getNombreTitular() != null && !"".equals(tiendasAlta.get(0).getNombreTitular())){
						tiendasAlta.get(0).setVigencia(AddcelCrypto.decryptTarjeta(tiendasAlta.get(0).getVigencia()));
						AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailAltaTarjeta(tiendasAlta.get(0))));
						
						portalMapper.actualizarKitAltaCARNET(tiendasAlta.get(0).getIdKit());
					}
					
				}catch(Exception e){
					logger.error("Ocurrio un error en la notificacion de alta de la tienda: {}", e);
				}
			}
		} catch (DuplicateKeyException pe) {
			resp = new AbstractVO(14, "EL usuario ya existe.");
			logger.error("Nombre de usuario duplicado: {}", pe.getMessage());
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error al registrar el usuario.");
			logger.error("Error al insertar usuario: {}", pe);
		}finally{
			if(resp != null){
				json = utilService.objectToJson(resp);
			}else{
				json = utilService.objectToJson(respExito);
			}
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	public String actualizarUsuario(String json){				
		AbstractVO resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		try {
			UsuarioVO usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), UsuarioVO.class);	
			usuarioOriginal = usuarioMapper.validarUsuario(usuario.getIdTienda(), usuario.getIdUsuario(), null, null);
			
			if("rootGpoRichard".equals(usuario.getLogin()) || usuario.getIdUsuario() == 60){
				resp = new AbstractVO(106, "El Administrador Root solo puede cambiar su password.");
			}else if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = new AbstractVO(101, "No existe el usuario.");
			}else if(usuario.getStatus() == 2){
				resp = new AbstractVO(106,  ConstantesGpoRichard.MESS_USUARIO_STATUS_CAM_PASS + ", antes de actualizar sus datos.");
			}else{
				usuarioMapper.actualizarUsuario(usuario);
				resp = new AbstractVO("Usuario actualizado correctamente.");
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error al actualizar el usuario.");
			logger.error("Error al actualizar usuario: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	public String actualizarPassword(String json){				
		AbstractVO resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		String newPassword = null;
		try {
			UsuarioVO usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), UsuarioVO.class);	
			usuarioOriginal = usuarioMapper.validarUsuario(0, usuario.getIdUsuario(), null, null);
			
			newPassword = usuario.getNewPassword();
			usuario.setPassword(AddcelCrypto.encryptPassword(usuario.getPassword()));
			usuario.setNewPassword(AddcelCrypto.encryptPassword(usuario.getNewPassword()));
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = new AbstractVO(101, "No existe el usuario.");
			}else if(!usuarioOriginal.get(0).getPassword().equals(usuario.getPassword())){
				resp = new AbstractVO(102, "Password incorrecto.");
			}else if(!usuarioOriginal.get(0).getImei().equals(usuario.getImei() )){
				resp = new AbstractVO(103, "No se puede actualizar el password en este equipo.");
			}else{
				usuario.setStatus(1);
				usuarioMapper.actualizarPassword(usuario);
				resp = new AbstractVO("Password actualizado correctamente.");
				
				AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailPassword(usuarioOriginal.get(0).getMail(), newPassword, usuarioOriginal.get(0).getNombre())));
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error al actualizar el password.");
			logger.error("Error al cambiar password usuario: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	public String recuperarPassword(String json){				
		AbstractVO resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		String password = null;
		try {
			UsuarioVO usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), UsuarioVO.class);	
			usuarioOriginal = usuarioMapper.validarUsuario(0, 0, usuario.getLogin(), null);
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = new AbstractVO(101, "No existe el usuario.");
			}else if(!usuarioOriginal.get(0).getImei().equals(usuario.getImei() )){
				resp = new AbstractVO(103, "No se puede actualizar el password en este equipo.");
			}else{
				password = utilService.generatePassword(8);
				usuario.setNewPassword(AddcelCrypto.encryptPassword(password));
				usuario.setIdUsuario(usuarioOriginal.get(0).getIdUsuario());
				usuario.setStatus(2);
				
				usuarioMapper.actualizarPassword(usuario);
				resp = new AbstractVO("Password actualizado correctamente, se ha enviado su nuevo password a su Email.");
				
				AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailPasswordReset(usuarioOriginal.get(0).getMail(), password, usuarioOriginal.get(0).getNombre())));
			}
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error en el resert del password.");
			logger.error("Error al cambiar password usuario: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}

	public String login(String json) {
		AbstractVO resp = null;
		UsuarioVO usuario = null;
		List<UsuarioVO> usuarioOriginal = null;
		try {
			usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), UsuarioVO.class);	
			usuarioOriginal = usuarioMapper.validarUsuario(0, 0, usuario.getLogin(), null);
			
			usuario.setPassword(AddcelCrypto.encryptPassword(usuario.getPassword()));
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				resp = new AbstractVO(107, "Usuario o Password incorrectos.");
			}else if(!usuarioOriginal.get(0).getPassword().equals(usuario.getPassword())){
				resp = new AbstractVO(107, "Usuario o Password incorrectos.");
			}else if(!usuarioOriginal.get(0).getImei().equals(usuario.getImei()) ){
				resp = new AbstractVO(103, "No puede iniciar session en este equipo.");
			}else if(usuarioOriginal.get(0).getStatus() < 1 || usuarioOriginal.get(0).getStatus() > 2){
				resp = new AbstractVO(105, ConstantesGpoRichard.ERROR_USUARIO_STATUS_INACTIVO);
			}else if(usuarioOriginal.get(0).getStatusTienda() != 1){
				resp = new AbstractVO(112, "La tienda no está activa.");
			}else{
				usuario = usuarioOriginal.get(0);
				usuario.setPassword("");
				usuario.setImei("");
				if(usuario.getStatus() == 2){
					usuario.setMensajeError(ConstantesGpoRichard.MESS_USUARIO_STATUS_CAM_PASS);
					logger.debug("Se requiere cambio de contraseña del usuario con id ==> {}",usuario.getIdUsuario());
				}
				usuario.setTiempoNoti(usuarioMapper.valorNotificacion());
			}
		}catch(Exception e){
			resp = new AbstractVO(2, "Ocurrio un error en el login.");
			logger.error("Error en el login: {}", e.getMessage());
			
		}finally{
			if(resp != null){
				json = utilService.objectToJson(resp);
				
			}else{
				usuario.setTipoTarjeta(ConstantesGpoRichard.TIPO_TARJETA);
				json = utilService.objectToJson(usuario);
			}
			json=AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
		}
		return json;		
	}
	
	@SuppressWarnings("unchecked")
	public String notificaLocalizacion(String json){				
		AbstractVO resp = null;
		HashMap<String, Object> localizacion = null;
		try {
			localizacion = (HashMap<String, Object>) utilService.jsonToObject(AddcelCrypto.decryptHard(json), HashMap.class);	
			usuarioMapper.agregarBitacoraLocalizacion(localizacion);
			
			resp = new AbstractVO("Localizacion guardada correctamente.");
				
		}catch (Exception pe) {
			resp = new AbstractVO(2, "Ocurrio un error al guardar la localizacion.");
			logger.error("Error al al guardar la localizacion: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}

}
