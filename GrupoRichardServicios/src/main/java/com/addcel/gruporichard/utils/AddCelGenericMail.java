package com.addcel.gruporichard.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gruporichard.model.vo.CorreoVO;
import com.addcel.gruporichard.model.vo.DatosPago;
import com.addcel.gruporichard.model.vo.DatosPagoResponse;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;
import com.addcel.gruporichard.model.vo.common.TiendaVO;
import com.addcel.utils.Utilerias;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String HTML_SUBJECT_REG = "Bienvenido - On Comercio";
	private static final String HTML_BODY_REG = 
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Bienvenidos a On Comercio</title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> <link rel=\"icon\" type=\"image/x-icon\" href=\"http://www.gruporichard.com.mx/sitio/favicon.ico\"> </script> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } /*Font-size: 1.0em = 10px when browser default size is 16px*/ .page-container { width: 900px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .main { clear: both; width: 900px; padding-bottom: 30px; }  .main-content { display: inline; /*Fix IE floating margin bug*/; float: left; width: 840px; margin: 0 0 0 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer { clear: both; width: 900px; height: 5em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; border-bottom: solid 7px rgb(0, 26, 51); font-family: Verdana, Geneva, sans-serif; color: #184274; font-weight: bold; font-size: 220%; }  .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; /* font-weight: bold; */ color: #2A5A8A; }  .header { width: 900px; font-family: \"trebuchet ms\", arial, sans-serif; }  .header-top { width: 900px; height: 162px; background: rgb(24, 66, 116); overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .header-breadcrumbs { clear: both; width: 900px; padding: 1.0em 0 1.5em 0; background: rgb(100, 100, 100) }  .column1-unit { width: 840px; margin-bottom: 10px !important /*Non-IE6*/; margin-bottom: 5px /*IE6*/; }  .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 110%; }  .footer p.credits { font-weight: normal; } </style> </head> <body> <div class=\"page-container\"> <div class=\"header\"> <div class=\"header-top\"> <img src=\"cid:identifierCID00\" alt=\"\" /> </div> <div class=\"header-breadcrumbs\"></div> </div> <br> <div class=\"main-content\"> <h1 class=\"pagetitle\">Bienvenido (a): <#NOMBRE#></h1> <div class=\"column1-unit\"> <p> Gracias por usar la aplicación <strong> On Comercio </strong>. </p> <p>A continuación se encuentra su password para poder acceder a la aplicación desde su Tablet.</p> <p>Sera necesario cambiar su password la primera vez que ingrese.</p> <h3>&nbsp;</h3> <p>Password: <#PASSWORD#></p> <h3>&nbsp;</h3> </div> </div> <div class=\"footer\"> <p>Grupo Richard © 2014 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> <p class=\"credits\">&nbsp;</p> </div> </div> </body> </html>";
	
	
	private static final String HTML_SUBJECT_PAGO_PRODUCTOS = "Acuse On Comercio - Referencia: ";
	private static final String HTML_BODY_PAGO_PRODUCTOS = 
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Bienvenidos a On Comercio</title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> <link rel=\"icon\" type=\"image/x-icon\" href=\"http://www.gruporichard.com.mx/sitio/favicon.ico\"> </script> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 10px; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } /*Font-size: 1.0em = 10px when browser default size is 16px*/ .page-container { width: 900px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .main { clear: both; width: 900px; padding-bottom: 30px; }  .main-content { display: inline; /*Fix IE floating margin bug*/; float: left; width: 840px; margin: 0 0 0 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer { clear: both; width: 900px; height: 7em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; } /* MAIN CONTENT */ .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; border-bottom: solid 7px rgb(0, 26, 51); font-family: Verdana, Geneva, sans-serif; color: #184274; font-weight: bold; font-size: 220%; }  .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; /* font-weight: bold; */ color: #2A5A8A; }  .main-content table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; } .main-content table td { height: 2.5em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); text-align: center; font-weight: normal; color: #184274; font-size: 11.5px; } /* HEADER */ .header { width: 900px; font-family: \"trebuchet ms\", arial, sans-serif; }  .header-top { width: 900px; height: 162px; background: rgb(24, 66, 116) url(bg_head_top_5.jpg); overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .header-breadcrumbs { clear: both; width: 900px; padding: 1.0em 0 1.5em 0; background: rgb(100, 100, 100) } /* MAIN CONTENT */ .column1-unit { width: 840px; margin-bottom: 10px !important /*Non-IE6*/; margin-bottom: 5px /*IE6*/; } /*  FOOTER SECTION  */ .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 110%; }  .footer p.credits { font-weight: normal; } </style> </head> <body> <div class=\"page-container\"> <div class=\"header\"> <div class=\"header-top\"> <img src=\"cid:identifierCID00\" alt=\"\" /> </div> <div class=\"header-breadcrumbs\"></div> </div> <br> <div class=\"main-content\"> <h1 class=\"pagetitle\">Estimado (a): <#NOMBRE#></h1> <div class=\"column1-unit\"> <p> Gracias por realizar su compra con nosotros. <strong> On Comercio </strong>. </p> <p>Acontinuación se encuentran los datos de su compra.</p> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Producto:</td> <td colspan=\"3\"><#PRODUCTO#></td> </tr> <tr> <td>Monto:</td> <td><#MONTO#></td> <td>Fecha:</td> <td><#FECHA#></td> </tr> <tr> <td>Autorizacion Bancaria:</td> <td><#AUTBAN#></td> <td>Referencia:</td> <td><#REFE#></td> </tr> <tr> <td>Tarjeta:</td> <td><#TARJETA#></td> <td></td> <td></td> </tr> </tbody> </table> <h3>&nbsp;</h3> </div> </div> <div class=\"footer\"> <p>Nota. Este correo es de caráter informativo, no es necesario que responda al mismo.</p> <br /> <p>Grupo Richard © 2014 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> <p class=\"credits\">&nbsp;</p> </div> </div> </body> </html>";
	
	
	private static final String HTML_BODY__ERROR_24 = 
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> <html> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>  <title>:: Chedraui :: - Contacto</title>  <style type=\"text/css\"> body { font-family: Verdana, Geneva, sans-serif; font-size: 12px; padding: 0px; margin: 0px; }  body { background-color: white; font-family: Verdana; }  .content { font-stretch: normal; font-weight: normal; width: 715px; color: #6e767e; font-size: 13px; padding: 20px; font-family: \"Lucida Sans Unicode\", \"Lucida Grande\", sans-serif; }  .titulo { font-weight: bold; color: #ff6c00; font-size: 25px; border-bottom: thin; border-bottom-color: #d1d1d1; border-bottom-style: solid; padding-bottom: 20px; padding-top: 20px; font-family: Verdana, Geneva, sans-serif; }  .linea { border-bottom: thin; border-bottom-color: #d1d1d1; border-bottom-style: solid; }  .texto_naranja { color: #ff6c00; }  .shadow { -moz-box-shadow: 0 0 20px 15px #C2C2C2; -webkit-box-shadow: 0 0 20px 15px #C2C2C2; box-shadow: 0 0 20px 15px #C2C2C2; }  #page { width: 800px; margin: auto; position: relative; top: 0px; background-color: #f0f0f0; }  #header { width: 800px; height: 144px; position: relative; }  .share_btn { position: fixed; cursor: default; margin-top: 200px; left: -10px; }  #logo { width: 181px; height: 137px; position: absolute; top: 2px; left: 5px; }  #content { position: relative; display: block; }  .container { width: 800px; margin: 0 auto; position: relative; }  #foot { width: 970px; position: relative; clear: both; }  #contentr { float: left; padding: 20px; background-color: #FFF; background-position: left; background-repeat: repeat-y; color: #6e767e; font-size: 12px; width: 760px; min-height: 300px; z-index: 2; }  .footer { width: 800px; }  .footer_titulo { color: #ff6c00; font-family: Verdana, Geneva, sans-serif; font-weight: bold; display: block; margin-left: 25px; text-align: left; }  #footer_chedraui { border-top: 1px solid #FFF; border-right: 1px solid #FFF; padding-top: 0px; }  #footer_mail { border-bottom: 1px solid #d1d1d1; border-right: 1px solid #FFF; } </style>  </head>  <body> <!-- share --> <div class=\"share_btn\"> <img src=\"cid:identifierCID00\" usemap=\"#Map\" height=\"348\" border=\"0\" width=\"130\"/> <map name=\"Map\"> <area shape=\"rect\" coords=\"26,95,88,158\" href=\"http://www.facebook.com/CHEDRAUIOFICIAL\" target=\"_blank\"/> <area shape=\"rect\" coords=\"25,171,91,235\" href=\"https://twitter.com/#%21/Chedrauioficial\" target=\"_blank\"/> <area shape=\"rect\" coords=\"25,247,90,310\" href=\"http://www.facebook.com/CHEDRAUIOFICIAL\" target=\"_blank\"/> </map> </div> <!--share-->  <div id=\"page\" class=\"shadow\"> <div id=\"header\"> <div id=\"logo\"> <img src=\"cid:identifierCID00\" border=\"0\"/> </div> </div> <!-- header -->  <div id=\"content\"> <div class=\"container\"> <div id=\"contentr\"> <div id=\"content\"> <div class=\"linea\"> <div class=\"titulo\">Estimado <#USUARIO#></div> </div> <div class=\"linea\"> <p> <span class=\"texto_naranja\"><b>¡Tarjeta Declinada!</b> </span><br/> Favor de validar con su Banco: </p> </div> <div> <br/> <!-- some parts of the form --> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"1\"> <tbody> <tr> <td width=\"100px\"></td> <td>Status:</td> <td><b><#STATUS#></b></td> <td width=\"100px\"></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> <tr> <td width=\"100px\"></td> <td>Producto:</td> <td><b><#PRODUCTO#></b></td> <td width=\"100px\"></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> <tr> <td></td> <td>Monto:</td> <td><b><#MONTO#></b></td> <td></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> <tr> <td></td> <td>Moneda:</td> <td><b><#MONEDA#></b></td> <td></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> <tr> <td></td> <td>Fecha:</td> <td><b><#FECHA#></b></td> <td></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> <tr> <td></td> <td>Tarjeta:</td> <td><b><#TARJETA#></b></td> <td></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> <tr> <td></td> <td>Referencia:</td> <td><b><#REFE#></b></td> <td></td> </tr> <tr> <td colspan=\"4\"><br/> <div class=\"linea\"></div></td> </tr> </tbody> </table> </div> <!-- content --> </div> </div>  </div> <!-- footer --> <div id=\"foot\"> <table id=\"footer\" height=\"200\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"800px\"> <tbody> <tr> <td id=\"footer_mail\" height=\"30\"><p class=\"footer_titulo\" style=\"font-size: 12px;\">Este correo es de caracter informativo, no es necesario que responda al mismo.</p></td>  </tr> <tr> <td id=\"footer_chedraui\" valign=\"top\" width=\"100%\"><p class=\"footer_titulo\" style=\"font-size: 16px;\">Gracias por usarCard.</p> <div align=\"center\"> <img src=\"cid:identifierCID02\" height=\"129\" border=\"0\" width=\"188\"> </div></td> </tr> </tbody> </table> </div> </div> <!-- page --> </div> </body> </html>";
	
	
	private static final String HTML_SUBJECT_PASS = "Cambio Password - On Comercio";
	private static final String HTML_BODY_PASS =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Bienvenidos a On Comercio</title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />  <link rel=\"icon\" type=\"image/x-icon\" href=\"http://www.gruporichard.com.mx/sitio/favicon.ico\"> </script> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } /*Font-size: 1.0em = 10px when browser default size is 16px*/ .page-container { width: 900px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .main { clear: both; width: 900px; padding-bottom: 30px; }  .main-content { display: inline; /*Fix IE floating margin bug*/; float: left; width: 840px; margin: 0 0 0 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer { clear: both; width: 900px; height: 5em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; } /* MAIN CONTENT */ .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; border-bottom: solid 7px rgb(0, 26, 51); font-family: Verdana, Geneva, sans-serif; color: #184274; font-weight: bold; font-size: 220%; }  .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; /* font-weight: bold; */ color: #2A5A8A; } /* HEADER */ .header { width: 900px; font-family: \"trebuchet ms\", arial, sans-serif; }  .header-top { width: 900px; height: 162px; background: rgb(24, 66, 116) url(bg_head_top_5.jpg); overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .header-breadcrumbs { clear: both; width: 900px; padding: 1.0em 0 1.5em 0; background: rgb(100, 100, 100) } /* MAIN CONTENT */ .column1-unit { width: 840px; margin-bottom: 10px !important /*Non-IE6*/; margin-bottom: 5px /*IE6*/; } /*  FOOTER SECTION  */ .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 110%; }  .footer p.credits { font-weight: normal; } </style> </head> <body> <div class=\"page-container\"> <div class=\"header\"> <div class=\"header-top\"> <img src=\"cid:identifierCID00\" alt=\"\" /> </div> <div class=\"header-breadcrumbs\"></div> </div> <br> <div class=\"main-content\"> <h1 class=\"pagetitle\">Estimado (a): <#NOMBRE#></h1> <div class=\"column1-unit\"> <p> Gracias por usar la aplicación <strong> On Comercio </strong>. </p> <p>Su password a cambiado, acontinuación se encuentra el nuevo password.</p> <h3>&nbsp;</h3> <p>Password: <#PASSWORD#></p> <h3>&nbsp;</h3> </div> </div> <div class=\"footer\"> <p>Grupo Richard © 2014 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> <p class=\"credits\">&nbsp;</p> </div> </div> </body> </html>";
	
	
	private static final String HTML_SUBJECT_PASS_RESET = "Recuperar Password - On Comercio";
	private static final String HTML_BODY_PASS_RESET =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Bienvenidos a On Comercio</title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />  <link rel=\"icon\" type=\"image/x-icon\" href=\"http://www.gruporichard.com.mx/sitio/favicon.ico\"> </script> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } /*Font-size: 1.0em = 10px when browser default size is 16px*/ .page-container { width: 900px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .main { clear: both; width: 900px; padding-bottom: 30px; }  .main-content { display: inline; /*Fix IE floating margin bug*/; float: left; width: 840px; margin: 0 0 0 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer { clear: both; width: 900px; height: 5em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; } /* MAIN CONTENT */ .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; border-bottom: solid 7px rgb(0, 26, 51); font-family: Verdana, Geneva, sans-serif; color: #184274; font-weight: bold; font-size: 220%; }  .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; /* font-weight: bold; */ color: #2A5A8A; } /* HEADER */ .header { width: 900px; font-family: \"trebuchet ms\", arial, sans-serif; }  .header-top { width: 900px; height: 162px; background: rgb(24, 66, 116); overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .header-breadcrumbs { clear: both; width: 900px; padding: 1.0em 0 1.5em 0; background: rgb(100, 100, 100) } /* MAIN CONTENT */ .column1-unit { width: 840px; margin-bottom: 10px !important /*Non-IE6*/; margin-bottom: 5px /*IE6*/; } /*  FOOTER SECTION  */ .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 110%; }  .footer p.credits { font-weight: normal; } </style> </head> <body> <div class=\"page-container\"> <div class=\"header\"> <div class=\"header-top\"> <img src=\"cid:identifierCID00\" alt=\"\" /> </div> <div class=\"header-breadcrumbs\"></div> </div> <br> <div class=\"main-content\"> <h1 class=\"pagetitle\">Estimado (a): <#NOMBRE#></h1> <div class=\"column1-unit\"> <p> Gracias por usar la aplicación <strong> On Comercio </strong>. </p> <p>Se a solicitado el reset de su password, acontinuación se encuentra el nuevo password.</p> <p>Sera necesario cambiar su password la primera vez que ingrese.</p> <h3>&nbsp;</h3> <p>Password: <#PASSWORD#></p> <h3>&nbsp;</h3> </div> </div> <div class=\"footer\"> <p>Grupo Richard © 2014 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> <p class=\"credits\">&nbsp;</p> </div> </div> </body> </html>";
	
	private static final String HTML_SUBJECT_ALTA_CARNET = "Alta Tarjeta - On Comercio";
	private static final String HTML_BODY_ALTA_CARNET =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Bienvenidos a Grupo Richard</title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> <link rel=\"icon\" type=\"image/x-icon\" href=\"http://www.gruporichard.com.mx/sitio/favicon.ico\"> </script> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } /*Font-size: 1.0em = 10px when browser default size is 16px*/ .page-container { width: 900px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .main { clear: both; width: 900px; padding-bottom: 30px; }  .main-content { display: inline; /*Fix IE floating margin bug*/; float: left; width: 840px; margin: 0 0 0 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer { clear: both; width: 900px; height: 5em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; } /* MAIN CONTENT */ .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; border-bottom: solid 7px rgb(0, 26, 51); font-family: Verdana, Geneva, sans-serif; color: #184274; font-weight: bold; font-size: 220%; }  .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; /* font-weight: bold; */ color: #2A5A8A; } /* HEADER */ .header { width: 900px; font-family: \"trebuchet ms\", arial, sans-serif; }  .header-top { width: 900px; height: 162px; background: rgb(24, 66, 116); overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .header-breadcrumbs { clear: both; width: 900px; padding: 1.0em 0 1.5em 0; background: rgb(100, 100, 100) } /* MAIN CONTENT */ .column1-unit { width: 840px; margin-bottom: 10px !important /*Non-IE6*/; margin-bottom: 5px /*IE6*/; } /*  FOOTER SECTION  */ .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 100%; }  .footer p.credits { font-weight: normal; } </style> </head> <body> <div class=\"page-container\"> <div class=\"header\"> <div class=\"header-top\"> <img src=\"cid:identifierCID00\" alt=\"\" /> </div> <div class=\"header-breadcrumbs\"></div> </div> <br> <div class=\"main-content\"> <h1 class=\"pagetitle\">Solicitud de ALTA Tarjeta.</h1> <div class=\"column1-unit\"> <p> Se solicita el alta de la tarjeta para su uso en la aplicación <strong> On Comercio</strong>. </p> <p>·  Nombre Cliente: &nbsp;&nbsp;&nbsp;&nbsp;<strong> <#NOMBRE#></strong></p> <p>·  Número de Tarjeta: &nbsp;&nbsp;&nbsp;<strong> <#CARNET#></strong></p> <p>·  Vigencia: &nbsp;&nbsp;&nbsp;<strong> <#VIGENCIA#></strong></p> <h3>&nbsp;</h3> </div> </div> <div class=\"footer\"> <p>Grupo Richard © 2014 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> <p class=\"credits\">&nbsp;</p> </div> </div> </body> </html>";

	private static final String FROM_INFO_ADDCEL = "info@addcel.com";
	
	public static CorreoVO generatedMailRegistro(String email, String passwd, String nombres){
		logger.info("Genera objeto Registro para envio de mail: " + email);
		
		CorreoVO correo = new CorreoVO();
		try{
			String body = HTML_BODY_REG.toString();
			body = body.replaceAll("<#NOMBRE#>", nombres);
			body = body.replaceAll("<#PASSWORD#>", passwd);
			
			String[] to = {email};
			String[] cid = {
					"/usr/java/resources/images/GrupoRichard/GrupoRichard_head.png"
					};
	
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(FROM_INFO_ADDCEL);
			correo.setSubject(HTML_SUBJECT_REG);
			correo.setTo(to);
			
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static CorreoVO generatedMailPagoProductos(DatosPago datosPagoVO, DatosPagoResponse datosPagoResponse){
		logger.info("Genera objeto pago de productos para envio de mail: " + datosPagoVO.getMail());
		
		CorreoVO correo = new CorreoVO();
		String body = null;
		String[] cid = null;
				
		try{
			body = HTML_BODY_PAGO_PRODUCTOS.toString();
			body = body.replaceAll("<#NOMBRE#>", datosPagoVO.getNombres());
			body = body.replaceAll("<#PRODUCTO#>", datosPagoVO.getDescProducto());
			body = body.replaceAll("<#MONTO#>", Utilerias.formatoImporteMon(datosPagoVO.getTotal()));
			body = body.replaceAll("<#FECHA#>", (datosPagoResponse.getFecha()!= null ? datosPagoResponse.getFecha().substring(0, 19) : ""));
			body = body.replaceAll("<#AUTBAN#>", datosPagoResponse.getNumAutorizacion());
			body = body.replaceAll("<#REFE#>", String.valueOf(datosPagoResponse.getIdBitacora()));
			if(datosPagoVO.getIdTipoTarjeta() != 0){
				body = body.replaceAll("<#TARJETA#>", "XXXX XXXX XXXX " + datosPagoVO.getTarjeta().substring(datosPagoVO.getTarjeta().length() -4 , datosPagoVO.getTarjeta().length()));
			}else{
				body = body.replaceAll("<#TARJETA#>", "EFECTIVO");
			}
			
			
			cid = new String[]{
					"/usr/java/resources/images/GrupoRichard/GrupoRichard_head.png"
					};
			
			String[] to = {datosPagoVO.getMail()};
			
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(FROM_INFO_ADDCEL);
			correo.setSubject(HTML_SUBJECT_PAGO_PRODUCTOS + datosPagoResponse.getIdBitacora());
			correo.setTo(to);
			
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static CorreoVO generatedMailError(DatosPago datosPagoVO, String status){
		logger.info("Genera objeto pago de productos error para envio de mail Error: " + datosPagoVO.getMail());
		
		CorreoVO correo = new CorreoVO();
//		String body = null;
//		String subject = null;
//		
//		
//		String[] cid = null;
//				
//		try{
//			subject = "Tarjeta Declinada PAGO Chedraui - Referencia: " + datosPagoVO.getReferencia();
//			
//			body = HTML_BODY__ERROR_24.toString();
//			body = body.replaceAll("<#NOMBRE#>", datosPagoVO.getNombres());
//			
//			
//			body = body.replaceAll("<#STATUS#>", "Tarjeta Declinada");
//			body = body.replaceAll("<#TARJETA#>", "**** **** **** " + 
//					(datosPagoVO.getTarjeta()!= null? datosPagoVO.getTarjeta().substring(datosPagoVO.getTarjeta().length() - 4, datosPagoVO.getTarjeta().length()):"****") );
//			body = body.replaceAll("<#PRODUCTO#>", datosPagoVO.getDescProducto());
//			body = body.replaceAll("<#MONTO#>", Utilerias.formatoImporteMon(datosPagoVO.getTotal()));
//			body = body.replaceAll("<#MONEDA#>", "MXN");
//			body = body.replaceAll("<#FECHA#>", (datosPagoVO.getFecha()!= null ? datosPagoVO.getFecha().substring(0, 19) : ""));
//			body = body.replaceAll("<#REFE#>", String.valueOf(datosPagoVO.getIdBitacora()));
//			
//			
//			String[] to = {datosPagoVO.getMail()};
//			cid = new String[]{
//					"/usr/java/resources/images/Chedraui/share_btn.png",
//					"/usr/java/resources/images/Chedraui/logo.png",
//					"/usr/java/resources/images/Chedraui/great_place.png"
//					};
//	//		correo.setAttachments(attachments);
//			correo.setBcc(new String[]{});
//			correo.setCc(new String[]{});
//			correo.setCid(cid);
//			correo.setBody(body);
//			correo.setFrom(from);
//			correo.setSubject(subject);
//			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
//		}catch(Exception e){
//			correo = null;
//			logger.error("Ocurrio un error al enviar el email ", e);
//		}
		return correo;
	}
	
	public static CorreoVO generatedMailPassword(String email, String passwd, String nombres){
		logger.info("Genera objeto cambio de password para envio de mail: " + email);
		
		CorreoVO correo = new CorreoVO();
		try{
			String body = HTML_BODY_PASS.toString();
			body = body.replaceAll("<#NOMBRE#>", nombres);
			body = body.replaceAll("<#PASSWORD#>", passwd);
			
			String[] to = {email};
			String[] cid = {
					"/usr/java/resources/images/GrupoRichard/GrupoRichard_head.png"
					};
	
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(FROM_INFO_ADDCEL);
			correo.setSubject(HTML_SUBJECT_PASS);
			correo.setTo(to);
			
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static CorreoVO generatedMailPasswordReset(String email, String passwd, String nombres){
		logger.info("Genera objeto reset password para envio de mail: " + email);
		
		CorreoVO correo = new CorreoVO();
		try{
			String body = HTML_BODY_PASS_RESET.toString();
			body = body.replaceAll("<#NOMBRE#>", nombres);
			body = body.replaceAll("<#PASSWORD#>", passwd);
			
			String[] to = {email};
			String[] cid = {
					"/usr/java/resources/images/GrupoRichard/GrupoRichard_head.png"
					};
	
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(FROM_INFO_ADDCEL);
			correo.setSubject(HTML_SUBJECT_PASS_RESET);
			correo.setTo(to);
			
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static CorreoVO generatedMailAltaTarjeta(TiendaPortalVO tienda){
		
		CorreoVO correo = new CorreoVO();
		try{
			String body = HTML_BODY_ALTA_CARNET.toString();
			body = body.replaceAll("<#NOMBRE#>", tienda.getNombreTitular());
			body = body.replaceAll("<#CARNET#>", tienda.getMascara());
			body = body.replaceAll("<#VIGENCIA#>", tienda.getVigencia());
			
			String[] to = {"agalvanl@abccapital.com.mx"};
			String[] cid = {
					"/usr/java/resources/images/GrupoRichard/GrupoRichard_head.png"
					};
	
			correo.setCc(new String[]{"jorge@addcel.com", "sistemas@grupointl.com"});
			correo.setBcc(new String[]{"jesus.lopez@addcel.com"});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(FROM_INFO_ADDCEL);
			correo.setSubject(HTML_SUBJECT_ALTA_CARNET);
			correo.setTo(to);
			
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			//logger.debug("data = " + data);

			URL url = new URL(ConstantesGpoRichard.URL_MAIL_SENDER);
			logger.info("Conectando con " + ConstantesGpoRichard.URL_MAIL_SENDER);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
