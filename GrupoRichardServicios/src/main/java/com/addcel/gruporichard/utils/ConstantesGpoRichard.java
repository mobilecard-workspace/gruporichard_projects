package com.addcel.gruporichard.utils;

import java.util.ArrayList;
import java.util.List;

import com.addcel.gruporichard.model.vo.common.TipoTarjetaVO;

public class ConstantesGpoRichard {
	
	public static final int ID_PROVEEDOR = 26 ;
	public static final String PARAMETER_NAME_JSON = "json=";
	
	//variables PROD o QA
	
	public static final String URL_AUTH_PROSA = "https://localhost:8443/ProsaWeb/ProsaAuth?";
//	public static final String URL_MAIL_SENDER = "https://localhost:8443/MailSenderAddcel/enviaCorreoAddcel";
	public static final String URL_PAGO_TAE = "https://localhost:8443/AddCelBridge/Servicios/adc_purchase.jsp";
	public static final String URL_PAGO_IAVE = "https://localhost:8443/AddCelBridge/Servicios/adc_purchase_iave.jsp";
	public static final String URL_PAGO_PASE = "https://localhost:8443/AddCelBridge/Servicios/adc_purchase_pase.jsp";
	
	public static final String URL_TOKEN_MEGA = "https://localhost:8443/MegacableServicios/getToken";
	public static final String URL_SALDO_MEGA = "https://localhost:8443/MegacableServicios/consultaSaldo";
	public static final String URL_PAGO_MEGA = "https://localhost:8443/MegacableServicios/pago-externo";
	
	public static final String URL_MONTO_INJET = "https://localhost:8443/InterjetWeb/getPNR3";
	public static final String URL_TRAN_INJET = "https://localhost:8443/InterjetWeb/getInterjetTransaction";
//	public static final String URL_PAGO_INJET = "https://localhost:8443/ProCom/comercio_fin.jsp?";
	public static final String URL_PAGO_INJET = "https://www.mobilecard.mx/ProCom/comercio_fin.jsp?";
	
	public static final String URL_ORIG_ETN = "https://localhost:8443/ETNWSConsumer/ObtenerOrigenes";
	public static final String URL_DEST_ETN = "https://localhost:8443/ETNWSConsumer/ObtenerDestinos";
	public static final String URL_CORR_ETN = "https://localhost:8443/ETNWSConsumer/ObtenerCorridas";
	public static final String URL_ITINE_ETN = "https://localhost:8443/ETNWSConsumer/ConsultarItinerario";
	public static final String URL_OCUPA_ETN = "https://localhost:8443/ETNWSConsumer/ConsultarOcupacion";
	public static final String URL_DIAG_ETN = "https://localhost:8443/ETNWSConsumer/ConsultarDiagrama";
	public static final String URL_BOLT_ETN = "https://localhost:8443/ETNWSConsumer/ConsultaBoletoEtn";
	public static final String URL_RESE_ETN = "https://localhost:8443/ETNWSConsumer/ConsultarReservacion";
	public static final String URL_SEC_ETN = "https://localhost:8443/AddCelBridge/ClienteGeneraSecuencia";
//	public static final String URL_PAGO_ETN = "https://localhost:8443/ProcomETN/envio.jsp";
//	public static final String URL_PAGO_ETN = "https://50.57.192.214/ProcomETN/envio.jsp";
	public static final String URL_PAGO_ETN = "https://www.mobilecard.mx/ProcomETN/envio.jsp";
	
	public static final String URL_TOKEN_DIESTEL = "https://localhost:8443/DiestelServices/getToken";
	public static final String URL_CATE_DIESTEL = "https://localhost:8443/DiestelServices/ObtenCategorias";
	public static final String URL_MONTO_DIESTEL = "https://localhost:8443/DiestelServices/montosRecarga";
	public static final String URL_CAT_SER_DIESTEL = "https://localhost:8443/DiestelServices/obtenCatalogoServicios";
	public static final String URL_INFO_SER_DIESTEL = "https://localhost:8443/DiestelServices/obtenInfoPorServicio";
	public static final String URL_PAGO_REC_DIESTEL = "https://localhost:8443/DiestelServices/realizaRecarga";
	public static final String URL_PAGO_SERV_DIESTEL = "https://localhost:8443/DiestelServices/realizaPagoServicio";
	
//	public static final String URL_TOKEN_DIESTEL = "https://50.57.192.214:8443/DiestelServices/getToken";
//	public static final String URL_CATE_DIESTEL = "https://50.57.192.214:8443/DiestelServices/ObtenCategorias";
//	public static final String URL_MONTO_DIESTEL = "https://50.57.192.214:8443/DiestelServices/montosRecarga";
//	public static final String URL_CAT_SER_DIESTEL = "https://50.57.192.214:8443/DiestelServices/obtenCatalogoServicios";
//	public static final String URL_INFO_SER_DIESTEL = "https://50.57.192.214:8443/DiestelServices/obtenInfoPorServicio";
//	public static final String URL_PAGO_REC_DIESTEL = "https://50.57.192.214:8443/DiestelServices/realizaRecarga";
//	public static final String URL_PAGO_SERV_DIESTEL = "https://50.57.192.214:8443/DiestelServices/realizaPagoServicio";

	
	//variables locales
	
//	public static String URL_AUTH_PROSA = "https://50.57.192.214:8443/ProsaWeb/ProsaAuth";
	public static String URL_MAIL_SENDER = "https://50.57.192.214:8443/MailSenderAddcel/enviaCorreoAddcel";
//	public static final String URL_PAGO_TAE = "https://localhost:8081/AddCelBridge/Servicios/adc_purchase.jsp";
//	public static final String URL_PAGO_IAVE = "https://localhost:8081/AddCelBridge/Servicios/adc_purchase_iave.jsp";
//	public static final String URL_PAGO_PASE = "https://localhost:8081/AddCelBridge/Servicios/adc_purchase_pase.jsp";
//	public static final String URL_PAGO_TAE = "https://50.57.192.214:8081/AddCelBridge/Servicios/adc_purchase.jsp";
//	public static final String URL_PAGO_IAVE = "https://50.57.192.214:8081/AddCelBridge/Servicios/adc_purchase_iave.jsp";
//	public static final String URL_PAGO_PASE = "https://50.57.192.214:8081/AddCelBridge/Servicios/adc_purchase_pase.jsp";
	
	public static final String URL_FOLDER_ARCHIVOS = "/usr/java/resources/files/GrupoRichard/";
	
	public static final String DEFAULT_PASSWORD = "oncom3rci0";
	
	public static final String ERROR_USUARIO_STATUS_INACTIVO = "Usuario Inactivo o dado de Baja. Consulte con el Administrador.";
	public static final String ERROR_USUARIO_NO_EXISTE = "Usuario o Password incorrectos.";
	
	public static final String MESS_USUARIO_STATUS_CAM_PASS = "Se requiere cambio de contraseña";
	public static final String MESS_USUARIO_STATUS_CAM_DATA = "Modifique su Informacion por seguridad";
	
	public static final String NOM_CAMPO_ID_ERROR = "idError";
	public static final String NOM_CAMPO_MENJ_ERROR = "mensajeError";
	public static final String NOM_CAMPO_TIENDAS = "tiendas";
	public static final String NOM_CAMPO_ID_TIENDA = "idTienda";
	public static final String NOM_CAMPO_ID_USUARIO = "idUsuario";
	public static final String NOM_CAMPO_ROLES = "roles";
	public static final String NOM_CAMPO_USUARIOS = "usuarios";
	public static final String NOM_CAMPO_PAGOS = "pagos";
	public static final String NOM_CAMPO_COMBO_KITS = "comboKits";
	
	public static final String USER_TOKEN_MEGA = "userPrueba";
	public static final String PASS_TOKEN_MEGA = "passwordPrueba";
	
	public static final int ID_NO_SESSION = 1000;
	public static final String ERROR_NO_SESSION = "NO existe una session activa.";
	
	public static final  String CONSULTA_MONEDERO_EXITO = "<T24result> <head> <resultId>1000007253</resultId> <messageId /> </head> <body> <table columnsCount=\"20\" id=\"1\" rowsCount=\"1\"> <row> <field mv=\"1\" name=\"NO.CTE\" sv=\"1\">1000007253</field> <field mv=\"1\" name=\"NOM.CTE\" sv=\"1\">EMPRESA X</field> <field mv=\"1\" name=\"TIPO.CTE\" sv=\"1\">3</field> <field mv=\"1\" name=\"DIR.1\" sv=\"1\">CALLE 101 8</field> <field mv=\"1\" name=\"DIR.2\" sv=\"1\">RESIDENCIAL SAN AGUSTIN 66260</field> <field mv=\"1\" name=\"DIR.3\" sv=\"1\">SAN PEDRO GARZA GARCIA NUEVO LEON</field> <field mv=\"1\" name=\"RFC\" sv=\"1\">XXXX010101XYZ</field> <field mv=\"1\" name=\"CURP\" sv=\"1\">XXXX010101MNLXXX00</field> <field mv=\"1\" name=\"FEC.NACIMIENTO\" sv=\"1\">19880831</field> <field mv=\"1\" name=\"CORREO\" sv=\"1\">atovar@abccapital.com.mx</field> <field mv=\"1\" name=\"CELULAR\" sv=\"1\">1234567890</field> <field mv=\"1\" name=\"NUM.CTA\" sv=\"1\">00000006043</field> <field mv=\"1\" name=\"PROD.CTA\" sv=\"1\">Cta. Sin Chequera sin Intereses</field> <field mv=\"1\" name=\"CLABE\" sv=\"1\">138580000000060432</field> <field mv=\"1\" name=\"GPO.CLUB\" sv=\"1\">30</field> <field mv=\"1\" name=\"EXENTO.IMP\" sv=\"1\">S</field> <field mv=\"1\" name=\"SAL.CTA\" sv=\"1\">1500.00</field> <field mv=\"1\" name=\"SAL.BLO.CTA\" sv=\"1\">0.00</field> <field mv=\"1\" name=\"SAL.SBC.CTA\" sv=\"1\">0.00</field> <field mv=\"1\" name=\"SAL.DISP.CTA\" sv=\"1\">12000.00</field> </row> </table> </body> </T24result>";
	public static final String CONSULTA_MONEDERO_ERROR = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <T24result>  <body>  <error> <message>El resultado indica que no se encontro informacion: \"No records were found that matched the selection criteria\"</message> <source>mx.com.ixpan.core.IxpanCoreException</source> <stackTrace>mx.com.ixpan</stackTrace> </error> </body> </T24result>";
	
	public static List<TipoTarjetaVO> TIPO_TARJETA =  null;
	
	static{
		TIPO_TARJETA =  new ArrayList<TipoTarjetaVO>();
		TIPO_TARJETA.add(new TipoTarjetaVO(0, "EFECTIVO"));
		TIPO_TARJETA.add(new TipoTarjetaVO(1, "VISA"));
		TIPO_TARJETA.add(new TipoTarjetaVO(2, "MASTER CARD"));
		TIPO_TARJETA.add(new TipoTarjetaVO(4, "CARNET"));
	}
}
