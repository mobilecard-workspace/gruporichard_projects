package com.addcel.gruporichard.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.gruporichard.model.mapper.UtilsMapper;

@Component
public class StaticService {
	private static final Logger logger = LoggerFactory.getLogger(StaticService.class);
	
	@Autowired
	private UtilsMapper utilsMapper;	
	
	public String getTParametro(String clave){
		return utilsMapper.getTParametro(clave);
	}
}
