package com.addcel.gruporichard.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.addcel.gruporichard.model.vo.common.AbstractVO;
import com.addcel.gruporichard.model.vo.common.TiendaPortalVO;

public class UtilsFileService extends AbstractVO{
	private static final Logger logger = LoggerFactory.getLogger(UtilsFileService.class);
	
	public static final String patronCom = "yyyy-MM-dd HH-mm-ss";
	public static final SimpleDateFormat formatoFull = new SimpleDateFormat(patronCom);
	
	private MultipartFile archivo;
	private File archivoDestino;
	private InputStreamReader fileReader;
	private BufferedReader buffReader;
	
	private String type;
	private int numberLine; 
	private String fileName;
	private List<HashMap<String, String>> data;
	
	public UtilsFileService(){
		
	}
	
//	public UtilsFileService(File archivoDestino) throws FileNotFoundException{
//		this.archivoDestino = archivoDestino;
//		this.fileReader = new FileReader (this.archivoDestino);
//		this.buffReader = new BufferedReader(this.fileReader);
//	}
	
	public UtilsFileService(MultipartFile archivo) throws IOException{
//		this.archivoDestino = archivoDestino;
		this.archivo = archivo;
		this.fileName = archivo.getOriginalFilename();
		this.fileReader = new InputStreamReader(archivo.getInputStream());
		this.buffReader = new BufferedReader(this.fileReader);
		
		validateFile(this.archivo);
	}
	
	public void validateFile(MultipartFile archivo){				
		try {
			if (archivo == null) {
				setIdError(310);
				setMensajeError("EL archivo es obligatorio.");
			}else if (!archivo.isEmpty() && !archivo.getContentType().equals("text/plain")) {
				setIdError(311);
				setMensajeError("Tipo de archivo o extención no validos.");
			}else if (archivo.isEmpty()) {
//				setIdError(301);
//				setMensajeError("EL archivo " + archivo.getOriginalFilename() + " esta vacio.");
				logger.error("El archivo esta vacio: {}", archivo.getOriginalFilename());
			} else {
				
				logger.debug("Validando Archivo: " + archivo.getOriginalFilename());
				
				this.fileName = archivo.getOriginalFilename();
				this.fileReader = new InputStreamReader(archivo.getInputStream());
				this.buffReader = new BufferedReader(this.fileReader);
				
				if("archivos[0]".equals(archivo.getName())){
					this.type = "Tiendas";
					readFileDataStore();
				}else if("archivos[1]".equals(archivo.getName())){
					this.type = "IMEI";
					readFileDataIMEI();
				}else if("archivos[2]".equals(archivo.getName())){
					this.type = "CARNET";
					readFileDataCARNET();
				}else if("archivos[3]".equals(archivo.getName())){
					this.type = "Dispositivo";
					readFileDataDevice();
				}else if("archivos[4]".equals(archivo.getName())){
					this.type = "SIM";
					readFileDataSIM();
				}else{
					setIdError(310);
					setMensajeError("Tipo de archivo no valido.");
				}
			} 
		
		}catch (Exception pe) {
			setIdError(301);
			setMensajeError("Ocurrio un error al validar el archivo: {}" + archivo.getOriginalFilename());
			logger.error("Ocurrio un error al validar el archivo: {}", pe);
		}finally{
			//closeBufferedReader();
		}
	}
	
	public void readFileDataStore(){
		this.numberLine = 0;
		this.data = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> field = null;
		String line = null;
		String[] dataLine = null;
		
		try {
			while ((line = this.buffReader.readLine()) != null){
				this.numberLine++;
				
				dataLine = line.split("\\|");
				field = new HashMap<String, String>();
				
				field.put("nombre", dataLine[0]);
				field.put("calle", dataLine[1]);
				field.put("numExt", dataLine[2]);
				field.put("numInt", dataLine[3]);
				field.put("colonia", dataLine[4]);
				field.put("cp", dataLine[5]);
				field.put("ciudad", dataLine[6]);
				field.put("estado", dataLine[7]);
				field.put("cx", dataLine[8]);
				field.put("cy", dataLine[9]);
				
				this.data.add(field);
			}
		}catch (Exception pe) {
			setIdError(310);
			setMensajeError("Formato no valido archivo de Tiendas: " + this.fileName);
			logger.error("Ocurrio un error al leer el archivo de Tiendas: {}", pe);
		}
	}
	
	public void readFileDataIMEI(){
		this.numberLine = 0;
		this.data = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> field = null;
		String line = null;
		String[] dataLine = null;
		
		try {
			while ((line = this.buffReader.readLine()) != null){
				this.numberLine++;
				
				dataLine = line.split("\\|");
				field = new HashMap<String, String>();
				
				field.put("imei", dataLine[0]);
				field.put("marca", dataLine[1]);
				field.put("modelo", dataLine[2]);
				
				this.data.add(field);
			}
		}catch (Exception pe) {
			setIdError(310);
			if(getMensajeError() != null){
				setMensajeError("<BR/>Formato no valido archivo de Tablets: " + this.fileName);
			}else{
				setMensajeError("Formato no valido archivo de Tablets: " + this.fileName);
			}
			
			logger.error("Ocurrio un error al leer el archivo de Tablets: {}", pe);
		}
	}
	
	public void readFileDataCARNET(){
		this.numberLine = 0;
		this.data = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> field = null;
		String line = null;
		String[] dataLine = null;
		
		try {
			while ((line = this.buffReader.readLine()) != null){
				this.numberLine++;
				
				dataLine = line.split("\\|");
				field = new HashMap<String, String>();
				
				field.put("folio", dataLine[0]);
				field.put("mascara", dataLine[1].substring(dataLine[1].length() - 4));
				field.put("carnet", dataLine[1]);
				field.put("vigencia", dataLine[2]);
				
				this.data.add(field);
			}
		}catch (Exception pe) {
			setIdError(310);
			setMensajeError("Formato no valido archivo de CARNET's: " + this.fileName);
			logger.error("Ocurrio un error al leer el archivo de CARNET's: {}", pe);
		}
	}
	
	public void readFileDataDevice(){
		this.numberLine = 0;
		this.data = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> field = null;
		String line = null;
		String[] dataLine = null;
		
		try {
			while ((line = this.buffReader.readLine()) != null){
				this.numberLine++;
				
				dataLine = line.split("\\|");
				field = new HashMap<String, String>();
				
				field.put("sn", dataLine[0]);
				field.put("marca", dataLine[1]);
				field.put("modelo", dataLine[2]);
				
				this.data.add(field);
			}
		}catch (Exception pe) {
			setIdError(310);
			setMensajeError("Formato no valido archivo de Dispositivos: " + this.fileName);
			logger.error("Ocurrio un error al leer el archivo de Dispositivos: {}", pe);
		}
	}
	
	public void readFileDataSIM(){
		this.numberLine = 0;
		this.data = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> field = null;
		String line = null;
		String[] dataLine = null;
		
		try {
			while ((line = this.buffReader.readLine()) != null){
				this.numberLine++;
				
				dataLine = line.split("\\|");
				field = new HashMap<String, String>();
				
				field.put("sim", dataLine[0]);
				field.put("telefono", dataLine[1]);
				
				this.data.add(field);
			}
		}catch (Exception pe) {
			setIdError(310);
			setMensajeError("Formato no valido archivo de SIM's: " + this.fileName);
			logger.error("Ocurrio un error al leer el archivo de SIM's: {}", pe);
		}
	}
	
	
	public boolean saveFile(String prefix){
		boolean resultado = false;
		if (!this.archivo.isEmpty()) {
			logger.debug("copiando archivo en : " + ConstantesGpoRichard.URL_FOLDER_ARCHIVOS +  prefix + " " + this.archivo.getOriginalFilename());
		
			this.archivoDestino = new File(ConstantesGpoRichard.URL_FOLDER_ARCHIVOS +  prefix + " " + this.archivo.getOriginalFilename());
			
			try {
				this.archivo.transferTo(this.archivoDestino);
				resultado = true;
			} catch (IllegalStateException e) {
				logger.error("Ocurrio un error al guardar el archivo: {}: {}", this.archivo.getOriginalFilename(), e);
			} catch (IOException e) {
				logger.error("Ocurrio un error al guardar el archivo: {}: {}", this.archivo.getOriginalFilename(), e);
			}
		} 
		return resultado;
	}
	
	public static boolean saveFile(String filePath, String fileContent, boolean cleanFileContent){
	    boolean resp = false;
	    FileWriter file = null;
	    BufferedWriter writer = null;
	     
	    try{
	    	logger.error("Guardando archivo: {}", filePath);
	        file = new FileWriter(filePath, !cleanFileContent);
	        writer = new BufferedWriter(file);
	        writer.write(fileContent, 0, fileContent.length());
	        writer.flush();
	        resp = true;
	    }catch (IOException ex){
	    	logger.error("Ocurrio un error al guardar el archivo: {}: {}", filePath, ex);
	    }finally{
	    	if( writer != null ){
	    		try {
					writer.close();
				} catch (IOException e) {
					logger.error("Ocurrio un error al cerrar el archivo Respuesta: {}", e);
				}
	    	}
	    	if( file != null ){
	    		try {
					file.close();
				} catch (IOException e) {
					logger.error("Ocurrio un error al cerrar el archivo Respuesta: {}", e);
				}
	    	}
	    }
	    return resp;
	}

	public void delete(){
		archivoDestino.delete();
		close();
	}
	
	public void close(){
		try{
			if(this.fileReader != null){
				fileReader.close();
			}
			if(this.buffReader != null){
				buffReader.close();
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al cerrar los archivos: {}", e.getMessage());
		}
		
	}
	
	public void closeBufferedReader(){
		try{
			if(this.buffReader != null){
				buffReader.close();
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al cerrar el BufferedReader: {}", e.getMessage());
		}
		
	}
	
	public static List<TiendaPortalVO> generaCombos(List<UtilsFileService> listArchivos){
		List<TiendaPortalVO> comboList = new ArrayList<TiendaPortalVO>();
		TiendaPortalVO combo = null;
		int numero = 0;
		int totalRegistros = listArchivos.get(0).getNumberLine();

		for (int i = 0; i < totalRegistros; i++ ) {
			combo = new TiendaPortalVO();
			
			for (UtilsFileService archivo : listArchivos) {
				numero = (int)(Math.random()*archivo.getData().size()); //2
				logger.debug("ciclo [ " + i + " ]:    numero: " + numero + "\t   type: " + archivo.getType() );
				
				if("Tiendas".equals(archivo.getType())){
					combo.setNombre(archivo.getData().get(numero).get("nombre"));
					combo.setNumExt(archivo.getData().get(numero).get("numExt"));
					combo.setNumInt(archivo.getData().get(numero).get("numInt"));
					combo.setCalle(archivo.getData().get(numero).get("calle"));
					combo.setColonia(archivo.getData().get(numero).get("colonia"));
					combo.setCp(archivo.getData().get(numero).get("cp"));
					combo.setCiudad(archivo.getData().get(numero).get("ciudad"));
					combo.setEstado(archivo.getData().get(numero).get("estado"));
					combo.setCx(archivo.getData().get(numero).get("cx"));
					combo.setCy(archivo.getData().get(numero).get("cy"));
					
				}else if("IMEI".equals(archivo.getType())){
					combo.setImei(archivo.getData().get(numero).get("imei"));
					
				}else if("CARNET".equals(archivo.getType())){
					combo.setMascara(archivo.getData().get(numero).get("mascara"));
					combo.setCarnet(archivo.getData().get(numero).get("carnet"));
					combo.setVigencia(archivo.getData().get(numero).get("vigencia"));
					combo.setFolio(archivo.getData().get(numero).get("folio"));
					combo.setNombreTitular(archivo.getData().get(numero).get("nombre"));

				}else if("Dispositivo".equals(archivo.getType())){
					combo.setDispositivo(archivo.getData().get(numero).get("sn"));
					
				}else if("SIM".equals(archivo.getType())){
					combo.setSim(archivo.getData().get(numero).get("sim"));
					combo.setTelefono(archivo.getData().get(numero).get("telefono"));
				}
				
				archivo.getData().remove(numero);
			}
			logger.debug("combo [ " + i + " ]: IMEI: " + combo.getImei() + "   Dispositivo: " + combo.getDispositivo() +  "    CARNET: " + combo.getCarnet() + "   Tienda: " + combo.getNombre() );
			comboList.add(combo);
		}
		return comboList;
	}
	
	public static String isEmpty(String cadena){
		return cadena != null? cadena: "";
	}

	public int getNumberLine() {
		return numberLine;
	}

	public void setNumberLine(int numberLine) {
		this.numberLine = numberLine;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<HashMap<String, String>> getData() {
		return data;
	}

	public void setData(List<HashMap<String, String>> data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
