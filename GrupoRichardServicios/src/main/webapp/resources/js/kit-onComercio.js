$(function() {
	
	$.datepicker.setDefaults($.datepicker.regional["es"]);

	$( "#vigencia" ).datepicker({
		changeMonth: true, 
		changeYear: true,
		minDate: "+0d",
		//yearRange: "c-0:c+15",
		dateFormat: 'mm/y',
			 
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	       
	    beforeShow: function() {
	       var hoja = document.createElement('style');
	       hoja.innerHTML = ".ui-datepicker-calendar { display: none;}";
	       document.body.appendChild(hoja);
	       if ((selDate = $(this).val()).length > 0) {
	          iYear = selDate.substring(selDate.length - 4, selDate.length);
	          iMonth = selDate.substring(0, selDate.length - 5);
	          //alert('Year: ' + iYear + "\nMonth: " + $(this).datepicker('option', 'numberOfMonths'));
	          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth-1, 1)); 
	          $(this).datepicker('setDate', new Date(iYear, iMonth-1, 1));
	       }
	    }
	});

	$("#altaForm").validate({
        rules :{
        	carnet : {
                required  : true,
                digits   : true,
                minlength : 16,
                maxlength : 16 
            },
            vigencia : {
            	required :true,
                maxlength : 7
            },
            folio : {
            	required :true,
                maxlength : 20
            },
            nombreTitular : {
                maxlength : 100
            },
            imei : {
                required : true, 
                digits   : true,
                maxlength : 45
            },
            imeiConf: {
                required : true,
                digits   : true,
                maxlength : 45,
                equalTo: "#imei"
            },
            dispositivo: {
            	/*required : true,*/
                maxlength : 45,
            },
            dispositivoConf : {
                /*required : true,*/
                maxlength : 45,
                equalTo: "#dispositivo"
            },
            sim: {
            	required : true,
                maxlength : 20,
            },
            simConf : {
                required :true,
                maxlength : 20,
                equalTo: "#sim"
            },
            telefono: {
            	required : true,
            	digits   : true,
                maxlength : 10,
            },
            telefonoConf : {
                required : true,
                digits   : true,
                maxlength : 10,
                equalTo: "#telefono"
            }
        },
        messages : {
        	imeiConf : {
        		equalTo : "Por favor, escribir el mismo valor que el campo IMEI.",
            },
            dispositivoConf : {
            	equalTo : "Por favor, escribir el mismo valor que el campo DISPOSITIVO.",
            },
            telefonoConf : {
            	equalTo : "Por favor, escribir el mismo valor que el campo TELEFONO.",
            }
        }
    });


	$("#saveButton").click(function(){
		$('#altaForm').submit(function(event) {
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
				event.stopImmediatePropagation();
			    return false;
			}else{
				$( "#cargando" ).dialog( "open" );
			}
			
		});
		$("#altaForm").submit(); //SUBMIT FORM
			
	}); 
	
});