$(function() {
	
	$.datepicker.setDefaults($.datepicker.regional["es"]);

	$( "#vigencia" ).datepicker({
		changeMonth: true, 
		changeYear: true,
		minDate: "+0d",
		//yearRange: "c-0:c+15",
		dateFormat: 'mm/y',
			 
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	       
	    beforeShow: function() {
	       var hoja = document.createElement('style');
	       hoja.innerHTML = ".ui-datepicker-calendar { display: none;}";
	       document.body.appendChild(hoja);
	       if ((selDate = $(this).val()).length > 0) {
	          iYear = selDate.substring(selDate.length - 4, selDate.length);
	          iMonth = selDate.substring(0, selDate.length - 5);
	          //alert('Year: ' + iYear + "\nMonth: " + $(this).datepicker('option', 'numberOfMonths'));
	          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth-1, 1)); 
	          $(this).datepicker('setDate', new Date(iYear, iMonth-1, 1));
	       }
	    }
	});

	
	validActualizar = $("#actualizarForm").validate({
		rules :{
        	carnet : {
                /*required  : true,*/
                digits   : true,
                minlength : 16,
                maxlength : 16 
            },
            vigencia : {
            	required :true,
                maxlength : 7
            },
            folio : {
            	required :true,
                maxlength : 20
            },
            nombreTitular : {
                maxlength : 100
            },
            imei : {
                required : true, 
                digits   : true,
                maxlength : 45
            },
            imeiConf: {
                required : true,
                maxlength : 45,
                equalTo: "#imei"
            },
            dispositivo: {
            	/*required :true,*/
                maxlength : 45,
            },
            dispositivoConf : {
            	/*required :true,*/
                maxlength : 45,
                equalTo: "#dispositivo"
            },
            sim: {
            	required :true,
                maxlength : 20,
            },
            simConf : {
                required :true,
                maxlength : 20,
                equalTo: "#sim"
            },
            telefono: {
            	required :true,
            	digits   : true,
                maxlength : 10,
            },
            telefonoConf : {
                required :true,
                digits   : true,
                maxlength : 10,
                equalTo: "#telefono"
            }
        },
        messages : {
        	imeiConf : {
        		equalTo : "Por favor, escribir el mismo valor que el campo IMEI.",
            },
            dispositivoConf : {
            	equalTo : "Por favor, escribir el mismo valor que el campo DISPOSITIVO.",
            },
            telefonoConf : {
            	equalTo : "Por favor, escribir el mismo valor que el campo TELEFONO.",
            }
        }
    });

	
	/************************************************************
	 * 
	 */
	
	 var pqFilter = {
        search: function () {
            var txt = $("input.pq-filter-txt").val().toUpperCase(),
            colIndx = $("select#pq-filter-select-column").val(),
            DM = $grid.pqGrid("option", "dataModel");
            DM.filterIndx = colIndx;
            DM.filterValue = txt;
            $grid.pqGrid("refreshDataAndView");
        },
        render: function (ui) {
            var DM = ui.dataModel,
		    rowData = ui.rowData,
		    dataIndx = ui.dataIndx,
		    val = rowData[dataIndx],
		    txt = DM.filterValue;
            txtUpper = txt.toUpperCase(),
            valUpper = val.toUpperCase();
            if (dataIndx == DM.filterIndx) {
                var indx = valUpper.indexOf(txtUpper);
                if (indx >= 0) {
                    var txt1 = val.substring(0, indx);
                    var txt2 = val.substring(indx, indx + txt.length);
                    var txt3 = val.substring(indx + txt.length);
                    return txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
                }
            }
            return val;
        }
    }; 
     
	       //define colModel
     var colM = [{ title: "ID Kit", width: 50, dataType: "string", dataIndx: "idKit" },
                 { title: "Status", width: 55, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
            			if (rowData["status"] == 0) {
                            return "<span style='color:green;'>Disponible</span>";
                        }else if (rowData["status"] == 1) {
                            return "<span style='color:green;'>Activo</span>";
                        }
                        else {
                            return "<span style='color:red;'>Desactivo</span>";
                        }
                     }},
                 { title: "Cambio", width: 20, dataType: "string",  dataIndx: "status", align: "center", 
                	 render: function (ui) {
                		var rowData = ui.rowData; ;
                		if(rowData["idTienda"] == 0){
	                        if (rowData["status"] == 0 || rowData["status"] == 1) {
	                            return "<a href='javascript:void(0)' title='Desactivar Kit' ><img src='/GrupoRichardServicios/resources/images/pqgrid/tick.png'/></a>";
	                        }
	                        else {
	                            return "<a href='javascript:void(0)' title='Activar Kit' ><img src='/GrupoRichardServicios/resources/images/pqgrid/cross.png'/></a>";
	                        }
                		}
                     }},
                 { title: "Actualizar", width: 40, dataType: "string",  dataIndx: "status", align: "center", 
                    	 render: function (ui) {
                                return "<a href='javascript:void(0)' title='Modificar Kit' ><img src='/GrupoRichardServicios/resources/images/pqgrid/pencil_16.png'/></a>";
                         }},
                 { title: "IMEI", width: 150, dataType: "string",  dataIndx: "imei" },
                 { title: "CARNET", width: 50, dataType: "string",  dataIndx: "carnet" },
                 { title: "VIGENCIA", width: 70, dataType: "string",  dataIndx: "vigencia" },
                 { title: "DISPOSITIVO", width: 100, dataType: "string",  dataIndx: "dispositivo" },
                 { title: "SIM", width: 140, dataType: "string",  dataIndx: "sim" },
                 { title: "TELEFONO", width: 80, dataType: "string",  dataIndx: "telefono" },
                 { title: "Nombre Tienda", width: 200, dataType: "string", dataIndx: "nombre" }
                 ];
	         
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
             var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             var sort = ['','idKit', 'carnet', 'imei', 'dispositivo'];
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
                 "&sortBy=" + this.sortIndx + "&dir=" + sortDir;
             if (this.filterIndx != null && sort [this.filterIndx] ) {
                 queryString += "&filterBy=" + this.filterIndx + "&filterValue=" + this.filterValue;
             }else{
            	 queryString += "&filterBy=0&filterValue=";
             }
             var obj = { url: "/GrupoRichardServicios/portal/filtroKits", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 840, 
    		 height: 600, 
    		 title: "Grid Kit's", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: true, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true,
    		 freezeCols: 2
    	};
     
   //obj.render = pqFilter.pqgridrender;
     //append the filter toolbar in top section of grid
  
     obj.render = function (evt, obj) {
         var $toolbar = $("<div class='pq-grid-toolbar pq-grid-toolbar-search'></div>").appendTo($(".pq-grid-top", this));
  
         $("<span>Filtro de Busqueda:</span>").appendTo($toolbar);
  
         $("<input type='text' class='pq-filter-txt'  maxlen='50'/>").appendTo($toolbar).keyup(function (evt) {
             if (evt.keyCode == 13) {
                 pqFilter.search();
             }
         });
  
         $("<select id='pq-filter-select-column'>\
         <option value='1'>ID Kit</option>\
         <option value='2'>CARNET</option>\
		 <option value='3'>IMEI</option>\
		 <option value='4'>DISPOSITIVO</option>\
         </select>").appendTo($toolbar).change(function () {
             pqFilter.search();
         });
         $("<span class='pq-separator'></span><span>Para realizar la busqueda presione la tecla 'Enter'</span>").appendTo($toolbar);
  
    };
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	
	/*************************************************************
	 * Fin Busuqeda de Kits
	 */
    
    $grid.on( "pqgridcellclick", function( event, ui ) {
    	
    	if(ui.colIndx != null){
    		if (ui.colIndx == 2 ) {
            	actalizarStatusKit(event, ui );
            }else if (ui.colIndx == 3 ) {
            	actalizarKit(event, ui );
            }
    	}
    } );
    
    function actalizarKit(event, ui ){
    	
    	var rowData = ui.dataModel.data; 
    	
		validActualizar.resetForm();//remove error class on name elements and clear history
		$( "#mensaje" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		
		$( "#idKit" ).val( rowData[ui.rowIndx]["idKit"] );
		$( "#version" ).val( rowData[ui.rowIndx]["version"] );
		$( "#carnet" ).text( "" );
		$( "#carnetActual" ).text( "XXXX XXXX XXXX " + rowData[ui.rowIndx]["carnet"] );
		$( "#vigencia" ).val( rowData[ui.rowIndx]["vigencia"] );
		$( "#folio" ).val( rowData[ui.rowIndx]["folio"] );
		$( "#nombreTitular" ).val( rowData[ui.rowIndx]["nombreTitular"] );
		$( "#imei" ).val( rowData[ui.rowIndx]["imei"] );
		$( "#imeiConf" ).val( rowData[ui.rowIndx]["imei"] );
		/* $( "#imeiConf" ).val( "" ); */
		$( "#dispositivo" ).val( rowData[ui.rowIndx]["dispositivo"] );
		$( "#dispositivoConf" ).val( rowData[ui.rowIndx]["dispositivo"] );
		/* $( "#dispositivoConf" ).val( "" ); */
		$( "#sim" ).val( rowData[ui.rowIndx]["sim"] );
		$( "#simConf" ).val( rowData[ui.rowIndx]["sim"] );
		/* $( "#simConf" ).val( "" ); */
		$( "#telefono" ).val( rowData[ui.rowIndx]["telefono"] );
		$( "#telefonoConf" ).val( rowData[ui.rowIndx]["telefono"] );
		/* $( "#telefonoConf" ).val( "" ); */
		
		$( "#container1" ).dialog( "open" );	
		
    }
    
    function actalizarKitAjax( ){
    	
    	$('#actualizarForm').submit(function(event) {
			$('input[type="submit"]').attr('disabled','disabled');
			
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
			}else{
    	
		    	//var rowData = ui.dataModel.data; 
		    	var dataJson = {
		    			"idKit" : $( "#idKit" ).val(  ) ,
		    			"version" : $( "#version" ).val(  ) ,
		    			"carnet": $( "#carnet" ).val(  ),
		    			"vigencia": $( "#vigencia" ).val(  ),
		    			"folio": $( "#folio" ).val(  ),
		    			"nombreTitular": $( "#nombreTitular" ).val(  ),
		    			"imei": $( "#imei" ).val(  ),
		    			"dispositivo": $( "#dispositivo" ).val(  ),
		    			"sim": $( "#sim" ).val(  ),
		    			"telefono": $( "#telefono" ).val(  ),
		    			"folio": $( "#folio" ).val(  ),
		    			"nombreTitular": $( "#nombreTitular" ).val(  )
		    	};
		    	
		    	$.ajax({
					 url: '/GrupoRichardServicios/portal/actualizaKit',
					 type: 'POST',
					 data: JSON.stringify(dataJson),
					 dataType: 'json',
					 contentType: "application/json; charset=utf-8",
					 mimeType: 'application/json; charset=utf-8',
					 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
		             cache: false,    //This will force requested pages not to be cached by the browser          
		             processData:false, //To avoid making query String instead of JSON
					 statusCode: {
						 404: function() {
						 alert( "Pagina no encontrada." );
						 }
					 },
					 success: function (data) {
						$('input[type="submit"]').removeAttr('disabled');
		
					 	var caja2 = $('<div title="' + (data.idError == 0?'Mensaje del Sistema' :' Error' ) + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									if(data.idError == ID_ERROR_SESSION ){
										$(location).attr('href', '/GrupoRichardServicios/portal/login');
									}else if(data.idError == 0){
								 		$grid.pqGrid("refreshDataAndView");
								        $( "#container1" ).dialog( "close" );
									}
									$( this ).dialog( "close" );
								}
							}]
					    });
					 	//$( "#cargando" ).dialog( "close" );
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 }
				}); 
			}
			
			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
    	});
    	
    	$("#actualizarForm").submit(); //SUBMIT FORM
    }
    
    function actalizarStatusKit(event, ui ){
    	var rowData = ui.dataModel.data; 
    	var dataJson = {
    			"idKit" : rowData[ui.rowIndx]["idKit"] ,
    			"status": ((rowData[ui.rowIndx]["status"] == 1 || rowData[ui.rowIndx]["status"] == 0)? 2:1)
    	};
    	
    	if(rowData[ui.rowIndx]["idTienda"] != 0){
    		return false;
    	}
    	$.ajax({
			 url: '/GrupoRichardServicios/portal/actualizaKitStatus',
			 type: 'POST',
			 data: JSON.stringify(dataJson),
			 dataType: 'json',
			 contentType: "application/json; charset=utf-8",
			 mimeType: 'application/json; charset=utf-8',
			 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
             cache: false,    //This will force requested pages not to be cached by the browser          
             processData:false, //To avoid making query String instead of JSON
			 statusCode: {
				 404: function() {
				 alert( "Pagina no encontrada." );
				 }
			 },
			 success: function (data) {
				$('input[type="submit"]').removeAttr('disabled');

			 	if(data.idError != 0){
				 	var caja2 = $('<div title="Error">' + data.mensajeError +'</div>');
					caja2.dialog({
						dialogClass: "alert",
						modal: true,
						resizable: false,
						draggable: false,
						buttons: [ {
							text: "Aceptar",
							click: function() {
								if(data.idError == ID_ERROR_SESSION ){
									$(location).attr('href', '/GrupoRichardServicios/portal/login');
								}
								$( this ).dialog( "close" );
							}
						}]
						});
			 	}else{
			 		var rowInd = ui.rowIndx;
			 		$grid.pqGrid("refreshDataAndView");
			        if (rowInd != null) {
			        	$grid.pqGrid("setSelection", { rowInd: rowInd });
			        }
				}
			 	//$( "#cargando" ).dialog( "close" );
			 },
			 error: function (jqXHR, textStatus, errorThrown) {
				//$( "#cargando" ).dialog( "close" );
			 	alert('An error has occured!! \njqXHR: ' + jqXHR +
					 	'\ntextStatus: ' +  textStatus +
					 	'\nerrorThrown: ' + errorThrown);
			 }
		}); 
    }
    
    $( "#container1" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 550,
		width: 730,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				actalizarKitAjax();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});

    
});