$(function() {
	
	validActualizar = $("#actualizarForm").validate({
        rules :{
        	nombre : {
                required  : true,
                minlength : 3, //para validar campo con minimo 3 caracteres
                maxlength : 100  //para validar campo con maximo 10 caracteres
            },
            afiliacion : {
            	digits   : true,
                maxlength : 20
            },
/*            idKit : {
//            	required :true,
            	digits   : true,
                maxlength : 1,
                min      : 1
            },*/
            calle : {
                required : true, 
                maxlength : 45
            },
            numExt: {
                required : true,
                maxlength : 45,
            },
            numInt: {
                maxlength : 45
            },
            colonia : {
                required :true,
                maxlength : 45,
            },
            ciudad : {
                required :true,
                maxlength : 45
            },
            estado : {
                required :true,
                maxlength : 45
            },
            cp : {
                required :true,
                digits   : true,
                maxlength : 45
            },
            cx : {
                required :true,
                maxlength : 45
            },
            cy : {
                required :true,
                maxlength : 45
            }
        },
        messages : {
        	idKit : {
        		min : "Por favor, seleccione un KIT.",
            }
        }
    });

	
	/************************************************************
	 * 
	 */
	
	 var pqFilter = {
        search: function () {
            var txt = $("input.pq-filter-txt").val().toUpperCase(),
            colIndx = $("select#pq-filter-select-column").val(),
            DM = $grid.pqGrid("option", "dataModel");
            DM.filterIndx = colIndx;
            DM.filterValue = txt;
            $grid.pqGrid("refreshDataAndView");
        },
        render: function (ui) {
            var DM = ui.dataModel,
		    rowData = ui.rowData,
		    dataIndx = ui.dataIndx,
		    val = rowData[dataIndx],
		    txt = DM.filterValue;
            txtUpper = txt.toUpperCase(),
            valUpper = val.toUpperCase();
            if (dataIndx == DM.filterIndx) {
                var indx = valUpper.indexOf(txtUpper);
                if (indx >= 0) {
                    var txt1 = val.substring(0, indx);
                    var txt2 = val.substring(indx, indx + txt.length);
                    var txt3 = val.substring(indx + txt.length);
                    return txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
                }
            }
            return val;
        }
    }; 
     
	       //define colModel
     var colM = [{ title: "ID Tienda", width: 50, dataType: "string", dataIndx: "idTienda" },
                 { title: "Nombre", width: 250, dataType: "string", dataIndx: "nombre" },
                 { title: "Status", width: 55, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
                        if (rowData["status"] == 1) {
                            return "<span style='color:green;'>Activo</span>";
                        }
                        else {
                            return "<span style='color:red;'>Desactivo</span>";
                        }
                     }},
                 { title: "Cambio", width: 20, dataType: "string",  dataIndx: "status", align: "center", 
                	 render: function (ui) {
                		var rowData = ui.rowData; ;
                        if (rowData["status"] == 1) {
                            return "<a href='javascript:void(0)' title='Desactivar Tienda' ><img src='/GrupoRichardServicios/resources/images/pqgrid/tick.png'/></a>";
                        }
                        else {
                            return "<a href='javascript:void(0)' title='Activar Tienda' ><img src='/GrupoRichardServicios/resources/images/pqgrid/cross.png'/></a>";
                        }
                     }},
                 { title: "Actualizar", width: 40, dataType: "string",  dataIndx: "status", align: "center", 
                    	 render: function (ui) {
                                return "<a href='javascript:void(0)' title='Modificar Tienda' ><img src='/GrupoRichardServicios/resources/images/pqgrid/pencil_16.png'/></a>";
                         }},
                 { title: "Afiiación", width: 100, dataType: "string",  dataIndx: "afiliacion" },
                 { title: "IMEI", width: 150, dataType: "string",  dataIndx: "imei" },
                 { title: "CARNET", width: 50, dataType: "string",  dataIndx: "carnet" },
                 //{ title: "VIGENCIA", width: 70, dataType: "string",  dataIndx: "vigencia" },
                 { title: "DISPOSITIVO", width: 100, dataType: "string",  dataIndx: "dispositivo" },
                 { title: "SIM", width: 100, dataType: "string",  dataIndx: "sim" },
                 { title: "TELEFONO", width: 100, dataType: "string",  dataIndx: "telefono" },
                 { title: "Calle", width: 150, dataType: "string", dataIndx: "calle" },
                 { title: "Num Ext", width: 50, dataType: "string", dataIndx: "numExt" },
                 { title: "Num Int", width: 50, dataType: "string", dataIndx: "numInt" },
                 { title: "Colonia", width: 120, dataType: "string", dataIndx: "colonia" },
                 { title: "Ciudad", width: 120, dataType: "string", dataIndx: "ciudad" },
                 { title: "Estado", width: 120, dataType: "string", dataIndx: "estado" },
                 { title: "CP", width: 70, dataType: "string", dataIndx: "cp" },
                 { title: "User Alta", width: 70, dataType: "string",  dataIndx: "idUserAlta" },
                 { title: "Fecha Alta", width: 120, dataType: "string",  dataIndx: "fechaAlta" },
                 { title: "User Baja", width: 70, dataType: "string",  dataIndx: "idUserBaja" },
                 { title: "Fecha Baja", width: 120, dataType: "string",  dataIndx: "fechaBaja" },
                 { title: "statusHidden", width: 200, dataType: "string",  dataIndx: "statusHidden", hidden:true }];
	         
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
             var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             var sort = ['idTienda', 'nombre', 'status', 'imei', 'carnet', 'vigencia', 'dispositivo',
                          'calle', 'numInt', 'numExt', 'colonia', 'ciudad', 'estado', 'cp',
                          'idUserAlta', 'fechaAlta', 'idUserBaja', 'fechaBaja'];
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
                 "&sortBy=" + this.sortIndx + "&dir=" + sortDir;
             if (this.filterIndx != null && sort [this.filterIndx] ) {
                 queryString += "&filterBy=" + this.filterIndx + "&filterValue=" + this.filterValue;
             }else{
            	 queryString += "&filterBy=0&filterValue=";
             }
             var obj = { url: "/GrupoRichardServicios/portal/filtroTiendas", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 840, 
    		 height: 600, 
    		 title: "Grid Tiendas", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: true, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true,
    		 freezeCols: 3
    	};
     
   //obj.render = pqFilter.pqgridrender;
     //append the filter toolbar in top section of grid
  
     obj.render = function (evt, obj) {
         var $toolbar = $("<div class='pq-grid-toolbar pq-grid-toolbar-search'></div>").appendTo($(".pq-grid-top", this));
  
         $("<span>Filtro de Busqueda:</span>").appendTo($toolbar);
  
         $("<input type='text' class='pq-filter-txt'  maxlen='50'/>").appendTo($toolbar).keyup(function (evt) {
             if (evt.keyCode == 13) {
                 pqFilter.search();
             }
         });
  
         $("<select id='pq-filter-select-column'>\
         <option value='1'>ID Tienda</option>\
		 <option value='2'>Nombre</option>\
		 <option value='3'>IMEI</option>\
		 <option value='4'>CARNET</option>\
		 <option value='5'>DISPOSITIVO</option>\
		 <option value='6'>Calle</option>\
		 <option value='7'>Colonia</option>\
		 <option value='8'>Ciudad</option>\
		 <option value='9'>Estado</option>\
		 <option value='10'>CP</option>\
         </select>").appendTo($toolbar).change(function () {
             pqFilter.search();
         });
         $("<span class='pq-separator'></span><span>Para realizar la busqueda presione la tecla 'Enter'</span>").appendTo($toolbar);
  
    };
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	
	/*************************************************************
	 * Fin Busuqeda de Tiendas
	 */
    
    $grid.on( "pqgridcellclick", function( event, ui ) {
    	
    	if(ui.colIndx != null){
    		if (ui.colIndx == 3 ) {
            	actalizarStatusTienda(event, ui );
            }else if (ui.colIndx == 4 ) {
            	actalizarTienda(event, ui );
            }
    	}
        
    	
    } );
    
    function actalizarTienda(event, ui ){
    	
    	var rowData = ui.dataModel.data; 
    	
		validActualizar.resetForm();//remove error class on name elements and clear history
		$( "#lblErrorCamTarj" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		
		$( "#idTienda" ).val( rowData[ui.rowIndx]["idTienda"] );
		$( "#nombre" ).val( rowData[ui.rowIndx]["nombre"] );
		$( "#afiliacion" ).val( rowData[ui.rowIndx]["afiliacion"] );
		//$( "#idKit" ).val( rowData[ui.rowIndx]["idKit"] );
		if(rowData[ui.rowIndx]["idKit"] != 0){
			$( "#KitActual" ).text("ID_KIT: " + rowData[ui.rowIndx]["idKit"] +
					"    IMEI: " + rowData[ui.rowIndx]["imei"] + 
					"    CARNET: " + rowData[ui.rowIndx]["carnet"] + 
					"    DISPOSITIVO: " + rowData[ui.rowIndx]["dispositivo"] +
					"    SIM: " + rowData[ui.rowIndx]["sim"] +
					"    TELEFONO: " + rowData[ui.rowIndx]["telefono"]);
		}else{
			$( "#KitActual" ).text("La tienda no tiene un KIT asignado.");
		}
		
		$( "#calle" ).val( rowData[ui.rowIndx]["calle"] );
		$( "#numExt" ).val( rowData[ui.rowIndx]["numExt"] );
		$( "#numInt" ).val( rowData[ui.rowIndx]["numInt"] );
		$( "#colonia" ).val( rowData[ui.rowIndx]["colonia"] );
		$( "#ciudad" ).val( rowData[ui.rowIndx]["ciudad"] );
		$( "#estado" ).val( rowData[ui.rowIndx]["estado"] );
		$( "#cp" ).val( rowData[ui.rowIndx]["cp"] );
		$( "#cx" ).val( rowData[ui.rowIndx]["cx"] );
		$( "#cy" ).val( rowData[ui.rowIndx]["cy"] );
		
		$( "#container1" ).dialog( "open" );	
		
		$.ajax({
            type: "POST",
            url: "/GrupoRichardServicios/portal//buscarKITSCombo",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                
            	if(data.idError != 0){
				 	var caja2 = $('<div title="Error">' + data.mensajeError +'</div>');
					caja2.dialog({
						dialogClass: "alert",
						modal: true,
						resizable: false,
						draggable: false,
						buttons: [ {
							text: "Aceptar",
							click: function() {
								if(data.idError == ID_ERROR_SESSION ){
									$(location).attr('href', '/GrupoRichardServicios/portal/login');
								}
								$( this ).dialog( "close" );
							}
						}]
						});
			 	}else{
			 		$('#idKit').empty(); //remove all child nodes
			 		var option = $(document.createElement('option'));
                    option.text("-- Seleccionar --");
                    option.val(0);

	                    $("#idKit").append(option);
			 		$(data.comboKits).each(function () {
	                    var option = $(document.createElement('option'));
	                    this.label = this.label.replace(/&nbsp;/gi," "); 	
	                    option.text(this.label);
	                    option.val(this.value);

	                    $("#idKit").append(option);
	                });
				}

                
            },
            error: function (msg) {
                $("#dvAlerta > span").text("Error al llenar el combo");
            }
        });
		
    }
    
    function actalizarTiendaAjax( ){
        	
        $('#actualizarForm').submit(function(event) {
        	
        	
        	if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
			}else{
		    	//var rowData = ui.dataModel.data; 
		    	var dataJson = {
		    			"idTienda" : $( "#idTienda" ).val(  ) ,
		    			"idTienda": $( "#idTienda" ).val(  ),
		    			"afiliacion": $( "#afiliacion" ).val(  ),
		    			"nombre": $( "#nombre" ).val(  ),
						"idKit": $( "#idKit" ).val(  ),
		    			"calle": $( "#calle" ).val(  ),
		    			"numExt": $( "#numExt" ).val(  ),
		    			"numInt": $( "#numInt" ).val(  ),
		    			"colonia": $( "#colonia" ).val(  ),
		    			"ciudad": $( "#ciudad" ).val(  ),
		    			"estado": $( "#estado" ).val(  ),
		    			"cp": $( "#cp" ).val(  ),
		    			"cx": $( "#cx" ).val( ),
		    			"cy": $( "#cy" ).val(  )
		    	};
		    	
		    	$.ajax({
					 url: '/GrupoRichardServicios/portal/actualizaTienda',
					 type: 'POST',
					 data: JSON.stringify(dataJson),
					 dataType: 'json',
					 contentType: "application/json; charset=utf-8",
					 mimeType: 'application/json; charset=utf-8',
					 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
		             cache: false,    //This will force requested pages not to be cached by the browser          
		             processData:false, //To avoid making query String instead of JSON
					 statusCode: {
						 404: function() {
						 alert( "Pagina no encontrada." );
						 }
					 },
					 success: function (data) {
						$('input[type="submit"]').removeAttr('disabled');
		
					 	var caja2 = $('<div title="' + (data.idError == 0?'Mensaje del Sistema' :' Error' ) + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									if(data.idError == ID_ERROR_SESSION ){
										$(location).attr('href', '/GrupoRichardServicios/portal/login');
									}else if(data.idError == 0){
								 		$grid.pqGrid("refreshDataAndView");
								        $( "#container1" ).dialog( "close" );
									}
									$( this ).dialog( "close" );
								}
							}]
					    });
					 	//$( "#cargando" ).dialog( "close" );
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 }
		    	}); 
			}
		
			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
		});
		
		$("#actualizarForm").submit(); //SUBMIT FORM
	}
    
    function actalizarStatusTienda(event, ui ){
    	var rowData = ui.dataModel.data; 
    	var dataJson = {
    			"idTienda" : rowData[ui.rowIndx]["idTienda"] ,
    			"status": (rowData[ui.rowIndx]["status"] == 1 ? 2:1)
    	};
    	
    	$.ajax({
			 url: '/GrupoRichardServicios/portal/actualizaTienda',
			 type: 'POST',
			 data: JSON.stringify(dataJson),
			 dataType: 'json',
			 contentType: "application/json; charset=utf-8",
			 mimeType: 'application/json; charset=utf-8',
			 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
             cache: false,    //This will force requested pages not to be cached by the browser          
             processData:false, //To avoid making query String instead of JSON
			 statusCode: {
				 404: function() {
				 alert( "Pagina no encontrada." );
				 }
			 },
			 success: function (data) {
				$('input[type="submit"]').removeAttr('disabled');

			 	if(data.idError != 0){
				 	var caja2 = $('<div title="Error">' + data.mensajeError +'</div>');
					caja2.dialog({
						dialogClass: "alert",
						modal: true,
						resizable: false,
						draggable: false,
						buttons: [ {
							text: "Aceptar",
							click: function() {
								if(data.idError == ID_ERROR_SESSION ){
									$(location).attr('href', '/GrupoRichardServicios/portal/login');
								}
								$( this ).dialog( "close" );
							}
						}]
						});
			 	}else{
			 		var rowInd = ui.rowIndx;
			 		$grid.pqGrid("refreshDataAndView");
			        if (rowInd != null) {
			        	$grid.pqGrid("setSelection", { rowInd: rowInd });
			        }
				}
			 	//$( "#cargando" ).dialog( "close" );
			 },
			 error: function (jqXHR, textStatus, errorThrown) {
				//$( "#cargando" ).dialog( "close" );
			 	alert('An error has occured!! \njqXHR: ' + jqXHR +
					 	'\ntextStatus: ' +  textStatus +
					 	'\nerrorThrown: ' + errorThrown);
			 }
		}); 
    }
    
    $( "#container1" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 550,
		width: 730,
		buttons: [ {
			text: "Aceptar",
			click: function() {
					actalizarTiendaAjax();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});

	
    function getRowIndx() {
        //var $grid = $("#grid_render_cells");
 
        //var obj = $grid.pqGrid("getSelection");
        //debugger;
        var arr = $grid.pqGrid("selection", { type: 'row', method: 'getSelection' });
        if (arr && arr.length > 0) {
            var rowIndx = arr[0].rowIndx;
 
            //if (rowIndx != null && colIndx == null) {
            return rowIndx;
        }
        else {
            //alert("Select a row.");
            return null;
        }
    }
    
    function getColIndx() {
        //var $grid = $("#grid_render_cells");
 
        //var obj = $grid.pqGrid("getSelection");
        //debugger;
        var arr = $grid.pqGrid("selection", { type: 'row', method: 'getSelection' });
        if (arr && arr.length > 0) {
            var colIndx = arr[0].colIndx;
 
            //if (rowIndx != null && colIndx == null) {
            return colIndx;
        }
        else {
            //alert("Select a row.");
            return null;
        }
    }
	
});