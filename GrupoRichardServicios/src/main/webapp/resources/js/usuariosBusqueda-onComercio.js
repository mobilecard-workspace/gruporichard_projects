$(function() {
	
	
	validActualizar = $("#actualizarForm").validate({
		rules :{
        	nombre : {
                required  : true,
                maxlength : 45 
            },
            apellido : {
            	required :true,
                maxlength : 45
            },
            materno : {
                maxlength : 45
            },
            mail: {
                required : true,
                email   : true,
                maxlength : 45
            },
            telefono: {
            	required :true,
            	digits   : true,
                maxlength : 10,
            }
        }
       
    });

	
	/************************************************************
	 * 
	 */
	
	 var pqFilter = {
        search: function () {
            var txt = $("input.pq-filter-txt").val().toUpperCase(),
            colIndx = $("select#pq-filter-select-column").val(),
            DM = $grid.pqGrid("option", "dataModel");
            DM.filterIndx = colIndx;
            DM.filterValue = txt;
            $grid.pqGrid("refreshDataAndView");
        },
        render: function (ui) {
            var DM = ui.dataModel,
		    rowData = ui.rowData,
		    dataIndx = ui.dataIndx,
		    val = rowData[dataIndx],
		    txt = DM.filterValue;
            txtUpper = txt.toUpperCase(),
            valUpper = val.toUpperCase();
            if (dataIndx == DM.filterIndx) {
                var indx = valUpper.indexOf(txtUpper);
                if (indx >= 0) {
                    var txt1 = val.substring(0, indx);
                    var txt2 = val.substring(indx, indx + txt.length);
                    var txt3 = val.substring(indx + txt.length);
                    return txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
                }
            }
            return val;
        }
    }; 
     
	       //define colModel
     var colM = [{ title: "ID Usuario", width: 70, dataType: "string", dataIndx: "idUsuario" },
                 { title: "Login", width: 80, dataType: "string",  dataIndx: "login" },
                 { title: "Status", width: 55, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
            			if (rowData["status"] == 1) {
                            return "<span style='color:green;'>Activo</span>";
                        }else if (rowData["status"] == 2) {
                            return "<span style='color:green;'>Cam pass</span>";
                        }
                        else {
                            return "<span style='color:red;'>Desactivo</span>";
                        }
                     }},
                 { title: "Cambio", width: 20, dataType: "string",  dataIndx: "status", align: "center", 
                	 render: function (ui) {
                		var rowData = ui.rowData; ;
                        if (rowData["status"] == 0 || rowData["status"] == 1) {
                            return "<a href='javascript:void(0)' title='Desactivar Kit' ><img src='/GrupoRichardServicios/resources/images/pqgrid/tick.png'/></a>";
                        }
                        else if (rowData["status"] == 3) {
                            return "<a href='javascript:void(0)' title='Activar Kit' ><img src='/GrupoRichardServicios/resources/images/pqgrid/cross.png'/></a>";
                        }
                     }},
                 { title: "Actualizar", width: 40, dataType: "string",  dataIndx: "status", align: "center", 
                    	 render: function (ui) {
                                return "<a href='javascript:void(0)' title='Modificar Kit' ><img src='/GrupoRichardServicios/resources/images/pqgrid/pencil_16.png'/></a>";
                         }},
                 { title: "Nombre", width: 100, dataType: "string",  dataIndx: "nombre" },
                 { title: "Apellido", width: 100, dataType: "string",  dataIndx: "apellido" },
                 { title: "Materno", width: 100, dataType: "string",  dataIndx: "materno" },
                 { title: "E-mail", width: 150, dataType: "string",  dataIndx: "mail" },
                 { title: "ID Tienda", width: 50, dataType: "string", dataIndx: "idTienda" },
                 { title: "Nombre Tienda", width: 200, dataType: "string", dataIndx: "imei" }
                 ];
	         
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
             var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             var sort = ['','idUsuario', 'idTienda', 'login', 'nombre','apellido'];
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
                 "&sortBy=" + this.sortIndx + "&dir=" + sortDir;
             if (this.filterIndx != null && sort [this.filterIndx] ) {
                 queryString += "&filterBy=" + this.filterIndx  + "&filterValue=" + this.filterValue;
             }else{
            	 queryString += "&filterBy=0&filterValue=";
             }
             var obj = { url: "/GrupoRichardServicios/portal/filtroUsuarios", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 840, 
    		 height: 600, 
    		 title: "Grid Usuarios", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: true, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true,
    		 freezeCols: 3
    	};
     
   //obj.render = pqFilter.pqgridrender;
     //append the filter toolbar in top section of grid
  
     obj.render = function (evt, obj) {
         var $toolbar = $("<div class='pq-grid-toolbar pq-grid-toolbar-search'></div>").appendTo($(".pq-grid-top", this));
  
         $("<span>Filtro de Busqueda:</span>").appendTo($toolbar);
  
         $("<input type='text' class='pq-filter-txt'  maxlen='50'/>").appendTo($toolbar).keyup(function (evt) {
             if (evt.keyCode == 13) {
                 pqFilter.search();
             }
         });
  
         $("<select id='pq-filter-select-column'>\
         <option value='1'>ID Usuario</option>\
         <option value='2'>ID Tienda</option>\
		 <option value='3'>Login</option>\
		 <option value='4'>Nombre</option>\
		 <option value='5'>Apellido</option>\
         </select>").appendTo($toolbar).change(function () {
             pqFilter.search();
         });
         $("<span class='pq-separator'></span><span>Para realizar la busqueda presione la tecla 'Enter'</span>").appendTo($toolbar);
  
    };
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	
	/*************************************************************
	 * Fin Busuqeda de Kits
	 */
    
    $grid.on( "pqgridcellclick", function( event, ui ) {
    	
    	if(ui.colIndx != null){
    		if (ui.colIndx == 3 ) {
            	actalizarStatusKit(event, ui );
            }else if (ui.colIndx == 4 ) {
            	actalizarKit(event, ui );
            }
    	}
    } );
    
    
    function actalizarKit(event, ui ){
    	
    	var rowData = ui.dataModel.data; 
    	
		validActualizar.resetForm();//remove error class on name elements and clear history
		$( "#mensaje" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		
		$( "#idUsuario" ).val( rowData[ui.rowIndx]["idUsuario"] );
		$( "#login" ).text( rowData[ui.rowIndx]["login"] );
		$( "#nombre" ).val( rowData[ui.rowIndx]["nombre"] );
		$( "#apellido" ).val( rowData[ui.rowIndx]["apellido"] );
		$( "#materno" ).val( rowData[ui.rowIndx]["materno"] );
		$( "#mail" ).val( rowData[ui.rowIndx]["mail"] );
		$( "#telefono" ).val( rowData[ui.rowIndx]["telefono"] );
		
		$( "#container1" ).dialog( "open" );	
		
    }
    
    function actalizarKitAjax( ){
    	
    	$('#actualizarForm').submit(function(event) {
			$('input[type="submit"]').attr('disabled','disabled');
			
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
			}else{
    	
		    	//var rowData = ui.dataModel.data; 
		    	var dataJson = {
		    			"idUsuario" : $( "#idUsuario" ).val(  ) ,
		    			"nombre": $( "#nombre" ).val(  ),
		    			"apellido": $( "#apellido" ).val(  ),
		    			"materno": $( "#materno" ).val(  ),
		    			"mail": $( "#mail" ).val(  ),
		    			"telefono": $( "#telefono" ).val(  )
		    	};
		    	
		    	$.ajax({
					 url: '/GrupoRichardServicios/portal/actualizaUsuario',
					 type: 'POST',
					 data: JSON.stringify(dataJson),
					 dataType: 'json',
					 contentType: "application/json; charset=utf-8",
					 mimeType: 'application/json; charset=utf-8',
					 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
		             cache: false,    //This will force requested pages not to be cached by the browser          
		             processData:false, //To avoid making query String instead of JSON
					 statusCode: {
						 404: function() {
						 alert( "Pagina no encontrada." );
						 }
					 },
					 success: function (data) {
						$('input[type="submit"]').removeAttr('disabled');
		
					 	var caja2 = $('<div title="' + (data.idError == 0?'Mensaje del Sistema' :' Error' ) + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									if(data.idError == ID_ERROR_SESSION ){
										$(location).attr('href', '/GrupoRichardServicios/portal/login');
									}else if(data.idError == 0){
								 		$grid.pqGrid("refreshDataAndView");
								        $( "#container1" ).dialog( "close" );
									}
									$( this ).dialog( "close" );
								}
							}]
					    });
					 	//$( "#cargando" ).dialog( "close" );
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 }
				}); 
			}
			
			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
    	});
    	
    	$("#actualizarForm").submit(); //SUBMIT FORM
    }
    
    function actalizarStatusKit(event, ui ){
    	var rowData = ui.dataModel.data; 
    	
    	if (rowData[ui.rowIndx]["status"] == 2){
    		return;
    	}
    	
    	var dataJson = {
    			"idUsuario" : rowData[ui.rowIndx]["idUsuario"] ,
    			"status": (rowData[ui.rowIndx]["status"] == 1 ? 3: 1)
    	};
    	$.ajax({
			 url: '/GrupoRichardServicios/portal/actualizaUsuarioStatus',
			 type: 'POST',
			 data: JSON.stringify(dataJson),
			 dataType: 'json',
			 contentType: "application/json; charset=utf-8",
			 mimeType: 'application/json; charset=utf-8',
			 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
             cache: false,    //This will force requested pages not to be cached by the browser          
             processData:false, //To avoid making query String instead of JSON
			 statusCode: {
				 404: function() {
				 alert( "Pagina no encontrada." );
				 }
			 },
			 success: function (data) {
				$('input[type="submit"]').removeAttr('disabled');

			 	if(data.idError != 0){
				 	var caja2 = $('<div title="Error">' + data.mensajeError +'</div>');
					caja2.dialog({
						dialogClass: "alert",
						modal: true,
						resizable: false,
						draggable: false,
						buttons: [ {
							text: "Aceptar",
							click: function() {
								if(data.idError == ID_ERROR_SESSION ){
									$(location).attr('href', '/GrupoRichardServicios/portal/login');
								}
								$( this ).dialog( "close" );
							}
						}]
						});
			 	}else{
			 		var rowInd = ui.rowIndx;
			 		$grid.pqGrid("refreshDataAndView");
			        if (rowInd != null) {
			        	$grid.pqGrid("setSelection", { rowInd: rowInd });
			        }
				}
			 	//$( "#cargando" ).dialog( "close" );
			 },
			 error: function (jqXHR, textStatus, errorThrown) {
				//$( "#cargando" ).dialog( "close" );
			 	alert('An error has occured!! \njqXHR: ' + jqXHR +
					 	'\ntextStatus: ' +  textStatus +
					 	'\nerrorThrown: ' + errorThrown);
			 }
		}); 
    }
    
    $( "#container1" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 550,
		width: 730,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				actalizarKitAjax();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});

    
});