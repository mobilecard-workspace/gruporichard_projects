<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/pqgrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/pq-localize-es.js"/>"></script>
<script src="<c:url value="/resources/js/tiendaBusqueda-onComercio.js"/>"></script>

<STYLE type="text/css">
	div.pq-grid-toolbar-search
	{
	    text-align:left;
	}
	div.pq-grid-toolbar-search *
	{
	    margin:1px 5px 1px 0px;
	    vertical-align:middle;      
	}
	div.pq-grid-toolbar-search .pq-separator
	{
	   margin-left:10px;  
	   margin-right:10px;  
	}
	div.pq-grid-toolbar-search select
	{
	    height:18px;   
	    position:relative;
	}
	div.pq-grid-toolbar-search input.pq-filter-txt
	{
	    width:180px;border:1px solid #b5b8c8;       
	    height:16px;
	    padding:0px 5px;       
	}
</STYLE>

	<div id="container" >

		<form id="altaForm" name="altaForm" class="oncomercio topLabel page"
			autocomplete="on" method="post" modelAttribute="tienda"
			action="${pageContext.request.contextPath}/portal/altaTiendaFinal">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Busqueda de tienda </label>
				<label class="desc" style="font-size: 12px"> Para realizar la busqueda presione la tecla 'Enter'. </label>
			</div>
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
			</ul>
			
			<div id="grid_json"></div>
			
			<!-- <div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" name="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div> -->
			<br>
		</form>
		
	</div>
	<!--container-->
	
	<div id="container1" style="display: none;">

		<form id="actualizarForm" name="actualizarForm" class="conteiner1 topLabel page"
			autocomplete="on" method="post"
			action="${pageContext.request.contextPath}/portal/actualizarTienda">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Actualizar Tienda </label>
				<label class="desc" style="font-size: 12px"> Se deben de ingresar los siguientes datos. </label>
			</div>
			
			<input type="hidden" id="idTienda" name="idTienda" />
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Nombre Tienda 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="nombre" name="nombre" />
							<label >Nombre </label>
						</span>
						<span class="right country"> 
							<input id="afiliacion" name="afiliacion" />
							<label >Afiliación </label>
						</span> 
					</div>
				</li>
				
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Kit Actual
						<span class="req">*</span>
					</label>
					<div>
						<label class="desc" id="KitActual"> Kit Actual 
					</label>
				</li>
				
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Selecionar Kit 
						<span class="req">*</span>
					</label>
					<div>
						<span class="full zip">
							<select name="idKit" id="idKit"  >
				              <option value="0" >-- Seleccionar --</option>
				          	</select>
						</span>
					</div>
				</li>

				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > Dirección 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="calle" name="calle" />
							<label >Calle <span class="req">*</span></label>
						</span>
						<span class="right country"> 
							<input id="numExt" name="numExt" />
							<label >Número Exterior <span class="req">*</span></label>
						</span> 
					</div>
				</li>
				<li id="foli2" class="complex notranslate      ">
					<div>
						<span class="left zip">
							<input id="numInt" name="numInt" /> 
							<label >Número Interior </label>
						</span>
						<span class="right country"> 
							<input id="colonia" name="colonia" > 
							<label >Colonia <span class="req">*</span></label>
						</span> 
					</div>
				</li>

				<li id="foli3" class="complex notranslate      ">
					<div>
						<span class="left zip"> 
							<input id="ciudad" name="ciudad"  /> 
							<label >Ciudad <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<input id="estado" name="estado"  /> 
							<label >Estado <span class="req">*</span></label>
						</span>
					</div>
					<div>
						<span class="left zip"> 
							<input id="cp" name="cp" /> 
							<label >Codigo Postal <span class="req">*</span></label>
						</span>
						<span class="right country"> 
					</div>
				</li>
				

				<li id="foli5" class="complex notranslate      ">
					<label class="desc" > Coordenadas de la Tienda 
						<span class="req">*</span>
					</label>	
					<div>
						<span class="left zip"> 
							<input id="cx" name="cx" /> 
							<label >Latitud</label>
						</span> 
						<span class="right country"> 
							<input id="cy" name="cy" /> 
							<label >Longitud</label>
						</span>
					</div>
				</li>
				
			</ul>
			
			<!-- <div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" name="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div> -->
		</form>
		
	</div>


