<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" 	prefix="tiles" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="icon" type="image/x-icon" href="<c:url value="/resources/images/icono_16x16.png"/>">

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<!-- CSS -->


<STYLE type="text/css">


/* ----- Backgrounds ----- */
html {
	background-image: none;
	background-color: #FFFFFF;
}

html.embed {
	background-color: #FFFFFF;
}

body {
	margin: 17px 0 15px 0;
	padding: 0;
	font-family: verdana, arial, sans-serif;
	font-size: 80%;
	background-color: rgb(255, 255, 255);
} /*Font-size: 1.0em = 10px when browser default size is 16px*/

#container {
	width: 700px;
	text-align: left;
	background: #fff;
	margin: 0px auto;
	margin-top: 10px;
	margin-bottom: 10px;
	border: solid 1px rgb(150, 150, 150);
	-webkit-box-shadow: rgba(0, 0, 0, 0.2) 0px 0px 5px;
	-moz-box-shadow: rgba(0, 0, 0, 0.2) 0px 0px 5px;
	-o-box-shadow: rgba(0, 0, 0, 0.2) 0px 0px 5px;
	box-shadow: rgba(0, 0, 0, 0.2) 0px 0px 5px
}

.main-content {
	display: inline; /*Fix IE floating margin bug*/;
	float: left;
	width: 640px;
	margin: 0 0 0 30px;
	overflow: visible !important /*Firefox*/;
	overflow: hidden /*IE6*/;
	margin-bottom: 10px !important /*Non-IE6*/;
	margin-bottom: 5px /*IE6*/;
}

.main-content p {
	margin: 0 0 1.1em 0;
	line-height: 1.2em;
	font-size: 100%;
	text-align: justify;
	color: #000000;
}

.main-content table {
	clear: both;
	width: 100%;
	margin: 0 0 0 0;
	table-layout: fixed;
	border-collapse: collapse;
	empty-cells: show;
	background-color: rgb(233, 232, 244);
	text-align: justify;
}

.main-content table td {
	height: 1.5em;
	padding: 5px 20px 5px 20px;
	border-left: solid 2px rgb(255, 255, 255);
	border-right: solid 2px rgb(255, 255, 255);
	border-top: solid 2px rgb(255, 255, 255);
	border-bottom: solid 2px rgb(255, 255, 255);
	background-color: rgb(225, 225, 225);
	text-align: center;
	font-weight: normal;
	color: #000000;
	font-size: 11.5px;
}

	/* - - - Footer Theme - - - */

.footer {
	clear: both;
	width: 700px;
	height: 3.5em;
	padding: 1px 0 0;
	background: rgb(225, 225, 225);
	overflow: visible !important /*Firefox*/;
	overflow: hidden /*IE6*/;
}

.footer p {
	line-height: 1.2em;
	text-align: center;
	color: rgb(125, 125, 125);
	font-weight: bold;
	font-size: 80%;
}

.footer p.credits {
	font-weight: normal;
}
</STYLE>


</head>

<body id="body">
	
	<div id="container" >
		
		<tiles:insertAttribute name="body" />

		<tiles:insertAttribute name="footer" />

	</div>
</body>
</html>
