<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>


<div id="menu">
	<form id="menuForm" name="menuForm" method="post" >
	</form>
	<ul id="nav">
		<li class="current"><a id="menu" href="#">onComercio</a></li>
		<li><a href="javascript:void(0)" onclick="return false;">Tiendas</a>
			<ul>
				<li><a id="menuAltaTienda" href="#" >Alta</a></li>
				<li><a id="menuBusquedaTiendas" href="#">Busqueda</a></li>
			</ul>
		</li>
		<li><a href="javascript:void(0)">KIT's</a>
			<ul>
				<li><a id="menuAltaKit" href="#">Alta</a></li>
				<li><a id="menuBusquedaKits" href="#">Busqueda</a></li>
			</ul>
		</li>
		<li><a id="aHrefCamTarj" href="#">Usuarios</a>
			<ul>
				<li><a id="menuBusquedaUsuarios" href="#">Busqueda</a></li>
			</ul>
		</li>
		<li><a id="menuAltaKitArchivo" href="#">Alta con Archivos</a></li>
		<li><a id="menuLogout" href="#">Cerrar sesion</a></li>
	</ul>
</div>