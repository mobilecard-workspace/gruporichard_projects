package org.addcel.android.etn.constant;

public class Url {

	private static final String PROD = "http://www.mobilecard.mx:8080/";
	private static final String DEV = "50.57.192.213:8080/";

	private static final String BASE = DEV + "ETNWSConsumer/";

	public final static String ORIGEN_GET = BASE + "ObtenerOrigenes";
	public final static String DESTINO_GET = BASE + "ObtenerDestinos";
	public final static String CORRIDAS_GET = BASE + "ObtenerCorridas";
	public final static String ITINERARIO_GET = BASE + "ConsultarItinerario";
	public final static String OCUPACION_GET = BASE + "ConsultarOcupacion";
	public final static String DIAGRAMA_GET = BASE + "ConsultarDiagrama";
	public final static String RESERVACION_GET = BASE + "ConsultarReservacion";
	public final static String BOLETOS_GET = BASE + "ConsultaBoletoEtn";
	public final static String CONFIRMACION_GET = BASE + "ConfirmacionVenta";

	public final static String PURCHASE_POST = "http://mobilecard.mx:8080/ProcomETN/envio.jsp";
	public final static String RETURN_POST = "http://mobilecard.mx:8080/ProcomETN/regreso.jsp";

	public final static String SECUENCIA_GET = "http://mobilecard.mx:8080/AddCelBridge/ClienteGeneraSecuencia";

	
	public final static String URL_WS_ITINERARIO_GET = 		BASE + "ConsultarItinerario";	

}
