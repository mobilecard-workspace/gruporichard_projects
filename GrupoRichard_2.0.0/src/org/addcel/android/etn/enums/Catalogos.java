package org.addcel.android.etn.enums;

public enum Catalogos {

	ORIGEN("lista", "clave", "descripcion"), 
	DESTINO("listaDestinos", "claveDestino", "descripcionDestino");
	
	private String nombre;
	private String clave;
	private String descripcion;
	
	private Catalogos(String _nombre, String _clave, String _descripcion) {
		nombre = _nombre;
		clave = _clave;
		descripcion = _descripcion;
		
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getClave() {
		return clave;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	
	
}
