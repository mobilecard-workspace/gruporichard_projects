package org.addcel.android.etn.to;

public class Asiento {

	private String numAsiento;
	private int ocupado;
	private boolean esAsiento;
	
	public Asiento() {}

	public String getNumAsiento() {
		return numAsiento;
	}

	public int getOcupado() {
		return ocupado;
	}

	public boolean isEsAsiento() {
		return esAsiento;
	}

	public Asiento setNumAsiento(String numAsiento) {
		this.numAsiento = numAsiento;
		
		return this;
	}

	public Asiento setOcupado(int ocupado) {
		this.ocupado = ocupado;
		
		return this;
	}

	public Asiento setEsAsiento(boolean esAsiento) {
		this.esAsiento = esAsiento;
		
		return this;
	}

	@Override
	public String toString() {
		return "Asiento [numAsiento=" + numAsiento + ", ocupado=" + ocupado
				+ "]";
	}
	
	
}
