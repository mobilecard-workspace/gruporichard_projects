package org.addcel.android.etn.to;


import org.addcel.interfaces.JSONable;
import org.addcel.util.Text;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Corrida implements JSONable, Parcelable {

	private int porcentajeTramoFrontera;
	private int dispInsen;
	private String fechaDesfaza;
	private String cveDestino;
	private String horaCorrida;
	private String moneda;
	private String tipoLectura;
	private String corrida;
	private double tarifaEstudiante;
	private double tarifaInsen;
	private String secuencia;
	private String fechaInicial;
	private String anden;
	private String tipoItinerario;
	private String tipoCorrida;
	private double tarifaAdulto;
	private double tarifaNino;
	private double tarifaPromo;
	private int dispEstudiante;
	private String detServicio;
	private String servicio;
	private int dispMaestro;
	private String cveCtl;
	private String ubicacion;
	private String empresaCorrida;
	private String sala;
	private double tarifa;
	private String destino;
	private String fechaLlegada;
	private int dispPaisano;
	private int cupoAutobus;
	private int dispPromo;
	private double tarifaMaestro;
	private int errorNumero;
	private int dispNinos;
	private double tarifaProgPaisano;
	private String errorCadena;
	private int dispGeneral;
	
	public Corrida() {
	}

	public Corrida(Parcel source) {
		readFromParcel(source);
	}

	public Corrida(JSONObject json) {
		fromJSON(json);
	}

	public int getPorcentajeTramoFrontera() {
		return porcentajeTramoFrontera;
	}

	public int getDispInsen() {
		return dispInsen;
	}

	public String getFechaDesfaza() {
		return fechaDesfaza;
	}

	public String getCveDestino() {
		return cveDestino;
	}

	public String getHoraCorrida() {
		return horaCorrida;
	}

	public String getMoneda() {
		return moneda;
	}

	public String getTipoLectura() {
		return tipoLectura;
	}

	public String getCorrida() {
		return corrida;
	}

	public double getTarifaEstudiante() {
		return tarifaEstudiante;
	}

	public double getTarifaInsen() {
		return tarifaInsen;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public String getAnden() {
		return anden;
	}

	public String getTipoItinerario() {
		return tipoItinerario;
	}

	public String getTipoCorrida() {
		return tipoCorrida;
	}

	public double getTarifaAdulto() {
		return tarifaAdulto;
	}

	public double getTarifaNino() {
		return tarifaNino;
	}

	public double getTarifaPromo() {
		return tarifaPromo;
	}

	public int getDispEstudiante() {
		return dispEstudiante;
	}

	public String getDetServicio() {
		return detServicio;
	}

	public String getServicio() {
		return servicio;
	}

	public int getDispMaestro() {
		return dispMaestro;
	}

	public String getCveCtl() {
		return cveCtl;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public String getEmpresaCorrida() {
		return empresaCorrida;
	}

	public String getSala() {
		return sala;
	}

	public double getTarifa() {
		return tarifa;
	}

	public String getDestino() {
		return destino;
	}

	public String getFechaLlegada() {
		return fechaLlegada;
	}

	public int getDispPaisano() {
		return dispPaisano;
	}

	public int getCupoAutobus() {
		return cupoAutobus;
	}

	public int getDispPromo() {
		return dispPromo;
	}

	public double getTarifaMaestro() {
		return tarifaMaestro;
	}

	public int getErrorNumero() {
		return errorNumero;
	}

	public int getDispNinos() {
		return dispNinos;
	}

	public double getTarifaProgPaisano() {
		return tarifaProgPaisano;
	}

	public String getErrorCadena() {
		return errorCadena;
	}

	public int getDispGeneral() {
		return dispGeneral;
	}

	public Corrida setPorcentajeTramoFrontera(int porcentajeTramoFrontera) {
		this.porcentajeTramoFrontera = porcentajeTramoFrontera;
		return this;
	}

	public Corrida setDispInsen(int dispInsen) {
		this.dispInsen = dispInsen;
		return this;
	}

	public Corrida setFechaDesfaza(String fechaDesfaza) {
		this.fechaDesfaza = fechaDesfaza;
		return this;
	}

	public Corrida setCveDestino(String cveDestino) {
		this.cveDestino = cveDestino;
		return this;
	}

	public Corrida setHoraCorrida(String horaCorrida) {
		this.horaCorrida = horaCorrida;
		return this;
	}

	public Corrida setMoneda(String moneda) {
		this.moneda = moneda;
		return this;
	}

	public Corrida setTipoLectura(String tipoLectura) {
		this.tipoLectura = tipoLectura;
		return this;
	}

	public Corrida setCorrida(String corrida) {
		this.corrida = corrida;
		return this;
	}

	public Corrida setTarifaEstudiante(double tarifaEstudiante) {
		this.tarifaEstudiante = tarifaEstudiante;
		return this;
	}

	public Corrida setTarifaInsen(double tarifaInsen) {
		this.tarifaInsen = tarifaInsen;
		return this;
	}

	public Corrida setSecuencia(String secuencia) {
		this.secuencia = secuencia;
		return this;
	}

	public Corrida setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
		return this;
	}

	public Corrida setAnden(String anden) {
		this.anden = anden;
		return this;
	}

	public Corrida setTipoItinerario(String tipoItinerario) {
		this.tipoItinerario = tipoItinerario;
		return this;
	}

	public Corrida setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
		return this;
	}

	public Corrida setTarifaAdulto(double tarifaAdulto) {
		this.tarifaAdulto = tarifaAdulto;
		return this;
	}

	public Corrida setTarifaNino(double tarifaNino) {
		this.tarifaNino = tarifaNino;
		return this;
	}

	public Corrida setTarifaPromo(double tarifaPromo) {
		this.tarifaPromo = tarifaPromo;
		return this;
	}

	public Corrida setDispEstudiante(int dispEstudiante) {
		this.dispEstudiante = dispEstudiante;
		return this;
	}

	public Corrida setDetServicio(String detServicio) {
		this.detServicio = detServicio;
		return this;
	}

	public Corrida setServicio(String servicio) {
		this.servicio = servicio;
		return this;
	}

	public Corrida setDispMaestro(int dispMaestro) {
		this.dispMaestro = dispMaestro;
		return this;
	}

	public Corrida setCveCtl(String cveCtl) {
		this.cveCtl = cveCtl;
		return this;
	}

	public Corrida setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
		return this;
	}

	public Corrida setEmpresaCorrida(String empresaCorrida) {
		this.empresaCorrida = empresaCorrida;
		return this;
	}

	public Corrida setSala(String sala) {
		this.sala = sala;
		return this;
	}

	public Corrida setTarifa(double tarifa) {
		this.tarifa = tarifa;
		return this;
	}

	public Corrida setDestino(String destino) {
		this.destino = destino;
		return this;
	}

	public Corrida setFechaLlegada(String fechaLlegada) {
		this.fechaLlegada = fechaLlegada;
		return this;
	}

	public Corrida setDispPaisano(int dispPaisano) {
		this.dispPaisano = dispPaisano;
		return this;
	}

	public Corrida setCupoAutobus(int cupoAutobus) {
		this.cupoAutobus = cupoAutobus;
		return this;
	}

	public Corrida setDispPromo(int dispPromo) {
		this.dispPromo = dispPromo;
		return this;
	}

	public Corrida setTarifaMaestro(double tarifaMaestro) {
		this.tarifaMaestro = tarifaMaestro;
		return this;
	}

	public Corrida setErrorNumero(int errorNumero) {
		this.errorNumero = errorNumero;
		return this;
	}

	public Corrida setDispNinos(int dispNinos) {
		this.dispNinos = dispNinos;
		return this;
	}

	public Corrida setTarifaProgPaisano(double tarifaProgPaisano) {
		this.tarifaProgPaisano = tarifaProgPaisano;
		return this;
	}

	public Corrida setErrorCadena(String errorCadena) {
		this.errorCadena = errorCadena;
		return this;
	}

	public Corrida setDispGeneral(int dispGeneral) {
		this.dispGeneral = dispGeneral;

		return this;
	}
	


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(porcentajeTramoFrontera);
		dest.writeInt(dispInsen);
		dest.writeString(fechaDesfaza);
		dest.writeString(cveDestino);
		dest.writeString(horaCorrida);
		dest.writeString(moneda);
		dest.writeString(tipoLectura);
		dest.writeString(corrida);
		dest.writeDouble(tarifaEstudiante);
		dest.writeDouble(tarifaInsen);
		dest.writeString(secuencia);
		dest.writeString(fechaInicial);
		dest.writeString(anden);
		dest.writeString(tipoItinerario);
		dest.writeString(tipoCorrida);
		dest.writeDouble(tarifaAdulto);
		dest.writeDouble(tarifaNino);
		dest.writeDouble(tarifaPromo);
		dest.writeInt(dispEstudiante);
		dest.writeString(detServicio);
		dest.writeString(servicio);
		dest.writeInt(dispMaestro);
		dest.writeString(cveCtl);
		dest.writeString(ubicacion);
		dest.writeString(empresaCorrida);
		dest.writeString(sala);
		dest.writeDouble(tarifa);
		dest.writeString(destino);
		dest.writeString(fechaLlegada);
		dest.writeInt(dispPaisano);
		dest.writeInt(cupoAutobus);
		dest.writeInt(dispPromo);
		dest.writeDouble(tarifaMaestro);
		dest.writeInt(errorNumero);
		dest.writeInt(dispNinos);
		dest.writeDouble(tarifaProgPaisano);
		dest.writeString(errorCadena);
		dest.writeInt(dispGeneral);
		
	}

	public void readFromParcel(Parcel source) {
		porcentajeTramoFrontera = source.readInt();
		dispInsen = source.readInt();
		fechaDesfaza = source.readString();
		cveDestino = source.readString();
		horaCorrida = source.readString();
		moneda = source.readString();
		tipoLectura = source.readString();
		corrida = source.readString();
		tarifaEstudiante = source.readDouble();
		tarifaInsen = source.readDouble();
		secuencia = source.readString();
		fechaInicial = source.readString();
		anden = source.readString();
		tipoItinerario = source.readString();
		tipoCorrida = source.readString();
		tarifaAdulto = source.readDouble();
		tarifaNino = source.readDouble();
		tarifaPromo = source.readDouble();
		dispEstudiante = source.readInt();
		detServicio = source.readString();
		servicio = source.readString();
		dispMaestro = source.readInt();
		cveCtl = source.readString();
		ubicacion = source.readString();
		empresaCorrida = source.readString();
		sala = source.readString();
		tarifa = source.readDouble();
		destino = source.readString();
		fechaLlegada = source.readString();
		dispPaisano = source.readInt();
		cupoAutobus = source.readInt();
		dispPromo = source.readInt();
		tarifaMaestro = source.readDouble();
		errorNumero = source.readInt();
		dispNinos = source.readInt();
		tarifaProgPaisano = source.readDouble();
		errorCadena = source.readString();
		dispGeneral = source.readInt();
		
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		porcentajeTramoFrontera = json.optInt("porcentajeTramoFrontera");
		dispInsen = json.optInt("dispInsen");
		fechaDesfaza = json.optString("fechaDesfaza");
		cveDestino = json.optString("cveDestino");
		horaCorrida = json.optString("horaCorrida");
		moneda = json.optString("moneda");
		tipoLectura = json.optString("tipoLectura");
		corrida = json.optString("corrida");
		tarifaEstudiante = json.optDouble("tarifaEstudiante");
		tarifaInsen = json.optDouble("tarifaInsen");
		secuencia = json.optString("secuencia");
		fechaInicial = json.optString("fechaInicial");
		anden = json.optString("anden");
		tipoItinerario = json.optString("tipoItinerario");
		tipoCorrida = json.optString("tipoCorrida");
		tarifaAdulto = json.optDouble("tarifaAdulto");
		tarifaNino = json.optDouble("tarifaNino");
		tarifaPromo = json.optDouble("tarifaPromo");
		dispEstudiante = json.optInt("dispEstudiante");
		detServicio = json.optString("detServicio");
		servicio = json.optString("servicio");
		dispMaestro = json.optInt("dispMaestro");
		cveCtl = json.optString("cveCtl");
		ubicacion = json.optString("Ubicacion");
		empresaCorrida = json.optString("empresaCorrida");
		sala = json.optString("sala");
		tarifa = json.optInt("tarifa");
		destino = json.optString("destino");
		fechaLlegada = json.optString("fechaLlegada");
		dispPaisano = json.optInt("dispPaisano");
		cupoAutobus = json.optInt("dispPromo");
		dispPromo = json.optInt("dispGeneral");
		tarifaMaestro = json.optDouble("tarifaMaestro");
		errorNumero = json.optInt("dispGeneral");
		dispNinos = json.optInt("dispNinos");
		tarifaProgPaisano = json.optDouble("tarifaProgPaisano");
		errorCadena = json.optString("errorCadena");
		dispGeneral = json.optInt("dispGeneral");
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{Corrida -> " + corrida + ", Tarifa ->" + Text.formatCurrency(tarifa) +"}"; 
	}

	public static final Parcelable.Creator<Corrida> CREATOR = new Creator<Corrida>() {

		@Override
		public Corrida[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Corrida[size];
		}

		@Override
		public Corrida createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Corrida(source);
		}
	};

	
	
}
