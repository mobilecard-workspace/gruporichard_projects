package org.addcel.android.etn.to;

import java.util.Date;


import org.addcel.interfaces.Mapable;
import org.addcel.util.Text;
import org.apache.http.message.BasicNameValuePair;

import android.os.Parcel;
import android.os.Parcelable;

public class CorridaReq implements Mapable, Parcelable {
	
	private String origen;
	private String destino;
	private Date fechaIni;
	private int numAdulto;
	private int numNino;
	private int numInsen;
	private int numEst;
	private int numMto;
	
	public CorridaReq() {}
	
	public CorridaReq(Parcel source) {
		readFromParcel(source);
	}
	
	public String getOrigen() {
		return origen;
	}

	public String getDestino() {
		return destino;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public int getNumAdulto() {
		return numAdulto;
	}

	public int getNumNino() {
		return numNino;
	}

	public int getNumInsen() {
		return numInsen;
	}

	public int getNumEst() {
		return numEst;
	}

	public int getNumMto() {
		return numMto;
	}

	public CorridaReq setOrigen(String _origen) {
		origen = _origen;
		
		return this;
	}
	
	public CorridaReq setDestino(String _destino) {
		destino = _destino;
		
		return this;
	}
	
	public CorridaReq setFechaIni(Date _fechaIni) {
		fechaIni = _fechaIni;
		
		return this;
	}
	
	public CorridaReq setNumAdulto(int _numAdulto) {
		numAdulto = _numAdulto;
		
		return this;
	}
	
	public CorridaReq setNumNino(int numNino) {
		this.numNino = numNino;
		
		return this;
	}

	public CorridaReq setNumInsen(int numInsen) {
		this.numInsen = numInsen;
		return this;
	}

	public CorridaReq setNumEst(int numEst) {
		this.numEst = numEst;
		return this;
	}

	public CorridaReq setNumMto(int numMto) {
		this.numMto = numMto;
		return this;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
		dest.writeString(origen);
		dest.writeString(destino);
		dest.writeSerializable(fechaIni);
		dest.writeInt(numAdulto);
		dest.writeInt(numNino);
		dest.writeInt(numInsen);
		dest.writeInt(numEst);
		dest.writeInt(numMto);

	}
	
	public void readFromParcel(Parcel source) {
		origen = source.readString();
		destino = source.readString();
		fechaIni = (Date) source.readSerializable();
		numAdulto = source.readInt();
		numNino = source.readInt();
		numInsen = source.readInt();
		numEst = source.readInt();
		numMto = source.readInt();
	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub
		BasicNameValuePair[] map = new BasicNameValuePair[9];
		
		map[0] = new BasicNameValuePair("origen", origen);
		map[1] = new BasicNameValuePair("destino", destino);
		map[2] = new BasicNameValuePair("fechaIni", Text.formatDateNoSpace(fechaIni));
		map[3] = new BasicNameValuePair("numAdulto", String.valueOf(numAdulto));
		map[4] = new BasicNameValuePair("numNino", String.valueOf(numNino));
		map[5] = new BasicNameValuePair("numInsen", String.valueOf(numNino));
		map[6] = new BasicNameValuePair("numEst", String.valueOf(numEst));
		map[7] = new BasicNameValuePair("numMto", String.valueOf(numMto));
		map[8] = new BasicNameValuePair("numPaisano", "0");
		
		return map;
	}
	
	public static final Parcelable.Creator<CorridaReq> CREATOR = new Creator<CorridaReq>() {
		
		@Override
		public CorridaReq[] newArray(int size) {
			// TODO Auto-generated method stub
			return new CorridaReq[size];
		}
		
		@Override
		public CorridaReq createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new CorridaReq(source);
		}
	};

}
