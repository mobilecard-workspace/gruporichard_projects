package org.addcel.android.etn.to;

import org.addcel.interfaces.Mapable;
import org.apache.http.message.BasicNameValuePair;

public class DestinoReq implements Mapable {
	
	BasicNameValuePair origen;
	
	public DestinoReq() {
		
	}
	
	public DestinoReq setOrigen(BasicNameValuePair _origen) {
		origen = _origen;
		
		return this;
	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub
		
		BasicNameValuePair[] map = new BasicNameValuePair[1];
		map[0] = new BasicNameValuePair("origen", origen.getValue());
		
		return map;
	}

}
