package org.addcel.android.etn.to;

public class DetallePasajero {
	private String tipo;
	private String nombre;
	private String asiento;
	private double tarifa;
	
	public DetallePasajero(String tipo) {
		this.tipo = tipo;
		this.nombre = new String();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAsiento() {
		return asiento;
	}

	public void setAsiento(String asiento) {
		this.asiento = asiento;
	}

	public double getTarifa() {
		return tarifa;
	}

	public void setTarifa(double tarifa) {
		this.tarifa = tarifa;
	}

	@Override
	public String toString() {
		return "DetallePasajero [tipo=" + tipo + ", nombre=" + nombre
				+ ", asiento=" + asiento + ", tarifa=" + tarifa + "]";
	}
	
	
}
