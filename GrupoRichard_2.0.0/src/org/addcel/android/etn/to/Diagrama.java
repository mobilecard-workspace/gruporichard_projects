package org.addcel.android.etn.to;

import java.util.ArrayList;
import java.util.List;

import org.addcel.interfaces.JSONable;
import org.json.JSONArray;
import org.json.JSONObject;

public class Diagrama implements JSONable {
	
	private int numFilas;
	private int numFilasArriba;
	private int numAsientos;
	private List<Asiento> diagramaAutobus;
	
	public Diagrama() {
		diagramaAutobus = new ArrayList<Asiento>();
	}
	
	public Diagrama(JSONObject json) {
		this();
		fromJSON(json);
	}
	
	public int getNumFilas() {
		return numFilas;
	}

	public int getNumFilasArriba() {
		return numFilasArriba;
	}

	public int getNumAsientos() {
		return numAsientos;
	}

	public List<Asiento> getDiagramaAutobus() {
		return diagramaAutobus;
	}

	public Diagrama setNumFilas(int numFilas) {
		this.numFilas = numFilas;
		
		return this;
	}

	public Diagrama setNumFilasArriba(int numFilasArriba) {
		this.numFilasArriba = numFilasArriba;
		
		return this;
	}

	public Diagrama setNumAsientos(int numAsientos) {
		this.numAsientos = numAsientos;
		
		return this;
	}

	public Diagrama setDiagramaAutobus(List<Asiento> diagramaAutobus) {
		this.diagramaAutobus = diagramaAutobus;
		
		return this;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		numFilas = json.optInt("numFilas");
		numFilasArriba = json.optInt("numFilasArriba");
		numAsientos = json.optInt("numAsientos");
		
		JSONArray array = json.optJSONArray("diagramaAutobus");
		
		if (null != array && 0 < array.length()) {
			
			for (int i = 0; i < array.length(); i++) {
				
				Asiento a = new Asiento();
				a.setNumAsiento(array.optString(i));
				
				if (a.getNumAsiento().contains("000")) {
					a.setEsAsiento(false);
					a.setOcupado(2);
				} else
					a.setEsAsiento(true);
				
				diagramaAutobus.add(a);
			}
			
		}
	}

}
