package org.addcel.android.etn.to;

import java.util.Date;

import org.addcel.interfaces.Mapable;
import org.addcel.util.Text;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class DiagramaReq implements Mapable {
	
	private Corrida corrida;
	
	public DiagramaReq(Corrida c) {
		corrida = c;
	}
	
	private String getDateWithFormat() {
		
		Log.i("DiagramaReq", "Fecha sin formato" + corrida.getFechaInicial());
		
		Date d = Text.getEtnDateFromStringWithSlash(corrida.getFechaInicial());
		return Text.formatDateNoSpace(d);
		
	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub
		
		BasicNameValuePair[] map = new  BasicNameValuePair[2];
		map[0] = new BasicNameValuePair("fecha", getDateWithFormat());
		map[1] = new BasicNameValuePair("corrida", corrida.getCorrida());

		return map;
	}

}
