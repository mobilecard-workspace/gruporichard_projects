package org.addcel.android.etn.to;

public class ItinerarioRequest {
	private String corrida;
	private String fecha;
	private String origen;
	
	public ItinerarioRequest(String corrida, String fecha, String origen) {
		this.corrida = corrida;
		this.fecha = fecha;
		this.origen = origen;
	}

	public String getCorrida() {
		return corrida;
	}

	public void setCorrida(String corrida) {
		this.corrida = corrida;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	@Override
	public String toString() {
		return "ItinerarioRequest [corrida=" + corrida + ", fecha=" + fecha
				+ ", origen=" + origen + "]";
	}
}
