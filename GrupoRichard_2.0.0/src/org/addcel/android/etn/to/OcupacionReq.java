package org.addcel.android.etn.to;

import java.util.Date;


import org.addcel.interfaces.Mapable;
import org.addcel.util.Text;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class OcupacionReq implements Mapable {

	private String origen;
	private String destino;
	private String fecha;
	private String corrida;

	public OcupacionReq() {
	}

	public String getOrigen() {
		return origen;
	}

	public String getDestino() {
		return destino;
	}

	public String getFecha() {
		return fecha;
	}

	public String getCorrida() {
		return corrida;
	}

	public OcupacionReq setOrigen(String origen) {
		this.origen = origen;
		return this;
	}

	public OcupacionReq setDestino(String destino) {
		this.destino = destino;
		return this;
	}

	public OcupacionReq setFecha(String fecha) {
		this.fecha = fecha;
		return this;
	}

	public OcupacionReq setCorrida(String corrida) {
		this.corrida = corrida;
		return this;
	}

	private String getDateWithFormat() {

		Log.i("DiagramaReq", "Fecha sin formato" + fecha);

		Date d = Text.getEtnDateFromStringWithSlash(fecha);
		return Text.formatDateNoSpace(d);

	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub
		BasicNameValuePair[] map = new BasicNameValuePair[4];

		map[0] = new BasicNameValuePair("origen", origen);
		map[1] = new BasicNameValuePair("destino", destino);
		map[2] = new BasicNameValuePair("fecha", getDateWithFormat());
		map[3] = new BasicNameValuePair("corrida", corrida);

		return map;
	}

}
