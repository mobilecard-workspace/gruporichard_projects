package org.addcel.android.etn.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.etn.constant.Url;
import org.addcel.android.etn.to.Corrida;
import org.addcel.android.etn.to.CorridaReq;
import org.addcel.android.etn.ws.client.ETNClient;
import org.addcel.android.etn.ws.listener.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.globals.RichardApplication;
import org.addcel.util.ErrorUtil;
import org.addcel.view.Corridas_ListAdapter;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ETN_Corridas_Activity extends Activity {
	List<Corrida> corridas;
	Corrida c;
	Corrida corridaSalida;
	ListView listCorridasSalida;
	LinearLayout corridaRegreso;
	TextView salidaSelect;

	private RichardApplication myRichardApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_elegir_corrida);
		// corridas = getIntent().getParcelableArrayListExtra("corridas");
		// Log.i("CorridasActivity", corridas.toString());
		myRichardApp = (RichardApplication) getApplicationContext();
		salidaSelect = (TextView) findViewById(R.id.textSalidaSlect);
		Button btnSig = (Button)findViewById(R.id.btn_etnSig2);
		btnSig.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (corridaSalida != null) {
					myRichardApp.setCorridaSalida(corridaSalida);
					startActivity(new Intent(ETN_Corridas_Activity.this,ETN_Asientos_Activity.class));					
				}else {
					Toast.makeText(ETN_Corridas_Activity.this, "Intenta de nuevo", Toast.LENGTH_SHORT).show();
				}
				
				}
		});

		// c = corridas.get(0);
		listCorridasSalida = (ListView) findViewById(R.id.listaSalidas);
		corridaRegreso = (LinearLayout) findViewById(R.id.layout_corridaRegreso);
		getCorridas();
	}

	public void getCorridas() {

		CorridaReq request = new CorridaReq()
				.setDestino(myRichardApp.getDestino())
				.setFechaIni(myRichardApp.getDateSalida())
				.setNumAdulto(myRichardApp.getAdultos())
				.setNumEst(myRichardApp.getEstudiantes())
				.setNumInsen(myRichardApp.getInsen())
				.setNumMto(myRichardApp.getMaestros())
				.setNumNino(myRichardApp.getNinos())
				.setOrigen(myRichardApp.getOrigen());

		new ETNClient(ETN_Corridas_Activity.this).hasLoader(true)
				.setCancellable(false).setListener(new CorridasGetListener())
				.setUrl(Url.CORRIDAS_GET).execute(request.toMap());

	}

	private class CorridasGetListener implements WSResponseListener {

		private static final String TAG = "CorridasGetListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

			if (null == response || "".equals(response)) {
				Toast toast = Toast.makeText(ETN_Corridas_Activity.this,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {
				Log.d(TAG, response.toString());

				JSONArray array = response.optJSONArray("listaCorridas");

				if (null != array && 0 < array.length()) {

					final ArrayList<Corrida> corridas = new ArrayList<Corrida>(
							array.length());

					for (int i = 0; i < array.length(); i++) {
						corridas.add(new Corrida(array.optJSONObject(i)));
					}
					final Corridas_ListAdapter myAdapter = new Corridas_ListAdapter(
							ETN_Corridas_Activity.this, corridas);
					listCorridasSalida.setAdapter(myAdapter);
					listCorridasSalida
							.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int pos, long id) {
									// TODO Auto-generated method stub

									salidaSelect.setText(corridas.get(pos)
											.getHoraCorrida() + " hrs");
									corridaSalida = corridas.get(pos);
								}
							});

				} else {
					Toast.makeText(ETN_Corridas_Activity.this,
							ErrorUtil.emptyArray("corridas"),
							Toast.LENGTH_SHORT).show();
				}
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub

		}

	}
}
