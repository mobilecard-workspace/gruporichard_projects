package org.addcel.android.etn.view;

import java.util.Date;
import java.util.List;

import org.addcel.android.etn.constant.Url;
import org.addcel.android.etn.enums.Catalogos;
import org.addcel.android.etn.to.DestinoReq;
import org.addcel.android.etn.ws.client.ETNClient;
import org.addcel.android.etn.ws.listener.WSResponseListener;
import org.addcel.android.widget.SpinnerValues;
import org.addcel.android.widget.SpinnerValuesListener;
import org.addcel.richard.R;
import org.addcel.richard.globals.RichardApplication;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.R.style;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

public class ETN_Main_Activity extends Activity {

	private LinearLayout regresoLayout;
	private Spinner origenSpinner, destinoSpinner;
	private SpinnerValues origenValues, destinoValues;

	private List<BasicNameValuePair> origenes;
	private List<BasicNameValuePair> destinos;

	private RichardApplication myRichardApp;
	private Context contxt = ETN_Main_Activity.this;

	private NumberPicker pikerMenor;
	private NumberPicker pickerAdulto;
	private NumberPicker pickerSenil;
	private NumberPicker pickerEstudiante;
	private NumberPicker pickerProfesor;

	private DatePicker pikerDateSalida;
	private DatePicker pickerDateRegreso;
	
	private int adultos;
	private int ninos;
	private int insen;
	private int maestros;
	private int estudiantes;
	
	private Date dateSalida;
	private Date dateRegreso;

	static final int PICKER_MINIMO = 0;
	static final int PICKER_MAXIMO = 10;
	
	static final int SALIDA = 0;
	static final int REGRESO = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main_etn);
		
		myRichardApp = (RichardApplication)getApplicationContext();
		
		dateSalida = new Date();
		dateRegreso = new Date();

		origenSpinner = (Spinner) findViewById(R.id.spinOrigen);
		destinoSpinner = (Spinner) findViewById(R.id.spinDestino);

		regresoLayout = (LinearLayout) findViewById(R.id.layout_inicio_regreso);

		RadioGroup myRadioGroup = (RadioGroup) findViewById(R.id.radioGrouoTipo);

		((CompoundButton) findViewById(R.id.rdSencillo)).setChecked(true);

		new ETNClient(ETN_Main_Activity.this).hasLoader(true)
				.setCancellable(false).setCatalogo(Catalogos.ORIGEN)
				.setListener(new OrigenListener()).setUrl(Url.ORIGEN_GET)
				.execute();

		pikerMenor = (NumberPicker) findViewById(R.id.spinMenor);
		pickerAdulto = (NumberPicker) findViewById(R.id.spinAdulto);
		pickerSenil = (NumberPicker) findViewById(R.id.spinSenec);
		pickerEstudiante = (NumberPicker) findViewById(R.id.spinEstudiante);
		pickerProfesor = (NumberPicker) findViewById(R.id.spinProfe);

		startPicker(pikerMenor);
		startPicker(pickerAdulto);
		startPicker(pickerSenil);
		startPicker(pickerEstudiante);
		startPicker(pickerProfesor);

		pikerDateSalida = (DatePicker) findViewById(R.id.datePicker_FechaSalida);
		pickerDateRegreso = (DatePicker) findViewById(R.id.datePicker_FechaRegreso);

		ImageButton pickDateSalida = (ImageButton) findViewById(R.id.btn_calendarS);
		ImageButton pickDateRegreso = (ImageButton) findViewById(R.id.btn_calendarR);

		pickDateSalida.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startCalendar(SALIDA);
			}
		});

		pickDateRegreso.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startCalendar(REGRESO);
			}
		});

		myRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub

				switch (checkedId) {
				case R.id.rdRedondo:
					if (regresoLayout.getVisibility() == View.GONE) {
						regresoLayout.setVisibility(View.VISIBLE);
					}
					break;
				case R.id.rdSencillo:
					if (regresoLayout.getVisibility() == View.VISIBLE) {
						regresoLayout.setVisibility(View.GONE);
					}
					break;
				}
			}
		});

		findViewById(R.id.btn_etnSig).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int nmAs = getNoAsientos();
				if (nmAs == 0) {
					Toast.makeText(contxt, "Seleccione n�mero de asientos.",
							Toast.LENGTH_SHORT).show();
				} else if (nmAs > 19) {
					Toast.makeText(contxt,
							"No puede comprar m�s de 19 boletos.",
							Toast.LENGTH_SHORT).show();
				} else if (ninos >= 1
						&& (adultos == 0 && maestros == 0 && insen == 0)) {
					if (ninos != 0 && estudiantes != 0) {
						Toast.makeText(contxt,
								"No pueden viajar ni�os solos con estudiantes",
								Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(contxt, "No pueden viajar ni�os solos.",
								Toast.LENGTH_SHORT).show();
					}
				} else {
					
					dateSalida.equals(pikerDateSalida);	

					if (regresoLayout.getVisibility() == View.VISIBLE) {
						dateRegreso.equals(pickerDateRegreso);
						myRichardApp.setDateRegreso(dateRegreso);
					}
					saveDataApplication();
					startActivity(new Intent(contxt, ETN_Corridas_Activity.class));
				}
			}
		});
	}

	private void startCalendar(final int type) {
		int year = 0;
		int monthOfYear = 0;
		int dayOfMonth = 0;
		DatePickerDialog myCalendar = new DatePickerDialog(contxt,
				style.Theme_Holo_Light, new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int month,
							int day) {
						// TODO Auto-generated method stub
						if (type == SALIDA) {
							pikerDateSalida.init(year, month, day, null);
						} else if (type == REGRESO) {
							pickerDateRegreso.init(year, month, day, null);
						}
					}
				}, year, monthOfYear, dayOfMonth);
		myCalendar.show();
	}

	private int getNoAsientos() {
		int tot;
		ninos = pikerMenor.getValue();
		adultos = pickerAdulto.getValue();
		insen = pickerSenil.getValue();
		estudiantes = pickerEstudiante.getValue();
		maestros = pickerProfesor.getValue();
		
		tot = ninos + adultos + insen + estudiantes + maestros;
		return tot;
	}
	
	private void saveDataApplication(){
		myRichardApp.setNinos(ninos);
		myRichardApp.setAdultos(adultos);
		myRichardApp.setInsen(insen);
		myRichardApp.setEstudiantes(estudiantes);
		myRichardApp.setMaestros(maestros);
		
		myRichardApp.setDateSalida(dateSalida);
		
		myRichardApp.setDestino(destinoValues.getValorSeleccionado());
		myRichardApp.setOrigen(origenValues.getValorSeleccionado());
		myRichardApp.setOrigenText(origenValues.getTextoSeleccionado());
		myRichardApp.setDestinoText(destinoValues.getTextoSeleccionado());
		
	}

	private void startPicker(NumberPicker picker) {
		picker.setMinValue(PICKER_MINIMO);
		picker.setMaxValue(PICKER_MAXIMO);
		picker.setWrapSelectorWheel(true);
	}

	private class OrigenListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			origenes = catalog;

			if (null == origenValues) {

				origenValues = new SpinnerValues(ETN_Main_Activity.this,
						origenSpinner, origenes);
				origenValues.setSpinListener(new SpinnerValuesListener() {

					@Override
					public void ItemSelected(String texto, String value) {
						// TODO Auto-generated method stub
						Log.i("OrigenListener", texto + ": " + value);

						BasicNameValuePair request = new BasicNameValuePair(
								texto, value);

						new ETNClient(ETN_Main_Activity.this)
								.hasLoader(true)
								.setCancellable(false)
								.setCatalogo(Catalogos.DESTINO)
								.setListener(new DestinoListener())
								.setUrl(Url.DESTINO_GET)
								.execute(
										new DestinoReq().setOrigen(request)
												.toMap());
					}

				});
			}

			new ETNClient(ETN_Main_Activity.this)
					.hasLoader(true)
					.setCancellable(false)
					.setCatalogo(Catalogos.DESTINO)
					.setListener(new DestinoListener())
					.setUrl(Url.DESTINO_GET)
					.execute(
							new DestinoReq().setOrigen(origenes.get(0)).toMap());

			
		}
		
		
		
	}

	private class DestinoListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			destinos = catalog;

			if (null == destinoValues){
				destinoValues = new SpinnerValues(ETN_Main_Activity.this,
						destinoSpinner, destinos);
			
			}
			else{
				destinoValues.setDatos(destinos);
				
				}
		}
	}

	

}
