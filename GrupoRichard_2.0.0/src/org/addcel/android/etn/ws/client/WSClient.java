package org.addcel.android.etn.ws.client;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.crypto.Crypto;
import org.addcel.android.util.Text;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml.Encoding;


/**
 * <p>Clase que extiende de AsyncTask, espera un par�metro String que es
 *	un request a enviar, y devuelve un String que es el response a una petici�n http.
 *	Idealmente esperar�a �nicamente un JSONObject y el resultado ser�a otro JSONObject, pero por 
 *	implementaciones legadas, debe poder recibir Strings.</p>
 *
 * @author carlosgs
 * 
 */
public class WSClient extends AsyncTask<String, Void, String> {

	private Context con;
	private boolean loader;
	private WSResponseListener listener;
	private String url;
	private Cifrados cifrado;
	private org.addcel.android.etn.enums.Catalogos catalogo;
	private static final String TAG = "WSClient";
	private ProgressDialog progressDialog;
	private String key;
	private int connectionTimeout;
	private int responseTimeout;
	private boolean cancellable;
	
	/**
	 * 
	 * @param _con Context desde el que se ejecuta la clase. El context se usa 
	 * para mostrar un ProgressDialog.
	 */
	public WSClient(Context _con) {
		con = _con;
		connectionTimeout = 10000;
		responseTimeout = 30000;
		cancellable = true;
	}
	
	public WSClient hasLoader(boolean _loader) {
		loader = _loader;
		
		return this;
	}
	
	public WSClient setListener(WSResponseListener _listener) {
		if (null != _listener)
			listener = _listener;
		
		return this;
	}
	
	public WSClient setUrl(String _url) {
		if (!TextUtils.isEmpty(_url))
			url = _url;
		
		return this;
	}
	
	public WSClient setCifrado(Cifrados _cifrado) {
		cifrado = _cifrado;
		
		return this;
	}
	
	public WSClient setKey(String _key) {
		key = _key;
		
		return this;
	}
	
	public WSClient setCatalogo(org.addcel.android.etn.enums.Catalogos _catalogo) {
		catalogo = _catalogo;
		
		return this;
	}
	
	public WSClient setConnectionTimeout(int _timeout) {
		connectionTimeout = _timeout;
		
		return this;
	}
	
	public WSClient setResponseTimeout(int _timeout) {
		responseTimeout =_timeout;
		
		return this;
	}
	
	public WSClient setCancellable(boolean _cancellable) {
		cancellable = _cancellable;
		
		return this;
	}
	

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (loader) {
			progressDialog = ProgressDialog.show(con, "Cargando...", "Espere por favor...", cancellable);
		}
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		try {
			
			HttpParams connParams = new BasicHttpParams();
			
			HttpConnectionParams.setConnectionTimeout(connParams, connectionTimeout);
			HttpConnectionParams.setSoTimeout(connParams, responseTimeout);
			
			DefaultHttpClient httpClient = new DefaultHttpClient(connParams);
			HttpPost httpPost = new HttpPost(url);
			

			Log.d(TAG, "Intentando conectar con: " + url);
			
			if (params.length > 0 && params != null) {
				
				Log.d(TAG, Arrays.toString(params));
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				
				nameValuePairs.add(new BasicNameValuePair("json", encryptRequest(cifrado, params[0])));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
		        Log.d(TAG, "Con params: " + nameValuePairs.toString());
			}
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			String response = EntityUtils.toString(httpEntity, Encoding.UTF_8.name());
			 
			return decryptResponse(cifrado, response);
			
		} catch (ClientProtocolException e) {
			Log.e(TAG, "ClientProtocolException",e);
			return "";
		} catch (ConnectTimeoutException e) {
			Log.e(TAG, "ConnectTimeoutException", e);
			return "";
		} catch(SocketTimeoutException e) {
			Log.e(TAG, "SocketTimeoutException", e);
			return "";
		} catch (IOException e) {
			Log.e(TAG, "IOException", e);
			return "";
		}
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		
		if (loader)
			progressDialog.dismiss();
		
		try {
			JSONObject resultJSON = new JSONObject(result);
			
			if (this.catalogo != null) {
				
				JSONArray array = resultJSON.getJSONArray(catalogo.getNombre());
				List<BasicNameValuePair> catalogo = new ArrayList<BasicNameValuePair>(array.length());
				
				for (int i = 0; i < array.length(); i++) {
					JSONObject item = array.getJSONObject(i);
					catalogo.add(new BasicNameValuePair(item.getString(this.catalogo.getDescripcion()), item.getString(this.catalogo.getClave())));
				}
				
				listener.onCatalogReceived(catalogo);
				
			} else {
				listener.onJsonObjectReceived(resultJSON);
			}
		} catch (JSONException obEx) {
			
			Log.e(TAG, "", obEx);
			
			try {
				JSONArray resultJSONArray = new JSONArray(result);
				listener.onJsonArrayReceived(resultJSONArray);	
			} catch (JSONException arrEx) {
				
				Log.e(TAG, "", arrEx);
				
				listener.onStringReceived(result);
			}
			
		} catch (NullPointerException e) {
			
			Log.e(TAG, "", e);
			listener.onStringReceived(result);
		}
	}
	
	/**
	 * 
	 * @param c M�todo de cifrado a utilizar
	 * @param request String a cifrar.
	 * @return String cifrada, usando el m�todo seleccionado.
	 */
	private String encryptRequest(Cifrados c, String request) {
		
		if (null == c)
			return AddcelCrypto.encryptHard(request);
		
		switch (c) {
		case LIMPIO:
			return request;
		case HARD:
			return AddcelCrypto.encryptHard(request);
		case SENSITIVE:
			return AddcelCrypto.encryptSensitive(AddcelCrypto.getKey(), request);
		case MIXED:
			return AddcelCrypto.encryptSensitive(AddcelCrypto.getKey(), request);
		case MOBILECARD:
			String encrypt = Crypto.aesEncrypt(Text.parsePass(key), request);
			return Text.mergeStr(encrypt, key);
		
		default:
			return AddcelCrypto.encryptHard(request);
		}
	}
	
	/**
	 * 
	 * @param c M�todo de cifrado a utilizar
	 * @param response String a descifrar.
	 * @return String limpia, descifrada con el m�todo seleccionado.
	 */
	private String decryptResponse(Cifrados c, String response) {
		
		if (null == c)
			return AddcelCrypto.decryptHard(response);
		
		switch (c) {
		case LIMPIO:
			return response;
		case HARD:
			return AddcelCrypto.decryptHard(response);
		case SENSITIVE:
			return AddcelCrypto.decryptSensitive(response);
		case MIXED:
			return AddcelCrypto.decryptHard(response);
		case MOBILECARD:
			return Crypto.aesDecrypt(Text.parsePass(key), response);

		default:
			return AddcelCrypto.decryptHard(response);
		}
	}

}
