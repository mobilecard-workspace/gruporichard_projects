package org.addcel.android.etn.ws.listener;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public interface WSResponseListener {

	public void onStringReceived(String response);
	public void onJsonObjectReceived(JSONObject response);
	public void onJsonArrayReceived(JSONArray response);
	public void onCatalogReceived(List<BasicNameValuePair> catalog);
}
