package org.addcel.android.widget;


import java.util.List;

import org.addcel.util.ListUtils;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SpinnerValues {
	/**
	 * Propiedades
	 */
	
	private Spinner spinner = null;
	private List<BasicNameValuePair> datos;
	private Context ctx = null;
	private String textoSeleccionado = "";
	private String valorSeleccionado = "";
	private SpinnerValuesListener spinListener;
	
	public Spinner getSpinner(){
		return spinner;
	}
	
	
	public List<BasicNameValuePair> getDatos(){
		return datos;
	}
	
	
	public void setDatos(List<BasicNameValuePair> datos){
		if (datos != null){
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				ctx, 
				android.R.layout.simple_spinner_item,
				ListUtils.ArrayNameValuePairsToArrayKeys(datos)
			);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			this.spinner.setAdapter(adapter);
			this.datos = datos;
			
			//inicializamos texto y valor seleccionados
			if (datos.size() > 0){
				setTextoSeleccionado(datos.get(0).getName(), false);
				setValorSeleccionado(datos.get(0).getValue(), false);
			}
		}
	}
	
	
	/**
	 * Constructores
	 */
	
	public SpinnerValues(Context ctx, Spinner spinner){
		this(ctx, spinner, null);
	}
	
	
	public SpinnerValues(Context ctx, Spinner spinner, String[] textos, String[] valores){
		this(ctx, spinner, ListUtils.arraysToArrayNameValuePairs(textos, valores));
	}
	
	
	public SpinnerValues(Context ctx, Spinner spinner, List<BasicNameValuePair> datos){
		this.ctx = ctx;
		this.spinner = spinner;
		setDatos(datos);
		
		this.spinner.setOnItemSelectedListener(
			new OnItemSelectedListener(){
				public void onItemSelected(AdapterView<?> adapterView, View view, int posicion, long id) {
					setTextoSeleccionado(SpinnerValues.this.datos.get(posicion).getName(), false);
					setValorSeleccionado(SpinnerValues.this.datos.get(posicion).getValue(), false);
					if(spinListener != null){
						spinListener.ItemSelected(getTextoSeleccionado(), getValorSeleccionado());
					}
				}

				public void onNothingSelected(AdapterView<?> adapterView){}
			}
		);
	}
	
	
	/**
	 * Metodos
	 */
	
	public void setTextoSeleccionado(String texto){
		setTextoSeleccionado(texto, true);
	}
	
	
	public void setTextoSeleccionado(String texto, boolean updateSpinner){
		textoSeleccionado = texto;
		
		if (updateSpinner){
			for (int a = 0; a < datos.size(); a++){
				if (datos.get(a).getName().equals(texto)){
					spinner.setSelection(a);
					break;
				}
			}
		}
	}
	
	
	public String getTextoSeleccionado(){
		return textoSeleccionado;
	}
	
	
	public void setValorSeleccionado(String valor){
		setValorSeleccionado(valor, true);
	}
	
	
	public void setValorSeleccionado(String valor, boolean updateSpinner){
		valorSeleccionado = valor;
		
		if (updateSpinner){
			for (int a = 0; a < datos.size(); a++){
				if (datos.get(a).getValue().equals(valor)){
					spinner.setSelection(a);
					break;
				}
			}
		}
	}
	
	
	public String getValorForText(String text){
		for (BasicNameValuePair nvp : datos){
			if (nvp.getName().equals(text)){
				return nvp.getValue();
			}
		}
		
		return "";
	}
	
	
	public String getTextForValor(String valor){
		for (BasicNameValuePair nvp : datos){
			if (nvp.getValue().equals(valor)){
				return nvp.getName();
			}
		}
		
		return "";
	}
	
	
	public String getValorSeleccionado(){
		return valorSeleccionado;
	}
	
	public void setSpinListener(SpinnerValuesListener spinListener){
		this.spinListener = spinListener;
	}
}
