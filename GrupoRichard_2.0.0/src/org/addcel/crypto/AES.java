package org.addcel.crypto;

import java.io.IOException;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Base64;

/**
 * Encrypt/decrypt .bin files in AES format (Windows/Linux).
 * 
 * @author David Croft
 * @version $Revision: 1.1 $
 */
public class AES {
	private static final byte EOF_MARKER = (byte) 0xFD;

	private AES() {
	}

	// Java Crypto API - can't use because user will have to install 192-bit
	// policy file.
	// SecretKey key = new SecretKeySpec(KEY, "AES");
	// Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");//CBC
	// cipher.init(Cipher.DECRYPT_MODE, key);
	// byte[] decrypted = cipher.doFinal(bytes);

	public static String decode(String inputTextBase64, String key)
			throws IOException {
		byte[] inputBytes = Base64.decode(inputTextBase64.getBytes());
		BufferedBlockCipher cipher = getCipher(false, key);
		byte[] outputBytes = new byte[cipher.getOutputSize(inputBytes.length)];
		int outputLen = cipher.processBytes(inputBytes, 0, inputBytes.length,
				outputBytes, 0);

		try {
			outputLen += cipher.doFinal(outputBytes, outputLen);
		} catch (InvalidCipherTextException e) {
			throw new IOException("Can't decrypt file: "
					+ e.getLocalizedMessage());
		}

		for (int i = outputLen - 16; i < outputLen; ++i) {
			byte b = outputBytes[i];

			if (b == EOF_MARKER) {
				outputLen = i;
				break;
			}
		}

		byte[] finalBytes = new byte[outputLen];
		System.arraycopy(outputBytes, 0, finalBytes, 0, outputLen);

		/**
		 * NOTE: Mucho cuidado con el encoding que se le aplica al string
		 * final!!
		 */
		return new String(finalBytes, "UTF-8");
	}

	public static String encode(String inputText, String key)
			throws IOException {
		/**
		 * NOTE: Mucho cuidado con el encoding que se le aplica al string
		 * final!!
		 */
		byte[] inputBytes = inputText.getBytes("UTF-8");

		/*
		 * If input was multiple of 16, NO padding. Example:
		 * res\levels\BulletinBoardSystem\BulletinBoardSystem.level.bin
		 */
		/* Otherwise pad to next 16 byte boundary */
		int origSize = inputBytes.length;

		if (origSize % 16 != 0) {
			int padding = 16 - origSize % 16;
			int newSize = origSize + padding;

			byte[] newInputBytes = new byte[newSize];
			System.arraycopy(inputBytes, 0, newInputBytes, 0, origSize);
			inputBytes = newInputBytes;

			/*
			 * Write up to 4 0xFD bytes immediately after the original file. The
			 * remainder can stay as the 0x00 provided by Arrays.copyOf.
			 */
			for (int i = origSize; i < origSize + 4 && i < newSize; ++i) {
				inputBytes[i] = EOF_MARKER;
			}
		}

		BufferedBlockCipher cipher = getCipher(true, key);
		byte[] outputBytes = new byte[cipher.getOutputSize(inputBytes.length)];
		int outputLen = cipher.processBytes(inputBytes, 0, inputBytes.length,
				outputBytes, 0);

		try {
			outputLen += cipher.doFinal(outputBytes, outputLen);
		} catch (InvalidCipherTextException e) {
			throw new IOException("Can't encrypt file: "
					+ e.getLocalizedMessage());
		}

		byte[] finalBytes = new byte[outputLen];
		System.arraycopy(outputBytes, 0, finalBytes, 0, outputLen);

		return new String(Base64.encode(finalBytes));
	}

	private static BufferedBlockCipher getCipher(boolean forEncryption,
			String key) {
		BlockCipher engine = new AESEngine();
		BufferedBlockCipher cipher = new BufferedBlockCipher(
				new CBCBlockCipher(engine));
		cipher.init(forEncryption, new KeyParameter(key.getBytes()));

		return cipher;
	}
}