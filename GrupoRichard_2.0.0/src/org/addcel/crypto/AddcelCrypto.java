package org.addcel.crypto;

import java.util.Date;

import android.util.Log;

public class AddcelCrypto {

	private final static String keyCat = "1234567890ABCDEF0123456789ABCDEF";

	public static String encryptHard(String json) {
		String respuesta = null;
		try {
			respuesta = encrypt(keyCat, json);
		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso Asf.", e.getMessage());
			respuesta = null;
		}
		return respuesta;
	}

	public static String encryptSensitive(String key, String json) {
		String respuesta = null;
		try {
			json = encrypt(parsePass(key).toString(), json);
			respuesta = mergeAsf(json, key);
			Log.d("CADENA mergeAsf respuesta: ", respuesta);
		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso decryptSensitive.",
					e.getMessage());
			respuesta = null;
		} finally {
			key = null;
		}
		return respuesta;

	}

	private static String encrypt(String key, String json) {
		String respuesta = null;
		try {
			Log.d("CADENA encrypt key: ", key);
			Log.d("CADENA encrypt json: ", json);
			respuesta = Crypto.aesEncrypt(key, json);
			Log.d("CADENA encrypt respuesta: ", respuesta);
		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso decrypt.",
					e.getMessage());
			respuesta = null;
		} finally {
			key = null;
			json = null;
		}
		return respuesta;
	}

	public static String decryptHard(String json) {
		String respuesta = null;
		try {
			respuesta = decrypt(keyCat, json);
		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso Asf.", e.getMessage());
			respuesta = null;
		}
		return respuesta;
	}

	public static String decryptSensitive(String json) {
		String key = null;
		String[] cad = null;
		String respuesta = null;
		try {
			cad = Asf(json);
			key = cad[0];
			json = cad[1];
			respuesta = decrypt(key, json);
		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso decryptSensitive.",
					e.getMessage());
			respuesta = null;
		} finally {
			key = null;
			cad = null;
		}
		return respuesta;
	}

	private static String decrypt(String key, String json) {
		String respuesta = null;
		try {
			Log.d("CADENA decrypt key: ", key);
			Log.d("CADENA decrypt json: ", json);
			respuesta = Crypto.aesDecrypt(key, json.replace(" ", "+"));
			Log.d("CADENA decrypt respuesta: ", respuesta);
		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso decrypt.",
					e.getMessage());
			respuesta = null;
		} finally {
			key = null;
		}
		return respuesta;
	}

	private static String[] Asf(String cadena) {

		StringBuffer key = new StringBuffer();
		String cadenaCompleta = "";
		String[] res = new String[3];
		int Long = 0;
		String tempo = "";
		try {

			Long = Integer.parseInt(cadena.substring(0, 2));
			Log.i("Longuitud:", String.valueOf(Long));
			Log.i("Longuitud cadena: ", String.valueOf(cadena.length()));

			cadenaCompleta = cadena.substring(2);
			Log.d("Cadena completa: ", cadenaCompleta);

			if (Long <= 8) {
				key.append(cadena.substring(21, 23))
						.append(cadena.substring(25, 27))
						.append(cadena.substring(29, 31))
						.append(cadena.substring(33, 35));
				cadenaCompleta = cadenaCompleta.substring(0, 19)
						+ cadenaCompleta.substring(21);
				cadenaCompleta = cadenaCompleta.substring(0, 21)
						+ cadenaCompleta.substring(23);
				cadenaCompleta = cadenaCompleta.substring(0, 23)
						+ cadenaCompleta.substring(25);
				cadenaCompleta = cadenaCompleta.substring(0, 25)
						+ cadenaCompleta.substring(27);
			}
			// else{
			// for (int i =0, j = 21; i < ciclos[Long]; i++, j+=4){
			// key.append(cadena.substring(j,j+2));
			// cadenaCompleta = cadenaCompleta.substring(i,i+19) +
			// cadenaCompleta.substring(i+21) ;
			// log.debug("i: " + i + "   key: " + key + "    cadena: " +
			// cadenaCompleta);
			// }
			// }
			// log.debug(new StringBuffer("Paso 0: ").append( key ));

			if (Long == 9) {
				key.append(cadena.substring(21, 23))
						.append(cadena.substring(25, 27))
						.append(cadena.substring(29, 31))
						.append(cadena.substring(33, 35))
						.append(cadena.substring(37, 38));
				cadenaCompleta = cadenaCompleta.substring(0, 19)
						+ cadenaCompleta.substring(21);
				cadenaCompleta = cadenaCompleta.substring(0, 21)
						+ cadenaCompleta.substring(23);
				cadenaCompleta = cadenaCompleta.substring(0, 23)
						+ cadenaCompleta.substring(25);
				cadenaCompleta = cadenaCompleta.substring(0, 25)
						+ cadenaCompleta.substring(27);
				cadenaCompleta = cadenaCompleta.substring(0, 27)
						+ cadenaCompleta.substring(28);
			}
			// log.debug(new StringBuffer("Paso 1: ").append( key ));

			if (Long == 10) {
				key.append(cadena.substring(21, 23))
						.append(cadena.substring(25, 27))
						.append(cadena.substring(29, 31))
						.append(cadena.substring(33, 35))
						.append(cadena.substring(37, 39));
				cadenaCompleta = cadenaCompleta.substring(0, 19)
						+ cadenaCompleta.substring(21);
				cadenaCompleta = cadenaCompleta.substring(0, 21)
						+ cadenaCompleta.substring(23);
				cadenaCompleta = cadenaCompleta.substring(0, 23)
						+ cadenaCompleta.substring(25);
				cadenaCompleta = cadenaCompleta.substring(0, 25)
						+ cadenaCompleta.substring(27);
				cadenaCompleta = cadenaCompleta.substring(0, 27)
						+ cadenaCompleta.substring(29);
			}

			// log.debug(new StringBuffer("Paso 2: ").append( key ));
			if (Long == 11) {
				key.append(cadena.substring(21, 23))
						.append(cadena.substring(25, 27))
						.append(cadena.substring(29, 31))
						.append(cadena.substring(33, 35))
						.append(cadena.substring(37, 38))
						.append(cadena.substring(38, 39))
						.append(cadena.substring(41, 42));
				cadenaCompleta = cadenaCompleta.substring(0, 19)
						+ cadenaCompleta.substring(21);
				cadenaCompleta = cadenaCompleta.substring(0, 21)
						+ cadenaCompleta.substring(23);
				cadenaCompleta = cadenaCompleta.substring(0, 23)
						+ cadenaCompleta.substring(25);
				cadenaCompleta = cadenaCompleta.substring(0, 25)
						+ cadenaCompleta.substring(27);
				cadenaCompleta = cadenaCompleta.substring(0, 27)
						+ cadenaCompleta.substring(29);
				cadenaCompleta = cadenaCompleta.substring(0, 29)
						+ cadenaCompleta.substring(30);
			}
			// log.debug(new StringBuffer("Paso 3: ").append( key ));

			if (Long == 12) {
				key.append(cadena.substring(21, 23))
						.append(cadena.substring(25, 27))
						.append(cadena.substring(29, 31))
						.append(cadena.substring(33, 35))
						.append(cadena.substring(37, 38))
						.append(cadena.substring(38, 39))
						.append(cadena.substring(41, 42))
						.append(cadena.substring(42, 43));

				cadenaCompleta = cadenaCompleta.substring(0, 19)
						+ cadenaCompleta.substring(21);
				cadenaCompleta = cadenaCompleta.substring(0, 21)
						+ cadenaCompleta.substring(23);
				cadenaCompleta = cadenaCompleta.substring(0, 23)
						+ cadenaCompleta.substring(25);
				cadenaCompleta = cadenaCompleta.substring(0, 25)
						+ cadenaCompleta.substring(27);
				cadenaCompleta = cadenaCompleta.substring(0, 27)
						+ cadenaCompleta.substring(29);
				cadenaCompleta = cadenaCompleta.substring(0, 29)
						+ cadenaCompleta.substring(31);
			}

			for (int y = Long - 1; y != -1; y--) {
				tempo += key.substring(y, y + 1);
			}

			res[2] = key.toString();
			key = parsePass(tempo);
			res[0] = key.toString();
			res[1] = cadenaCompleta.toString();

			Log.i("Key:", key.toString());
			Log.i("cadenaCompleta: ", cadenaCompleta);

		} catch (Exception e) {
			Log.e("Ocurrio un error durante el proceso Asf.", e.getMessage());
			res = null;
		} finally {
			key = null;
			cadenaCompleta = null;
			tempo = null;
		}
		return res;
	}

	private static StringBuffer parsePass(String pass) {
		int len = pass.length();
		StringBuffer key = new StringBuffer();

		for (int i = 0; i < 32 / len; i++) {
			key.append(pass);
		}

		int carry = 0;
		while (key.length() < 32) {
			key.append(pass.charAt(carry));
			carry++;
		}
		return key;
	}

	private static String mergeAsf(String json, String key) {
		StringBuffer result = new StringBuffer();
		StringBuilder builder = null;
		int offset = 0;
		String next = null;
		String sub1 = null;
		String sub2 = null;
		try {
			builder = new StringBuilder(key);
			key = builder.reverse().toString();

			result.append((Integer.toString(key.length()).length() < 2) ? "0"
					+ key.length() : key.length());

			sub1 = json.substring(0, 19);
			sub2 = json.substring(19, json.length());

			result.append(sub1);

			for (int i = 0; i < key.length(); i += 2) {
				offset = ((i + 2) <= key.length()) ? (i + 2) : (i + 1);
				next = key.substring(i, offset);

				result.append(next);
				result.append(sub2.substring(0, 2));
				sub2 = sub2.substring(2);

			}
			result.append(sub2);
		} catch (Exception e) {

		}
		return result.toString();
	}

	public static String mergeStr(String first, String second) {
		StringBuilder builder = null;
		String result = "";
		builder = new StringBuilder(second);
		String other = builder.reverse().toString();

		result += (Integer.toString(other.length()).length() < 2) ? "0"
				+ other.length() : Integer.toString(other.length());

		String sub1 = first.substring(0, 19);
		String sub2 = first.substring(19, first.length());

		result += sub1;

		for (int i = 0; i < other.length(); i += 2) {
			int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);
			String next = other.substring(i, offset);

			result += next;

			result += sub2.substring(0, 2);

			sub2 = sub2.substring(2);

			// log.debug("i: " + i + "  offset: " + offset + "   result: " +
			// result + "    next: " + next + "   sub2: " + sub2);
		}
		result += sub2;

		return result;
	}

	private static final int ciclos[] = new int[20];

	static {
		ciclos[9] = 5;
		ciclos[10] = 5;
		ciclos[11] = 6;
		ciclos[12] = 6;
		ciclos[13] = 7;
		ciclos[14] = 7;
		ciclos[15] = 8;
		ciclos[16] = 8;
		ciclos[17] = 9;
		ciclos[18] = 9;
	}

	public static String getKey() {
		Long seed = new Date().getTime();
		
		String key = String.valueOf(seed).substring(0, 8);
		Log.d("AddcelCrypto", "SENSITIVE KEY: " + key);
		
		return key;
	}
}
