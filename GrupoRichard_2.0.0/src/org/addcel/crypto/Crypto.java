package org.addcel.crypto;

import java.security.MessageDigest;

import android.util.Log;

public class Crypto {
	public static String md5(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte passDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexPass = new StringBuffer();
			for (int i = 0; i < passDigest.length; i++) {
				String h = Integer.toHexString(0xFF & passDigest[i]);
				while (h.length() < 2) {
					h = "0" + h;
				}

				hexPass.append(h);
			}

			return hexPass.toString();
		} catch (Exception e) {
		}

		return "";
	}

	public static String sha1(String s) {
		StringBuffer sha1 = new StringBuffer();

		try {
			MessageDigest digester = MessageDigest.getInstance("SHA-1");
			digester.update(s.getBytes());
			byte[] digest = digester.digest();

			for (byte b : digest) {
				sha1.append(String.format("%02x", b & 0xff));
			}
		} catch (Exception e) {
			Log.e("Exception", e.getMessage());
		}

		return sha1.toString();
	}

	public static String aesEncrypt(String seed, String cleartext) {
		try {
			return AES.encode(replaceConAcento(cleartext), seed);
		} catch (Exception e) {
			Log.e("Exception", e.getMessage());
			return "";
		}
	}

	public static String aesDecrypt(String seed, String encrypted) {
		try {
			return reemplazaHTML(AES.decode(encrypted, seed));
		} catch (Exception e) {
			Log.e("Exception", e.getMessage());
			return "";
		}
	}

	public static String replaceConAcento(String text) {
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < text.length(); i++) {
			if (text.charAt(i) == '�')
				sBuffer.append("&Ntilde;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&ntilde;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Aacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&aacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Eacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&eacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Iacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&iacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Oacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&oacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Uacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&uacute;");
			else
				sBuffer.append(text.charAt(i));
		}

		return sBuffer.toString();
	}

	public static String reemplazaHTML(String html) {
		String caracteres[] = { "&Aacute;", "&aacute;", "&Eacute;", "&eacute;",
				"&Iacute;", "&iacute;", "&Oacute;", "&oacute;", "&Uacute;",
				"&uacute;", "&Ntilde;", "&ntilde;" };
		String letras[] = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
				"�", "�" };

		for (int counter = 0; counter < caracteres.length; counter++) {
			html = reemplaza(html, caracteres[counter], letras[counter]);
		}
		return html;
	}

	public static String reemplaza(String src, String orig, String nuevo) {
		if (src == null) {
			return null;
		}
		int tmp = 0;
		String srcnew = "";
		while (tmp >= 0) {
			tmp = src.indexOf(orig);
			if (tmp >= 0) {
				if (tmp > 0) {
					srcnew += src.substring(0, tmp);
				}
				srcnew += nuevo;
				src = src.substring(tmp + orig.length());
			}
		}
		srcnew += src;
		return srcnew;
	}

}