package org.addcel.interfaces;

import org.json.JSONObject;

public interface JSONable {
	
	public JSONObject toJSON();
	
	public void fromJSON(JSONObject json);

}
