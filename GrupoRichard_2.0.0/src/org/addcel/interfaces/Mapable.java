package org.addcel.interfaces;

import org.apache.http.message.BasicNameValuePair;

public interface Mapable {
	
	public BasicNameValuePair[] toMap();

}
