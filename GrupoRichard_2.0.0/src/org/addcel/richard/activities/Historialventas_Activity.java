package org.addcel.richard.activities;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Record;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.addcel.view.List_Adapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Historialventas_Activity extends Activity implements
		OnClickListener {

	public Historialventas_Activity HistorialVentas = null;
	private SessionManager session;
	private Context contxt = Historialventas_Activity.this;

	private int errornum;
	private String errormsg;
	private JSONArray jsonArray;
	private ListView listview;
	private Button bhoy;
	private Button bsem;
	private Button bmes;
	private Button bmesan;

	private int lastPush = 0;
	private List_Adapter adapter;

	private JSONArray jsonarray;
	//private JSONArray jsonarray1;
	//private JSONArray jsonarray2;
	//private JSONArray jsonarray3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_historialventas);

		listview = (ListView) findViewById(R.id.listaHistorial);
		HistorialVentas = this;

		bhoy = (Button) findViewById(R.id.btn_Hoy);
		bsem = (Button) findViewById(R.id.btn_Semana);
		bmes = (Button) findViewById(R.id.btn_Mes);
		bmesan = (Button) findViewById(R.id.btn_Mesanterior);

		bhoy.setOnClickListener(this);
		bsem.setOnClickListener(this);
		bmes.setOnClickListener(this);
		bmesan.setOnClickListener(this);


	}

	private void findRecord(int type) {
		session = new SessionManager(contxt);
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_TIENDA, session.getIdTienda());
			json.put(Constructors.ID_PROVEEDOR, session.getIdProveedor());
			json.put(Constructors.TIPO_BUSQUEDA, type);
			json.put(Constructors.TIPO_PAGO, 0);
			json.put(Constructors.IMEI, Dispositivo.imei);
			
			Log.i(getPackageName(), json.toString());
		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new recordListener(), contxt, true,
				Urls.url_historial).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private class recordListener implements WSResponseListener {
		private static final String TAG = "recordListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub

			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response.toString());
				try {
					JSONObject json = new JSONObject(response);
					errornum = json.getInt(Constructors.ID_ERROR);
					errormsg = json.getString(Constructors.MENSAJE_ERROR);
				} catch (JSONException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				if (errornum != 0 ) {
					Toast.makeText(contxt, errormsg, Toast.LENGTH_SHORT).show();
					//listview.getAdapter().notify();
				} else {
					try {
						JSONObject json = new JSONObject(response);
						jsonArray = json.getJSONArray(Constructors.PAGOS);
						Log.i(TAG, jsonArray.toString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					adapter = new List_Adapter(HistorialVentas, contxt,
							jsonArray);
					
					
					//listview.setOnItemClickListener(onClickRow);
					
					listview.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int pos, long id) {
							// TODO Auto-generated method stub
							
							List_Adapter l = (List_Adapter) parent.getAdapter();
							JSONObject json = (JSONObject) l.getItem(pos);
							
							//Toast.makeText(contxt, "Producto" + Integer.toString(pos), Toast.LENGTH_SHORT).show();
							
							String idBitacora = null;
							String nombres = null;
							String descProducto = null;
							String total = null;
							String fecha = null;
							try {
								
								Log.i("onItemClickListener", json.toString());
								idBitacora = json.getString(Constructors.ID_BITACORA);
								nombres = json.getString(Constructors.NOMBRES);
								descProducto = json.getString(Constructors.DESCRIPCION_PRODUCT);
								total = json.optString(Constructors.TOTAL);
								fecha = json.getString(Constructors.FECHA);

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							AlertDialog.Builder builder = new AlertDialog.Builder(contxt, 3);
							builder.setTitle("Detalle de Venta");
							builder.setMessage("Descripci�n de producto: " + descProducto
									+ "\nCliente: " + nombres + "\nFecha de compra: " + fecha
									+ "\nReferencia: " + idBitacora + "\nTotal: " + total);

							builder.setPositiveButton("ACEPTAR",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											
										}
									});

							AlertDialog dialog = builder.create();
							dialog.show();
							
						}
					});
					listview.setAdapter(adapter);

				}
			}

		}
	}

	// changeSelector(lastPush, v.getId()); lastPush = v.getId(); }

	private void changeSelector(int last, int id) {
		// TODO Auto-generated
		// methodstub //
		// setContentView(R.layout.activity_historialventas);

		switch (id) {
		case R.id.btn_Hoy:
			if (last == 0) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Semana) {
				bsem.setEnabled(true);
				bsem.setBackgroundResource(R.drawable.selector_semana);
			} else if (last == R.id.btn_Mes) {
				bmes.setEnabled(true);
				bmes.setBackgroundResource(R.drawable.selector_mes);
			} else if (last == R.id.btn_Mesanterior) {
				bmesan.setEnabled(true);
				bmesan.setBackgroundResource(R.drawable.selector_mesanterior);
			}
			bhoy.setEnabled(false);
			bhoy.setBackgroundResource(R.drawable.b_hoy_o);
			break;
		case R.id.btn_Semana:
			if (last == 0) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Hoy) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Mes) {
				bmes.setEnabled(true);
				bmes.setBackgroundResource(R.drawable.selector_mes);
			} else if (last == R.id.btn_Mesanterior) {
				bmesan.setEnabled(true);
				bmesan.setBackgroundResource(R.drawable.selector_mesanterior);
			}
			bsem.setEnabled(false);
			bsem.setBackgroundResource(R.drawable.b_semana_o);
			break;
		case R.id.btn_Mes:
			if (last == 0) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Hoy) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Semana) {
				bsem.setEnabled(true);
				bsem.setBackgroundResource(R.drawable.selector_semana);
			} else if (last == R.id.btn_Mesanterior) {
				bmesan.setEnabled(true);
				bmesan.setBackgroundResource(R.drawable.selector_mesanterior);
			}
			bmes.setEnabled(false);
			bmes.setBackgroundResource(R.drawable.b_mes_o);
			break;

		case R.id.btn_Mesanterior:

			if (last == 0) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Hoy) {
				bhoy.setEnabled(true);
				bhoy.setBackgroundResource(R.drawable.selector_hoy);
			} else if (last == R.id.btn_Mes) {
				bmes.setEnabled(true);
				bmes.setBackgroundResource(R.drawable.selector_mes);
			} else if (last == R.id.btn_Semana) {
				bsem.setEnabled(true);
				bsem.setBackgroundResource(R.drawable.selector_semana);
			}
			bmesan.setEnabled(false);
			bmesan.setBackgroundResource(R.drawable.b_mesanterior_o);
			break;

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// JSONArray json = new JSONArray();
		switch (v.getId()) {
		case R.id.btn_Hoy:
			changeSelector(lastPush, v.getId());

			findRecord(Record.BUSQUEDA_HOY);
			// listview.setOnItemClickListener(onClickRow);
			// json = jsonarray;
			break;
		case R.id.btn_Semana:
			changeSelector(lastPush, v.getId());
			findRecord(Record.BUSQUEDA_SEMANA);
			// json = jsonarray1;
			break;
		case R.id.btn_Mes:
			changeSelector(lastPush, v.getId());
			findRecord(Record.BUSQUEDA_MES);
			// json = jsonarray2;
			break;
		case R.id.btn_Mesanterior:
			changeSelector(lastPush, v.getId());
			findRecord(Record.BUSQUEDA_MES_ANTERIOR);
			// json = jsonarray3;
			break;
		}
		lastPush = v.getId();
		// adapter = new List_Adapter(HistorialVentas, contxt, json);
		// listview.setOnItemClickListener(onClickRow);
		// listview.setAdapter(adapter);
	}

	private OnItemClickListener onClickRow = new OnItemClickListener() {
		 String idBitacora;
	//	private String email;
		 String nombres;
		 String descProducto;
	//	private String telefono;
	//	private String tarjetaTag;
		 String total;
	//	private String status;
	//	private String tipoTarjeta;
	//	private String tipoPago;
		 String fecha;

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int pos,
				long id) {
			// TODO Auto-generated method stub
			JSONObject json = new JSONObject();
			try {
				json = jsonarray.getJSONObject(pos);
				Log.i("onItemClickListener", json.toString());
				idBitacora = json.getString(Constructors.ID_BITACORA);
	//			email = json.getString("email");
				nombres = json.getString(Constructors.NOMBRES);
				descProducto = json.getString(Constructors.DESCRIPCION_PRODUCT);
	//			telefono = json.getString("telefono");
	//			tarjetaTag = json.getString("tarjetaTag");
				total = json.optString(Constructors.TOTAL);
	//			status = json.getString("status");
	//			tipoTarjeta = json.getString("tipoTarjeta");
	//			tipoPago = json.getString("tipoPago");
				fecha = json.getString(Constructors.FECHA);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			AlertDialog.Builder builder = new AlertDialog.Builder(contxt, 3);
			builder.setTitle("Detalle de Venta");
			builder.setMessage("Descripci�n de producto: " + descProducto
					+ "\nCliente: " + nombres + "\nFecha de compra: " + fecha
					+ "\nReferencia: " + idBitacora + "\nTotal: " + total);

			builder.setPositiveButton("ACEPTAR",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
						}
					});

			AlertDialog dialog = builder.create();
			dialog.show();

		}
	};

}

/*
 * public class Historialventas_Activity extends Activity implements
 * OnClickListener {
 * 
 * public Stack<String> mFragmentStack; private int lastPush = 0;
 * 
 * @Override public void onBackPressed() { // TODO Auto-generated method stub
 * Log.i(getClass().toString(), "onBackPressed selected stack size: " +
 * mFragmentStack.size() + mFragmentStack.peek().toString());
 * 
 * // Fragment fragment = //
 * getFragmentManager().findFragmentByTag(mFragmentStack.peek());
 * mFragmentStack.clear(); super.onBackPressed(); startActivity(new
 * Intent(Historialventas_Activity.this,
 * Menu_Activity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)); /*if
 * (mFragmentStack.size() == 1) { startActivity(new
 * Intent(Historialventas_Activity.this, Menu_Activity.class)); } else {
 * 
 * removeFragment(); super.onBackPressed(); } }
 * 
 * private void removeFragment() { mFragmentStack.pop();
 * 
 * // remove from backstack and from mFragmentStack FragmentTransaction
 * transaction = getFragmentManager() .beginTransaction(); Fragment fragment =
 * getFragmentManager().findFragmentByTag( mFragmentStack.peek());
 * transaction.setCustomAnimations(R.anim.slide_out_right,
 * R.anim.slide_in_left); transaction.show(fragment); transaction.commit(); }
 * 
 * @Override public void onCreate(Bundle savedInstanceState) { // TODO
 * Auto-generated method stub super.onCreate(savedInstanceState);
 * requestWindowFeature(Window.FEATURE_NO_TITLE);
 * setContentView(R.layout.activity_historialventas);
 * 
 * final Button bhoy = (Button) findViewById(R.id.btn_Hoy); final Button bsem =
 * (Button) findViewById(R.id.btn_Semana); final Button bmes = (Button)
 * findViewById(R.id.btn_Mes); final Button bmesan = (Button)
 * findViewById(R.id.btn_Mesanterior);
 * 
 * bhoy.setOnClickListener(this); bsem.setOnClickListener(this);
 * bmes.setOnClickListener(this); bmesan.setOnClickListener(this);
 * 
 * bhoy.setEnabled(false); bhoy.setBackgroundResource(R.drawable.b_hoy_o);
 * 
 * mFragmentStack = new Stack<String>();
 * 
 * FragmentManager fragmentManager = getFragmentManager(); FragmentTransaction
 * transaction = fragmentManager.beginTransaction(); Fragment fragmenthoy = new
 * Hoy_Fragment(); String tag = fragmenthoy.toString(); mFragmentStack.add(tag);
 * transaction.add(R.id.fragment_detalleHistorial, fragmenthoy, tag);
 * transaction.addToBackStack(tag); transaction.commit();
 * 
 * }
 * 
 * @Override public void onClick(View v) {
 * 
 * // TODO Auto-generated method stub switch (v.getId()) { case R.id.btn_Hoy:
 * 
 * Fragment hoyFragment = new Hoy_Fragment(); showfragment(hoyFragment); //
 * break; case R.id.btn_Semana:
 * 
 * Fragment semFragment = new Semana_Fragment(); showfragment(semFragment);
 * 
 * break; case R.id.btn_Mes: Fragment mesFragment = new Mes_Fragment();
 * showfragment(mesFragment);
 * 
 * break;
 * 
 * case R.id.btn_Mesanterior: Fragment mespFragment = new MesPasado_Fragment();
 * showfragment(mespFragment);
 * 
 * break; }
 * 
 * changeSelector(lastPush, v.getId()); lastPush = v.getId(); }
 * 
 * private void changeSelector(int last, int id) { // TODO Auto-generated method
 * stub // setContentView(R.layout.activity_historialventas); final Button bhoy
 * = (Button) findViewById(R.id.btn_Hoy); final Button bsem = (Button)
 * findViewById(R.id.btn_Semana); final Button bmes = (Button)
 * findViewById(R.id.btn_Mes); final Button bmesan = (Button)
 * findViewById(R.id.btn_Mesanterior);
 * 
 * switch (id) { case R.id.btn_Hoy: if (last == 0) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Semana) { bsem.setEnabled(true);
 * bsem.setBackgroundResource(R.drawable.selector_semana); } else if (last ==
 * R.id.btn_Mes) { bmes.setEnabled(true);
 * bmes.setBackgroundResource(R.drawable.selector_mes); } else if (last ==
 * R.id.btn_Mesanterior) { bmesan.setEnabled(true);
 * bmesan.setBackgroundResource(R.drawable.selector_mesanterior); }
 * bhoy.setEnabled(false); bhoy.setBackgroundResource(R.drawable.b_hoy_o);
 * break; case R.id.btn_Semana: if (last == 0) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Hoy) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Mes) { bmes.setEnabled(true);
 * bmes.setBackgroundResource(R.drawable.selector_mes); } else if (last ==
 * R.id.btn_Mesanterior) { bmesan.setEnabled(true);
 * bmesan.setBackgroundResource(R.drawable.selector_mesanterior); }
 * bsem.setEnabled(false); bsem.setBackgroundResource(R.drawable.b_semana_o);
 * break; case R.id.btn_Mes: if (last == 0) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Hoy) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Semana) { bsem.setEnabled(true);
 * bsem.setBackgroundResource(R.drawable.selector_semana); } else if (last ==
 * R.id.btn_Mesanterior) { bmesan.setEnabled(true);
 * bmesan.setBackgroundResource(R.drawable.selector_mesanterior); }
 * bmes.setEnabled(false); bmes.setBackgroundResource(R.drawable.b_mes_o);
 * break;
 * 
 * case R.id.btn_Mesanterior:
 * 
 * if (last == 0) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Hoy) { bhoy.setEnabled(true);
 * bhoy.setBackgroundResource(R.drawable.selector_hoy); } else if (last ==
 * R.id.btn_Mes) { bmes.setEnabled(true);
 * bmes.setBackgroundResource(R.drawable.selector_mes); } else if (last ==
 * R.id.btn_Semana) { bsem.setEnabled(true);
 * bsem.setBackgroundResource(R.drawable.selector_semana); }
 * bmesan.setEnabled(false);
 * bmesan.setBackgroundResource(R.drawable.b_mesanterior_o); break;
 * 
 * }
 * 
 * }
 * 
 * private void showfragment(Fragment newfragment) { // TODO Auto-generated
 * method stub Bundle args = new Bundle(); String tag = newfragment.toString();
 * newfragment.setArguments(args);
 * 
 * FragmentTransaction transaction = getFragmentManager() .beginTransaction();
 * transaction.setCustomAnimations(R.anim.slide_in_left,
 * R.anim.slide_out_right);
 * 
 * if (mFragmentStack.size() != 0) { Fragment currentFragment =
 * getFragmentManager().findFragmentByTag( mFragmentStack.peek());
 * transaction.hide(currentFragment); }
 * 
 * transaction.add(R.id.fragment_detalleHistorial, newfragment, tag);
 * transaction.addToBackStack(tag); mFragmentStack.add(tag);
 * 
 * transaction.commit(); }
 * 
 * }
 */
/*
JSONObject json1 = new JSONObject();
try {
	json1.put("idBitacora", "1234567");
	json1.put("email", "prueba@addcel.com");
	json1.put("nombres", "Nombre Completo");
	json1.put("descProducto", "Pago del producto");
	json1.put("telefono", null);
	json1.put("tarjetaTag", null);
	json1.put("total", 1000.0);
	json1.put("status", 1);
	json1.put("tipoTarjeta", "VISA");
	json1.put("tipoPago", "Pago producto");
	json1.put("fecha", "2013-10-30 12:01:00");
} catch (JSONException e) {
	// TODO: handle exception
	e.printStackTrace();
}

JSONObject json2 = new JSONObject();
try {
	json2.put("idBitacora", "12345672");
	json2.put("email", "prueba@addcel.com2");
	json2.put("nombres", "Nombre Completo2");
	json2.put("descProducto", "Pago del producto2");
	json2.put("telefono", "46468468468");
	json2.put("tarjetaTag", "546468");
	json2.put("total", 1000.02);
	json2.put("status", 12);
	json2.put("tipoTarjeta", "VISA2");
	json2.put("tipoPago", "Pago producto2");
	json2.put("fecha", "2013-10-30 12:01:00");
} catch (JSONException e) {
	// TODO: handle exception
	e.printStackTrace();
}

JSONObject json3 = new JSONObject();
try {
	json3.put("idBitacora", "123456723");
	json3.put("email", "prueba@addcel.com23");
	json3.put("nombres", "Nombre Completo23");
	json3.put("descProducto", "Pago del producto23");
	json3.put("telefono", "464684684683");
	json3.put("tarjetaTag", "5464683");
	json3.put("total", 1000.023);
	json3.put("status", 12);
	json3.put("tipoTarjeta", "VISA23");
	json3.put("tipoPago", "Pago producto23");
	json3.put("fecha", "2013-10-30 12:01:003");
} catch (JSONException e) {
	// TODO: handle exception
	e.printStackTrace();
}

JSONObject json4 = new JSONObject();
try {
	json4.put("idBitacora", "123456724");
	json4.put("email", "prueba@addcel.com24");
	json4.put("nombres", "Nombre Completo24");
	json4.put("descProducto", "Pago del producto24");
	json4.put("telefono", "464684684684");
	json4.put("tarjetaTag", "5464684");
	json4.put("total", 1000.02);
	json4.put("status", 12);
	json4.put("tipoTarjeta", "VISA24");
	json4.put("tipoPago", "Pago producto24");
	json4.put("fecha", "2013-10-30 12:01:004");
} catch (JSONException e) {
	// TODO: handle exception
	e.printStackTrace();
}

JSONObject json5 = new JSONObject();
try {
	json5.put("idBitacora", "123456725");
	json5.put("email", "prueba@addcel.com25");
	json5.put("nombres", "Nombre Completo25");
	json5.put("descProducto", "Pago del producto25");
	json5.put("telefono", "464684684685");
	json5.put("tarjetaTag", "5464685");
	json5.put("total", 1000.02);
	json5.put("status", 12);
	json5.put("tipoTarjeta", "VISA25");
	json5.put("tipoPago", "Pago producto25");
	json5.put("fecha", "2013-10-30 12:01:005");
} catch (JSONException e) {
	// TODO: handle exception
	e.printStackTrace();
}

jsonarray = new JSONArray();
try {
	jsonarray.put(json1);
	jsonarray.put(json2); 

} catch (Exception e) {
	// TODO: handle exception
}
Log.i("JSONArray", jsonarray.toString());

jsonarray1 = new JSONArray();
try {
	jsonarray1.put(json2);
	jsonarray1.put(json3);

} catch (Exception e) {
	// TODO: handle exception
}
Log.i("JSONArray", jsonarray.toString());

jsonarray2 = new JSONArray();
try {
	jsonarray2.put(json3);
	jsonarray2.put(json4);

} catch (Exception e) {
	// TODO: handle exception
}
Log.i("JSONArray", jsonarray.toString());

jsonarray3 = new JSONArray();
try {
	jsonarray3.put(json4);
	jsonarray3.put(json5);

} catch (Exception e) {
	// TODO: handle exception
}
Log.i("JSONArray", jsonarray.toString());*/
