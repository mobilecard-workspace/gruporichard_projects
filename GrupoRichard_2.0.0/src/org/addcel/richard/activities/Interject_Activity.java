package org.addcel.richard.activities;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.richard.pagos.CompraWeb_Activity;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
//import org.addcel.session.SessionManager;

public class Interject_Activity extends Activity {

	private TableLayout datos_layout;
	private EditText referencia;
	//private SessionManager session;

	private TextView viewMonto;
	private TextView lablmonto;

	private Button pagar;

	private String monto;
	private String pnr;
	String token;

	private Context contxt = Interject_Activity.this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tvdepaga_activity_layout);
		//session = new SessionManager(contxt);

		ImageView header = (ImageView) findViewById(R.id.header_tvdepaga);
		header.setBackgroundResource(R.drawable.h_interjet);

		datos_layout = (TableLayout) findViewById(R.id.megadatos_layout);
		// datos_layout.setVisibility(View.VISIBLE);

		findViewById(R.id.tvrelativeLnom).setVisibility(View.GONE);
		findViewById(R.id.tvrelativeLdom).setVisibility(View.GONE);
		findViewById(R.id.tvrelativeLcomis).setVisibility(View.GONE);
		findViewById(R.id.tvrelativeLstat).setVisibility(View.GONE);
		findViewById(R.id.tvrelativeLcuent).setVisibility(View.GONE);

		viewMonto = (TextView) findViewById(R.id.megaSaldo);
		lablmonto = (TextView) findViewById(R.id.label_saldoTVpaga);
		lablmonto.setText("Monto: ");

		referencia = (EditText) findViewById(R.id.megacableReferencia);

		Button consultar = (Button) findViewById(R.id.btn_Consultareferencia);
		pagar = (Button) findViewById(R.id.btn_pagoMegacable);
		pagar.setEnabled(false);
		pagar.setBackgroundResource(R.drawable.b_pagar_o);

		consultar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (TextUtils.isEmpty(referencia.getText())) {
					Toast.makeText(contxt,
							"Porfavor escriba su c�digo de referencia.",
							Toast.LENGTH_SHORT).show();
				} else {
					pnr = referencia.getText().toString().trim();
					chechDatos();
				}
			}
		});

		pagar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				pedirToken();
				// getIdTransaction(pnr, monto);
			}
		});
	}

	private void chechDatos() {

		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.INTERJECT_PNR, pnr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(getPackageName(), "Error creando JSON");
		}

		new WebServiceClient(new getPNR3Listener(), contxt, true,
				Urls.url_interject_monto).execute(AddcelCrypto.encryptHard(json
				.toString()));

		/*****
		 * new MultiparamClient(new getPNR3Listener(), contxt, true,
		 * Urls.url_pnr3) .execute(new BasicNameValuePair("pnr",
		 * referencia.getText() .toString().trim()));
		 *****/
	}

	private class getPNR3Listener implements WSResponseListener {
		String TAG = "PNR3Listener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {
				Log.i(TAG, response);
				response = AddcelCrypto.decryptHard(response);
				JSONObject jsonResponse = null;
				try {
					jsonResponse = new JSONObject(response);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int idError = jsonResponse.optInt(Constructors.ID_ERROR, -1);
				String errorMesage = jsonResponse.optString(
						Constructors.MENSAJE_ERROR, "Error obteniedo mensaje");
				JSONObject datos = jsonResponse
						.optJSONObject(Constructors.INTERJECT_DATOS);
				String status;
				if (idError == 0 && datos != null) {
					status = datos.optString("status", "");
					monto = datos.optString("monto", "");
					String pnr2 = datos.optString("pnr", "");

					if (monto != null && !monto.equals("")) {

						if (status.equals("1")) {
							datos_layout.setVisibility(View.VISIBLE);
							viewMonto.setText("$" + monto); // Llenar campo con
															// monto obtenido
							pagar.setEnabled(true); // Habilitar bot�n de pago
							pagar.setBackgroundResource(R.drawable.selector_pagar);

						} else if (status.equals("null")) {
							Toast.makeText(contxt,
									"El n�mero de reservaci�n no existe.",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(
									contxt,
									"El periodo de pago para esta reservaci�n ha expirado.",
									Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(
								contxt,
								"El n�mero de reservaci�n introducido no es v�lido.",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(contxt, errorMesage, Toast.LENGTH_SHORT)
							.show();
				}
			}
		}
	}
	/*****************
	private void getIdTransaction(String pnr, String monto) {

		BasicNameValuePair[] idParams = new BasicNameValuePair[3];
		idParams[0] = new BasicNameValuePair("user", Integer.toString(session
				.getIdUsuario()));
		idParams[1] = new BasicNameValuePair("pnr", pnr);
		try {
			idParams[2] = new BasicNameValuePair("monto", URLDecoder.decode(
					monto, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new MultiparamClient(new idTransactionListener(), contxt, true,
				Urls.url_interjectTransactional).execute(idParams);

	}

	private class idTransactionListener implements WSResponseListener {
		String TAG = "idTransactionListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {
				Log.i(TAG, response);
				JSONObject json = null;
				try {
					json = new JSONObject(response);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String id = json.optString("id");
				if (id.equals("") || id == null) {
					Toast.makeText(
							contxt,
							"Transacci�n no v�lida. Intente de nuevo m�s tarde",
							Toast.LENGTH_LONG).show();
				} else {
					// setIdTransaccion(id);
					String url = getPurchaseUrl(id); // Descripci�n del m�todo
														// viene abajo

					if (url.equals("")) {
						Toast.makeText(contxt,
								"Url no v�lida. Intente de nuevo m�s tarde",
								Toast.LENGTH_LONG).show();

					} else {

						Intent intent = new Intent(contxt,
								CompraWeb_Activity.class);
						intent.putExtra("url", url);
						intent.putExtra("tipo_pago", "interjet");

						startActivity(intent);
					}
				}
			}

		}

		private String getPurchaseUrl(String idT) {

			BasicNameValuePair user = new BasicNameValuePair("user",
					Integer.toString(session.getIdUsuario())); // Nombre usuario
																// mobilecard,
																// para
																// OnComercio
																// mandar el id
																// de usuario.
			BasicNameValuePair referencia = new BasicNameValuePair(
					"referencia", idT);
			BasicNameValuePair bnvpmonto = new BasicNameValuePair("monto",
					monto);

			List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
			params.add(user);
			params.add(referencia); // -> Id obtenida utilizando el servicio
									// getInterjetTransaction
			params.add(bnvpmonto);

			String url = Text.addParamsToUrl(
					"http://www.mobilecard.mx:8080/ProCom/comercio_fin.jsp",
					params);

			Log.i("url_pago", url);

			return url;
		}
	}
******************/
	private void pedirToken() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.USUARIO_TOKEN, Servicios.userToken);
			json.put(Constructors.PASSWORD, Servicios.passToken);
		} catch (JSONException e) {
			// TODO: handle exception
			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new TokenListener(), contxt, true,
				Urls.url_getToken).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private class TokenListener implements WSResponseListener {

		private static final String TAG = "TokenListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.i("TOKEN", response);

				// if (!AddcelCrypto.decryptHard(response).startsWith("{")) {
				// token = response;
				// enviarPago();
				// } else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				int erridtok = 0;
				String errmsgtk = null;

				try {

					JSONObject json = new JSONObject(response);

					erridtok = json.getInt(Constructors.ID_ERROR);
					errmsgtk = json.getString(Constructors.MENSAJE_ERROR);
					token = json.getString(Constructors.TOKEN);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
				Log.e(TAG, errmsgtk);
				if (erridtok != 0) {
					Toast.makeText(contxt, "Error: " + errmsgtk,
							Toast.LENGTH_SHORT).show();
				} else {
					Intent intent = new Intent(contxt, CompraWeb_Activity.class);
					intent.putExtra("token", token);
					intent.putExtra("monto", monto);
					intent.putExtra("pnr", pnr);
					startActivity(intent);
				}

			}
			// }
		}

	}

}
