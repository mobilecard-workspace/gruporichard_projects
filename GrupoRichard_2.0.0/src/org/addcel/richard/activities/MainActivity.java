package org.addcel.richard.activities;

import org.addcel.richard.R;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			// fetch data

			startActivity(new Intent(MainActivity.this, SplashScreenActivity.class));

		} else {
			// display error
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Sin conexión, ¿Deseas conectarte a una red?")
					.setTitle("Advertencia")
					.setCancelable(false)
					.setNegativeButton("Cancelar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();

									try {
										Thread.sleep(3000);
									} catch (InterruptedException e) {
										// TODO: handle exception
										e.printStackTrace();
									}

									startActivity(new Intent(MainActivity.this,
											MainActivity.class));

								}
							})
					.setPositiveButton("Continuar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// Activity para abrir conexión a internet
									startActivity(new Intent(
											WifiManager.ACTION_PICK_WIFI_NETWORK));

									// Toast redToast =
									// Toast.makeText(MainActivity.this,
									// "Conectado", Toast.LENGTH_LONG);
									// redToast.show();
								}
							});
			AlertDialog alert = builder.create();
			alert.show();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
