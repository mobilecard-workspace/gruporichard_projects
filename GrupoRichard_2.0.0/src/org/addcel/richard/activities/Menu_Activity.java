package org.addcel.richard.activities;

import org.addcel.richard.R;
import org.addcel.richard.pagos.CobroenTienda_Activity;
import org.addcel.richard.pagos.PagodeServicios_Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class Menu_Activity extends Activity {

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.menu_layout);

		

		final Button pagoServicios = (Button) findViewById(R.id.menuBtnpago);
		final Button cobroProductos = (Button) findViewById(R.id.menuBtncobro);
		final Button historialVenta = (Button) findViewById(R.id.menuBtnhistorial);
		//final Button solicitarCredito = (Button) findViewById(R.id.menuBtnsolicitacredit);
		final Button actualizarDatos = (Button) findViewById(R.id.menuBtnActualizarDatos);

		// final TextView monedero = (TextView)findViewById(R.id.monederoSaldo);
		// monedero.setText("Total: " + session.getCarnetTotal() +
		// "\nDisponible: " + session.getCarnetDisponible());

	/*	solicitarCredito.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				startActivity(new Intent(Menu_Activity.this,
						SolicitarCredito_Activity.class));

			}
		});*/

		pagoServicios.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Menu_Activity.this,
						PagodeServicios_Activity.class));

			}
		});

		historialVenta.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Menu_Activity.this,
						Historialventas_Activity.class));

			}
		});

		cobroProductos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Menu_Activity.this,
						CobroenTienda_Activity.class));

			}
		});

		actualizarDatos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Menu_Activity.this,
						Actualizar_Datos_Activity.class));
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent i = new Intent(Menu_Activity.this, Inicio_Activity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
		finish();
		return;
		// super.onBackPressed();
	}
}
