package org.addcel.richard.constant;

public class Servicios {

	public static final int idProvedor = 26;

	public static final String userToken = "userGRupoRichard";
	public static final String passToken = "uL8RmL2TNWOT6Lxi";

	public static final String MONTOS_IUSACELL[] = { "$50.00", "$100.00",
			"$150.00", "$200.00", "$300.00", "$500.00", "$1 000.00" };
	public static final String MONTOS_MOVISTAR[] = { "$20.00", "$30.00",
			"$60.00", "$100.00", "$120.00", "$200.00", "$300.00", "$500.00" };
	public static final String MONTOS_TELCEL[] = { "$20.00", "$30.00",
			"$50.00", "$100.00", "$150.00", "$200.00" };
	public static final String MONTOS_UNEFON[] = { "$50.00", "$100.00",
			"$150.00", "$200.00", "$300.00", "$500.00", "$1 000.00" };
	public static final String MONTOS_IAVE[] = { "$200.00", "$300.00",
			"$400.00", "$500.00" };
	public static final String MONTOS_PASE[] = { "$200.00", "$300.00",
			"$400.00", "$500.00" };

	public static final int ID_PRODUCTO_IUSACELL[] = { 5060050, 5060100,
			5060150, 5060200, 5060300, 5060500, 5061000 };
	public static final int ID_PRODUCTO_MOVISTAR[] = { 5050020, 5050030,
			5050060, 5050100, 5050120, 5050200, 5050300, 5050500 };
	public static final int ID_PRODUCTO_TELCEL[] = { 358, 332, 318, 285, 362,
			286 };
	public static final int ID_PRODUCTO_UNEFON[] = { 5065050, 5065100, 5065150,
			5065200, 5065300, 5065500, 5066000 };
	public static final int ID_PRODUCTO_IAVE[] = { 900, 901, 902, 903 };
	public static final int ID_PRODUCTO_PASE[] = { 916, 937, 938, 936 };
}
