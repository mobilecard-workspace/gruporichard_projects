package org.addcel.richard.fragments;

//import java.util.Calendar;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.activities.Inicio_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

//import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
//import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.RadioGroup;
//import android.widget.RadioGroup.OnCheckedChangeListener;
//import android.widget.Toast;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroFragment extends Fragment {
	private EditText addLogin;
	private EditText addLastnameP;
	private EditText addLastnameM;
	private EditText addnombre;
	private EditText addtelefono;
	private EditText addcorreo;
	private EditText addTienda;
	private EditText addImei;

	private int idError;
	private String mensajeError;
	private int idUsuario;

	private SessionManager session;

	private Button btnRegistrar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		// View v = inflater.inflate(R.layout.registro_layout, container,
		// false);
		View v = inflater.inflate(R.layout.registro_fragment_layout, container,
				false);
		// return super.onCreateView(inflater, container, savedInstanceState);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		session = new SessionManager(getActivity());

		addLogin = (EditText) getView().findViewById(R.id.addUser_login_frg);
		addLastnameP = (EditText) getView().findViewById(
				R.id.addUser_apellidoP_frg);
		addLastnameM = (EditText) getView().findViewById(
				R.id.addUser_apellidoM_frg);
		addnombre = (EditText) getView().findViewById(R.id.addUser_nombres_frg);
		addtelefono = (EditText) getView().findViewById(
				R.id.addUser_telefono_frg);
		addcorreo = (EditText) getView().findViewById(R.id.addUser_correo_frg);
		addTienda = (EditText) getView().findViewById(R.id.addUser_ntienda_frg);
		addImei = (EditText) getView().findViewById(R.id.addUser_imei_frg);

		addTienda.setText(session.getNombreTienda().toString());
		addTienda.setEnabled(false);

		addImei.setText(Dispositivo.imei.toString());
		addImei.setEnabled(false);

		btnRegistrar = (Button) getView().findViewById(R.id.btn_addUser_frg);

		btnRegistrar.setOnClickListener(new OnClickListener() {

			Context contxt = getActivity();

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (TextUtils.isEmpty(addLogin.getText().toString().trim())) {
					Toast.makeText(contxt, "Ingresa el nombre de Usuario",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(addLastnameP.getText().toString()
						.trim())) {
					Toast.makeText(contxt, "Ingresa apellido paterno",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(addLastnameM.getText().toString()
						.trim())) {
					Toast.makeText(contxt, "Ingresa apellido materno",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(addnombre.getText().toString()
						.trim())) {
					Toast.makeText(contxt, "Ingresa el nombre",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(addtelefono.getText().toString()
						.trim())) {
					Toast.makeText(contxt, "Ingresa un n�merod de telefono",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(addcorreo.getText().toString()
						.trim())) {
					Toast.makeText(contxt,
							"Ingresa una direcci�n de correo electronico",
							Toast.LENGTH_SHORT).show();
				} else {
					addUser();
				}

			}
		});

	}

	private void addUser() {

		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.LOGIN, addLogin.getText().toString().trim());
			json.put(Constructors.NOMBRE, addnombre.getText().toString().trim());
			json.put(Constructors.APELLIDO, addLastnameP.getText().toString().trim());
			json.put(Constructors.MATERNO, addLastnameM.getText().toString().trim());
			json.put(Constructors.MAIL, addcorreo.getText().toString().trim());
			json.put(Constructors.TELEFONO, addtelefono.getText().toString().trim());
			json.put(Constructors.IMEI, Dispositivo.imei);
			//json.put(Constructors.MODELO, Dispositivo.modelo);
			json.put(Constructors.ID_TIENDA, session.getIdTienda());
			//json.put(Constructors.SOFTWARE, Dispositivo.software_version);

			// Log.i(getParentFragment().toString(), json.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// Log.e(getParentFragment().toString(),
			// "Error creando objeto JSON");
		}
		new WebServiceClient(new adduserListener(), getActivity(), true,
				Urls.url_addUser).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private class adduserListener implements WSResponseListener {
		private static final String TAG = "adduserListener";
		Context contxt = getActivity();

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {

				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);
				try {
					JSONObject json = new JSONObject(response);

					idError = json.getInt(Constructors.ID_ERROR);
					mensajeError = json.getString(Constructors.MENSAJE_ERROR);
					idUsuario = json.getInt(Constructors.ID_USUARIO);
				} catch (JSONException e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				if (idError == 0) {
					Toast.makeText(contxt, mensajeError, Toast.LENGTH_SHORT)
							.show();
					startActivity(new Intent(contxt, Inicio_Activity.class));

				} else {
					Toast.makeText(contxt,
							mensajeError + " id de Usuario: " + idUsuario,
							Toast.LENGTH_SHORT).show();
				}

			}

		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

}
