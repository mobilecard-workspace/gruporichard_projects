package org.addcel.richard.fragments;

import java.util.Arrays;

import org.addcel.richard.R;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.richard.pagos.ConfirmacionTelepeaje_Activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class Telepeaje_Fragment extends Fragment implements OnClickListener {
	private String montos[] = null;
	private int pos = 0;
	private int[] id_producto;
	private String cr = null;
	private int id;
	private String seturl;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		final Button btnIave = (Button) getView().findViewById(R.id.btn_Iave);
		final Button btnPase = (Button) getView().findViewById(R.id.btn_Pase);

		btnIave.setOnClickListener(this);
		btnPase.setOnClickListener(this);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.telepeaje_fragment_layout,
				container, false);
		return v;
	}

	@Override
	public void onClick(final View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.btn_Iave:
			montos = Servicios.MONTOS_IAVE;
			id_producto = Servicios.ID_PRODUCTO_IAVE;			
			id = id_producto[0];
			cr = "RECARGA IAVE";
			seturl = Urls.url_pagoIAVE;
			break;

		case R.id.btn_Pase:
			montos = Servicios.MONTOS_PASE;
			id_producto = Servicios.ID_PRODUCTO_PASE;
			id = id_producto[0];
			cr = "RECARGA PASE";
			seturl = Urls.url_pagoPASE;
			break;

		default:
			break;
		}

		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), 3);

		builder.setTitle("Monto de Recarga");

		builder.setSingleChoiceItems(montos, 0,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						pos = which;
						id = id_producto[pos];
					}
				});

		builder.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Toast toast = Toast.makeText(getActivity(),
								"CANCELADO", Toast.LENGTH_SHORT);
						toast.show();
					}
				});

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Toast toast = Toast.makeText(getActivity(), "Recarga de"
						+ montos[pos], Toast.LENGTH_SHORT);
				toast.show();

				Intent intent = new Intent(getActivity(),
						ConfirmacionTelepeaje_Activity.class);
				intent.putExtra("tipotele", cr);
				intent.putExtra("url", seturl);
				intent.putExtra("monto", montos[pos]);
				intent.putExtra("id_producto", id);
				startActivity(intent);
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

}
