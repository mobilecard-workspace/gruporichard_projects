package org.addcel.richard.fragments;

import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.pagos.ConfirmacionRecarga_Activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class TiempoAire_Fragment extends Fragment implements OnClickListener {

	private String montos[] = null;
	private int pos = 0;
	private int[] id_producto;
	private String cr = null;
	private int id=0;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		final Button btnTelecel = (Button) getView().findViewById(
				R.id.btn_Telcel);
		final Button btnMovistar = (Button) getView().findViewById(
				R.id.btn_Movistar);
		final Button btnIusacell = (Button) getView().findViewById(
				R.id.btn_Iusacell);
		final Button btnUnefon = (Button) getView().findViewById(
				R.id.btn_Unefon);

		btnIusacell.setOnClickListener(this);
		btnMovistar.setOnClickListener(this);
		btnTelecel.setOnClickListener(this);
		btnUnefon.setOnClickListener(this);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.tiempoaire_fragment_layout,
				container, false);
		return v;

	}

	@Override
	public void onClick(final View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_Telcel:
			montos = Servicios.MONTOS_TELCEL;
			id_producto = Servicios.ID_PRODUCTO_TELCEL;
			id = id_producto[0];
			cr = "RECARGA TELCEL";
			break;

		case R.id.btn_Iusacell:
			montos = Servicios.MONTOS_IUSACELL;
			id_producto = Servicios.ID_PRODUCTO_IUSACELL;
			id = id_producto[0];
			cr = "RECARGA IUSACELL";
			break;
		case R.id.btn_Movistar:
			montos = Servicios.MONTOS_MOVISTAR;
			id_producto = Servicios.ID_PRODUCTO_MOVISTAR;
			id = id_producto[0];
			cr = "RECARGA MOVISTAR";
			break;
		case R.id.btn_Unefon:
			montos = Servicios.MONTOS_UNEFON;
			id_producto = Servicios.ID_PRODUCTO_UNEFON;
			id = id_producto[0];
			cr = "RECARGA UNEFON";
			break;
		default:
			break;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), 3);
		builder.setTitle("Monto de Recarga");

		builder.setSingleChoiceItems(montos, 0,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						pos = which;
						id = id_producto[pos];
					}
				});

		builder.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Toast toast = Toast.makeText(getActivity(),
								"CANCELADO", Toast.LENGTH_SHORT);
						toast.show();

					}
				});

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Toast toast = Toast.makeText(getActivity(), "Recarga de"
						+ montos[pos], Toast.LENGTH_SHORT);
				toast.show();

				Intent intent = new Intent(getActivity(),
						ConfirmacionRecarga_Activity.class);
				intent.putExtra(Constructors.CARRIER, cr);
				intent.putExtra(Constructors.MONTO, montos[pos]);
				intent.putExtra(Constructors.ID_PRODUCTO, id);
				startActivity(intent);
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

}
