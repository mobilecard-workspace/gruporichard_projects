package org.addcel.richard.fragments;

import org.addcel.android.etn.view.ETN_Main_Activity;
import org.addcel.richard.R;
import org.addcel.richard.activities.Interject_Activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class Viajes_Fragment extends Fragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		getView().findViewById(R.id.btn_Interject).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						startActivity(new Intent(getActivity(),
								Interject_Activity.class));
					}
				});
		getView().findViewById(R.id.btn_ETN).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						startActivity(new Intent(getActivity(),
								ETN_Main_Activity.class));
					}
				});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.viajes_fragment_layout, container,
				false);
		return v;
	}
}
