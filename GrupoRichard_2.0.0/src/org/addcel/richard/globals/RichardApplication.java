package org.addcel.richard.globals;

import java.util.Date;

import org.addcel.android.etn.to.Corrida;

import android.app.Application;

public class RichardApplication extends Application{

	private int adultos;
	private int ninos;
	private int insen;
	private int maestros;
	private int estudiantes;
	
	private Date dateSalida;
	private Date dateRegreso;
	
	private String origen;
	private String destino;
	private String origenText;
	private String destinoText;
	
	private Corrida corridaSalida;
	private Corrida corridaRegreso;
	
	private Double totalPagarIda;
	private Double totalPagarRegreso;
	
	/**
	 * @return the adultos
	 */
	public int getAdultos() {
		if (adultos == 0) {
			return 0;
		}else {
			return adultos;
		}
		
	}
	/**
	 * @param adultos the adultos to set
	 */
	public void setAdultos(int adultos) {
		this.adultos = adultos;
	}
	/**
	 * @return the ninos
	 */
	public int getNinos() {
		if (ninos == 0) {
			return 0;
		}else {
			return ninos;
		}
	}
	/**
	 * @param ninos the ninos to set
	 */
	public void setNinos(int ninos) {
		this.ninos = ninos;
	}
	/**
	 * @return the insen
	 */
	public int getInsen() {
		return insen;
	}
	/**
	 * @param insen the insen to set
	 */
	public void setInsen(int insen) {
		this.insen = insen;
	}
	/**
	 * @return the maestros
	 */
	public int getMaestros() {
		return maestros;
	}
	/**
	 * @param maestros the maestros to set
	 */
	public void setMaestros(int maestros) {
		this.maestros = maestros;
	}
	/**
	 * @return the estudiantes
	 */
	public int getEstudiantes() {
		return estudiantes;
	}
	/**
	 * @param estudiantes the estudiantes to set
	 */
	public void setEstudiantes(int estudiantes) {
		this.estudiantes = estudiantes;
	}
	/**
	 * @return the dateSalida
	 */
	public Date getDateSalida() {
		return dateSalida;
	}
	/**
	 * @param dateSalida the dateSalida to set
	 */
	public void setDateSalida(Date dateSalida) {
		this.dateSalida = dateSalida;
	}
	/**
	 * @return the dateRegreso
	 */
	public Date getDateRegreso() {
		return dateRegreso;
	}
	/**
	 * @param dateRegreso the dateRegreso to set
	 */
	public void setDateRegreso(Date dateRegreso) {
		this.dateRegreso = dateRegreso;
	}
	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}
	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}
	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}
	/**
	 * @return the origenText
	 */
	public String getOrigenText() {
		return origenText;
	}
	/**
	 * @param origenText the origenText to set
	 */
	public void setOrigenText(String origenText) {
		this.origenText = origenText;
	}
	/**
	 * @return the destinoText
	 */
	public String getDestinoText() {
		return destinoText;
	}
	/**
	 * @param destinoText the destinoText to set
	 */
	public void setDestinoText(String destinoText) {
		this.destinoText = destinoText;
	}
	/**
	 * @return the corridaSalida
	 */
	public Corrida getCorridaSalida() {
		return corridaSalida;
	}
	/**
	 * @param corridaSalida the corridaSalida to set
	 */
	public void setCorridaSalida(Corrida corridaSalida) {
		this.corridaSalida = corridaSalida;
	}
	/**
	 * @return the corridaRegreso
	 */
	public Corrida getCorridaRegreso() {
		return corridaRegreso;
	}
	/**
	 * @param corridaRegreso the corridaRegreso to set
	 */
	public void setCorridaRegreso(Corrida corridaRegreso) {
		this.corridaRegreso = corridaRegreso;
	}
	/**
	 * @return the totalPagar
	 */
	public Double getTotalPagarIda() {		
		if (corridaSalida != null) {
			totalPagarIda = corridaSalida.getTarifaAdulto()*adultos
					+ corridaSalida.getTarifaEstudiante()*estudiantes
					+ corridaSalida.getTarifaInsen()*insen
					+ corridaSalida.getTarifaMaestro()*maestros
					+ corridaSalida.getTarifaNino()*ninos;
		}else {
			totalPagarIda = 0.0;
		}	
		return totalPagarIda;
	}
	
	public Double getTotalPagarRegreso() {	
		if (corridaRegreso != null) {
			totalPagarRegreso = corridaRegreso.getTarifaAdulto()*adultos
					+ corridaRegreso.getTarifaEstudiante()*estudiantes
					+ corridaRegreso.getTarifaInsen()*insen
					+ corridaRegreso.getTarifaMaestro()*maestros
					+ corridaRegreso.getTarifaNino()*ninos;
		}else {
			totalPagarRegreso = 0.0;
		}	
		return totalPagarRegreso;
	}
	
}
