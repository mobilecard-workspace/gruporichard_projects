package org.addcel.richard.gps;

import org.addcel.richard.activities.SplashScreenActivity;
import org.addcel.session.SessionManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class ReceiverGPS extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		SessionManager session = new SessionManager(context);

		Intent intentLocation = new Intent(context, GPSservice.class);
		PendingIntent alarmIntent = PendingIntent.getService(context, 0,
				intentLocation, 0);

		AlarmManager alarmMgr = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		long firstTime = SystemClock.elapsedRealtime();
		firstTime += 3 * 1000; // Comienza 3 segundos despu�s del primer

		// registro

		int inter;
		int interAnt;
		String TAG = this.getClass().getName().toString();

		// session.setTime_GPS(2);

		inter = session.getTime_GPS();
		interAnt = session.getTime_GPS_ant();
		
		
		if (inter == 0) {
			Log.e(TAG, "No hay tiempo de intervalo establecido");
			inter = 60;

			session.setTime_GPS(inter);
			session.setTime_GPS_ant(inter);
		}else{
		session.setTime_GPS_ant(inter);
		}
		alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME, firstTime,
				inter * 1000 * 60, alarmIntent);// cada 30 minutos
	
		Intent startApp = new Intent(context,SplashScreenActivity.class);
		startApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(startApp);
		
	
	}

}
