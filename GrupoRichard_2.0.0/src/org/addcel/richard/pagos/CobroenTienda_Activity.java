package org.addcel.richard.pagos;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.activities.Menu_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CobroenTienda_Activity extends Activity {

	private int tipopago = 0;

	private SessionManager session;

	private int erridtok;
	private String errmsgtk;
	private String token;

	private String vigencia;

	private Context contxt = CobroenTienda_Activity.this;

	private EditText producto;
	private EditText nombre;
	private EditText correo;

	private EditText tarjetaNum;
	private EditText cvv2;
	private EditText vigenciamm;
	private EditText vigenciaaa;
	private EditText total;

	private List<String> list;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(this, Menu_Activity.class));
		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.cobroentienda_layout);
		ImageView header = (ImageView) findViewById(R.id.header_cobrentieda);
		header.setImageResource(R.drawable.h_cobrodeproductosdetutienda);
		
		session = new SessionManager(contxt);
	
		list = new ArrayList<String>();

		JSONArray myjsonArray = session.getTipotarjeta();
		try {
			for (int i = 0; i < myjsonArray.length()-1; i++) {
				JSONObject json = myjsonArray.getJSONObject(i);
				list.add(json.getString("descripcion"));
			}

		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		final Button buttonPago = (Button) findViewById(R.id.btn_pagoEntienda);
		final Spinner spinnerTipo = (Spinner) findViewById(R.id.spinnerCobroentienda);
		final TextView labelNombre = (TextView)findViewById(R.id.labelName);
		final TextView labeltarjNum = (TextView)findViewById(R.id.labelNumeroTarjeta);
		final TextView labelcvv = (TextView)findViewById(R.id.labelCVV2);
		final TextView labelvig = (TextView)findViewById(R.id.labelVigencia);

		producto = (EditText) findViewById(R.id.productoCobroentienda);
		nombre = (EditText) findViewById(R.id.nombreCobroentienda);
		correo = (EditText) findViewById(R.id.correoCobroentienda);

		tarjetaNum = (EditText) findViewById(R.id.tarjetaCobroentienda);
		cvv2 = (EditText) findViewById(R.id.cvvCobroentienda);
		vigenciamm = (EditText) findViewById(R.id.vigenciaCobroentiendamm);
		vigenciaaa = (EditText) findViewById(R.id.vigenciaCobroentiendaaa);
		total = (EditText) findViewById(R.id.totalCobroentienda);
		
		nombre.setVisibility(View.GONE);
		labelNombre.setVisibility(View.GONE);
		
		tarjetaNum.setVisibility(View.GONE);
		labeltarjNum.setVisibility(View.GONE);
		
		cvv2.setVisibility(View.GONE);
		labelcvv.setVisibility(View.GONE);
		
		vigenciaaa.setVisibility(View.GONE);
		vigenciamm.setVisibility(View.GONE);
		labelvig.setVisibility(View.GONE);
		
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(contxt,
				android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinnerTipo.setAdapter(adapter);

		spinnerTipo.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				Toast.makeText(
						contxt,
						"Tipo de pago: "
								+ parent.getItemAtPosition(pos).toString(),
						Toast.LENGTH_SHORT).show();
				tipopago = pos;	
				
				
				if (tipopago!= 0) {
					nombre.setVisibility(View.VISIBLE);
					labelNombre.setVisibility(View.VISIBLE);
					
					tarjetaNum.setVisibility(View.VISIBLE);
					labeltarjNum.setVisibility(View.VISIBLE);
					
					cvv2.setVisibility(View.VISIBLE);
					labelcvv.setVisibility(View.VISIBLE);
					
					vigenciaaa.setVisibility(View.VISIBLE);
					vigenciamm.setVisibility(View.VISIBLE);
					labelvig.setVisibility(View.VISIBLE);
					
					
				} else {
					nombre.setVisibility(View.GONE);
					labelNombre.setVisibility(View.GONE);
					
					tarjetaNum.setVisibility(View.GONE);
					labeltarjNum.setVisibility(View.GONE);
					
					cvv2.setVisibility(View.GONE);
					labelcvv.setVisibility(View.GONE);
					
					vigenciaaa.setVisibility(View.GONE);
					vigenciamm.setVisibility(View.GONE);
					labelvig.setVisibility(View.GONE);
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				tipopago = 0;
			}
			
			
		});

		buttonPago.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (tipopago == 0) {
					
					if (TextUtils.isEmpty(producto.getText())) {
						Toast.makeText(contxt, "Campo Producto vacío",
								Toast.LENGTH_SHORT).show();
					} /*else if (TextUtils.isEmpty(nombre.getText())) {
						Toast.makeText(contxt, "Campo Nombre vacío",
								Toast.LENGTH_SHORT).show();
					}*/ else if (TextUtils.isEmpty(correo.getText())) {
						Toast.makeText(contxt, "Campo Correo vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(total.getText())) {
						Toast.makeText(contxt, "Campo Total vacío",
								Toast.LENGTH_SHORT).show();
					}

					else {
						pedirToken();
						// enviarPago();

					}

				} else {

					if (TextUtils.isEmpty(producto.getText())) {
						Toast.makeText(contxt, "Campo Producto vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(nombre.getText())) {
						Toast.makeText(contxt, "Campo Nombre vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(correo.getText())) {
						Toast.makeText(contxt, "Campo Correo vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(tarjetaNum.getText())) {
						Toast.makeText(contxt,
								"Campo Número de Tarjeta vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(cvv2.getText())) {
						Toast.makeText(contxt, "Campo CVV2 vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(vigenciamm.getText())) {
						Toast.makeText(contxt, "Campo Vigencia mes vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(vigenciaaa.getText())) {
						Toast.makeText(contxt, "Campo Vigencia año vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(total.getText())) {
						Toast.makeText(contxt, "Campo Total vacío",
								Toast.LENGTH_SHORT).show();
					}

					else {
						vigencia = vigenciamm.getText().toString().trim() + "/"
								+ vigenciaaa.getText().toString().trim();
						pedirToken();
					}

				}
			}
		});

	}

	private void pedirToken() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.USUARIO_TOKEN, Servicios.userToken);
			json.put(Constructors.PASSWORD, Servicios.passToken);
		} catch (JSONException e) {
			// TODO: handle exception
			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new TokenListener(), contxt, true,
				Urls.url_getToken).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private void enviarPago() {
		// TODO Auto-generated method stub

		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.MAIL, correo.getText().toString().trim());
			json.put(Constructors.NOMBRES, nombre.getText().toString().trim());
			json.put(Constructors.DESCRIPCION_PRODUCT, producto.getText().toString().trim());
			json.put(Constructors.TOTAL, Float.parseFloat(total.getText().toString()));
			json.put(Constructors.TARGETA, tarjetaNum.getText().toString().trim());
			json.put(Constructors.VIGENCIA, vigencia);
			json.put(Constructors.CVV2, cvv2.getText().toString().trim());
			json.put(Constructors.PASSWORD, session.getUserPassword());
			json.put(Constructors.ID_TIPO_TARJETA, tipopago);
			json.put(Constructors.TOKEN, token);
			json.put(Constructors.IMEI, Dispositivo.imei);
			json.put(Constructors.MODELO, Dispositivo.modelo);
			json.put(Constructors.SOFTWARE, Dispositivo.software_version);

			Log.i(getPackageName(), json.toString());

		} catch (JSONException e) {
			// TODO: handle exception

			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new PagoListener(), contxt, true,
				Urls.url_pagoproductos).execute(AddcelCrypto.encryptSensitive(
				AppUtils.getKey(), json.toString()));

	}

	private class TokenListener implements WSResponseListener {

		private static final String TAG = "TokenListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.i("TOKEN", response);

				// if (!AddcelCrypto.decryptHard(response).startsWith("{")) {
				// token = response;
				// enviarPago();
				// } else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				try {

					JSONObject json = new JSONObject(response);

					erridtok = json.getInt(Constructors.ID_ERROR);
					errmsgtk = json.getString(Constructors.MENSAJE_ERROR);
					token = json.getString(Constructors.TOKEN);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
				Log.e(TAG, errmsgtk);
				if (erridtok != 0) {
					Toast.makeText(contxt, "Error: " + errmsgtk,
							Toast.LENGTH_SHORT).show();
				} else {
					enviarPago();
				}

			}
			// }
		}

	}

	private class PagoListener implements WSResponseListener {

		private static final String TAG = "PagoListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub

			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {

				response = AddcelCrypto.decryptSensitive(response);
				Log.i(TAG, response);
				try {

					JSONObject json = new JSONObject(response);
					String numAutorizacion = json.getString(Constructors.NUMERO_AUTORIZACION);
					//String idBitacora = json.getString("idBitacora");
					//String descRechazo = json.getString("descRechazo");
					String fecha = json.getString(Constructors.FECHA);
					String folio = json.getString(Constructors.FOLIO);
					//int status = json.getInt("status");
					String cargo = json.getString(Constructors.CARGO);
					String comision = json.getString(Constructors.COMISION);
					String mensajeError = json.getString(Constructors.MENSAJE_ERROR);

					switch (json.getInt(Constructors.ID_ERROR)) {
					case 0:

						AlertDialog.Builder builder = new AlertDialog.Builder(
								contxt, 3);

						builder.setTitle("Pago exitoso");

						if (tipopago == 0) {

							builder.setMessage("Pago de :  "
									+ producto.getText().toString().trim()
									+ "\nTotal: " + cargo);
						} else {
							builder.setMessage("Pago de :  "
									+ producto.getText().toString().trim()
									+ "\nTotal: " + cargo + "\n Comisión: "
									+ comision

									+ "\nAutorización bancaria: "
									+ numAutorizacion
									+ "\n Folio de autorización: " + folio
									+ "\n Fecha: " + fecha);
						}

						builder.setPositiveButton("ACEPTAR",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										startActivity(new Intent(contxt,
												Menu_Activity.class));

									}
								});
						AlertDialog aldialog = builder.create();
						aldialog.show();

						break;

					default:
						Log.e(TAG, mensajeError);
						Toast.makeText(
								contxt,
								mensajeError
										+ "\n Porfavor verifique sus datos",
								Toast.LENGTH_SHORT).show();
						break;
					}

				} catch (JSONException e) {
					e.printStackTrace();

				}
			}
		}
	}
}
