package org.addcel.richard.pagos;

import org.addcel.crypto.AddcelCrypto;
import org.addcel.richard.R;
import org.addcel.richard.activities.Menu_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class CompraWeb_Activity extends Activity {

	WebView myWebView;
	private SessionManager session;
	private Context contxt = CompraWeb_Activity.this;
	private String monto;
	private String token;
	private String pnr;
	private Button btn_fin;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_compraweb_layout);

		token = getIntent().getExtras().getString("token");
		pnr = getIntent().getExtras().getString("pnr");
		monto = getIntent().getExtras().getString("monto");

		btn_fin = (Button) findViewById(R.id.btn_fin_interject);
		btn_fin.setVisibility(View.GONE);
		btn_fin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CompraWeb_Activity.this,
						Menu_Activity.class));
			}
		});

		session = new SessionManager(contxt);

		myWebView = (WebView) findViewById(R.id.wview_compraweb);
		myWebView.setWebViewClient(new Callback());
		myWebView.clearSslPreferences();
		myWebView.getSettings().setJavaScriptEnabled(true);
		pago();

	}
	
	private void pago() {

		JSONObject params = new JSONObject();

		try {
			params.put(Constructors.ID_USUARIO, session.getIdUsuario());
			params.put(Constructors.ID_PROVEEDOR, session.getIdProveedor());
			params.put(Constructors.MAIL, "jordan.rosas@addcel.com");
			params.put(Constructors.TOTAL, monto);
			params.put(Constructors.INTERJECT_PNR, pnr);
			params.put(Constructors.PASSWORD, session.getUserPassword());
			params.put(Constructors.TOKEN, token);
			params.put(Constructors.IMEI, Dispositivo.imei);
			params.put(Constructors.MODELO, Dispositivo.modelo);
			params.put(Constructors.SOFTWARE, Dispositivo.software_version);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * new WebServiceClient(new PagoListener(), contxt, true,
		 * Urls.url_interject_pago).execute(AddcelCrypto.encryptSensitive(
		 * AppUtils.getKey(), params.toString()));
		 */
		myWebView.loadUrl(Urls.url_interject_pago
				+ "?json="
				+ AddcelCrypto.encryptSensitive(AppUtils.getKey(),
						params.toString()));

		// myWebView.loadDataWithBaseURL(Urls.url_interject_pago,AddcelCrypto.encryptSensitive(
		// AppUtils.getKey(), params.toString()) ,
		// "text/html", HTTP.UTF_8, null);

	}

	

	private class Callback extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);

			// if (!getDialog().isShowing() && getDialog() != null) {
			// getDialog().show();
			// }
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			//
			// if (getDialog().isShowing() && getDialog() != null) {
			// getDialog().dismiss();
			// }
			String proces_end = Urls.url_interjectRegresoPago;
			if (url.equals(proces_end)) {
				btn_fin.setVisibility(View.VISIBLE);
			}
			/***********
			 * if (url.equals(getUrlRegreso())) {
			 * getFinalizar().setVisibility(View.VISIBLE); count.cancel(); }
			 ************/
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return (false);
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			// super.onReceivedSslError(view, handler, error);
			System.out.println(error.toString());
			handler.proceed();
		}
	}
}