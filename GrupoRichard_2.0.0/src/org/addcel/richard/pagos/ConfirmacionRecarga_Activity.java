package org.addcel.richard.pagos;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.activities.Menu_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ConfirmacionRecarga_Activity extends Activity {

	private String cr;

	private SessionManager session;
	private int tipopago = 1;
	private String comistr = " + $2.00";
	private int producto;

	private String tarjetaTag = null;

	private int errornum;
	private String errormsg;
	private String autbanco;
	private String foliobnc;

	private String idBitacora;
	private String cargo;
	private String comision;

	private int erridtok;
	private String errmsgtk;
	private String token;

	private String vigencia;

	private Context contxt = ConfirmacionRecarga_Activity.this;

	private EditText num;
	private EditText numConf;
	private EditText nombre;
	private EditText correo;
	private EditText montoEdit;

	private EditText tarjetaNum;
	private EditText cvv2;
	private EditText vigenciamm;
	private EditText vigenciaaa;
	private EditText total;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.confirmacionrecarga_layout);

		List<String> list = new ArrayList<String>();

		session = new SessionManager(contxt);
		JSONArray myjsonArray = session.getTipotarjeta();
		try {
			for (int i = 0; i < myjsonArray.length(); i++) {
				JSONObject json = myjsonArray.getJSONObject(i);
				if (i == 0) {
					list.add("Tipo de Pago");
				}else {
				list.add(json.getString("descripcion"));
				}
			}

		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		final TextView ctextview = (TextView) findViewById(R.id.label_carrierRecarga);

		num = (EditText) findViewById(R.id.numeroTAE);
		numConf = (EditText) findViewById(R.id.cnfnumeroTAE);
		nombre = (EditText) findViewById(R.id.nombreTAE);
		correo = (EditText) findViewById(R.id.correoCobroTAE);
		montoEdit = (EditText) findViewById(R.id.montoCobroTAE);
		cvv2 = (EditText) findViewById(R.id.cvvCobroTAE);
		tarjetaNum = (EditText) findViewById(R.id.tarjetaCobroTAE);
		vigenciamm = (EditText) findViewById(R.id.vigenciaTAEmm);
		vigenciaaa = (EditText) findViewById(R.id.vigenciaTAEaa);
		total = (EditText) findViewById(R.id.totalTAE);

		final Button btnpago = (Button) findViewById(R.id.btn_pagoTAE);
		final Spinner spinnerTipo = (Spinner) findViewById(R.id.spinnerTAE);

		final String str = getIntent().getStringExtra("monto");
		cr = getIntent().getStringExtra("carrier");
		producto = getIntent().getIntExtra("id_producto", 0);

		montoEdit.setFocusable(false);
		montoEdit.setClickable(false);
		montoEdit.setText(str);

		total.setFocusable(false);
		total.setClickable(false);
		total.setText(str + comistr);

		ctextview.setText(cr);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(contxt,
				android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinnerTipo.setAdapter(adapter);

		spinnerTipo.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				Toast.makeText(
						contxt,
						"Tipo de pago: "
								+ parent.getItemAtPosition(pos).toString(),
						Toast.LENGTH_SHORT).show();
				tipopago = pos;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		btnpago.setOnClickListener(new OnClickListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (tipopago == 0) {
					Toast.makeText(getApplicationContext(), "Selecciona un tipo de Pago", Toast.LENGTH_SHORT).show();
					//tipopago =1;
				}else {

					if (TextUtils.isEmpty(num.getText())) {
						Toast.makeText(contxt, "Campo Número vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(numConf.getText())) {
						Toast.makeText(contxt, "Confirma número",
								Toast.LENGTH_SHORT).show();
					} else if (!num.getText().toString()
							.equals(numConf.getText().toString())) {
						Toast.makeText(contxt,
								"Confirmación de número incorrecta",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(nombre.getText())) {
						Toast.makeText(contxt, "Campo Nombre vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(correo.getText())) {
						Toast.makeText(contxt, "Campo Correo vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(tarjetaNum.getText())) {
						Toast.makeText(contxt,
								"Campo Número de Tarjeta vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(cvv2.getText())) {
						Toast.makeText(contxt, "Campo CVV2 vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(vigenciamm.getText())) {
						Toast.makeText(contxt, "Campo Vigencia mes vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(vigenciaaa.getText())) {
						Toast.makeText(contxt, "Campo Vigencia año vacío",
								Toast.LENGTH_SHORT).show();
					}

					else {
						vigencia = vigenciamm.getText().toString().trim() + "/"
								+ vigenciaaa.getText().toString().trim();
						pedirToken();

					}

				}
			}
		});

	}

	private void pedirToken() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.USUARIO_TOKEN, Servicios.userToken);
			json.put(Constructors.PASSWORD, Servicios.passToken);
		} catch (JSONException e) {
			// TODO: handle exception
			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new TokenListener(), contxt, true,
				Urls.url_getToken).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private void enviarPago() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.MAIL, correo.getText().toString().trim());
			json.put(Constructors.NOMBRES, nombre.getText().toString().trim());
			json.put(Constructors.PRODUCTO, producto);
			json.put(Constructors.TELEFONO, num.getText().toString().trim());
			json.put(Constructors.TARGETA_TAG, tarjetaTag);
			json.put(Constructors.ID_TIPO_TARJETA, tipopago);
			json.put(Constructors.VIGENCIA, vigencia);
			json.put(Constructors.TARGETA, tarjetaNum.getText().toString().trim());
			json.put(Constructors.CVV2, cvv2.getText().toString().trim());
			json.put(Constructors.PASSWORD, session.getUserPassword());
			json.put(Constructors.TOKEN, token);
			json.put(Constructors.IMEI, Dispositivo.imei);
			json.put(Constructors.MODELO, Dispositivo.modelo);
			json.put(Constructors.SOFTWARE, Dispositivo.software_version);

			Log.i(getPackageName(), json.toString());

		} catch (JSONException e) {
			// TODO: handle exception

			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new PagoListener(), contxt, true, Urls.url_pagoTAE)
				.execute(AddcelCrypto.encryptSensitive(AppUtils.getKey(),
						json.toString()));

	}

	private class TokenListener implements WSResponseListener {

		private static final String TAG = "TokenListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.i("TOKEN", response);

				// if (!AddcelCrypto.decryptHard(response).startsWith("{")) {
				// token = response;
				// enviarPago();
				// } else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				try {

					JSONObject json = new JSONObject(response);

					erridtok = json.getInt(Constructors.ID_ERROR);
					errmsgtk = json.getString(Constructors.MENSAJE_ERROR);
					token = json.getString(Constructors.TOKEN);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.i(TAG, response.toString());
				}
				Log.e(TAG, errmsgtk);
				if (erridtok != 0) {
					Toast.makeText(contxt, errmsgtk, Toast.LENGTH_SHORT).show();
				} else {
					enviarPago();
				}

			}
			// }

		}

	}

	private class PagoListener implements WSResponseListener {

		private static final String TAG = "PagoListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);

			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {

				response = AddcelCrypto.decryptSensitive(response);
				Log.i(TAG, response);
				try {
					JSONObject json = new JSONObject(response);

					errornum = json.getInt(Constructors.ID_ERROR);
					errormsg = json.getString(Constructors.MENSAJE_ERROR);
					autbanco = json.getString(Constructors.NUMERO_AUTORIZACION);
					foliobnc = json.getString(Constructors.FOLIO);
					comision = json.getString(Constructors.COMISION);
					cargo = json.getString(Constructors.CARGO);
					idBitacora = json.getString(Constructors.ID_BITACORA);

				} catch (JSONException e) {
					e.printStackTrace();
					Log.i(TAG, response.toString());
				}
			}

			if (errornum != 0) {
				Toast.makeText(contxt, "ERROR: " + errormsg, Toast.LENGTH_LONG)
						.show();
			} else {
				Log.i(this.getClass().getName().toString(),"Guardado en Bitacora, id= " + idBitacora.toString());
				AlertDialog.Builder builder = new AlertDialog.Builder(contxt, 3);

				builder.setTitle("Pago exitoso");

				if (tipopago != 0) {

					builder.setMessage("Recarga al número :  "
							+ num.getText().toString().trim() + "\nTotal: "
							+ cargo + "\nComision: " + comision + "\nFolio: "
							+ foliobnc);
				} else {
					builder.setMessage("Recarga al numero :  "
							+ num.getText().toString().trim() + "\nTotal: "
							+ cargo + "\nComision: " + comision + "\nFolio: "
							+ foliobnc + "\nAutorización bancaria: "
							+ autbanco);
				}

				builder.setPositiveButton("ACEPTAR",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								startActivity(new Intent(contxt,
										Menu_Activity.class));

							}
						});
				AlertDialog aldialog = builder.create();
				aldialog.show();
			}

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(this, Menu_Activity.class));
		finish();
	}
}
