package org.addcel.richard.pagos;

import java.util.Stack;

import org.addcel.richard.activities.Menu_Activity;
import org.addcel.richard.fragments.Telepeaje_Fragment;
import org.addcel.richard.fragments.TiempoAire_Fragment;
import org.addcel.richard.fragments.Tvdepaga_Fragment;
import org.addcel.richard.fragments.Viajes_Fragment;

import org.addcel.richard.R;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class PagodeServicios_Activity extends Activity implements
		OnClickListener {

	public Stack<String> mFragmentStack;
	private int lastPush = 0;

	private Button btn_TAE;
	private Button btn_Telepeaje;
	private Button btn_tvpaga;
	private Button btn_Viajes;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Log.i(getClass().toString(), "onBackPressed selected stack size: "
				+ mFragmentStack.size());

		// Fragment fragment =
		// getFragmentManager().findFragmentByTag(mFragmentStack.peek());

		mFragmentStack.clear();
		// super.onBackPressed();
		startActivity(new Intent(PagodeServicios_Activity.this,
				Menu_Activity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		finish();

		/*
		 * if (mFragmentStack.size() <= 1) { startActivity(new
		 * Intent(PagodeServicios_Activity.this, Menu_Activity.class)); } else {
		 * 
		 * removeFragment(); super.onBackPressed(); }
		 */
	}

	/*
	 * 
	 * private void removeFragment() { // TODO Auto-generated method stub
	 * mFragmentStack.pop();
	 * 
	 * // remove from backstack and from mFragmentStack FragmentTransaction
	 * transaction = getFragmentManager() .beginTransaction(); Fragment fragment
	 * = getFragmentManager().findFragmentByTag( mFragmentStack.peek());
	 * transaction.setCustomAnimations(R.anim.slide_out_right,
	 * R.anim.slide_in_left); transaction.show(fragment); transaction.commit();
	 * }
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pagodeservicios_layout);

		btn_TAE = (Button) findViewById(R.id.btn_Tiempoaire);
		btn_Telepeaje = (Button) findViewById(R.id.btn_Telepeaje);
		btn_tvpaga = (Button) findViewById(R.id.btn_Tvdepaga);
		btn_Viajes = (Button)findViewById(R.id.btn_Viajes);

		btn_tvpaga.setOnClickListener(this);
		btn_TAE.setOnClickListener(this);
		btn_Telepeaje.setOnClickListener(this);
		btn_Viajes.setOnClickListener(this);

		mFragmentStack = new Stack<String>();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (lastPush != v.getId()) {

			switch (v.getId()) {
			case R.id.btn_Tiempoaire:
				changeSelector(lastPush, v.getId());
				Fragment tiempoAire = new TiempoAire_Fragment();
				showfragment(tiempoAire);

				break;
			case R.id.btn_Telepeaje:
				changeSelector(lastPush, v.getId());
				Fragment telePeaje = new Telepeaje_Fragment();
				showfragment(telePeaje);

				break;
			case R.id.btn_Tvdepaga:
				changeSelector(lastPush, v.getId());
				Fragment teledePaga = new Tvdepaga_Fragment();
				showfragment(teledePaga);

				break;

			case R.id.btn_Viajes:
				changeSelector(lastPush, v.getId());
				Fragment viajesFrag = new Viajes_Fragment();
				showfragment(viajesFrag);
				break;

			}
			lastPush = v.getId();
		}

	}

	private void showfragment(Fragment newfragment) {
		// TODO Auto-generated method stub
		Bundle args = new Bundle();
		String tag = newfragment.toString();
		newfragment.setArguments(args);

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		transaction.setCustomAnimations(R.animator.slide_in_izquierda,
				R.animator.slide_in_derecha);

		if (mFragmentStack.size() != 0) {
			Fragment currentFragment = getFragmentManager().findFragmentByTag(
					mFragmentStack.peek());
			transaction.hide(currentFragment);
		}

		transaction.add(R.id.detalleServicios, newfragment, tag);
		transaction.addToBackStack(tag);
		mFragmentStack.add(tag);

		transaction.commit();
	}

	private void changeSelector(int last, int id) {
		// TODO Auto-generated
		// methodstub //
		// setContentView(R.layout.activity_historialventas);

		switch (id) {
		case R.id.btn_Tiempoaire:
			if (last == 0) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			} else if (last == R.id.btn_Telepeaje) {
				btn_Telepeaje.setEnabled(true);
				btn_Telepeaje
						.setBackgroundResource(R.drawable.selector_telepeaje);
			} else if (last == R.id.btn_Tvdepaga) {
				btn_tvpaga.setEnabled(true);
				btn_tvpaga.setBackgroundResource(R.drawable.selector_tvdepaga);
			}else if (last == R.id.btn_Viajes) {
				btn_Viajes.setEnabled(true);
				btn_Viajes.setBackgroundResource(R.drawable.selector_viajes);
			}
			btn_TAE.setEnabled(false);
			btn_TAE.setBackgroundResource(R.drawable.b_tiempoaire_o);
			break;
		case R.id.btn_Telepeaje:
			if (last == 0) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			} else if (last == R.id.btn_Tvdepaga) {
				btn_tvpaga.setEnabled(true);
				btn_tvpaga.setBackgroundResource(R.drawable.selector_tvdepaga);
			} else if (last == R.id.btn_Tiempoaire) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			}else if (last == R.id.btn_Viajes) {
				btn_Viajes.setEnabled(true);
				btn_Viajes.setBackgroundResource(R.drawable.selector_viajes);
			}
			btn_Telepeaje.setEnabled(false);
			btn_Telepeaje.setBackgroundResource(R.drawable.b_telepeaje_o);
			break;
		case R.id.btn_Tvdepaga:
			if (last == 0) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			} else if (last == R.id.btn_Tiempoaire) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			} else if (last == R.id.btn_Telepeaje) {
				btn_Telepeaje.setEnabled(true);
				btn_Telepeaje
						.setBackgroundResource(R.drawable.selector_telepeaje);
			}else if (last == R.id.btn_Viajes) {
				btn_Viajes.setEnabled(true);
				btn_Viajes.setBackgroundResource(R.drawable.selector_viajes);
			}
			btn_tvpaga.setEnabled(false);
			btn_tvpaga.setBackgroundResource(R.drawable.b_tvdepaga_o);
			break;
			
		case R.id.btn_Viajes:
			if (last == 0) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			}else if (last== R.id.btn_Tiempoaire) {
				btn_TAE.setEnabled(true);
				btn_TAE.setBackgroundResource(R.drawable.selector_tiempoarie);
			}else if (last == R.id.btn_Tvdepaga) {
				btn_tvpaga.setEnabled(true);
				btn_tvpaga.setBackgroundResource(R.drawable.selector_tvdepaga);
			}  else if (last == R.id.btn_Telepeaje) {
				btn_Telepeaje.setEnabled(true);
				btn_Telepeaje
						.setBackgroundResource(R.drawable.selector_telepeaje);
			}
			btn_Viajes.setEnabled(false);
			btn_Viajes.setBackgroundResource(R.drawable.b_viajes_o);
			break;
		}

	}

}
