package org.addcel.session;

import org.addcel.richard.activities.Inicio_Activity;
import org.addcel.richard.constant.Constructors;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

//@SuppressLint("CommitPrefEdits")
public class SessionManager {
	private SharedPreferences pref;
	private Editor editor;
	private Context _context;

	private int PRIVATE_MODE = 0;
	private int PUBLIC_MODE = Context.MODE_MULTI_PROCESS;

	
	public SessionManager(Context context) {
		// TODO Auto-generated constructor stub
		this._context = context;
		pref = _context.getSharedPreferences(Constructors.PREF_NAME, PUBLIC_MODE);
		editor = pref.edit();
	}

	public void dataKit(int idTienda, String nombreTienda, String calle,
			String numInt, String numExt, String colonia, String cp,
			String ciudad, int idkit, String carnet, String vigencia,
			String dispositivo) {

		editor.putInt(Constructors.ID_TIENDA, idTienda);
		editor.putString(Constructors.NOMBRE_TIENDA, nombreTienda);
		editor.putString(Constructors.CALLE, calle);
		editor.putString(Constructors.NUMERO_INTERIOR, numInt);
		editor.putString(Constructors.NUMERO_EXTERIOR, numExt);
		editor.putString(Constructors.COLONIA, colonia);
		editor.putString(Constructors.CODIGO_POSTAL, cp);
		editor.putString(Constructors.CIUDAD, ciudad);
		editor.putInt(Constructors.ID_KIT, idkit);
		editor.putString(Constructors.CARNET, carnet);
		editor.putString(Constructors.VIGENCIA, vigencia);
		editor.putString(Constructors.DISPOSITIVO, dispositivo);
		editor.commit();
	}

	public void createLoginSession(int idUsuario, int idUsuarioMod,
			int idProveedor, int idTienda, 
			String login, String nombre, String apellido, String materno,
			String mail, String telefono, int status,
			int statusTienda, String password, String tipoTarjeta) {
		// TODO Auto-generated method stub

		editor.putBoolean(Constructors.IS_LOGIN, true);
		editor.putInt(Constructors.ID_USUARIO, idUsuario);
		editor.putInt(Constructors.ID_USUARIO_MOD, idUsuarioMod);
		editor.putInt(Constructors.ID_PROVEEDOR, idProveedor);
		editor.putInt(Constructors.ID_TIENDA, idTienda);
		editor.putInt(Constructors.STATUS, status);
		editor.putInt(Constructors.STATUS_TIENDA, statusTienda);
		editor.putString(Constructors.LOGIN, login);
		editor.putString(Constructors.NOMBRE, nombre);
		editor.putString(Constructors.APELLIDO, apellido);
		editor.putString(Constructors.MATERNO, materno);
		editor.putString(Constructors.MAIL, mail);
		editor.putString(Constructors.TELEFONO, telefono);
		editor.putString(Constructors.PASSWORD, password);
		editor.putString(Constructors.TIPOTARJETA, tipoTarjeta);
		editor.commit();

	}

	public void checkLogin() {
		if (!this.isLoggedIn()) {
			Intent i = new Intent(_context, Inicio_Activity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			_context.startActivity(i);
		}
	}

	public void logoutUser() {
		editor.clear();
		editor.commit();
		// Staring Login Activity
	}

	public JSONArray getTipotarjeta() {
		String tp;
		tp = pref.getString(Constructors.TIPOTARJETA, null);
		JSONArray jatp = null;
		try {
			jatp = new JSONArray(tp);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jatp;

	}

	public boolean isLoggedIn() {
		return pref.getBoolean(Constructors.IS_LOGIN, false);
	}
	
	
	
	public void setCarnetTotal(String total){
		editor.putString(Constructors.CARNET_TOTAL, total);
		editor.commit();
	}
	
	public String getCarnetTotal(){
		return pref.getString(Constructors.CARNET_TOTAL, null);
	}
	
	public void setCarnetDisponible(String disponible){
		editor.putString(Constructors.CARNET_SALDO_DISPONIBLE, disponible);
		editor.commit();
	}
	
	public String getCarnetDisponible(){
		return pref.getString(Constructors.CARNET_SALDO_DISPONIBLE, null);
	}

	public void setPassword(String pass) {
		editor.putString(Constructors.PASSWORD, pass);
		editor.commit();
	}

	public void setTime_GPS(int interv){
		editor.putInt(Constructors.TIEMPO_NOTIFICACION, interv);
		editor.commit();
	}
	
	public int getTime_GPS(){
		return pref.getInt(Constructors.TIEMPO_NOTIFICACION, 0);
	}
	
	public void setTime_GPS_ant(int interv){
		editor.putInt(Constructors.TIEMPO_NOTIF_ANTERIOR, interv);
		editor.commit();
	}
	
	public int getTime_GPS_ant(){
		return pref.getInt(Constructors.TIEMPO_NOTIF_ANTERIOR, 0);
	}
	
	/**
	 * @return the idUsuario
	 */
	public int getIdUsuario() {
		return pref.getInt(Constructors.ID_USUARIO, 0);
	}

	/**
	 * @return the idUsuarioMod
	 */
	public int getIdUsuarioMod() {
		return pref.getInt(Constructors.ID_USUARIO_MOD, 0);
	}

	/**
	 * @return the idProveedor
	 */
	public int getIdProveedor() {
		return pref.getInt(Constructors.ID_PROVEEDOR, 0);
	}

	

	/**
	 * @return the idTienda
	 */
	public int getIdTienda() {
		return pref.getInt(Constructors.ID_TIENDA, 0);
	}

	
	/**
	 * @return the login
	 */
	public String getLogin() {
		return pref.getString(Constructors.LOGIN, null);
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return pref.getString(Constructors.NOMBRE, null);
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return pref.getString(Constructors.APELLIDO, null);
	}

	/**
	 * @return the materno
	 */
	public String getMaterno() {
		return pref.getString(Constructors.MATERNO, null);
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return pref.getString(Constructors.MAIL, null);
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return pref.getString(Constructors.TELEFONO, null);
	}

	

	/**
	 * @return the status
	 */
	public int getStatus() {
		return pref.getInt(Constructors.STATUS, 0);
	}

	/**
	 * @return the statusTienda
	 */
	public int getStatusTienda() {
		return pref.getInt(Constructors.STATUS_TIENDA, 0);
	}

	/**
	 * @return the User_password
	 */
	public String getUserPassword() {
		return pref.getString(Constructors.PASSWORD, null);
	}

	/**
	 * @return the nombreTienda
	 */
	public String getNombreTienda() {
		return pref.getString(Constructors.NOMBRE_TIENDA, "0000");
	}

	/**
	 * @return the calle
	 */
	public String getCalle() {
		return pref.getString(Constructors.CALLE, null);
	}

	/**
	 * @return the numInt
	 */
	public String getNumInt() {
		return pref.getString(Constructors.NUMERO_INTERIOR, null);
	}

	/**
	 * @return the numExt
	 */
	public String getNumExt() {
		return pref.getString(Constructors.NUMERO_EXTERIOR, null);
	}

	/**
	 * @return the colonia
	 */
	public String getColonia() {
		return pref.getString(Constructors.COLONIA, null);
	}

	/**
	 * @return the cp
	 */
	public String getCp() {
		return pref.getString(Constructors.CODIGO_POSTAL, null);
	}

	/**
	 * @return the ciudad
	 */
	public String getCiudad() {
		return pref.getString(Constructors.CIUDAD, null);
	}

	/**
	 * @return the idkit
	 */
	public int getIdkit() {
		return pref.getInt(Constructors.ID_KIT, 0);
	}

	/**
	 * @return the carnet
	 */
	public String getCarnet() {
		return pref.getString(Constructors.CARNET, null);
	}

	/**
	 * @return the vigencia
	 */
	public String getVigencia() {
		return pref.getString(Constructors.VIGENCIA, null);
	}

	/**
	 * @return the dispositivo
	 */
	public String getDispositivo() {
		return pref.getString(Constructors.DISPOSITIVO, null);
	}

	
	
}
