package org.addcel.util;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.etn.to.DetallePasajero;
import org.addcel.richard.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ETNUtils {
	
	public static String parseAsientos(ArrayList<DetallePasajero> pasajeros){
		String asientosParseados = new String();
		
		for(DetallePasajero pasajero: pasajeros){
			asientosParseados += trimAsiento(pasajero.getAsiento());
		}
		
		System.out.println(":::ASIENTOS PARSEADOS::: " + asientosParseados);
		
		return asientosParseados;
	}
	
	private static String trimAsiento(String asiento){
		
		String asientoLocal = asiento;
		
		if(asientoLocal.contains("T")){
			asientoLocal = asientoLocal.replace("T", "");
		}
		
		System.out.println(":::TRIM ASIENTO:::" + asientoLocal);
		return asientoLocal;
	}
	
	public static String parseNombres(ArrayList<DetallePasajero> pasajeros){
		String nombresParseados = new String();
		
		for(int i = 0; i < pasajeros.size(); i++){
			if(i == pasajeros.size() - 1){
				nombresParseados += pasajeros.get(i).getNombre();
			}
			else{
				nombresParseados += (pasajeros.get(i).getNombre() + ",");
			}
		}
		
		System.out.println(":::NOMBRES PARSEADOS:::" + nombresParseados);
		
		return nombresParseados;
	}
	
	public static boolean validarNombresPasajeros(ArrayList<DetallePasajero> pasajeros){
		for(DetallePasajero pasajero: pasajeros){
			if(pasajero.getNombre().equals("")){
				return false;
			}
		}
		
		return true;
	}
	
	public static double getTotalAPagar(ArrayList<DetallePasajero>pasajeros){
		double totalAPagar = 0.0;
		for(DetallePasajero pasajero: pasajeros){
			totalAPagar += pasajero.getTarifa();
		}
		
		return totalAPagar;
	}
	
	public static String formatHora(String hora){
		String horaLocal = hora;
		
		if(horaLocal.contains(":")){
			horaLocal = horaLocal.replace(":", "");
		}
		
		return horaLocal + "00";
	}
	
	public static String asientosAStringConSeparador(List<DetallePasajero> lista, String separador) {

		String listaAString = "";
		
		List<DetallePasajero> subList = lista.subList(0, lista.size() - 1);
		
		for (DetallePasajero pasajero : subList) {
		
			listaAString = listaAString + trimAsiento(pasajero.getAsiento()) + separador;
		
		}
		
		return listaAString + trimAsiento(lista.get(lista.size() - 1).getAsiento());
	
	}
	
	public static String formatFecha(String fecha){
		String anio = fecha.substring(0, 4);
		String mes = fecha.substring(4, 6);
		String dia = fecha.substring(6, 8);
		
		return dia + "/" + mes + "/" + anio;
	}
	
	public static String formatFecha2(String fecha) {
		String dia = fecha.substring(0, 2);
		String mes = fecha.substring(2, 4);
		String anio = fecha.substring(4, 8);
		
		return dia + "/" + mes + "/" + anio;
	}
	
	public static String cleanFecha(String fecha){
		if(fecha.contains("/")){
			return fecha.replace("/", "");
		}
		else{
			return fecha;
		}
	}
	
	public static void showAlertDialogError(Activity ctx, String header, String mensaje) {
		AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
		alertDiaBuilder.setTitle("Error en Reservaci�n");
		alertDiaBuilder.setMessage(mensaje)
			.setCancelable(false)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
		
		AlertDialog alertDialog = alertDiaBuilder.create();
		alertDialog.show();
	}
	/****
	public static void showAlertDialogSuccess(final Activity ctx, final DataReservacion reservacion, final long idViaje) {
		
		String message = "N�mero de reservacion: " + reservacion.getIdReservacion() + "\n"
					   +"Importe total: "+ reservacion.getImporteTotal() + "\n";
						 
		
		AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
		alertDiaBuilder.setTitle("Verifica tu reservaci�n")
		.setMessage(message)
		.setPositiveButton("Pagar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String urlPago = Text.getPurchaseUrl(ctx, UrlWebServices.URL_PURCHASE_ETN, idViaje,reservacion.getImporteTotal(), 0);
				Intent intent = new Intent(ctx, CompraWebActivity.class);
				intent.putExtra("url", urlPago);
				intent.putExtra("url_regreso", UrlWebServices.URL_REGRESO_ETN);
				ctx.startActivity(intent);
								
			}
		})
		.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
		AlertDialog alertDialog = alertDiaBuilder.create();
		alertDialog.show();
	}
	
	public static void showInitialDialog(final Activity ctx) {
		AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
		alertDiaBuilder.setTitle(ctx.getResources().getText(R.string.etn_leyenda_bienvenida))
			.setMessage(ctx.getResources().getText(R.string.etn_inicial_string))
			.setCancelable(false)
			.setNegativeButton("Reg�strate", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(ctx, RegistroActivity.class);
					ctx.startActivity(intent);
				}
			})
			.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
		
		AlertDialog alertDialog = alertDiaBuilder.create();
		alertDialog.show();
	}	
	********/
	
	public static void showLoginDialog(final Activity ctx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle("Ingresa a MobileCard");
		builder.setMessage("Para poder ver tus pases de abordar...");
		
		TextView usuarioText = new TextView(ctx);
		final EditText usuario = new EditText(ctx);
		
		TextView passwordText = new TextView(ctx);
		final EditText password = new EditText(ctx);
		
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
	        LinearLayout.LayoutParams.MATCH_PARENT,
	        LinearLayout.LayoutParams.WRAP_CONTENT);
		
		usuarioText.setLayoutParams(lp);
		usuarioText.setTextColor(ctx.getResources().getColor(R.color.blanco));
		usuario.setLayoutParams(lp);
		
		passwordText.setLayoutParams(lp);
		password.setLayoutParams(lp);
		
		builder.setView(usuarioText);
//		builder.setView(usuario);
//		builder.setView(passwordText);
//		builder.setView(password);
		
		builder.setPositiveButton("Iniciar Sesi�n", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//loginProcess.start();
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
/*AlertDialog.Builder editalert = new AlertDialog.Builder(this);

editalert.setTitle("messagetitle");
editalert.setMessage("here is the message");


final EditText input = new EditText(this);
LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.FILL_PARENT,
        LinearLayout.LayoutParams.FILL_PARENT);
input.setLayoutParams(lp);
editalert.setView(input);

editalert.setPositiveButton("Send via email", new DialogInterface.OnClickListener() {
    public void onClick(DialogInterface dialog, int whichButton) {


    }
});*/
	
}
