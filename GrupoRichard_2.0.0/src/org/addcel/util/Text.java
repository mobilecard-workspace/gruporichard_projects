package org.addcel.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class Text {
	
private static Locale MX_LOCALE = new Locale("es", "MX");
	
	public static Locale getMexLocale() {
		
		if (null == MX_LOCALE)
			MX_LOCALE = new Locale("es", "MX");
		
		return MX_LOCALE;
	}
	 

	public static boolean esCorreo(String correo) {
		return (correo
				.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
	}

	public static String parsePass(String pass) {
		int len = pass.length();
		String key = "";

		for (int i = 0; i < 32 / len; i++) {
			key += pass;
		}

		int carry = 0;
		while (key.length() < 32) {
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}

	public static String reverse(String chain) {
		String nuevo = "";

		for (int i = chain.length() - 1; i >= 0; i--) {
			nuevo += chain.charAt(i);
		}

		return nuevo;
	}

	public static String mergeStr(String first, String second) {
		String result = "";
		String other = Text.reverse(second);

		result += (Integer.toString(other.length()).length() < 2) ? "0"
				+ other.length() : Integer.toString(other.length());

		String sub1 = first.substring(0, 19);
		String sub2 = first.substring(19, first.length());

		result += sub1;

		for (int i = 0; i < other.length(); i += 2) {
			int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);
			String next = other.substring(i, offset);

			result += next;

			result += sub2.substring(0, 2);

			sub2 = sub2.substring(2);
		}
		result += sub2;

		return result;
	}

	public static String formatDate(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",
				Locale.getDefault());
		return format.format(date);
	}

	public static String formatDateForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy",
				Locale.getDefault());
		return format.format(date);
	}
	public static String formatDateNoSpace(Date date) {

		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy", Locale.US);
		return format.format(date);
	}
	
	public static Date getEtnDateFromStringWithSlash(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 Log.i("getSimpleDateFromStringWithSlash", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static String formatTimer(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd hh:mm:ss",
				Locale.getDefault());
		return format.format(date);
	}

	public static String formatCurrency(double cantidad, boolean label) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
		String moneyString = formatter.format(cantidad);

		if (!label)
			moneyString = moneyString.replace("$", "").replace(",", "");

		return moneyString;
	}
	
	public static String formatCurrency(double cantidad) {
		
		NumberFormat formatter = NumberFormat.getCurrencyInstance(getMexLocale());
		formatter.setMaximumFractionDigits(2);
		
		String moneyString = formatter.format(cantidad);
		
		return moneyString;
	}

	public static Integer[] getYearsArray() {
		Calendar c = Calendar.getInstance();
		Integer[] years = new Integer[10];

		int i = 0;
		int year = c.get(Calendar.YEAR);

		while (i < 10) {

			years[i] = year;
			year++;
			i++;
		}

		Log.i("years array", Arrays.toString(years));

		return years;

	}

	public static String trimMonth(int month) {
		month = month + 1;

		if (month < 10) {
			return "0" + Integer.toString(month);
		} else {
			return Integer.toString(month);
		}
	}

	public static String trimYear(int year) {
		return Integer.toString(year).substring(2, 4);
	}

	public static String getVigencia(int month, int year) {
		return trimMonth(month) + "/" + trimYear(year);
	}

	public static Date getDateFromString(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
				Locale.getDefault());
		try {
			Date fecha = format.parse(s);
			Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

	public static Date getSimpleDateFromString(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",
				Locale.getDefault());
		try {
			Date fecha = format.parse(s);
			Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

	public static String addParamsToUrl(String url,
			List<BasicNameValuePair> params) {
		if (!url.endsWith("?"))
			url += "?";

		String paramString = URLEncodedUtils.format(params, "utf-8");

		url += paramString;

		return url;
	}

	public static String getKey() {
		return String.valueOf(new Date().getTime()).substring(0, 8);

	}

	public static String getPeriodosConPipe(List<String> list) {
		String listString = "";

		List<String> subList = list.subList(0, list.size() - 1);

		for (String periodo : subList) {
			listString = listString + periodo + "|";
		}

		return listString + list.get(list.size() - 1);
	}

}
