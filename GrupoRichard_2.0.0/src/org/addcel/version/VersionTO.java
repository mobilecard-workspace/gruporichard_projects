package org.addcel.version;

public class VersionTO {

	private String idPlataforma;
	private String idApp;

	public VersionTO() {
	}

	public VersionTO(String idPlataforma, String idApp) {
		this.idPlataforma = idPlataforma;
		this.idApp = idApp;
	}

	public String getIdPlataforma() {
		return idPlataforma;
	}

	public void setIdPlataforma(String idPlataforma) {
		this.idPlataforma = idPlataforma;
	}

	public String getIdApp() {
		return idApp;
	}

	public void setIdApp(String idApp) {
		this.idApp = idApp;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{idPlataforma: " + idPlataforma + ", idApp: " + idApp + "}";
	}

}
