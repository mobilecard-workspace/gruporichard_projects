package org.addcel.view;

import java.util.ArrayList;

import org.addcel.android.etn.constant.Url;
import org.addcel.android.etn.data.DataItinerario;
import org.addcel.android.etn.to.Corrida;
import org.addcel.client.MultiparamClient;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.globals.RichardApplication;
import org.addcel.util.ETNUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Corridas_ListAdapter extends BaseAdapter {

	private Context myContext;
	ArrayList<Corrida> data;
	RichardApplication myRichard;
	String TAG = "CorridaAdapter";
	
	
	

	public Corridas_ListAdapter(Context c, ArrayList<Corrida> a) {
		myContext = c;
		data = a;
		myRichard = (RichardApplication) c.getApplicationContext();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	View row;
	@Override
	public View getView(final int pos, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		int nomId = R.id.txtFechaYHoraSalida;
		int espId = R.id.txtFechaYHoraLlegada;
		int dirId = R.id.txtLinea;
		int serId = R.id.txtServicio;
		int preId = R.id.txtPrecio;
		int itiId = R.id.txtItinerario;

		row = convertView;
		Holder holder = null;
		if (row == null) {
			LayoutInflater inflater = ((Activity) myContext)
					.getLayoutInflater();
			row = inflater.inflate(R.layout.etn_resultado_item, null);
			holder = new Holder();
			holder.dateInicial = (TextView) row.findViewById(nomId);
			holder.dateArribo = (TextView) row.findViewById(espId);
			holder.linea = (TextView) row.findViewById(dirId);
			holder.servicio = (TextView) row.findViewById(serId);
			holder.precio = (TextView) row.findViewById(preId);
			holder.itinerario = (TextView) row.findViewById(itiId);
			row.setTag(holder);
		} else {
			holder = (Holder) row.getTag();
		}
		
		

		final Corrida corr = data.get(pos);
		holder.dateInicial.setText(corr.getFechaInicial() + "  "
				+ corr.getHoraCorrida());
		holder.dateArribo.setText(corr.getFechaLlegada() + "  "
				+ corr.getSecuencia());
		holder.servicio.setText(corr.getDetServicio());
		holder.linea.setText(corr.getEmpresaCorrida());
		holder.precio.setText(Double.toString(corr.getTarifa()));

		holder.itinerario.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				BasicNameValuePair[] params = new BasicNameValuePair[3];
				params[0] = new BasicNameValuePair("corrida", corr.getCorrida()
						.toString());
				params[1] = new BasicNameValuePair("origen", myRichard
						.getOrigen());
				params[2] = new BasicNameValuePair("fecha", ETNUtils
						.cleanFecha(corr.getFechaInicial()));

				new MultiparamClient(new itinerarioListener(), myContext, true,
						Url.URL_WS_ITINERARIO_GET).execute(params);

			}
		});
		/************
		 * myListView.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 *           int pos, long id) { // TODO Auto-generated method stub
		 * 
		 *           Toast.makeText(myContext, corr.getHoraCorrida(),
		 *           Toast.LENGTH_SHORT).show();
		 *           //row.setBackgroundColor(color.transparent);
		 *           view.setSelected(true);
		 *           view.setBackgroundColor(myContext.getResources
		 *           ().getColor(R.color.gris_claro));
		 * 
		 *           } });
		 ****************/
		return row;
	}
	
	

	static class Holder {
		TextView dateInicial;
		TextView dateArribo;
		TextView linea;
		TextView servicio;
		TextView precio;
		TextView itinerario;

	}

	private class itinerarioListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(myContext,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {
				Log.i("itinerarioListener", response);

				JSONObject respons;
				JSONArray listaItinerarios = null;
				try {
					respons = new JSONObject(response);
					listaItinerarios = respons.getJSONArray("listaItinerarios");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ArrayList<DataItinerario> itinerarios = new ArrayList<DataItinerario>();
				JSONObject itinerario = null;
				for (int i = 0; i < listaItinerarios.length(); i++) {
					itinerario = listaItinerarios.optJSONObject(i);
					itinerarios.add(new DataItinerario(itinerario
							.optString("oficina"), itinerario
							.optString("fechaSalida"), itinerario
							.optString("horaSalida"), itinerario
							.optString("errorNumero"), itinerario
							.optString("errorCadena")));
				}
				String resultado = printItinerarios(itinerarios);
				
				AlertDialog.Builder alertBuilder = new AlertDialog.Builder(myContext);
				alertBuilder.setTitle("Itinerario");
				alertBuilder.setMessage(resultado);
				alertBuilder.setCancelable(false);
				alertBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				AlertDialog alertDialog = alertBuilder.create();
				alertDialog.show();
			}

		}
	}

	private String printItinerarios(ArrayList<DataItinerario> d) {
		String itinerariosString = new String();

		itinerariosString += "Ruta que seguir� la corrida elegida: \n\n";

		for (DataItinerario dataItinerario : d) {
			itinerariosString += "Fecha de salida: "
					+ ETNUtils.formatFecha(dataItinerario.getFechaSalida())
					+ ", Hora: " + dataItinerario.getHoraSalida()
					+ ", Terminal: " + dataItinerario.getOficina() + "\n\n";
		}

		return itinerariosString;
	}
}