package org.addcel.client;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.addcel.interfaces.WSResponseListener;
import org.addcel.util.Dialogos;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml.Encoding;

public class WebServiceClient extends AsyncTask<String, Void, String> {

	private String response;
	private Context con;
	private boolean hasLoader;
	private WSResponseListener listener;
	private String url;
	private static final String TAG = "WEB_SERVICE_CLIENT";

	/* Revisar Prgres Dialog */
	// ProgressDialog progDialog = new ProgressDialog();

	public WebServiceClient(WSResponseListener listener, Context con,
			boolean hasLoader, String url) {
		this.listener = listener;
		this.con = con;
		this.hasLoader = hasLoader;
		this.url = url;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

		if (hasLoader) {
			Dialogos.makeDialog(con, "Cargando...", "Espere por favor...");

			// progDialog.setMessage("Cargando");
			// progDialog.show();

		}
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {
			
			HttpParams connParams = new BasicHttpParams();
			int connectionTimeout = 10000;
			HttpConnectionParams.setConnectionTimeout(connParams, connectionTimeout );
			int responseTimeout = 60000;
			HttpConnectionParams.setSoTimeout(connParams, responseTimeout);

			
			
			DefaultHttpClient httpClient = new DefaultHttpClient(connParams);
			HttpPost httpPost = new HttpPost(url);

			if (params.length > 0 && params != null) {

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);
				nameValuePairs.add(new BasicNameValuePair("json", params[0]));
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}

			HttpResponse httpResponse = httpClient.execute(httpPost);

			HttpEntity httpEntity = httpResponse.getEntity();

			response = EntityUtils.toString(httpEntity, Encoding.UTF_8.name());
		} catch (ClientProtocolException e) {
			Log.e(TAG, "ClientProtocolException", e);
			return "";
		}catch (ConnectTimeoutException e) {
			Log.e(TAG, "ConnectTimeoutException", e);
			return "";
		} catch(SocketTimeoutException e) {
			Log.e(TAG, "SocketTimeoutException", e);
			return "";
		} catch (IOException e) {
			Log.e(TAG, "IOException", e);
			return "";
		}
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub

		if (hasLoader) {
			Dialogos.closeDialog();
			// progDialog.dismiss();
		}

		listener.StringResponse(result);
	}

}
