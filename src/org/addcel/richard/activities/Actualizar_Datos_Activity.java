package org.addcel.richard.activities;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Actualizar_Datos_Activity extends Activity {

	private SessionManager session;
	private EditText usuario;
	private EditText paterno;
	private EditText materno;
	private EditText nombres;
	private EditText telefono;
	private EditText correo;
	private Button btnUpdate;
	
	private String usr;
	private String pat;
	private String mat;
	private String nam;
	private String tel;
	private String mai;
	
	private Context contxt = Actualizar_Datos_Activity.this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.actualizar_datos_layout);

		session = new SessionManager(this);
		usr = session.getLogin();
		pat = session.getApellido();
		mat = session.getMaterno();
		nam = session.getNombre();
		tel = session.getTelefono();
		mai = session.getMail();
		 

		usuario = (EditText) findViewById(R.id.updateUser_login_frg);
		paterno = (EditText) findViewById(R.id.updateUser_apellidoP_frg);
		materno = (EditText) findViewById(R.id.updateUser_apellidoM_frg);
		nombres = (EditText) findViewById(R.id.updateUser_nombres_frg);
		telefono = (EditText) findViewById(R.id.updateUser_telefono_frg);
		correo = (EditText) findViewById(R.id.updateUser_correo_frg);
		btnUpdate = (Button) findViewById(R.id.BtnupdateUser);

		usuario.setText(usr);
		usuario.setEnabled(false);

		paterno.setText(pat);
		materno.setText(mat);
		nombres.setText(nam);
		telefono.setText(tel);
		correo.setText(mai);

		btnUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				pat = paterno.getText().toString().trim();
				mat = materno.getText().toString().trim();
				nam = nombres.getText().toString().trim();
				tel = telefono.getText().toString().trim();
				mai = correo.getText().toString().trim();
				
				AlertDialog.Builder builder = new AlertDialog.Builder(
						contxt, 3);

				builder.setTitle("Confirmar datos");
				builder.setMessage("Usuario: " + usr + "\nApellido Paterno: " + pat 
						+ "\nApellido Materno: " + mat
						+ "\nNombres: " + nam
						+ "\nTelefono: " + tel 
						+ "\nCorreo: " + mai
				);
				
				builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						
					}
				});
				
				builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						updateUser();
					}
				});		
				AlertDialog aldialog = builder.create();
				aldialog.show();
			}
		});

	}
	
	private void updateUser() {
		
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.NOMBRE, nam);
			json.put(Constructors.APELLIDO, pat);
			json.put(Constructors.MATERNO, mat);
			json.put(Constructors.MAIL, mai);
			json.put(Constructors.TELEFONO, tel);
			json.put(Constructors.ID_TIENDA, session.getIdTienda());
			json.put(Constructors.STATUS, session.getStatus());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.i(this.getClass().getName().toString(), json.toString());
		
		new WebServiceClient(new UpdateListener(), contxt, true, Urls.url_updateUSer).execute(AddcelCrypto.encryptHard(json.toString()));
	}
	
	
	private class UpdateListener implements WSResponseListener {

		private int errid;
		private String errmsg;

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.i("UpdateResponse", response);
				
				response = AddcelCrypto.decryptHard(response);
				Log.i(this.getClass().getName().toString(), response);

				
				try {
					JSONObject json = new JSONObject(response);
					errid = json.getInt(Constructors.ID_ERROR);
					errmsg = json.getString(Constructors.MENSAJE_ERROR);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (errid != 0) {
					Toast.makeText(contxt, "Error: " + errmsg,
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(contxt, errmsg, Toast.LENGTH_SHORT).show();
					startActivity(new Intent(contxt, Menu_Activity.class));
				}

				
			}
		}
		
	}
}
