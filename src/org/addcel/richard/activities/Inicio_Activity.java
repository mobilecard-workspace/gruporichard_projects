package org.addcel.richard.activities;

import java.util.Stack;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.richard.fragments.CambioPassword_Fragment;
import org.addcel.richard.fragments.Recuperar_Pass_Fragment;
import org.addcel.richard.fragments.RegistroFragment;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Inicio_Activity extends Activity {

	public Stack<String> mFragmentStack;

	private String usuario = null;
	private String passw = null;

	private SessionManager session;
	private Button iniciarSesion;
	private Button btnRegisto;
	private Fragment fragmentReg;
	private Button recuperarPass;
	private Context contxt = Inicio_Activity.this;

	private int lastPush = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_inicio);
		/*
		 * DisplayMetrics metrics = getResources().getDisplayMetrics(); int
		 * width = metrics.widthPixels; int height = metrics.heightPixels; float
		 * density = metrics.densityDpi; float scaleX = (float) ((800 / width) *
		 * 0.50); float scaleY = (float) ((444 / height) * 0.50); float scaleBtn
		 * = (float) ((120 * 0.80) / density); float scaleTX = (float) (120 /
		 * density);
		 */

		TelephonyManager tManager = (TelephonyManager) getBaseContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		Dispositivo.imei = tManager.getDeviceId();

		session = new SessionManager(Inicio_Activity.this);

		final EditText getUsuario = (EditText) findViewById(R.id.editTxt_nameUer);
		final EditText getPassword = (EditText) findViewById(R.id.editTxt_pswrd);
		recuperarPass = (Button) findViewById(R.id.btn_recuperarPass);

		mFragmentStack = new Stack<String>();

		iniciarSesion = (Button) findViewById(R.id.btn_iniciarSesion);
		btnRegisto = (Button) findViewById(R.id.btn_showregistro);

		/*
		 * iniciarSesion.setScaleX(scaleBtn); iniciarSesion.setScaleY(scaleBtn);
		 * btnRegisto.setScaleX(scaleBtn); btnRegisto.setScaleY(scaleBtn);
		 */

		//recuperarPass.setClickable(true);
		recuperarPass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//changeSelector(lastPush, arg0.getId());
				//lastPush=arg0.getId();
				AlertDialog.Builder builder = new AlertDialog.Builder(contxt)
						.setTitle("Advertencia")
						.setMessage(
								"Olvidaste tu contrase�a. \n �Desea cambiarla ahora?")
						.setCancelable(false);
				builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int arg1) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				builder.setPositiveButton("Aceptar",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								// TODO Auto-generated method stub
								Fragment fragment = new Recuperar_Pass_Fragment();
								showfragment(fragment);
							}
						});
				AlertDialog alert = builder.create();
				alert.show();

				lastPush = arg0.getId();
			}
		});

		iniciarSesion.setOnClickListener(new OnClickListener() {

			private int caso;

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				usuario = getUsuario.getText().toString();
				passw = getPassword.getText().toString();
				if (usuario.equals("admin")) {
					caso = 1;
				} else if (usuario.equals("vendor")) {
					caso = 2;
				} else if (usuario.equals("")) {
					caso = 3;
				} else {
					caso = 4;
				}
				Log.i("Caso del logeo", String.valueOf(caso));

				switch (caso) {
				case 1:

					if (passw.equals("admin")) {
						changeSelector(lastPush, R.id.btn_iniciarSesion);
						lastPush = R.id.btn_iniciarSesion;
						Toast toast = Toast.makeText(Inicio_Activity.this,
								"usuario Aministrador", Toast.LENGTH_SHORT);
						toast.show();

						iniciarSesion.setEnabled(false);
						iniciarSesion.setFocusable(true);
						iniciarSesion
								.setBackgroundResource(R.drawable.b_ingresa_o);

						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						ft.setCustomAnimations(R.animator.slide_in_izquierda,
								R.animator.slide_in_derecha);
						ft.show(fragmentReg);

						ft.commit();

					} else {
						Toast toast = Toast.makeText(Inicio_Activity.this,
								"Password incorrecto", Toast.LENGTH_SHORT);
						toast.show();
					}

					break;

				case 2:
					if (passw.equals("vendor")) {
						changeSelector(lastPush, R.id.btn_iniciarSesion);
						lastPush = R.id.btn_iniciarSesion;
						startActivity(new Intent(Inicio_Activity.this,
								Menu_Activity.class));

					} else {
						Toast toast = Toast.makeText(Inicio_Activity.this,
								"Password incorrecto", Toast.LENGTH_SHORT);
						toast.show();
					}

					break;
				case 3:
					Toast toastNull = Toast.makeText(Inicio_Activity.this,
							"Introducir nombre de usuario", Toast.LENGTH_SHORT);
					toastNull.show();

					break;

				/*
				 * default: Toast toast2 = Toast.makeText(Inicio_Activity.this,
				 * "Nombre de usuario incorrecto", Toast.LENGTH_SHORT);
				 * toast2.show(); break;
				 */

				default:
					// CADENA json:
					// {"login":"login1","password":"pass1","idProveedor":1}
					Login();
					break;

				}

			}
		});

		btnRegisto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				changeSelector(lastPush, R.id.btn_showregistro);
				Fragment fragment = new RegistroFragment();
				showfragment(fragment);
				Toast.makeText(Inicio_Activity.this,
						"Se agregara un nuevo usuario para esta app",
						Toast.LENGTH_SHORT).show();
				lastPush = R.id.btn_showregistro;
			}
		});

	}

	private void saldoMonedero() {
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_TIENDA, session.getIdTienda());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.IMEI, Dispositivo.imei);
		} catch (JSONException e) { // TODO: handle exception
			Log.e(getPackageName(), "error creando json");
		}

		new WebServiceClient(new SaldoLinstener(), Inicio_Activity.this, true,
				Urls.url_consultarMonedero).execute(AddcelCrypto
				.encryptSensitive(AppUtils.getKey(), json.toString()));
	}

	private class SaldoLinstener implements WSResponseListener {

		private int idError;
		private String mensajeError;
		private String total;
		private String disponible;
		private JSONObject json;

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(Inicio_Activity.this,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();
			} else {
				response = AddcelCrypto.decryptSensitive(response);
				Log.i(this.getClass().getName().toString(), response);

				try {
					json = new JSONObject(response);
					idError = json.getInt(Constructors.ID_ERROR);
					mensajeError = json.getString(Constructors.MENSAJE_ERROR);
					total = json.getString(Constructors.TOTAL);
					disponible = json.getString(Constructors.CARNET_DISPONIBLE);
					// idRole = json.getInt("idRole");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (idError == 0) {
					session.setCarnetTotal(total);
					session.setCarnetDisponible(disponible);
					startActivity(new Intent(contxt, Menu_Activity.class));

				} else {
					Toast.makeText(contxt, mensajeError, Toast.LENGTH_SHORT)
							.show();
				}

			}
		}

	}

	private void Login() {
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.LOGIN, usuario.trim());
			json.put(Constructors.PASSWORD, passw.trim());
			json.put(Constructors.IMEI, Dispositivo.imei);
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
		} catch (JSONException e) { // TODO: handle exception
			Log.e(getPackageName(), "error creando json");
		}

		new WebServiceClient(new LoginListener(), Inicio_Activity.this, true,
				Urls.url_login).execute(AddcelCrypto.encryptSensitive(
				AppUtils.getKey(), json.toString()));
	}

	private class LoginListener implements WSResponseListener {

		private int idUsuario;
		private int idUsuarioMod;
		private int idProveedor;
		// private int idAdministrador;
		private int idTienda;
		private int tiempo_gps;
		// private int idRole = 0;
		private int idError;
		private String mensajeError;
		private String login;

		private String nombre;
		private String apellido;
		private String materno;
		private String mail;
		private String telefono;
		// private String nombreRole = null;

		private JSONArray tipoTarjeta;
		private int status;
		private int statusTienda;

		private JSONObject json;

		private static final String TAG = "LoginListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);

			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(Inicio_Activity.this,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();
			} else {
				response = AddcelCrypto.decryptSensitive(response);
				Log.i(TAG, response);

				try {
					json = new JSONObject(response);
					idError = json.getInt(Constructors.ID_ERROR);
					mensajeError = json.getString(Constructors.MENSAJE_ERROR);
					status = json.getInt(Constructors.STATUS);
					idUsuario = json.getInt(Constructors.ID_USUARIO);
					idProveedor = json.getInt(Constructors.ID_PROVEEDOR);
					login = json.getString(Constructors.LOGIN);
					tiempo_gps = json.getInt(Constructors.TIEMPO_NOTIFICACION);
					// idRole = json.getInt("idRole");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (idError == 0) {
					switch (status) {
					case 1:
						sessionSave();
						checkAlarmLocation(tiempo_gps);
						// checkAlarmLocation(60);
						changeSelector(lastPush, R.id.btn_iniciarSesion);
						lastPush = R.id.btn_iniciarSesion;

						// saldoMonedero();
						startActivity(new Intent(contxt, Menu_Activity.class));

						break;
					case 2:
						AlertDialog.Builder builder = new AlertDialog.Builder(
								contxt)
								.setTitle("Advertencia")
								.setMessage(
										"Es necesario que cambie su contrase�a. \n �Desea cambiarla ahora?")
								.setCancelable(false);

						builder.setPositiveButton("Cambiar",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										changeSelector(lastPush,
												R.id.btn_iniciarSesion);
										lastPush = R.id.btn_iniciarSesion;
										Bundle bundle = new Bundle();
										bundle.putInt(Constructors.ID_USUARIO,
												idUsuario);
										bundle.putInt(
												Constructors.ID_PROVEEDOR,
												idProveedor);
										bundle.putString(Constructors.LOGIN,
												login);
										sessionSave();

										Fragment cambioPassword = new CambioPassword_Fragment();
										cambioPassword.setArguments(bundle);
										showfragment(cambioPassword);

										iniciarSesion.setEnabled(false);
										iniciarSesion
												.setBackgroundResource(R.drawable.b_ingresa_o);

									}
								});

						builder.setNegativeButton("Cancelar",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										startActivity(new Intent(contxt,
												Inicio_Activity.class));
									}
								});

						AlertDialog alert = builder.create();
						alert.show();

						break;

					default:
						Log.e(TAG,
								"Error inesperado, Status de usuario no encontrado");
						Toast.makeText(contxt, "Error de status",
								Toast.LENGTH_SHORT).show();
						break;
					}

				} else {

					Toast.makeText(contxt, mensajeError, Toast.LENGTH_SHORT)
							.show();
				}

			}
		}

		public void sessionSave() {

			try {

				// idUsuarioMod = json.getInt("idUsuarioMod");
				// idAdministrador = json.getInt("idAdministrador");
				idTienda = json.getInt(Constructors.ID_TIENDA);
				nombre = json.getString(Constructors.NOMBRE);
				apellido = json.getString(Constructors.APELLIDO);
				materno = json.getString(Constructors.MATERNO);
				mail = json.getString(Constructors.MAIL);
				telefono = json.getString(Constructors.TELEFONO);
				// nombreRole = json.getString("nombreRole");
				statusTienda = json.getInt(Constructors.STATUS_TIENDA);
				tipoTarjeta = json.getJSONArray(Constructors.TIPOTARJETA);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Log.i("tipo tarjeta", tipoTarjeta.toString());

			// Log.i(this.getClass().getSimpleName().toString() +
			// "  sessionSave" , json.toString());

			session.createLoginSession(idUsuario, idUsuarioMod, idProveedor,
					idTienda, login, nombre, apellido, materno, mail, telefono,
					status, statusTienda, passw.trim(), tipoTarjeta.toString());
			// Log.i(this.getClass().getSimpleName().toString() +
			// "  session guardada", session.getTipotarjeta().toString());

		}
	}

	public void checkAlarmLocation(int period) {
		// Inicio Servicio GPS
		String TAG = getClass().getName().toString();
		// long intervalo;
		int time_ant = session.getTime_GPS();

		if (time_ant == 0) {
			session.setTime_GPS(period);
			// startAlarmLocation(intervalo);
		} else if (time_ant == period) {
			Log.i(TAG,
					"El intervalo entre notificaci�nes es el mismo al antarior"
							+ time_ant);
			// startAlarmLocation(intervalo);
		} else if (time_ant != period) {
			Log.i(TAG,
					"El intervalo entre notificaciones es diferente al anterior, nuevo periodo: "
							+ period);
			session.setTime_GPS(period);
			// startAlarmLocation(intervalo);
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		// Log.i(getClass().toString(), "onBackPressed selected stack size: " +
		// mFragmentStack.size() + mFragmentStack.peek().toString());
		// mFragmentStack.clear();
		moveTaskToBack(true);
	}

	private void changeSelector(int last, int id) {
		switch (id) {
		case R.id.btn_iniciarSesion:
			if (last == 0) {
				iniciarSesion.setEnabled(true);
				iniciarSesion
						.setBackgroundResource(R.drawable.selector_ingresa);
			} else if (last == R.id.btn_showregistro) {
				btnRegisto.setEnabled(true);
				btnRegisto.setBackgroundResource(R.drawable.selector_registro);
			}/* else if (last == R.id.btn_recuperarPass) {
				recuperarPass.setEnabled(true);
				recuperarPass.setClickable(true);
			}*/
			iniciarSesion.setEnabled(false);
			iniciarSesion.setBackgroundResource(R.drawable.b_ingresa_o);
			break;
		case R.id.btn_showregistro:
			if (last == 0) {
				iniciarSesion.setEnabled(true);
				iniciarSesion
						.setBackgroundResource(R.drawable.selector_ingresa);
			} else if (last == R.id.btn_iniciarSesion) {
				iniciarSesion.setEnabled(true);
				iniciarSesion
						.setBackgroundResource(R.drawable.selector_registro);
			}/* else if (last == R.id.btn_recuperarPass) {
				recuperarPass.setEnabled(true);
				recuperarPass.setClickable(true);
			}*/
			btnRegisto.setEnabled(false);
			btnRegisto.setBackgroundResource(R.drawable.b_registro_o);
			break;
		/*case R.id.btn_recuperarPass:
			if (last == 0) {
				iniciarSesion.setEnabled(true);
				iniciarSesion
						.setBackgroundResource(R.drawable.selector_ingresa);
			} else if (last == R.id.btn_iniciarSesion) {
				iniciarSesion.setEnabled(true);
				iniciarSesion
						.setBackgroundResource(R.drawable.selector_registro);
			} else if (last == R.id.btn_showregistro) {
				btnRegisto.setEnabled(true);
				btnRegisto.setBackgroundResource(R.drawable.selector_registro);
			}
			recuperarPass.setEnabled(false);
			recuperarPass.setClickable(false);
			break;*/

		default:
			break;
		}

	}

	private void showfragment(Fragment newfragment) {
		// TODO Auto-generated method stub
		// Bundle args = new Bundle();
		String tag = newfragment.toString();
		// newfragment.setArguments(args);

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		transaction.setCustomAnimations(R.animator.slide_in_izquierda,
				R.animator.slide_in_derecha);

		if (mFragmentStack.size() != 0) {
			Fragment currentFragment = getFragmentManager().findFragmentByTag(
					mFragmentStack.peek());
			transaction.hide(currentFragment);
		}

		transaction.add(R.id.fragment_inicio, newfragment, tag);
		transaction.addToBackStack(tag);
		mFragmentStack.add(tag);

		transaction.commit();
	}

}
