package org.addcel.richard.activities;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.richard.pagos.ConfirmacionMegacable_Activity;
import org.addcel.richard.pagos.PagodeServicios_Activity;
import org.addcel.session.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MegaCable_Activity extends Activity {
	SessionManager session;

	private int idError;
	private String msgError;
	private JSONObject datos;

	private String nombre;
	private String cuenta;
	private String domicilio;
	private String status;
	//private String concepto;
	private String saldo;
	private String comision;

	private TextView txnombre;
	private TextView txcuenta;
	private TextView txdomicilio;
	private TextView txstatus;

	private TextView txsaldo;
	private TextView txcomision;
	private Button btnMegacable;
	private Button pagar;

	private TableLayout datos_layout;
	private Context contxt = MegaCable_Activity.this;
	private EditText referencia;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tvdepaga_activity_layout);

		referencia = (EditText) findViewById(R.id.megacableReferencia);

		txnombre = (TextView) findViewById(R.id.megaNombre);
		txcuenta = (TextView) findViewById(R.id.megaCuenta);
		txdomicilio = (TextView) findViewById(R.id.megaDomicilio);
		txstatus = (TextView) findViewById(R.id.megaStatus);
		txsaldo = (TextView) findViewById(R.id.megaSaldo);
		txcomision = (TextView) findViewById(R.id.megaComision);

		datos_layout = (TableLayout) findViewById(R.id.megadatos_layout);

		pagar = (Button) findViewById(R.id.btn_pagoMegacable);
		pagar.setEnabled(false);
		pagar.setBackgroundResource(R.drawable.b_pagar_o);

		pagar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MegaCable_Activity.this,
						ConfirmacionMegacable_Activity.class);
				intent.putExtra(Constructors.REFERENCIA, cuenta);
				float t = Float.parseFloat(saldo) + Float.parseFloat(comision);
				intent.putExtra("total", t);
				startActivity(intent);

			}
		});

		btnMegacable = (Button) findViewById(R.id.btn_Consultareferencia);
		btnMegacable.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkBalance();
			}
		});
	}

	private void checkBalance() {
		// TODO Auto-generated method stub
		session = new SessionManager(contxt);
		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.REFERENCIA, referencia.getText().toString().trim());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.TIPO_CONSULTA, 2);

			Log.i(getPackageName(), json.toString());

		} catch (JSONException e) {
			// TODO: handle exception

			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new checkBalanceListener(), contxt, true,
				Urls.url_check_megacableBalance).execute(AddcelCrypto
				.encryptHard(json.toString()));

	}

	private class checkBalanceListener implements WSResponseListener {

		private static final String TAG = "PagoListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub

			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {

				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				try {
					JSONObject json = new JSONObject(response);
					idError = json.getInt(Constructors.ID_ERROR);
					msgError = json.getString(Constructors.MENSAJE_ERROR);
					datos = json.getJSONObject(Constructors.DATOS);

				} catch (JSONException e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				if (idError == 0) {

					try {
						nombre = datos.getString(Constructors.NOMBRE);
						cuenta = datos.getString(Constructors.CUENTA);
						domicilio = datos.getString(Constructors.DOMICILIO);
						status = datos.getString(Constructors.STATUS);
						//concepto = datos.getString(Constructors.CONCEPTO);
						saldo = datos.getString(Constructors.SALDO);
						comision = datos.getString(Constructors.COMISION);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					txnombre.setText(nombre);
					txcuenta.setText(cuenta);
					txdomicilio.setText(domicilio);
					txstatus.setText(status);
					txsaldo.setText(saldo);
					txcomision.setText(comision);

					datos_layout.setVisibility(View.VISIBLE);
					btnMegacable.setEnabled(false);
					btnMegacable
							.setBackgroundResource(R.drawable.b_megacable_o);
					pagar.setEnabled(true);
					pagar.setBackgroundResource(R.drawable.selector_pagar);

				} else {
					Toast.makeText(contxt,
							msgError + "\nVerifiqué sus datos.",
							Toast.LENGTH_SHORT).show();
				}

			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(this, PagodeServicios_Activity.class));
		finish();
	}
}
