package org.addcel.richard.activities;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.ConnectivityUtil;
import org.addcel.util.CustomToast;
import org.addcel.util.DeviceUtils;
import org.addcel.util.Dialogos;
import org.addcel.version.UrlVersion;
import org.addcel.version.VersionTO;
import org.addcel.version.VersionWSClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class SplashScreenActivity extends Activity {

	private SessionManager session;
	private ConnectivityUtil connection = new ConnectivityUtil(); 
	//private static final int SPLASH_SCREEN_DELAY = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_screen);
		
		Dispositivo.imei = DeviceUtils.getIMEI(this);
		
		session = new SessionManager(this);

		
	}
	
	private void checkNetwork(){
		if(connection.checkConnection(this)){
			// fetch data
			getTiendaInfo();
		//	startActivity(new Intent(SplashScreenActivity.this, Inicio_Activity.class));

		} else {
			// display error
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Sin conexion, �Deseas conectarte a una red?")
					.setTitle("Advertencia")
					.setCancelable(false)
					.setNegativeButton("Cancelar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();

									try {
										Thread.sleep(3000);
									} catch (InterruptedException e) {
										// TODO: handle exception
										e.printStackTrace();
									}
									finish();
								//	startActivity(new Intent(SplashScreenActivity.this,
								//			Inicio_Activity.class));

								}
							})
					.setPositiveButton("Continuar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// Activity para abrir conexión a internet
									startActivity(new Intent(
											WifiManager.ACTION_PICK_WIFI_NETWORK));

									// Toast redToast =
									// Toast.makeText(MainActivity.this,
									// "Conectado", Toast.LENGTH_LONG);
									// redToast.show();
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	private void getTiendaInfo() {
		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put("imei", Dispositivo.imei);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(this.getClass().getName().toString(), e.toString());
		}
		Log.i(this.getClass().getName(), json.toString());

		new WebServiceClient(new infoTiendaListener(), this, true,
				Urls.url_consultaKit).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private class infoTiendaListener implements WSResponseListener {

		private int idError;
		private String mensajeError;
		private int idTienda;
		private String nombreTienda;
		private String calle;
		private String numInt;
		private String numExt;
		private String colonia;
		private String cp;
		private String ciudad;
		private int idkit;
		private String carnet;
		private String vigencia;
		private String dispositivo;

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(SplashScreenActivity.this,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();
				finish();
				//startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS),0);

			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i("infoTienda",response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					idError = json.getInt(Constructors.ID_ERROR);
					mensajeError = json.getString(Constructors.MENSAJE_ERROR);
					idTienda = json.getInt(Constructors.ID_TIENDA);
					nombreTienda = json.getString(Constructors.NOMBRE);
					calle = json.getString(Constructors.CALLE);
					numInt = json.getString(Constructors.NUMERO_INTERIOR);
					numExt = json.getString(Constructors.NUMERO_EXTERIOR);
					colonia = json.getString(Constructors.COLONIA);
					cp = json.getString(Constructors.CODIGO_POSTAL);
					ciudad = json.getString(Constructors.CIUDAD);
					idkit = json.getInt(Constructors.ID_KIT);
					carnet = json.getString(Constructors.CARNET);
					vigencia = json.getString(Constructors.VIGENCIA);
					dispositivo = json.getString(Constructors.DISPOSITIVO);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Intent intent = new Intent(SplashScreenActivity.this, Inicio_Activity.class);
				if (idError != 0) {
					Toast.makeText(SplashScreenActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
					intent.putExtra("registrado", 0);
					VersionTO request = new VersionTO("4", "10");
					new VersionWSClient(new VersionListener(), UrlVersion.GET).execute(request);

				//	startActivity(intent);
				}else {
					session.dataKit(idTienda, nombreTienda, calle, numInt, numExt, colonia, cp, ciudad, idkit, carnet, vigencia, dispositivo);
					/*Intent mainIntent = new Intent().setClass(
							SplashScreenActivity.this, Inicio_Activity.class);
					startActivity(mainIntent);*/
					intent.putExtra("registrado", 1);
					startActivity(intent);

				}
				
			}
			
		}

	}
	
	
	private class VersionListener implements WSResponseListener {

		private final static String TAG = "VersionListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				
				try {
					JSONObject json = new JSONObject(response);
					
					String version = AppUtils.getVersionName(SplashScreenActivity.this, SplashScreenActivity.class);
					
					String[] servVersion = json.getString("version").split("[.]");
					String[] currVersion = version.split("[.]");
					
					boolean pasa = true;
					
					for (int i = 0; i < servVersion.length; i++) {
						if (i < currVersion.length) {
							int max = Integer.parseInt(servVersion[i]);
							int min = Integer.parseInt(currVersion[i]);
							if (max > min) {
								pasa = false;
								break;
							}
						} else {
							break;
						}
					}
					
					if (pasa) {
						Intent intent = new Intent(SplashScreenActivity.this, Inicio_Activity.class);
						startActivity(intent);
					} else {
						String prior = json.getString("tipo");
						final String url = json.getString("url");
						
						if (prior.equals("1")) {
							Dialogos.makeOkAlert(SplashScreenActivity.this, "Hay nueva nueva versi�n importante. \n"+
									" su versi�n: "+version+"\n"+
									" nueva versi�n: "+json.getString("version"), new DialogOkListener() {
										
										@Override
										public void ok(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
											Intent intent = new Intent(Intent.ACTION_VIEW);
											intent.setData(Uri.parse(url));
											startActivity(intent);
											
										}
										
										@Override
										public void cancel(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
										}
									});
						} else {
							Dialogos.makeYesNoAlert(SplashScreenActivity.this, "Hay nueva nueva versi�n disponible. \n "+
									" su version: "+version+"\n"+
									" nueva version: "+json.getString("version")+"\n"+
									" �Desea Descargarla?", new DialogOkListener() {
										
										@Override
										public void ok(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
											Intent intent = new Intent(Intent.ACTION_VIEW);
											intent.setData(Uri.parse(url));
											startActivity(intent);
										}
										
										@Override
										public void cancel(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
											Intent intent = new Intent(SplashScreenActivity.this, Inicio_Activity.class);
											startActivity(intent);
										}
									});
						}
					}
					
					
					
				} catch (JSONException e) {
					CustomToast.build(SplashScreenActivity.this, "No hay conexi�n de internet disponible.");
					finish();
				}
			
			} else {
				CustomToast.build(SplashScreenActivity.this, "No hay conexi�n de internet disponible.");
				finish();
			}
		}
		
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		checkNetwork();
	}
	
}
