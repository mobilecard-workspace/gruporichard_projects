package org.addcel.richard.constant;

public class Constructors {

	//******************** CONTROL
	
	public static final String ID_ERROR ="idError";
	public static final String MENSAJE_ERROR = "mensajeError";
	public static final String STATUS = "status";
	public static final String STATUS_TIENDA = "statusTienda";
	
	//******************** CONSULTA HISTORIAL DE PAGOS
	
	public static final String TIPO_BUSQUEDA = "tipoBusqueda";
	public static final String TIPO_PAGO = "tipoPago";
	public static final String PAGOS = "pagos";
	
	public static final String ID_BITACORA = "idBitacora";
	public static final String NOMBRES = "nombres";
	public static final String DESCRIPCION_PRODUCT = "descProducto";
	public static final String TOTAL = "total";
	public static final String FECHA = "fecha";
	public static final String TARGETA_TAG = "tarjetaTag";

	//******************** DISPOSITIVO
	public static final String IMEI = "imei";
	
	//******************** CARNET
	public static final String CARNET_DISPONIBLE = "disponible";
	public static final String CARNET_TOTAL = "carnetTotal";
	public static final String CARNET_SALDO_DISPONIBLE = "carnetDisponible";
	
	//******************** MEGACABLE
	public static final String REFERENCIA = "referencia";
	public static final String TIPO_CONSULTA = "tipoConsulta";
	public static final String DATOS = "datos";
	public static final String CUENTA = "cuenta";
	public static final String DOMICILIO = "domicilio";
	public static final String CONCEPTO = "concepto";
	public static final String COMISION = "comision";
	public static final String SALDO = "saldo";
	
	//******************** TIEMPO AIRE

	public static final String CARRIER = "carrier";
	public static final String ID_PRODUCTO = "id_producto";
	public static final String MONTO = "monto";
	
	//******************** TOKEN

	public static final String USUARIO_TOKEN = "usuario";
	public static final String TOKEN = "token";
	
	//******************** PAGOS

	public static final String TARGETA = "tarjeta";
	public static final String  CVV2 = "cvv2";
	public static final String ID_TIPO_TARJETA = "idTipoTarjeta";
	public static final String NUMERO_AUTORIZACION = "numAutorizacion";
	public static final String DESC_RECHAZO = "descRechazo";
	public static final String FOLIO = "folio";
	public static final String CARGO = "cargo";
	public static final String PRODUCTO = "producto";
	
	public static final String PIN = "pin";
	
	//******************** REGISTRO USUARIO Y SERVICIOS

	public static final String PREF_NAME = "Data";
	public static final String IS_LOGIN = "IsLoggedIn";
	public static final String ID_USUARIO = "idUsuario";
	public static final String ID_USUARIO_MOD = "idUsuarioMod";
	public static final String ID_PROVEEDOR = "idProveedor";
	public static final String ID_TIENDA = "idTienda";
	public static final String LOGIN = "login";
	public static final String NOMBRE = "nombre";
	public static final String APELLIDO = "apellido";
	public static final String MATERNO = "materno";
	public static final String MAIL = "mail";
	public static final String TELEFONO = "telefono";
	public static final String PASSWORD = "password";
	public static final String NEW_PASSWORD = "newPassword";
	public static final String TIPOTARJETA = "tipoTarjeta";
	public static final String MODELO = "modelo";
	public static final String SOFTWARE = "software";

	public static final String NOMBRE_TIENDA = "nombreTienda";
	public static final String CALLE = "calle";
	public static final String NUMERO_INTERIOR = "numInt";
	public static final String NUMERO_EXTERIOR = "numExt";
	public static final String COLONIA = "colonia";
	public static final String CODIGO_POSTAL = "cp";
	public static final String CIUDAD = "ciudad";
	public static final String ID_KIT = "idKit";
	public static final String CARNET = "carnet";
	public static final String VIGENCIA = "vigencia";
	public static final String DISPOSITIVO = "dispositivo";
	
	//******************** NOTIFICACIONES
	
	public static final String TIEMPO_NOTIFICACION = "tiempoNoti";
	public static final String TIEMPO_NOTIF_ANTERIOR = "tiempoNotiAnt";
	public static final String LATITUD = "cy";
	public static final String LONGITUD = "cx";


	
}
