package org.addcel.richard.constant;

public class Record {

	public static final int BUSQUEDA_TODOS = 0;
	public static final int BUSQUEDA_HOY = 1;
	public static final int BUSQUEDA_SEMANA = 2;
	public static final int BUSQUEDA_MES = 3;
	public static final int BUSQUEDA_MES_ANTERIOR = 4;

	public static final int PAGO_TIENDA = 1;
	public static final int PAGO_RECARGAS = 2;
	public static final int PAGO_IMPUESTOS = 3;

}
