package org.addcel.richard.constant;

public class Urls {

	private static final String desarrollo = "http://50.57.192.214:8080/GrupoRichardServicios";
	private static final String produccion = "http://50.57.192.210:8080/GrupoRichardServicios";

	private static final String context = produccion;

	public static final String url_login = context + "/login";
	public static final String url_pagoproductos = context + "/pagoProductos";
	public static final String url_getToken = context + "/getToken";
	public static final String url_pagoTAE = context + "/pagoTAE";
	public static final String url_pagoIAVE = context + "/pagoIAVE";
	public static final String url_pagoPASE = context + "/pagoPASE";
	public static final String url_busquedaPagos = context + "/busquedaPagos";
	public static final String url_historial = context + "/busquedaPagos";
	public static final String url_check_megacableBalance = context
			+ "/consultaSaldoMegacable";
	public static final String url_megacablePago = context + "/pagoMegacable";
	public static final String url_addUser = context + "/agregarUsuario";
	public static final String url_changePass = context + "/actualizarPassword";
	public static final String url_recuperarPass = context
			+ "/recuperarPassword";
	public static final String url_consultaKit = context + "/consultaKIT";
	public static final String url_updateUSer = context + "/actualizarUsuario";
	public static final String url_consultarMonedero = context + "/consultaMonedero";
	public static final String url_envioUbicacion = context + "/notificaLocalizacion";
}
