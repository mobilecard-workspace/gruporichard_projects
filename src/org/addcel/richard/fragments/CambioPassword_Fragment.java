package org.addcel.richard.fragments;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.activities.Inicio_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CambioPassword_Fragment extends Fragment {

	private EditText oldPass;
	private EditText newPass;
	private EditText confPass;
	private Button btnPass;

	private String login;
	private int idusuario;
	private int idproveedor;

	private String oldpass;
	private String newpass;
	private String confpass;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.cambiopassword_fragment_layout,
				container, false);
		login = getArguments().getString(Constructors.LOGIN);
		idusuario = getArguments().getInt(Constructors.ID_USUARIO);
		idproveedor = getArguments().getInt(Constructors.ID_PROVEEDOR);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		oldPass = (EditText) getView().findViewById(R.id.oldPassword);
		newPass = (EditText) getView().findViewById(R.id.newPassword);
		confPass = (EditText) getView().findViewById(R.id.newPassword_conf);

		btnPass = (Button) getView().findViewById(R.id.btn_cambiarPassword);

		btnPass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				oldpass = oldPass.getText().toString().trim();
				newpass = newPass.getText().toString().trim();
				confpass = confPass.getText().toString().trim();

				if (TextUtils.isEmpty(oldpass)) {
					Toast.makeText(getActivity(), "Ingresa contrase�a",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(newpass)) {
					Toast.makeText(getActivity(), "Ingresa nueva contrase�a",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(confpass)) {
					Toast.makeText(getActivity(), "Confirma contrase�a nueva",
							Toast.LENGTH_SHORT).show();
				} else {

					if (newpass.equals(confpass)) {
						cambiarPassword();
					} else {
						Toast.makeText(getActivity(),
								"Confirmaci�n de contrase�a no coincide",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}

	private void cambiarPassword() {

		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_USUARIO, idusuario);
			json.put(Constructors.ID_PROVEEDOR, idproveedor);
			json.put(Constructors.LOGIN, login);
			json.put(Constructors.PASSWORD, oldpass);
			json.put(Constructors.NEW_PASSWORD, newpass);
			json.put(Constructors.IMEI, Dispositivo.imei);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.i("JSON_PASS", json.toString());

		new WebServiceClient(new cambiopassListener(), getActivity(), true,
				Urls.url_changePass).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private class cambiopassListener implements WSResponseListener {

		private int errid;
		private String errmsg;

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(getActivity(),
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.i("TOKEN", response);
				response = AddcelCrypto.decryptHard(response);
				Log.i("TOKEN", response);

				try {
					JSONObject json = new JSONObject(response);

					errid = json.getInt(Constructors.ID_ERROR);
					errmsg = json.getString(Constructors.MENSAJE_ERROR);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (errid == 0) {
					Toast.makeText(getActivity(), errmsg, Toast.LENGTH_SHORT)
							.show();
					SessionManager session = new SessionManager(getActivity());
					session.setPassword(newpass);
					startActivity(new Intent(getActivity(), Inicio_Activity.class));
				} else {
					Toast.makeText(getActivity(), errmsg, Toast.LENGTH_SHORT)
							.show();
					SessionManager session = new SessionManager(getActivity());
					session.logoutUser();
					startActivity(new Intent(getActivity(),
							Inicio_Activity.class));

				}
			}
		}
	}
}
