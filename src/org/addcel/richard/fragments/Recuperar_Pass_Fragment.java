 package org.addcel.richard.fragments;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.activities.Inicio_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Recuperar_Pass_Fragment extends Fragment {

	private EditText login;
	private Button brecup;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.recuperarpassword_fragment_layout,
				container, false);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		login = (EditText) getView().findViewById(R.id.recuPass_login);
		brecup = (Button) getView().findViewById(R.id.btn_recuperarPass);

		brecup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!TextUtils.isEmpty(login.getText().toString())) {
					recuperarPass();
				} else {
					Toast.makeText(getActivity(),
							"Introduce nombre de Usuario", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

	}

	protected void recuperarPass() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.LOGIN, login.getText().toString().trim());
			json.put(Constructors.IMEI, Dispositivo.imei);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new WebServiceClient(new recuperarpassListener(), getActivity(), true,
				Urls.url_recuperarPass).execute(AddcelCrypto.encryptHard(json
				.toString()));
	}

	private class recuperarpassListener implements WSResponseListener {
		private int idError;
		private String msgError;

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(getActivity(),
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i("Recuperarpassword Listener", response);

				try {
					JSONObject json = new JSONObject(response);
					idError = json.getInt(Constructors.ID_ERROR);
					msgError = json.getString(Constructors.MENSAJE_ERROR);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (idError == 0) {
					Toast.makeText(getActivity(), msgError, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(getActivity(), msgError, Toast.LENGTH_SHORT)
							.show();
				}

				startActivity(new Intent(getActivity(), Inicio_Activity.class));

			}
		}

	}
}
