package org.addcel.richard.gps;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

public class GPSservice extends Service{
	
	GPSTracker gps;
	private double latitude;
	private double longitude;
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		
		String TAG = getApplicationContext().getPackageName().toString();
		
		gps = new GPSTracker(getApplicationContext());
		
		if (gps.canGetLocation()) {

			latitude = gps.getLatitud();
			longitude = gps.getLongitud();

			// \n is for new line
			/*Toast.makeText(
					getApplicationContext(),
					"Tu Locacion es - \nLat: " + latitude + "\nLong: "
							+ longitude, Toast.LENGTH_LONG).show();*/
			
			Log.i(TAG, "Ubicaci�n generada: long "+longitude + " lati " + latitude);
			enviarUbicacion(latitude, longitude);
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}
		
		
	}
	
	
	
	private void enviarUbicacion(double latitud, double longitud){
		SessionManager session = new SessionManager(getApplicationContext());
		String TAG = getApplicationContext().getPackageName().toString();
		
		JSONObject json = new JSONObject();
		
		TelephonyManager tManager = (TelephonyManager) getBaseContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		
		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_TIENDA, session.getIdTienda());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.LATITUD, latitud);
			json.put(Constructors.LONGITUD, longitud);
			json.put(Constructors.IMEI, tManager.getDeviceId());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Error creando json envio ubicacion");
		}
		
		new WebServiceClient(new UbicacionListener(), getApplicationContext(), false, 
				Urls.url_envioUbicacion).execute(AddcelCrypto.encryptHard(json.toString()));
	}
	
	private class UbicacionListener implements WSResponseListener {
		
		
		String TAG = getApplicationContext().getPackageName().toString();
		private int idError;
		private String mensajeError;
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
			//	Toast toast = Toast.makeText(contxt,
			//			"Error de conexi�n.",
			//			Toast.LENGTH_SHORT);
			//	toast.show();

			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG , response);
				try {
					JSONObject json = new JSONObject(response);

					idError = json.getInt(Constructors.ID_ERROR);
					mensajeError = json.getString(Constructors.MENSAJE_ERROR);
				} catch (JSONException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				if (idError == 0) {
				//	Toast.makeText(contxt, mensajeError, Toast.LENGTH_SHORT)
				//			.show();
					Log.i(TAG, " GPSService: " + mensajeError);
				} else {
					//Toast.makeText(contxt,
					//		mensajeError + " id de Usuario: " + idUsuario,
					//		Toast.LENGTH_SHORT).show();
					Log.e(TAG, "Error GPSService: "+ idError + "Mensaje Error: " + mensajeError);
				}
				
			}
			
		}
		
	}
	
}
