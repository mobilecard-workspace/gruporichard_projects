package org.addcel.richard.pagos;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.richard.R;
import org.addcel.richard.activities.Menu_Activity;
import org.addcel.richard.constant.Constructors;
import org.addcel.richard.constant.Dispositivo;
import org.addcel.richard.constant.Servicios;
import org.addcel.richard.constant.Urls;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ConfirmacionTelepeaje_Activity extends Activity {

	private String cr;
	private SessionManager session;

	private int tipopago = 0;
	private String comistr = " + $8.00";

	private int producto;

	private String seturl;

	private int errornum;
	private String errormsg;
	private String autbanco;
	private String foliobnc;

	private String idBitacora;
	private String cargo;
	private String comision;

	private int erridtok;
	private String errmsgtk;
	private String token;

	private String vigencia;

	private Context contxt = ConfirmacionTelepeaje_Activity.this;

	private EditText tarjetaTag;
	private EditText verificador;
	private EditText nombre;
	private EditText correo;
	private EditText montoEdit;

	private EditText tarjetaNum;
	private EditText cvv2;
	private EditText vigenciamm;
	private EditText vigenciaaa;
	private EditText total;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.confirmaciontelepeaje_layout);

		List<String> list = new ArrayList<String>();
		session = new SessionManager(contxt);
		JSONArray myjsonArray = session.getTipotarjeta();
		try {
			for (int i = 0; i < myjsonArray.length(); i++) {
				JSONObject json = myjsonArray.getJSONObject(i);
				if (i == 0) {
					list.add("Tipo de Pago");
				}else {
				list.add(json.getString("descripcion"));
				}
			}

		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		final TextView ctextview = (TextView) findViewById(R.id.label_telepeaje);

		tarjetaTag = (EditText) findViewById(R.id.numeroTelepeaje);
		verificador = (EditText) findViewById(R.id.cnfnumeroTelepeaje);
		nombre = (EditText) findViewById(R.id.nombreTelepeaje);
		correo = (EditText) findViewById(R.id.correoCobroTelepeaje);
		montoEdit = (EditText) findViewById(R.id.montoCobroTelepeaje);
		cvv2 = (EditText) findViewById(R.id.cvvCobroTelepeaje);
		tarjetaNum = (EditText) findViewById(R.id.tarjetaCobroTelepeaje);
		vigenciamm = (EditText) findViewById(R.id.vigenciaTelepeajemm);
		vigenciaaa = (EditText) findViewById(R.id.vigenciaTelepeajeaa);
		total = (EditText) findViewById(R.id.totalTelepeaje);

		final Button btnpago = (Button) findViewById(R.id.btn_pagoTelepeaje);
		final Spinner spinnerTipo = (Spinner) findViewById(R.id.spinnerCobroTelepeaje);

		final String str = getIntent().getStringExtra("monto");
		cr = getIntent().getStringExtra("tipotele");
		producto = getIntent().getIntExtra("id_producto", 0);
		seturl = getIntent().getStringExtra("url");

		montoEdit.setFocusable(false);
		montoEdit.setClickable(false);
		montoEdit.setText(str);

		total.setFocusable(false);
		total.setClickable(false);
		total.setText(str + comistr);

		ctextview.setText(cr);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(contxt,
				android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinnerTipo.setAdapter(adapter);

		spinnerTipo.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				Toast.makeText(
						contxt,
						"Tipo de pago: "
								+ parent.getItemAtPosition(pos).toString(),
						Toast.LENGTH_SHORT).show();
				tipopago = pos;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		btnpago.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (tipopago == 0) {
					Toast.makeText(getApplicationContext(), "Selecciona un tipo de Pago", Toast.LENGTH_SHORT).show();
					//tipopago =1;
				}else{

					if (TextUtils.isEmpty(tarjetaTag.getText())) {
						Toast.makeText(contxt, "Campo Número vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(verificador.getText())) {
						Toast.makeText(contxt, "Confirma número",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(nombre.getText())) {
						Toast.makeText(contxt, "Campo Nombre vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(correo.getText())) {
						Toast.makeText(contxt, "Campo Correo vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(tarjetaNum.getText())) {
						Toast.makeText(contxt,
								"Campo Número de Tarjeta vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(cvv2.getText())) {
						Toast.makeText(contxt, "Campo CVV2 vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(vigenciamm.getText())) {
						Toast.makeText(contxt, "Campo Vigencia mes vacío",
								Toast.LENGTH_SHORT).show();
					} else if (TextUtils.isEmpty(vigenciaaa.getText())) {
						Toast.makeText(contxt, "Campo Vigencia año vacío",
								Toast.LENGTH_SHORT).show();
					} else {
						vigencia = vigenciamm.getText().toString().trim() + "/"
								+ vigenciaaa.getText().toString().trim();
						pedirToken();
					}

				}
			}
		});

	}

	private void pedirToken() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		try {
			json.put(Constructors.ID_PRODUCTO, Servicios.idProvedor);
			json.put(Constructors.USUARIO_TOKEN, Servicios.userToken);
			json.put(Constructors.PASSWORD, Servicios.passToken);
		} catch (JSONException e) {
			// TODO: handle exception
			Log.e(getPackageName(), "error creando Objeto json");
		}
		Log.i(getPackageName(), json.toString());
		new WebServiceClient(new TokenListener(), contxt, true,
				Urls.url_getToken).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private void enviarPago() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();

		try {
			json.put(Constructors.ID_USUARIO, session.getIdUsuario());
			json.put(Constructors.ID_PROVEEDOR, Servicios.idProvedor);
			json.put(Constructors.MAIL, correo.getText().toString().trim());
			json.put(Constructors.NOMBRES, nombre.getText().toString().trim());
			json.put(Constructors.PRODUCTO, producto);
			json.put(Constructors.PIN, verificador.getText().toString().trim());
			json.put(Constructors.TARGETA_TAG, tarjetaTag.getText().toString().trim());
			json.put(Constructors.ID_TIPO_TARJETA, tipopago);
			json.put(Constructors.VIGENCIA, vigencia);
			json.put(Constructors.TARGETA, tarjetaNum.getText().toString().trim());
			json.put(Constructors.CVV2, cvv2.getText().toString().trim());
			json.put(Constructors.PASSWORD, session.getUserPassword());
			json.put(Constructors.TOKEN, token);
			json.put(Constructors.IMEI, Dispositivo.imei);
			json.put(Constructors.MODELO, Dispositivo.modelo);
			json.put(Constructors.SOFTWARE, Dispositivo.software_version);

			Log.i(getPackageName(), json.toString());

		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
			Log.e(getPackageName(), "error creando Objeto json");
		}

		new WebServiceClient(new PagoListener(), contxt, true, seturl)
				.execute(AddcelCrypto.encryptSensitive(AppUtils.getKey(),
						json.toString()));

	}

	private class TokenListener implements WSResponseListener {

		private static final String TAG = "TokenListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.i("TOKEN", response);
				// if (!AddcelCrypto.decryptHard(response).startsWith("{")) {
				// token = response;
				// enviarPago();
				// } else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				try {

					JSONObject json = new JSONObject(response);

					erridtok = json.getInt(Constructors.ID_ERROR);
					errmsgtk = json.getString(Constructors.MENSAJE_ERROR);
					token = json.getString(Constructors.TOKEN);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.i(TAG, response.toString());
				}

				Log.e(TAG, errmsgtk);
				if (erridtok != 0) {
					Toast.makeText(contxt, errmsgtk, Toast.LENGTH_SHORT).show();
				} else {
					enviarPago();
				}
			}

			// }

		}

	}

	private class PagoListener implements WSResponseListener {

		private static final String TAG = "PagoListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				// CustomToast.build(Inicio_Activity.this,
				// "Error de conexi�n. Intente de nuevo m�s tarde.");
				Toast toast = Toast.makeText(contxt,
						"Error de conexión. Intente de nuevo más tarde.",
						Toast.LENGTH_SHORT);
				toast.show();

			} else {

				response = AddcelCrypto.decryptSensitive(response);
				Log.i(TAG, response.toString());
				try {
					JSONObject json = new JSONObject(response);

					errornum = json.getInt(Constructors.ID_ERROR);
					errormsg = json.getString(Constructors.MENSAJE_ERROR);
					autbanco = json.getString(Constructors.NUMERO_AUTORIZACION);
					foliobnc = json.getString(Constructors.FOLIO);
					comision = json.getString(Constructors.COMISION);
					cargo = json.getString(Constructors.CARGO);
					idBitacora = json.getString(Constructors.ID_BITACORA);

				} catch (JSONException e) {
					e.printStackTrace();
					Log.i(TAG, response.toString());
				}
				
			}

			if (errornum != 0) {
				Toast.makeText(contxt, "ERROR: " + errormsg, Toast.LENGTH_LONG)
						.show();
			} else {
				Log.i(this.getClass().getName().toString(), "Pago guardado en bitacora, ID = " + idBitacora);
				AlertDialog.Builder builder = new AlertDialog.Builder(contxt, 3);

				builder.setTitle("Pago exitoso");

				if (tipopago != 0) {

					builder.setMessage(cr + "\nTAG:"
							+ tarjetaTag.getText().toString().trim()
							+ "\nTotal: " + cargo + "\nComision: " + comision
							+ "\nFolio: " + foliobnc);
				} else {
					builder.setMessage(cr + "\nTAG:"
							+ tarjetaTag.getText().toString().trim()
							+ "\nTotal: " + cargo + "\nComision: " + comision
							+ "\nFolio: " + foliobnc
							+ "\nAutorización bancaria: " + autbanco);
				}

				builder.setPositiveButton("ACEPTAR",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								startActivity(new Intent(contxt,
										Menu_Activity.class));

							}
						});
				AlertDialog aldialog = builder.create();
				aldialog.show();
			}

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(this, Menu_Activity.class));
		finish();
	}
}
