package org.addcel.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ConnectivityUtil {

	private static final String TAG = "ConnectivityUtil";

	public boolean checkConnection(Context _context) {
		ConnectivityManager cm = (ConnectivityManager) _context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

		boolean connected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();
		Log.i(TAG, "CONECTADO A INTERNET: " + connected);
		return connected;
	}

}
