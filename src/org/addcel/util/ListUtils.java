package org.addcel.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ListUtils {
	public static List<NameValuePair> HashtableToListNameValuePair(
			Hashtable<String, String> hash) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		Enumeration<String> en = hash.keys();
		String key = "";

		while (en.hasMoreElements()) {
			key = en.nextElement();

			nameValuePairs.add(new BasicNameValuePair(key, hash.get(key)));
		}

		return nameValuePairs;
	}

	/**
	 * @description Convierte dos cadenas (Llave => Valor) a un Hashtable de un
	 *              elemento
	 */
	public static Hashtable<String, String> stringsToHashtable(String key,
			String value) {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		hash.put(key, value);

		return hash;
	}

	/**
	 * @description Convierte dos arrays de cadenas (Llave => Valor) a un
	 *              Hashtable
	 */
	public static Hashtable<String, String> arraysToHashtable(String[] keys,
			String[] values) {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		int minLargo = keys.length > values.length ? values.length
				: keys.length;

		for (int a = 0; a < minLargo; a++) {
			hash.put(keys[a], values[a]);
		}

		return hash;
	}

	/**
	 * @description Convierte dos arrays de cadenas (Llave => Valor) a un
	 *              ArrayList<BasicNameValuePair>
	 */
	public static ArrayList<BasicNameValuePair> arraysToArrayNameValuePairs(
			String[] keys, String[] values) {
		ArrayList<BasicNameValuePair> hash = new ArrayList<BasicNameValuePair>();
		int minLargo = keys.length > values.length ? values.length
				: keys.length;

		for (int a = 0; a < minLargo; a++) {
			hash.add(new BasicNameValuePair(keys[a], values[a]));
		}

		return hash;
	}

	public static String[] HashtableToArrayKeys(Hashtable<String, String> hash) {
		String[] keys;

		if (hash != null) {
			keys = new String[hash.size()];

			Enumeration<String> en = hash.keys();
			int a = 0;
			while (en.hasMoreElements()) {
				keys[a++] = en.nextElement();
			}
		} else {
			keys = new String[] {};
		}

		return keys;
	}

	public static String[] HashtableToArrayValues(Hashtable<String, String> hash) {
		return (String[]) hash.values().toArray();
	}

	public static String[] ArrayNameValuePairsToArrayKeys(
			ArrayList<BasicNameValuePair> list) {
		String[] keys;

		if (list != null) {
			int a = 0;
			keys = new String[list.size()];

			for (BasicNameValuePair nvp : list) {
				keys[a++] = nvp.getName();
			}
		} else {
			keys = new String[] {};
		}

		return keys;
	}

	public static String[] ArrayNameValuePairsToArrayValues(
			ArrayList<BasicNameValuePair> list) {
		String[] values;

		if (list != null) {
			int a = 0;
			values = new String[list.size()];

			for (BasicNameValuePair nvp : list) {
				values[a++] = nvp.getValue();
			}
		} else {
			values = new String[] {};
		}

		return values;
	}
}
