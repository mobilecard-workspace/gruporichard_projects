package org.addcel.view;

import org.addcel.richard.R;
import org.addcel.richard.constant.Constructors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class List_Adapter extends BaseAdapter {

	private JSONArray myjson;
	private static LayoutInflater inflater = null;
	private Activity activity;
	private JSONObject json;

	private String item_fecha;
	private String item_id;
	private String item_monto;
	private int j;

	public List_Adapter(Activity a, Context c, JSONArray l) {
		activity = a;
		myjson = l;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		j = myjson.length();
		return j;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		try {
			json = myjson.getJSONObject(position);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder {

		public TextView date;
		public TextView id;
		public TextView monto;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		ViewHolder holder;

		if (convertView == null) {
			vi = inflater.inflate(R.layout.item_historial, null);

			/****** View Holder Object contiene los elementos del item_historial.xml ******/

			holder = new ViewHolder();
			holder.date = (TextView) vi.findViewById(R.id.item_date);
			holder.id = (TextView) vi.findViewById(R.id.item_id);
			holder.monto = (TextView) vi.findViewById(R.id.item_monto);

			vi.setTag(holder);
		} else {
			holder = (ViewHolder) vi.getTag();
		}

		try {
			json = (JSONObject) myjson.getJSONObject(position);
			item_fecha = json.getString(Constructors.FECHA);
			item_id = json.getString(Constructors.DESCRIPCION_PRODUCT);
			item_monto = json.optString(Constructors.TOTAL);
			Log.i("Historial", json.toString());
			

			holder.date.setText(item_fecha);
			holder.id.setText(item_id);
			holder.monto.setText(item_monto);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return vi;
	}

}
